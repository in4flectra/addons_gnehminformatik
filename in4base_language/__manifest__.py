# -*- coding: utf-8 -*-
{
    'name': 'Language Functions (extended)',
    'summary': 'Extension of Language Functions',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Extra Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/language_export.xml',
        'views/language_install.xml',
    ],
}
