# -*- coding: utf-8 -*-

import contextlib
import io
import os

from flectra import api, fields, models, tools
NEW_LANG_KEY = '__new__'


class In4BaseLanguageBaseLanguageExport(models.TransientModel):

    _inherit = 'base.language.export'

    module_filter = fields.Char(string='Apps To Export Filter')
    filename = fields.Char(string='Export Filename')

    @api.multi
    def act_getfile(self):
        this = self[0]

        if not this.module_filter:
            return super(In4BaseLanguageBaseLanguageExport, self).act_getfile()

        lang = this.lang if this.lang != NEW_LANG_KEY else False

        module_ids = self.env['ir.module.module'].search([('name', 'ilike', this.module_filter)])
        for module_id in module_ids:
            if this.filename:
                filename = this.filename
            else:
                if lang:
                    filename = tools.get_iso_codes(lang)
                else:
                    filename = module_id.name
            if (not lang) and (this.format == 'po'):
                extension = 'pot'
            else:
                extension = this.format
            name = '%s.%s' % (filename, extension)

            with contextlib.closing(io.BytesIO()) as buf:
                tools.trans_export(lang, [module_id.name], buf, this.format, self._cr)
                val = buf.getvalue()

            for addons_path in tools.config['addons_path'].split(','):
                dir = addons_path + '/' + module_id.name + '/i18n'
                if os.path.isdir(dir):
                    with open(dir + '/' + name, 'wb') as f:
                        f.write(val)
        return True


class In4BaseLanguageBaseLanguageExportAll(models.TransientModel):

    _inherit = 'base.language.export.all'

    load_only_own_translations = fields.Text(
        default='Analytic Line\nCity\nEmployee\nFormat\nHelpdesk\nLeads\nMedium\nName\nPartner\nState\nStatus\nTask Stage\nTeam Name\nTotal\n<strong>Total</strong>',
    )
