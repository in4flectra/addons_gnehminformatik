# -*- coding: utf-8 -*-

import logging

from flectra import api, fields, models, tools, _

_logger = logging.getLogger(__name__)


class In4BaseLanguageBaseLanguageInstall(models.TransientModel):

    _inherit = 'base.language.install'

    delete = fields.Boolean(string='Delete Existing Terms', default=False)
    update = fields.Boolean(string='Update Imported Terms', default=False)

    @api.multi
    def lang_install(self):
        this = self[0]
        if this.delete:
            query = _(
                '''
                    DELETE
                      FROM ir_translation
                     WHERE lang = '%s'
                       AND (   (NOT module IS NULL)
                            OR (name in ('ir.module.module,shortdesc', 'ir.module.module,summary'))
                           )
                           ;
                '''
            ) % this.lang
            self._cr.execute(query, ())

        result = super(In4BaseLanguageBaseLanguageInstall, self).lang_install()

        if this.update:
            query = _(
                '''
                    lang1 = '%s'
                '''
            ) % this.lang
            if query.strip() != 'lang1 = \'' + this.lang + '\'':
                self._cr.execute(query, ())

            query = _(
                '''
                    lang2 = '%s'
                '''
            ) % this.lang
            if query.strip() != 'lang2 = \'' + this.lang + '\'':
                self._cr.execute(query, ())

            query = _(
                '''
                    lang3 = '%s'
                '''
            ) % this.lang
            if query.strip() != 'lang3 = \'' + this.lang + '\'':
                self._cr.execute(query, ())

            query = _(
                '''
                    lang4 = '%s'
                '''
            ) % this.lang
            if query.strip() != 'lang4 = \'' + this.lang + '\'':
                self._cr.execute(query, ())

            query = _(
                '''
                    lang5 = '%s'
                '''
            ) % this.lang
            if query.strip() != 'lang5 = \'' + this.lang + '\'':
                self._cr.execute(query, ())

        return result

    @api.model
    def update_language(self, lang_id):
        self.lang_install()
        self.env.cr.commit()
        _logger.info('Language "%s" updated' % (lang_id.name))

    @api.model_cr
    def _register_hook(self):
        if tools.config.options['stop_after_init']:
            lang_ids = self.env['res.lang'].search([])
            for lang_id in lang_ids:
                install_id = self.env['base.language.install'].create({
                    'lang': lang_id.code,
                    'overwrite': True,
                    'delete': True,
                    'update': True,
                })
                install_id.update_language(lang_id)

            domain = [('name', '=', 'in4tools_xml_interface'), ('state', '=', 'installed')]
            in4tools_xml_interface_installed = self.env['ir.module.module'].sudo().search_count(domain)
            if in4tools_xml_interface_installed > 0:
                self.env['xml_interface.import'].import_update_data()
