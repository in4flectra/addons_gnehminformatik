# -*- coding: utf-8 -*-

from flectra import fields, models


class In4ContactsCalendarCalendarEvent(models.Model):

    _inherit = 'calendar.event'

    background_color = fields.Char(string='Background Color', related='partner_id.calendar_background_color', store=False, size=7)
