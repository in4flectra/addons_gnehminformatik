# -*- coding: utf-8 -*-

from flectra import fields, models


class In4ContactsCalendarResPartner(models.Model):

    _inherit = 'res.partner'

    calendar_background_color = fields.Char(string='Calendar Background Color', size=7)
