# -*- coding: utf-8 -*-
{
    'name': 'Calendar: Background Color for Partners',
    'summary': 'Display the Background Color for Partners on Calendar',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Contacts',
    'version': '0.1',

    'depends': [
        'base',
        'calendar',
        'contacts',
        'in4calendar',
        'in4contacts',
        'web_widget_color',
    ],

    'data': [
        'views/calendar_event.xml',
        'views/res_partner.xml',
    ],

    'auto_install': True,
}
