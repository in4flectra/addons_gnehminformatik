# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceL10NCHBaseBankResPartnerBank(models.Model):

    _inherit = 'res.partner.bank'

    @api.depends('acc_number')
    def _compute_acc_type(self):
        super(In4InvoiceL10NCHBaseBankResPartnerBank, self)._compute_acc_type()

    @api.depends('acc_number')
    def _compute_l10n_ch_postal(self):
        for bank in self:
            if bank.acc_type == 'iban':
                bank.l10n_ch_postal = bank.isr_adherent_num or bank._retrieve_l10n_ch_postal(bank.sanitized_acc_number)
            else:
                bank.l10n_ch_postal = bank.sanitized_acc_number

    acc_type = fields.Selection(
        selection=[
            ('clearing', 'Clearing'),
            ('postal', 'Postal'),
            ('bank', 'Bank'),
            ('iban', 'IBAN'),
            ('other', 'Other'),
        ],
        compute=_compute_acc_type,
    )
