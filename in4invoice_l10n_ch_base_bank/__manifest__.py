# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (extended): Switzerland - Accounting and Bank Extension',
    'summary': 'Extension of Sales, Invoices and Payments: Swiss Accounting and Bank Extension',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'account_payment_mode',
        'account_payment_partner',
        'in4invoice_l10n_ch',
        'l10n_ch_base_bank',
    ],

    'data': [
        'views/account_invoice.xml',
        'views/res_partner.xml',
    ],

    'auto_install': True,
}
