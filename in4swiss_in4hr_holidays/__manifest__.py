# -*- coding: utf-8 -*-
{
    'name': 'Leave Management (Swiss Localization)',
    'summary': 'Swiss Localization of leave allocations and leave requests',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4hr_holidays',
        'in4swiss',
    ],

    'data': [
        'views/resource_calendar.xml',
    ],

    'auto_install': True
}
