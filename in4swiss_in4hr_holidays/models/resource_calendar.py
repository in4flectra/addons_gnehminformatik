# -*- coding: utf-8 -*-

import base64
import csv
import io
import pytz
import sys

from datetime import datetime

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4SwissResCityImportSwiss(models.TransientModel):

    _name = 'resource.calendar.swiss_holiday_import'
    _description = 'Import Swiss Holidays'

    file = fields.Binary(string='File to import', required=True)
    holiday_status_id = fields.Many2one('hr.holidays.status', string='Leave Type', required=True, company_dependent=True)
    assign_to_working_times = fields.Selection(
        [('choose', 'Leaves with selected Region'), ('empty', 'Leaves without a Region'), ('all', 'All Leaves')],
        string='Assign to',
        default='choose',
    )
    assign_to_working_times_region_id = fields.Many2one('resource.calendar.region', string='Region')

    @api.multi
    def import_swiss_holidays(self):
        file = io.StringIO(base64.decodebytes(self.file).decode('iso-8859-1'))
        reader = csv.reader(file, delimiter=';')
        csv.field_size_limit(sys.maxsize)

        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        leaves = self.env['resource.calendar.leaves']

        row_idx = 0
        for row in reader:
            if row_idx > 0:
                rec_date_from = datetime.strptime(row[0] + ' 00:00:00', '%d.%m.%Y %H:%M:%S')
                rec_date_to = datetime.strptime(row[0] + ' 23:59:00', '%d.%m.%Y %H:%M:%S')
                rec_name = row[1]
                date_from = pytz.timezone(tz).localize(rec_date_from).astimezone(pytz.utc)
                date_to = pytz.timezone(tz).localize(rec_date_to).astimezone(pytz.utc)
                domain = [
                    ('date_from', '>=', date_from.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                    ('date_to', '<=', date_to.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                    ('assign_to_working_times', '=', self.assign_to_working_times),
                ]
                if self.assign_to_working_times == 'empty':
                    domain.append(('assign_to_working_times_region_id', '=', False))
                elif self.assign_to_working_times == 'choose':
                    domain.append(('assign_to_working_times_region_id', '=', self.assign_to_working_times_region_id.id))
                leave = leaves.search(domain, limit=1)
                if not leave:
                    leaves.create({
                        'name': rec_name,
                        'holiday_status_id': self.holiday_status_id.id,
                        'assign_to_working_times': self.assign_to_working_times,
                        'assign_to_working_times_region_id': self.assign_to_working_times_region_id.id,
                        'date_from': date_from,
                        'date_to': date_to,
                    })
            row_idx += 1
