# -*- coding: utf-8 -*-
{
    'name': 'Leave Management (extended)',
    'summary': 'Extension of leave allocations and leave requests',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.4',

    'depends': [
        'hr_holidays',
        'resource',
    ],

    'data': [
        # security
        'security/in4hr_holidays.xml',
        'security/ir.model.access.csv',

        # views
        'views/hr_holidays.xml',
        'views/resource_calendar.xml',
    ],

    'pre_init_hook': 'pre_init_hook',
}
