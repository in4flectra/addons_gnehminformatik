# -*- coding: utf-8 -*-

import pytz

from datetime import timedelta

from flectra import api, fields, models


def to_tz(datetime, tz_name):
    tz = pytz.timezone(tz_name) if tz_name else pytz.UTC
    return pytz.UTC.localize(datetime.replace(tzinfo=None), is_dst=False).astimezone(tz).replace(tzinfo=None)


class In4HRHolidaysResourceCalendar(models.Model):

    _inherit = 'resource.calendar'
    region_id = fields.Many2one('resource.calendar.region', string='Region', ondelete='restrict')

    @api.model
    def get_leaves_domain(self):
        result = super(In4HRHolidaysResourceCalendar, self).get_leaves_domain()
        if self.region_id:
            result.append(('assign_to_working_times', '=', 'choose'))
            result.append(('assign_to_working_times_region_id', '=', self.region_id.id))
        else:
            result.append(('assign_to_working_times', 'in', ['all', 'empty']))
        return result

    @api.multi
    def write(self, values):
        result = super(In4HRHolidaysResourceCalendar, self).write(values)

        if 'region_id' in values:
            for calendar in self:
                domain = [('calendar_id', '=', calendar.id), ('resource_id', '=', False)]
                leaves_ids = self.env['resource.calendar.leaves'].search(domain)
                for leaves_id in leaves_ids:
                    leaves_id.unlink()
                leaves_ids = self.env['resource.calendar.leaves'].search(calendar.get_leaves_domain())
                for leaves_id in leaves_ids:
                    leaves_id.copy(default={'calendar_id': calendar.id, 'calendar_leaves_id': leaves_id.id})

        return result

    @api.multi
    def _get_leave_intervals(self, resource_id=None, start_datetime=None, end_datetime=None):
        """Get the leaves of the calendar. Leaves can be filtered on the resource,
        and on a start and end datetime.

        Leaves are encoded from a given timezone given by their tz field. COnverting
        them in naive user timezone require to use the leave timezone, not the current
        user timezone. For example people managing leaves could be from different
        timezones and the correct one is the one used when encoding them.

        :return list leaves: list of time intervals """
        self.ensure_one()
        if resource_id:
            domain = ['|', ('resource_id', '=', resource_id), ('resource_id', '=', False)]
        else:
            domain = [('resource_id', '=', False)]
        if start_datetime:
            domain += [('date_to', '>', fields.Datetime.to_string(start_datetime + timedelta(days=-1)))]
        if end_datetime:
            domain += [('date_from', '<', fields.Datetime.to_string(end_datetime + timedelta(days=1)))]
        leaves = self.env['resource.calendar.leaves'].search(domain + [('calendar_id', '=', self.id)])

        filtered_leaves = self.env['resource.calendar.leaves']
        all_holidays = self.env.context.get('all_holidays', False)
        for leave in leaves:
            holiday_status_id = leave.holiday_id.holiday_status_id or leave.holiday_status_id
            if holiday_status_id and (all_holidays or holiday_status_id.add_to_worked_days):
                if start_datetime:
                    leave_date_to = to_tz(fields.Datetime.from_string(leave.date_to), leave.tz)
                    if not leave_date_to >= start_datetime:
                        continue
                if end_datetime:
                    leave_date_from = to_tz(fields.Datetime.from_string(leave.date_from), leave.tz)
                    if not leave_date_from <= end_datetime:
                        continue
                filtered_leaves += leave
        return [
            self._interval_new(
                to_tz(fields.Datetime.from_string(leave.date_from), leave.tz),
                to_tz(fields.Datetime.from_string(leave.date_to), leave.tz),
                {'leaves': leave},
            ) for leave in filtered_leaves
        ]


class In4HRHolidaysResourceCalendarLeaves(models.Model):

    _inherit = 'resource.calendar.leaves'
    _order = 'date_from desc, date_to asc, name asc, assign_to_working_times_region_id asc, calendar_name asc'

    assign_to_working_times = fields.Selection(
        [('choose', 'Leaves with selected Region'), ('empty', 'Leaves without a Region'), ('all', 'All Leaves')],
        string='Assign to',
        default='choose',
    )
    assign_to_working_times_region_id = fields.Many2one('resource.calendar.region', string='Region', ondelete='restrict')

    @api.model
    def get_calendar_domain(self):
        if self.assign_to_working_times == 'empty':
            return [('region_id', '=', False)]
        elif self.assign_to_working_times == 'choose':
            return [('region_id', '=', self.assign_to_working_times_region_id.id)]
        else:
            return []

    @api.model
    def create(self, values):
        if ('calendar_id' in values) and ('resource_id' in values) and (not 'assign_to_working_times_region_id' in values):
            calendar_id = self.env['resource.calendar'].browse(values['calendar_id'])
            values['assign_to_working_times_region_id'] = calendar_id.region_id.id
        return super(In4HRHolidaysResourceCalendarLeaves, self).create(values)


class In4HRHolidaysResourceCalendarRegion(models.Model):

    _name = 'resource.calendar.region'

    name = fields.Char(string='Region')
    active = fields.Boolean(string='Active', default=True, copy=False)

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Region name already exists !'),
    ]
