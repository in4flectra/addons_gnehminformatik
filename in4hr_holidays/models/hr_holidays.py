# -*- coding: utf-8 -*-

import re
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.exceptions import UserError, ValidationError
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

holidays_validation_type = [
    ('Auto', _('Auto Approve')),
    ('Simple', _('Simple Validation')),
    ('Double', _('Double Validation')),
]

holidays_time_adjustment = [
    ('None', _('None')),
    ('Half', _('Half Days')),
    ('Full', _('Full Days')),
]


class In4HRHolidaysHRHolidaysStatus(models.Model):

    _inherit = 'hr.holidays.status'

    validation_type = fields.Selection(holidays_validation_type, string='Validation Type', required=True, default='Simple')
    auto_approve = fields.Boolean(string='Auto Approve')
    time_adjustment = fields.Selection(holidays_time_adjustment, string='Time Adjustment', required=True, default='None')
    add_to_worked_days = fields.Boolean(string='Add to Worked Days', default=True)
    name_for_reports = fields.Char(string='Name for Reports')

    @api.onchange('validation_type')
    def onchange_validation_type(self):
        if self.validation_type == 'Auto':
            if not self.auto_approve:
                self.auto_approve = True
            if self.double_validation:
                self.double_validation = False
        elif self.validation_type == 'Simple':
            if self.auto_approve:
                self.auto_approve = False
            if self.double_validation:
                self.double_validation = False
        elif self.validation_type == 'Double':
            if self.auto_approve:
                self.auto_approve = False
            if not self.double_validation:
                self.double_validation = True

    @api.onchange('double_validation', 'auto_approve')
    def onchange_validation_fields(self):
        if self.auto_approve:
            if self.validation_type != 'Auto':
                self.validation_type = 'Auto'
        elif self.double_validation:
            if self.validation_type != 'Double':
                self.validation_type = 'Double'
        else:
            if self.validation_type != 'Simple':
                self.validation_type = 'Simple'


class In4HRHolidaysHRHolidays(models.Model):

    _inherit = 'hr.holidays'

    def _default_allocation_year(self):
        return fields.Date.from_string(fields.Date.context_today(self)).strftime('%Y')

    @api.multi
    @api.depends('holiday_type', 'category_id', 'employee_id')
    def _compute_allocation_for(self):
        for holiday in self:
            if holiday.holiday_type == 'employee':
                holiday.allocation_for = holiday.employee_id.name
            else:
                holiday.allocation_for = holiday.category_id.name

    @api.multi
    @api.depends('holiday_status_id', 'holiday_status_id.auto_approve')
    def _compute_validation_type(self):
        for holidays_id in self:
            if holidays_id.holiday_status_id:
                holidays_id.validation_type = holidays_id.holiday_status_id.validation_type
            else:
                holidays_id.validation_type = False

    @api.multi
    @api.depends('holiday_status_id', 'holiday_status_id.auto_approve')
    def _compute_auto_approve(self):
        for holidays_id in self:
            if holidays_id.holiday_status_id:
                holidays_id.auto_approve = holidays_id.holiday_status_id.auto_approve
            else:
                holidays_id.auto_approve = False

    @api.multi
    @api.depends('holiday_status_id', 'holiday_status_id.time_adjustment')
    def _compute_time_adjustment(self):
        for holidays_id in self:
            if holidays_id.holiday_status_id:
                holidays_id.time_adjustment = holidays_id.holiday_status_id.time_adjustment
            else:
                holidays_id.time_adjustment = 'None'

    @api.one
    def _compute_date_fmt(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        lang_code = self._context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        datetime_format = lang_id.date_format + ' ' + lang_id.time_format[:-3]
        date_fmt_from = ''
        if self.date_from:
            date_from = datetime.strptime(self.date_from, DEFAULT_SERVER_DATETIME_FORMAT)
            date_from = pytz.UTC.localize(date_from).astimezone(pytz.timezone(tz))
            date_fmt_from = datetime.strftime(date_from, datetime_format)
            if (self.time_adjustment == 'Full') or ((self.time_adjustment == 'Half') and (not self.half_day_from)):
                date_fmt_from = date_fmt_from[:10]
        self.date_fmt_from = date_fmt_from
        date_fmt_to = ''
        if self.date_to:
            date_to = datetime.strptime(self.date_to, DEFAULT_SERVER_DATETIME_FORMAT)
            date_to = pytz.UTC.localize(date_to).astimezone(pytz.timezone(tz))
            date_fmt_to = datetime.strftime(date_to, datetime_format)
            if (self.time_adjustment == 'Full') or ((self.time_adjustment == 'Half') and (not self.half_day_to)):
                date_fmt_to = date_fmt_to[:10]
        self.date_fmt_to = date_fmt_to

    @api.model
    def get_has_access(self, user_must_be_a_manager):
        result = False
        user = self.env.user
        group_hr_officer = self.env.ref('hr_holidays.group_hr_holidays_user')
        if group_hr_officer in user.groups_id:
            result = True
        elif self.employee_id:
            if (   (    (self.employee_id.user_id == user)
                    and ((user_must_be_a_manager == False) or (self.employee_id.manager))
                   )
                or (self.employee_id.parent_id.user_id == user)
                or (self.employee_id.parent_id.parent_id.user_id == user)
                or (self.employee_id.parent_id.parent_id.parent_id.user_id == user)
                or (self.employee_id.parent_id.parent_id.parent_id.parent_id.user_id == user)
                or (self.employee_id.parent_id.parent_id.parent_id.parent_id.parent_id.user_id == user)
            ):
                result = True
        return result

    @api.multi
    def _compute_has_access(self):
        for holiday in self:
            if holiday.get_has_access(False):
                holiday.has_access = True

    @api.multi
    def _compute_can_edit(self):
        for holiday in self:
            if (holiday.state == 'draft') and holiday.get_has_access(False):
                holiday.can_edit = True

    @api.multi
    def _compute_can_reset(self):
        for holiday in self:
            if holiday.get_has_access(False):
                holiday.can_reset = True

    @api.multi
    def _compute_can_confirm(self):
        for holiday in self:
            if holiday.get_has_access(False):
                holiday.can_confirm = True

    @api.multi
    def _compute_can_validate(self):
        for holiday in self:
            if holiday.get_has_access(True):
                holiday.can_validate = True

    @api.multi
    def _compute_can_approve(self):
        for holiday in self:
            if holiday.get_has_access(True):
                holiday.can_approve = True

    @api.multi
    def _compute_can_refuse(self):
        for holiday in self:
            if holiday.get_has_access(True):
                holiday.can_refuse = True

    state = fields.Selection(default='draft')
    allocation_for = fields.Char(string='Category/Employee', compute=_compute_allocation_for)
    allocation_year = fields.Char(string='Year', default=_default_allocation_year)
    validation_type = fields.Selection(holidays_validation_type, string='Validation Type', compute=_compute_validation_type)
    auto_approve = fields.Boolean(string='Auto Approve', compute=_compute_auto_approve)
    time_adjustment = fields.Selection(holidays_time_adjustment, string='Time Adjustment', compute=_compute_time_adjustment)
    date_only_from = fields.Date(string='Start Date', track_visibility='onchange')
    half_day_from = fields.Boolean(string='Only Afternoon', default=False)
    date_only_to = fields.Date(string='End Date', copy=False, track_visibility='onchange')
    half_day_to = fields.Boolean(string='Only Morning', default=False)
    date_fmt_from = fields.Char(string='Start Date', compute=_compute_date_fmt)
    date_fmt_to = fields.Char(string='End Date', compute=_compute_date_fmt)
    has_access = fields.Boolean('Has access', default=True, compute=_compute_has_access)
    can_edit = fields.Boolean('Can edit', default=True, compute=_compute_can_edit)
    can_confirm = fields.Boolean('Can validate', compute=_compute_can_confirm)
    can_validate = fields.Boolean('Can validate', compute=_compute_can_validate)
    can_approve = fields.Boolean('Can approve', compute=_compute_can_approve)
    can_refuse = fields.Boolean('Can refuse', compute=_compute_can_refuse)

    def _check_allocation_year(self):
        pattern = '^[0-9]*$'
        for holiday in self:
            if holiday.allocation_year:
                if (len(holiday.allocation_year) != 4) or (not re.match(pattern, holiday.allocation_year)):
                    return False
        return True

    _constraints = [(_check_allocation_year, 'Please enter 4 digits into the Year.', ['allocation_year'])]

    @api.model
    def _check_state_access_right(self, vals):
        result = False
        if (   (self.auto_approve)
            or ('state' not in vals)
            or (    (vals.get('state'))
                and (   (vals['state'] in ['draft', 'confirm', 'cancel'])
                     or ((vals['state'] == 'refuse') and (self.can_refuse))
                     or ((vals['state'] == 'validate1') and (self.can_approve))
                     or ((vals['state'] == 'validate') and (self.can_validate))
                    )
                )
        ):
            result = True
        return result

    @api.multi
    def action_confirm(self):
        result = super(In4HRHolidaysHRHolidays, self).action_confirm()
        for holiday in self:
            if holiday.auto_approve:
                return holiday.sudo().action_approve()
        return result

    @api.multi
    def _check_security_action_refuse(self):
        for holiday in self:
            if (not holiday.auto_approve) and (not holiday.can_refuse):
                super(In4HRHolidaysHRHolidays, self)._check_security_action_refuse()

    @api.multi
    def _check_security_action_approve(self):
        for holiday in self:
            if (not holiday.auto_approve) and (not holiday.can_approve):
                super(In4HRHolidaysHRHolidays, self)._check_security_action_approve()

    @api.multi
    def _check_security_action_validate(self):
        for holiday in self:
            if (not holiday.auto_approve) and (not holiday.can_validate):
                    super(In4HRHolidaysHRHolidays, self)._check_security_action_validate()

    @api.model
    def _get_auto_approve_on_creation(self, vals):
        auto_approve = False
        if vals.get('holiday_status_id'):
            auto_approve = self.env['hr.holidays.status'].browse(
                vals.get('holiday_status_id')
            ).auto_approve
        return auto_approve

    @api.onchange('date_from', 'date_to')
    def onchange_datetime_fields(self):
        if (not self.holiday_status_id.time_adjustment) or (self.holiday_status_id.time_adjustment == 'None'):
            self.date_only_from = self.date_from
            self.date_only_to = self.date_to

    @api.onchange('holiday_status_id', 'employee_id', 'date_only_from', 'half_day_from', 'date_only_to', 'half_day_to')
    def onchange_time_adjustment_fields(self):
        if (    self.holiday_status_id
            and self.holiday_status_id.time_adjustment
            and self.employee_id
            and self.employee_id.resource_calendar_id
            and self.date_only_from
            and self.date_only_to
        ):
            date_from = datetime.strptime(self.date_only_from, DEFAULT_SERVER_DATE_FORMAT)
            date_to = datetime.strptime(self.date_only_to, DEFAULT_SERVER_DATE_FORMAT)
            if self.half_day_from:
                date_from = date_from + relativedelta(hour=12)
            if self.half_day_to:
                date_to = date_to + relativedelta(hour=12)
            else:
                date_to = date_to + relativedelta(hour=23, minute=59)
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            self.date_from = pytz.timezone(tz).localize(date_from, is_dst=None).astimezone(pytz.utc)
            self.date_to = pytz.timezone(tz).localize(date_to, is_dst=None).astimezone(pytz.utc)

    @api.multi
    def unlink(self):
        for holiday in self:
            if holiday.holiday_status_id and holiday.auto_approve:
                holiday.state = 'draft'
        for holiday in self.filtered(lambda holiday: holiday.state not in ['draft', 'refuse']):
            raise UserError(_('In order to delete a leave request, the state must be in "To Submit" or "Refused".'))
        for holiday in self.filtered(lambda holiday: holiday.state == 'refuse'):
            holiday.state = 'cancel'
        return super(In4HRHolidaysHRHolidays, self).unlink()

    @api.multi
    def action_draft(self):
        for holiday in self:
            if holiday.can_reset and (holiday.state == 'validate') and holiday.auto_approve:
                holiday.action_refuse()
        return super(In4HRHolidaysHRHolidays, self).action_draft()
