# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'hr_holidays'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'hr_holidays'
               ;
    """)
