# -*- coding: utf-8 -*-

from flectra import api, SUPERUSER_ID


def migrate(cr, version):
    env = api.Environment(cr, SUPERUSER_ID, {})

    holidays = env['hr.holidays'].search([('type', '=', 'remove')])
    for holiday in holidays:
        state = holiday.state
        first_approver_id = holiday.first_approver_id.id
        second_approver_id = holiday.second_approver_id.id
        if state in ['confirm', 'refuse']:
            holiday.action_draft()
        if state in ['validate1', 'validate']:
            holiday.action_refuse()
            holiday.action_draft()

        date_from = holiday.date_from
        date_to = holiday.date_to
        if date_to and date_from and (date_from <= date_to):
            number_of_days_temp = holiday._get_number_of_days(date_from, date_to, holiday.employee_id.id)
        else:
            number_of_days_temp = 0
        if number_of_days_temp != holiday.number_of_days_temp:
            holiday.write({'number_of_days_temp': number_of_days_temp})

        if state == 'confirm':
            holiday.action_confirm()

        if state == 'refuse':
            holiday.action_confirm()
            holiday.action_refuse()

        if state == 'validate1':
            holiday.action_confirm()
            if holiday.state == 'confirm':
                holiday.action_approve()

        if state == 'validate':
            holiday.action_confirm()
            if holiday.state == 'confirm':
                holiday.action_validate()

        if first_approver_id != holiday.first_approver_id.id:
            holiday.write({'first_approver_id': first_approver_id})
        if first_approver_id != holiday.first_approver_id.id:
            holiday.write({'second_approver_id': second_approver_id})
