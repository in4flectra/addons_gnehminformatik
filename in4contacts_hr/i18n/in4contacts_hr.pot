# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4contacts_hr
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4contacts_hr
#: model:ir.model,name:in4contacts_hr.model_res_partner
msgid "Contact"
msgstr ""

#. module: in4contacts_hr
#: model:ir.ui.view,arch_db:in4contacts_hr.hr_employee_form
msgid "e.g. Part Time"
msgstr ""

#. module: in4contacts_hr
#: model:ir.model,name:in4contacts_hr.model_hr_employee
msgid "Employee"
msgstr ""

#. module: in4contacts_hr
#: model:ir.ui.view,arch_db:in4contacts_hr.res_partner_filter
msgid "Employees"
msgstr ""

#. module: in4contacts_hr
#: model:ir.model.fields,field_description:in4contacts_hr.field_hr_employee_firstname
msgid "First Name"
msgstr ""

#. module: in4contacts_hr
#: model:ir.model.fields,field_description:in4contacts_hr.field_hr_employee_lastname
msgid "Last Name"
msgstr ""

#. module: in4contacts_hr
#: model:ir.model.fields,field_description:in4contacts_hr.field_hr_employee_names_order
msgid "Names Order"
msgstr ""

