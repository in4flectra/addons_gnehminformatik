# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsHRResPartner(models.Model):

    _inherit = 'res.partner'

    @api.multi
    def _compute_employee(self):
        for partner in self:
            employee_count = self.env['hr.employee'].search_count([('address_home_id', '=', partner.id)])
            partner.employee = employee_count > 0

    employee = fields.Boolean(compute=_compute_employee, store=True, readonly=True)
