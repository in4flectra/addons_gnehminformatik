# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsHRHREmployee(models.Model):

    _inherit = 'hr.employee'

    @api.model
    def _get_names_order(self):
        return self.env['ir.config_parameter'].sudo().get_param('partner_names_order', 'last_first')

    @api.model
    def _default_names_order(self):
        return self._get_names_order()

    name = fields.Char(string='name', compute='_compute_name', inverse='_inverse_name', required=False, store=True)
    firstname = fields.Char(string='First Name', index=True)
    lastname = fields.Char(string='Last Name', index=True)
    names_order = fields.Char(string='Names Order', compute='_compute_names_order', default=_default_names_order)

    @api.model
    def _merge_name_parts_into_name(self, lastname, firstname):
        last_name = ''
        first_name = ''
        if lastname:
            last_name = lastname.strip()
        if firstname:
            first_name = firstname.strip()

        result = last_name + first_name
        if (len(last_name) > 0) and (len(first_name) > 0):
            order = self._get_names_order()
            if order == 'last_first':
                result = last_name + ' ' + first_name
            elif order == 'last_first_comma':
                result = last_name + ', ' + first_name
            elif order == 'first_last':
                result = first_name + ' ' + last_name
            elif order == 'first_last_comma':
                result = first_name + ', ' + last_name
        return result

    @api.model
    def _split_name_into_name_parts(self, name):
        result = {'lastname': name or None, 'firstname': None}
        if name:
            order = self._get_names_order()
            clean_name = name.replace('  ', ' ')
            if order in ('last_first_comma', 'first_last_comma'):
                clean_name = clean_name.replace(', ', ',')
                clean_name = clean_name.replace(' ,', ',')
                name_parts = clean_name.split(',', 1)
            else:
                name_parts = clean_name.split(' ', 1)
            if len(name_parts) > 1:
                if order in ('last_first', 'last_first_comma'):
                    result = {'lastname': name_parts[0], 'firstname': name_parts[1]}
                else:
                    result = {'lastname': name_parts[1], 'firstname': name_parts[0]}
        return result

    @api.multi
    @api.depends('firstname', 'lastname')
    def _compute_name(self):
        for record in self:
            record.name = self._merge_name_parts_into_name(record.lastname, record.firstname)

    @api.multi
    def _inverse_name(self):
        for record in self:
            name_parts = self._split_name_into_name_parts(record.name)
            record.lastname = name_parts['lastname']
            record.firstname = name_parts['firstname']

    @api.one
    def _compute_names_order(self):
        self.names_order = self._get_names_order()

    @api.onchange('name')
    def _onchange_name(self):
        if self.env.context.get('skip_onchange'):
            self.env.context = self.with_context(skip_onchange=False).env.context
        else:
            self._inverse_name()

    @api.onchange('firstname', 'lastname')
    def _onchange_name_parts(self):
        self.env.context = self.with_context(skip_onchange=True).env.context
        self._compute_name()

    @api.model
    def create(self, values):
        context = dict(self.env.context)
        name = values.get('name', context.get('default_name'))

        if name:
            name_parts = self._split_name_into_name_parts(name)
            for key, value in name_parts.items():
                if (not values.get(key)) or context.get('copy'):
                    values[key] = value

            if 'name' in values:
                values['name'] = name
            if 'default_name' in context:
                del context['default_name']

        result = super(In4ContactsHRHREmployee, self.with_context(context)).create(values)

        if result.address_home_id:
            result.address_home_id._compute_employee()

        return result

    @api.multi
    def write(self, values):
        old_address_home_ids = []
        if 'address_home_id' in values:
            for employee in self:
                old_address_home_ids.append(employee.address_home_id.id)

        result = super(In4ContactsHRHREmployee, self).write(values)

        for partner_id in old_address_home_ids:
            partner = self.env['res.partner'].search([('id', '=', partner_id)], limit=1)
            if partner:
                partner._compute_employee()
        for employee in self:
            partner = self.env['res.partner'].search([('id', '=', employee.address_home_id.id)], limit=1)
            if partner:
                partner._compute_employee()

        return result

    @api.multi
    def unlink(self):
        old_address_home_id = self.address_home_id

        result = super(In4ContactsHRHREmployee, self).unlink()

        if old_address_home_id:
            partner = self.env['res.partner'].search([('id', '=', old_address_home_id)], limit=1)
            if partner:
                partner._compute_employee()

        return result

    def _sync_user(self, user):
        result = super(In4ContactsHRHREmployee, self)._sync_user(user)
        result.update(
            name=self._merge_name_parts_into_name(user.lastname, user.firstname),
            firstname=user.firstname,
            lastname= user.lastname,
        )
        if user.partner_id:
            if user.partner_id.partner_person_id:
                result.update(
                    address_home_id=user.partner_id.partner_person_id.id,
                )
            if user.partner_id.date_of_birth:
                result.update(
                    birthday=user.partner_id.date_of_birth,
                )
            elif user.partner_id.partner_person_id and user.partner_id.partner_person_id.date_of_birth:
                result.update(
                    birthday=user.partner_id.partner_person_id.date_of_birth,
                )
        return result

    @api.model
    def inverse_names(self):
        self._inverse_name()

    @api.model
    def install_contacts_names(self):
        self.search([('firstname', '=', False), ('lastname', '=', False)]).inverse_names()
