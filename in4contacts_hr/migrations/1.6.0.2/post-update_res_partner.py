# -*- coding: utf-8 -*-

from flectra import api, SUPERUSER_ID


def migrate(cr, version):
    env = api.Environment(cr, SUPERUSER_ID, {})
    partners = env['res.partner'].search([])
    for partner in partners:
        partner._compute_employee()
