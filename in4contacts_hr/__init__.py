# -*- coding: utf-8 -*-

from . import models

from flectra import api, SUPERUSER_ID


def post_init_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['hr.employee'].install_contacts_names()
