# -*- coding: utf-8 -*-
{
    'name': 'Employee Directory: First and Last Name',
    'summary': 'First and Last Name for Employees',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.2',

    'depends': [
        'hr',
        'in4contacts',
    ],

    'data': [
        'views/hr_employee.xml',
        'views/res_partner.xml',
    ],

    'auto_install': True,
    'post_init_hook': 'post_init_hook',
}
