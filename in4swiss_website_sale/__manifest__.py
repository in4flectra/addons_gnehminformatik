# -*- coding: utf-8 -*-
{
    'name': 'eCommerce (Swiss Localization)',
    'summary': 'Swiss Localization of Selling Products Online',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4swiss',
        'in4website_sale',
        'website_sale',
    ],

    'auto_install': True
}
