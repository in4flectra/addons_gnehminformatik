flectra.define('in4contacts_point_of_sale.contacts', function (require) {
'use strict';

var models = require('point_of_sale.models');

models.load_fields('res.partner', ['firstname', 'lastname']);

});
