# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale: First and Last Name',
    'summary': 'First and Last Name for Point of Sale',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Point of Sale',
    'version': '0.1',

    'depends': [
        'in4contacts',
        'point_of_sale',
    ],

    'data': [
        'views/assets.xml',
    ],

    'qweb': [
        'static/src/xml/pos.xml',
    ],

    'auto_install': True,
}
