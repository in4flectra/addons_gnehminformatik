# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4swiss_in4construction
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4swiss_in4construction
#: model:ir.model,name:in4swiss_in4construction.model_res_city
#: model:ir.model.fields,field_description:in4swiss_in4construction.field_crm_lead_object_city_id
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_opportunity
msgid "City"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.model.fields,field_description:in4swiss_in4construction.field_crm_lead_object_city_line
msgid "City Line"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_opportunity
msgid "City, State, ZIP"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.model,name:in4swiss_in4construction.model_res_city_swiss_zip_import
msgid "Import Swiss ZIP"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.model,name:in4swiss_in4construction.model_crm_lead
msgid "Lead/Opportunity"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.model.fields,field_description:in4swiss_in4construction.field_res_city_object_lead_ids
msgid "Leads & Opportunities"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_opportunity
msgid "State"
msgstr ""

#. module: in4swiss_in4construction
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_in4construction.crm_lead_form_opportunity
msgid "ZIP"
msgstr ""

