# -*- coding: utf-8 -*-
{
    'name': 'Construction: City-Link on Addresses (Swiss Localization)',
    'summary': 'Swiss Localization of City-Link on Construction-Addresses',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'base_address_city',
        'crm',
        'in4construction',
        'in4contacts',
        'in4swiss',
    ],

    'data': [
        'views/crm_lead.xml',
    ],

    'auto_install': True,
}
