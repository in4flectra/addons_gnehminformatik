# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissIn4ConstructionResCity(models.Model):

    _inherit = 'res.city'

    object_lead_ids = fields.One2many('crm.lead', 'object_city_id', string='Leads & Opportunities')

    @api.multi
    def write(self, vals):
        result = super(In4SwissIn4ConstructionResCity, self).write(vals)

        if (   ('name' in vals)
            or ('zipcode' in vals)
            or ('state_id' in vals)
            or ('country_id' in vals)
        ):
            for lead_id in self.object_lead_ids:
                lead_id.write({
                    'object_city': self.name,
                    'object_zip': self.zipcode,
                    'object_state_id': self.state_id.id,
                    'object_country_id': self.country_id.id,
                })

        return result


class In4SwissIn4ConstructionResCityImportSwiss(models.TransientModel):

    _inherit = 'res.city.swiss_zip_import'

    @api.multi
    def update_swiss_zip(self, ch_country_id):
        super(In4SwissIn4ConstructionResCityImportSwiss, self).update_swiss_zip(ch_country_id)

        domain = [('object_country_id', '=', ch_country_id.id), ('object_city_id', '=', False)]
        object_lead_ids = self.env['crm.lead'].search(domain)
        for lead_id in object_lead_ids:
            lead_id.write({
                'object_city': lead_id.city,
                'object_zip': lead_id.zip,
                'object_country_id': lead_id.country_id.id,
            })
