# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissIn4ConstructionCRMLead(models.Model):

    _inherit = 'crm.lead'

    @api.depends('object_zip', 'object_city', 'object_state_id')
    def _compute_object_city_line(self):
        for lead_id in self:
            object_city_line = ''
            if lead_id.object_zip:
                object_city_line += lead_id.object_zip
                if lead_id.object_city or lead_id.object_state_id:
                    object_city_line += ' '
            if lead_id.object_city:
                object_city_line += lead_id.object_city
                if lead_id.object_state_id:
                    object_city_line += ' '
            if lead_id.object_state_id and lead_id.display_state_on_views:
                object_city_line += lead_id.object_state_id.name
            lead_id.object_city_line = object_city_line

    object_city_id = fields.Many2one('res.city', string='City')
    object_city_line = fields.Char(string='City Line', compute=_compute_object_city_line)

    @api.model
    def _set_object_city_id(self, vals):
        if (   ('object_city' in vals)
            or ('object_zip' in vals)
            or ('object_country_id' in vals)
        ):
            vals['object_city_id'] = None

            object_city = self.object_city
            if 'object_city' in vals:
                object_city = vals['object_city']
            object_zip = self.object_zip
            if 'object_zip' in vals:
                object_zip = vals['object_zip']
            id_object_country = self.object_country_id.id
            if 'object_country_id' in vals:
                id_object_country = int(vals['object_country_id'])

            domain = []
            if object_city:
                domain.append(('name', '=ilike', object_city))
            if object_zip:
                domain.append(('zipcode', '=', object_zip))
            if id_object_country:
                domain.append(('country_id', '=', id_object_country))
            object_city_ids = self.env['res.city'].search(domain)

            if (not object_city_ids) and object_city:
                domain = []
                domain.append(('name', '=ilike', object_city + '%'))
                if object_zip:
                    domain.append(('zipcode', '=', object_zip))
                if id_object_country:
                    domain.append(('country_id', '=', id_object_country))
                object_city_ids = self.env['res.city'].search(domain)

            if len(object_city_ids) == 1:
                vals['object_city_id'] = object_city_ids[0].id
                vals['object_city'] = object_city_ids[0].name
                vals['object_zip'] = object_city_ids[0].zipcode
                vals['object_state_id'] = object_city_ids[0].state_id.id
                vals['object_country_id'] = object_city_ids[0].country_id.id

    @api.model
    def create(self, vals):
        self._set_object_city_id(vals)
        return super(In4SwissIn4ConstructionCRMLead, self).create(vals)

    @api.multi
    def write(self, vals):
        self._set_object_city_id(vals)
        return super(In4SwissIn4ConstructionCRMLead, self).write(vals)

    @api.onchange('object_city_id')
    def _onchange_object_city_id(self):
        if self.object_city_id:
            self.object_city = self.object_city_id.name
            self.object_zip = self.object_city_id.zipcode
            self.object_state_id = self.object_city_id.state_id

    @api.onchange('object_city')
    def _onchange_object_city(self):
        if (not self.object_city) or (self.object_city_id.name != self.object_city):
            self.object_city_id = False

    @api.onchange('object_zip')
    def _onchange_object_zip(self):
        if (not self.object_zip) or (self.object_city_id.zipcode != self.object_zip):
            self.object_city_id = False

    @api.onchange('object_state_id')
    def _onchange_object_state_id(self):
        super(In4SwissIn4ConstructionCRMLead, self)._onchange_object_state()
        if self.object_city_id and (self.object_city_id.state_id != self.object_state_id):
            self.object_city_id = False

    @api.onchange('object_country_id')
    def _onchange_object_country_id(self):
        super(In4SwissIn4ConstructionCRMLead, self)._onchange_object_country()
        if self.object_city_id and (self.object_city_id.country_id != self.object_country_id):
            self.object_city_id = False
