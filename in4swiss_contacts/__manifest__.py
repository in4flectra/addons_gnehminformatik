# -*- coding: utf-8 -*-
{
    'name': 'Contacts (Swiss Localization)',
    'summary': 'Swiss Localization of Customers, Vendors, Partners, ...',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'base',
        'contacts',
        'in4swiss',
    ],

    'data': [
        # views
        'views/res_partner.xml',

        # data
        'data/res.country.csv',
    ],

    'auto_install': True,
}
