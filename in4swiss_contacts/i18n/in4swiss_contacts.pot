# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4swiss_contacts
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4swiss_contacts
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_base
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_short
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_simple
msgid "e.g. +41 23 456 78 90"
msgstr ""

#. module: in4swiss_contacts
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_base
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_short
#: model:ir.ui.view,arch_db:in4swiss_contacts.res_partner_form_simple
msgid "e.g. +41 79 123 45 67"
msgstr ""

