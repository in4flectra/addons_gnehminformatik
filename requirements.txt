# asterisk_click2dial
py-Asterisk==0.5.18

# in4contacts_google_sync
google-api-python-client==2.9.0
google-auth-httplib2==0.1.0
google-auth-oauthlib==0.4.4
oauth2client==4.1.3

# in4nextcloud
webdavclient3==3.14.5

# in4tools_backup2sftp
paramiko==2.7.2

# in4tools_sync_sqlany
sqlanydb==1.0.11
