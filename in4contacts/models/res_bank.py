# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsResBank(models.Model):

    _inherit = 'res.bank'

    @api.multi
    def _compute_display_on_views(self):
        for bank_id in self:
            if bank_id.country:
                bank_id.display_state_on_views = bank_id.country.display_state_on_views
                bank_id.display_country_on_views = bank_id.country.display_country_on_views
            else:
                bank_id.display_state_on_views = True
                bank_id.display_country_on_views = True

    display_state_on_views = fields.Boolean(string='Display State on Views', compute=_compute_display_on_views)
    display_country_on_views = fields.Boolean(string='Display Country on Views', compute=_compute_display_on_views)
