# -*- coding: utf-8 -*-

from flectra import api, models


class In4ContactsResUsers(models.Model):

    _inherit = 'res.users'

    @api.model
    def _get_names_order(self):
        return self.env['ir.config_parameter'].sudo().get_param('partner_names_order', 'last_first')

    @api.model
    def _merge_name_parts_into_name(self, lastname, firstname):
        last_name = ''
        first_name = ''
        if lastname:
            last_name = lastname.strip()
        if firstname:
            first_name = firstname.strip()

        result = last_name + first_name
        if (len(last_name) > 0) and (len(first_name) > 0):
            order = self._get_names_order()
            if order == 'last_first':
                result = last_name + ' ' + first_name
            elif order == 'last_first_comma':
                result = last_name + ', ' + first_name
            elif order == 'first_last':
                result = first_name + ' ' + last_name
            elif order == 'first_last_comma':
                result = first_name + ', ' + last_name
        return result

    @api.model
    def _split_name_into_name_parts(self, name, is_company=False):
        result = {'lastname': name or None, 'firstname': None}
        if (not is_company) and name:
            order = self._get_names_order()
            clean_name = name.replace('  ', ' ')
            if order in ('last_first_comma', 'first_last_comma'):
                clean_name = clean_name.replace(', ', ',')
                clean_name = clean_name.replace(' ,', ',')
                name_parts = clean_name.split(',', 1)
            else:
                name_parts = clean_name.split(' ', 1)
            if len(name_parts) > 1:
                if order in ('last_first', 'last_first_comma'):
                    result = {'lastname': name_parts[0], 'firstname': name_parts[1]}
                else:
                    result = {'lastname': name_parts[1], 'firstname': name_parts[0]}
        return result

    @api.onchange('firstname', 'lastname')
    def _compute_name(self):
        for record in self:
            record.name = self._merge_name_parts_into_name(record.lastname, record.firstname)

    @api.model
    def default_get(self, fields_list):
        result = super(In4ContactsResUsers, self).default_get(fields_list)
        name_parts = self._split_name_into_name_parts(result.get('name', ''), result.get('is_company', False))
        for field in list(name_parts.keys()):
            if field in fields_list:
                result[field] = name_parts.get(field)
        return result

    @api.multi
    def write(self, values):
        result = super(In4ContactsResUsers, self).write(values)
        for value in values:
            if value.startswith('in_group_'):
                group = self.env['res.groups'].browse(int(value[9:]))
                if group:
                    group.set_private_addresses_groups()
        return result


class In4ContactsResGroups(models.Model):

    _inherit = 'res.groups'

    @api.model
    def set_private_addresses_groups(self):
        domain = [
            ('model', '=', 'res.groups'),
            ('module', '=', 'base'),
            ('name', '=', 'group_private_addresses'),
        ]
        model_data_with = self.env['ir.model.data'].search(domain, limit=1)
        if model_data_with and (self.id == model_data_with.res_id):
            domain = [
                ('model', '=', 'res.groups'),
                ('module', '=', 'in4contacts'),
                ('name', '=', 'group_without_private_addresses'),
            ]
            model_data_without = self.env['ir.model.data'].search(domain, limit=1)
            if model_data_without:
                group_without = self.env['res.groups'].browse(model_data_without.res_id)
                if group_without:
                    users = self.env['res.users'].with_context(active_test=False).search([])
                    group_users = []
                    for user in users:
                        if user.id in self.with_context(active_test=False).users.ids:
                            if user.id in group_without.with_context(active_test=False).users.ids:
                                group_users.append((3, user.id))
                        else:
                            if user.id not in group_without.with_context(active_test=False).users.ids:
                                group_users.append((4, user.id))
                    if group_users:
                        group_without.write({'users': group_users})
        domain = [
            ('model', '=', 'res.groups'),
            ('module', '=', 'in4contacts'),
            ('name', '=', 'group_without_private_addresses'),
        ]
        model_data_without = self.env['ir.model.data'].search(domain, limit=1)
        if model_data_without and (self.id == model_data_without.res_id):
            domain = [
                ('model', '=', 'res.groups'),
                ('module', '=', 'base'),
                ('name', '=', 'group_private_addresses'),
            ]
            model_data_with = self.env['ir.model.data'].search(domain, limit=1)
            if model_data_with:
                group_with = self.env['res.groups'].browse(model_data_with.res_id)
                if group_with:
                    users = self.env['res.users'].with_context(active_test=False).search([])
                    group_users = []
                    for user in users:
                        if user.id in self.with_context(active_test=False).users.ids:
                            if user.id in group_with.with_context(active_test=False).users.ids:
                                group_users.append((3, user.id))
                        else:
                            if user.id not in group_with.with_context(active_test=False).users.ids:
                                group_users.append((4, user.id))
                    if group_users:
                        group_with.write({'users': group_users})

    @api.multi
    def write(self, values):
        result = super(In4ContactsResGroups, self).write(values)
        if 'users' in values:
            self.set_private_addresses_groups()
        return result
