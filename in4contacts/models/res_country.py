# -*- coding: utf-8 -*-

from flectra import fields, models


class In4ContactsResCountry(models.Model):

    _inherit = 'res.country'

    display_state_on_views = fields.Boolean(string='Display State on Views', default=True)
    display_country_on_views = fields.Boolean(string='Display Country on Views', default=True)
