# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsResPartner(models.Model):

    _inherit = 'res.company'

    @api.multi
    def _compute_display_on_views(self):
        for company_id in self:
            if company_id.country_id:
                company_id.display_state_on_views = company_id.country_id.display_state_on_views
                company_id.display_country_on_views = company_id.country_id.display_country_on_views
            else:
                company_id.display_state_on_views = True
                company_id.display_country_on_views = True

    display_state_on_views = fields.Boolean(string='Display State on Views', compute=_compute_display_on_views)
    display_country_on_views = fields.Boolean(string='Display Country on Views', compute=_compute_display_on_views)
