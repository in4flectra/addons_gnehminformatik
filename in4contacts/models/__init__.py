# -*- coding: utf-8 -*-

from . import res_bank
from . import res_company
from . import res_config_settings
from . import res_country
from . import res_partner
from . import res_users
