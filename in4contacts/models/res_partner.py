# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, exceptions, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


class In4ContactsEmptyNamesError(exceptions.ValidationError):

    def __init__(self, record, value=_('No name is set.')):
        self.record = record
        self._value = value
        self.name = _('Error(s) with the name of %d.') % (record.id)
        self.args = (self.name, value)


class In4ContactsResPartner(models.Model):

    _inherit = 'res.partner'

    @api.multi
    @api.depends('firstname', 'lastname')
    def _compute_name(self):
        for partner_id in self:
            partner_id.name = self._merge_name_parts_into_name(partner_id.lastname, partner_id.firstname)

    @api.multi
    def _inverse_name(self):
        for partner_id in self:
            name_parts = partner_id._split_name_into_name_parts(partner_id.name, partner_id.is_company)
            if name_parts['lastname'] or name_parts['firstname']:
                partner_id.lastname = name_parts['lastname']
                partner_id.firstname = name_parts['firstname']
            else:
                partner_id.lastname = partner_id.name

    @api.model
    def _get_names_order(self):
        return self.env['ir.config_parameter'].sudo().get_param('partner_names_order', 'last_first')

    @api.one
    def _compute_names_order(self):
        self.names_order = self._get_names_order()

    @api.model
    def _default_names_order(self):
        return self._get_names_order()

    @api.depends('type')
    def _compute_type_without_private(self):
        for partner_id in self:
            if partner_id.type == 'private':
                partner_id.type_without_private = None
            else:
                partner_id.type_without_private = partner_id.type

    @api.onchange('type_without_private')
    def _inverse_type_without_private(self):
        for partner_id in self:
            partner_id.type = partner_id.type_without_private

    @api.depends('is_company')
    def _compute_contact_type(self):
        for partner_id in self:
            partner_id.contact_type = 'company' if partner_id.is_company else 'person'

    @api.onchange('contact_type')
    def _inverse_contact_type(self):
        for partner_id in self:
            partner_id.is_company = partner_id.contact_type == 'company'

    @api.multi
    def _compute_display_on_views(self):
        for partner_id in self:
            if partner_id.country_id:
                partner_id.display_state_on_views = partner_id.country_id.display_state_on_views
                partner_id.display_country_on_views = partner_id.country_id.display_country_on_views
            else:
                partner_id.display_state_on_views = True
                partner_id.display_country_on_views = True

    @api.depends('salutation_calc', 'salutation_user')
    def _compute_salutation(self):
        for partner_id in self:
            if partner_id.salutation_user:
                partner_id.salutation = partner_id.salutation_user
            else:
                partner_id.salutation = partner_id.salutation_calc

    def _inverse_salutation(self):
        for partner_id in self:
            if partner_id.salutation and (partner_id.salutation != partner_id.salutation_calc):
                partner_id.salutation_user = partner_id.salutation
            else:
                partner_id.salutation_user = None

    @api.depends('name', 'lastname', 'firstname', 'title', 'lang')
    def _compute_salutation_calc(self):
        for partner_id in self:
            salutation = ''
            if (not salutation) and partner_id.title:
                if partner_id.lang:
                    domain = [
                        ('lang', '=', partner_id.lang),
                        ('res_id', '=', partner_id.title.id),
                        ('name', '=', 'res.partner.title,salutation'),
                    ]
                    translation = self.env['ir.translation'].search(domain, limit=1)
                    if translation:
                        salutation = translation.value
                if not salutation:
                    salutation = partner_id.title.salutation
                if salutation:
                    salutation = salutation.format(
                        name=partner_id.name,
                        lastname=partner_id.lastname,
                        firstname=partner_id.firstname
                    )
            if (not salutation) and partner_id.is_company:
                salutation = _('Dear Ladies and Gentlemen')
            if not salutation:
                salutation = _('Dear Sir / Madam')
            partner_id.salutation_calc = salutation

    @api.multi
    @api.depends('date_of_birth')
    def compute_birthday(self):
        for record in self:
            if record.date_of_birth:
                birthday = datetime.strptime(record.date_of_birth, DEFAULT_SERVER_DATE_FORMAT)
                birthday_this_year = datetime.strptime(str(datetime.today().year) + record.date_of_birth[4:], DEFAULT_SERVER_DATE_FORMAT)
                if birthday.month < 10:
                    record.birthday_month = '0' + str(birthday.month)
                else:
                    record.birthday_month = str(birthday.month)
                week_nr = birthday_this_year.isocalendar()[1]
                if week_nr < 10:
                    record.birthday_week = '0' + str(week_nr)
                else:
                    record.birthday_week = str(week_nr)
                if birthday.day < 10:
                    record.birthday_day = '0' + str(birthday.day)
                else:
                    record.birthday_day = str(birthday.day)
                get_param = self.env['ir.config_parameter'].sudo().get_param
                birthdays_prev_days = int(get_param('birthdays_prev_days'))
                birthdays_next_days = int(get_param('birthdays_next_days'))
                birthday_in_2000 = datetime.strptime('2000' + record.date_of_birth[4:], DEFAULT_SERVER_DATE_FORMAT)
                birthday_from = birthday_in_2000 + relativedelta(days=birthdays_prev_days)
                birthday_to = birthday_in_2000 - relativedelta(days=birthdays_next_days)
                if birthday_from.year > 2000:
                    record.birthday_begin_from = birthday_from - relativedelta(years=1)
                    record.birthday_begin_to = birthday_to - relativedelta(years=1)
                else:
                    record.birthday_begin_from = birthday_from
                    record.birthday_begin_to = birthday_to
                if birthday_to.year < 2000:
                    record.birthday_end_from = birthday_from + relativedelta(years=1)
                    record.birthday_end_to = birthday_to + relativedelta(years=1)
                else:
                    record.birthday_end_from = birthday_from
                    record.birthday_end_to = birthday_to

    name = fields.Char(string='Name', compute=_compute_name, inverse=_inverse_name, required=False, store=True)
    firstname = fields.Char(string='First Name', index=True)
    lastname = fields.Char(string='Last Name', index=True)
    names_order = fields.Char(string='Names Order', compute=_compute_names_order, default=_default_names_order)

    type_without_private = fields.Selection(
        string='Address Type',
        selection=[
            ('contact', 'Contact'),
            ('invoice', 'Invoice address'),
            ('delivery', 'Shipping address'),
            ('other', 'Other address'),
        ],
        compute=_compute_type_without_private,
        inverse=_inverse_type_without_private,
        help="Used to select automatically the right address according to the context in sales and purchases documents."
    )
    contact_type = fields.Selection(
        string='Contact Type',
        selection=[('person', 'Contact'), ('company', 'Department')],
        compute=_compute_contact_type,
        inverse=_inverse_contact_type,
    )

    display_state_on_views = fields.Boolean(string='Display State on Views', compute=_compute_display_on_views)
    display_country_on_views = fields.Boolean(string='Display Country on Views', compute=_compute_display_on_views)

    salutation = fields.Char(string='Salutation', compute=_compute_salutation, inverse=_inverse_salutation)
    salutation_calc = fields.Char(string='Salutation (calculated)', compute=_compute_salutation_calc)
    salutation_user = fields.Char(string='Salutation (overwritten)')

    date_of_birth = fields.Date(string='Date of Birth')
    birthday_month = fields.Selection(
        selection=[
            ('01', 'January'),  ('02', 'February'), ('03', 'March'),
            ('04', 'April'),    ('05', 'May'),      ('06', 'June'),
            ('07', 'July'),     ('08', 'August'),   ('09', 'September'),
            ('10', 'October'),  ('11', 'November'), ('12', 'December'),
        ],
        string='Birthday (Month)',
        compute=compute_birthday,
        store=True,
    )
    birthday_week = fields.Selection(
        string='Birthday (Week)',
        selection=[
            ('01', 'Week 1'),  ('02', 'Week 2'),  ('03', 'Week 3'),  ('04', 'Week 4'),  ('05', 'Week 5'),
            ('06', 'Week 6'),  ('07', 'Week 7'),  ('08', 'Week 8'),  ('09', 'Week 9'),  ('10', 'Week 10'),
            ('11', 'Week 11'), ('12', 'Week 12'), ('13', 'Week 13'), ('14', 'Week 14'), ('15', 'Week 15'),
            ('16', 'Week 16'), ('17', 'Week 17'), ('18', 'Week 18'), ('19', 'Week 19'), ('20', 'Week 20'),
            ('21', 'Week 21'), ('22', 'Week 22'), ('23', 'Week 23'), ('24', 'Week 24'), ('25', 'Week 25'),
            ('26', 'Week 26'), ('27', 'Week 27'), ('28', 'Week 28'), ('29', 'Week 29'), ('30', 'Week 30'),
            ('31', 'Week 31'), ('32', 'Week 32'), ('33', 'Week 33'), ('34', 'Week 34'), ('35', 'Week 35'),
            ('36', 'Week 36'), ('37', 'Week 37'), ('38', 'Week 38'), ('39', 'Week 39'), ('40', 'Week 40'),
            ('41', 'Week 41'), ('42', 'Week 42'), ('43', 'Week 43'), ('44', 'Week 44'), ('45', 'Week 45'),
            ('46', 'Week 46'), ('47', 'Week 47'), ('48', 'Week 48'), ('49', 'Week 49'), ('50', 'Week 50'),
            ('51', 'Week 51'), ('52', 'Week 52'), ('53', 'Week 53'), ('54', 'Week 54'),
        ],
        compute=compute_birthday,
        store=True,
    )
    birthday_day = fields.Selection(
        string='Birthday (Day)',
        selection=[
            ('01', 'Day 1'),  ('02', 'Day 2'),  ('03', 'Day 3'),  ('04', 'Day 4'),  ('05', 'Day 5'),
            ('06', 'Day 6'),  ('07', 'Day 7'),  ('08', 'Day 8'),  ('09', 'Day 9'),  ('10', 'Day 10'),
            ('11', 'Day 11'), ('12', 'Day 12'), ('13', 'Day 13'), ('14', 'Day 14'), ('15', 'Day 15'),
            ('16', 'Day 16'), ('17', 'Day 17'), ('18', 'Day 18'), ('19', 'Day 19'), ('20', 'Day 20'),
            ('21', 'Day 21'), ('22', 'Day 22'), ('23', 'Day 23'), ('24', 'Day 24'), ('25', 'Day 25'),
            ('26', 'Day 26'), ('27', 'Day 27'), ('28', 'Day 28'), ('29', 'Day 29'), ('30', 'Day 30'),
            ('31', 'Day 31'),
        ],
        compute=compute_birthday,
        store=True,
    )

    birthday_begin_from = fields.Date(string='Birthday from (Begin)', compute=compute_birthday, store=True)
    birthday_begin_to = fields.Date(string='Birthday to (Begin)', compute=compute_birthday, store=True)
    birthday_end_from = fields.Date(string='Birthday from (End)', compute=compute_birthday, store=True)
    birthday_end_to = fields.Date(string='Birthday to (End)', compute=compute_birthday, store=True)

    child_ids = fields.One2many(help='Contacts of this company, e.g. employees, departments etc.')
    partner_person_id = fields.Many2one('res.partner', string='Individual')
    partner_person_ids = fields.One2many(
        'res.partner',
        'partner_person_id',
        string='Contacts',
        help="Contacts with a link to this individual, e.g. this individual is a employee on one or more companies.",
    )
    partner_invoice_id = fields.Many2one(
        'res.partner',
        string='Invoice Address',
        help="Send invoices to this external individual or company, e.g. to the trustee of this contact.",
    )
    partner_shipping_id = fields.Many2one(
        'res.partner',
        string='Delivery Address',
        help="Send shippings to this external individual or company, e.g. to the logistics company of this contact.",
    )

    @api.multi
    @api.constrains('firstname', 'lastname')
    def _check_name(self):
        for record in self:
            if all((
                isinstance(record.id, int),
                record.type == 'contact' or record.is_company,
                not (record.firstname or record.lastname)
            )):
                raise In4ContactsEmptyNamesError(record)

    @api.model
    def _merge_name_parts_into_name(self, lastname, firstname):
        last_name = ''
        first_name = ''
        if lastname:
            last_name = lastname.strip()
        if firstname:
            first_name = firstname.strip()

        result = last_name + first_name
        if (len(last_name) > 0) and (len(first_name) > 0):
            order = self._get_names_order()
            if order == 'last_first':
                result = last_name + ' ' + first_name
            elif order == 'last_first_comma':
                result = last_name + ', ' + first_name
            elif order == 'first_last':
                result = first_name + ' ' + last_name
            elif order == 'first_last_comma':
                result = first_name + ', ' + last_name
        return result

    @api.model
    def _split_name_into_name_parts(self, name, is_company=False):
        result = {'lastname': name or None, 'firstname': None}
        if name == self.name:
            if is_company:
                result = {'lastname': self.name, 'firstname': None}
            else:
                result = {'lastname': self.lastname, 'firstname': self.firstname}
        elif (not is_company) and name:
            order = self._get_names_order()
            clean_name = name.replace('  ', ' ')
            if order in ('last_first_comma', 'first_last_comma'):
                clean_name = clean_name.replace(', ', ',')
                clean_name = clean_name.replace(' ,', ',')
                name_parts = clean_name.split(',', 1)
            else:
                name_parts = clean_name.split(' ', 1)
            if len(name_parts) == 1:
                result = {'lastname': name_parts[0], 'firstname': None}
            elif len(name_parts) > 1:
                if order in ('last_first', 'last_first_comma'):
                    result = {'lastname': name_parts[0], 'firstname': name_parts[1]}
                else:
                    result = {'lastname': name_parts[1], 'firstname': name_parts[0]}
        return result

    @api.onchange('name')
    def _onchange_name(self):
        if self.env.context.get('skip_onchange'):
            self.env.context = self.with_context(skip_onchange=False).env.context
        else:
            self._inverse_name()

    @api.onchange('firstname', 'lastname')
    def _onchange_name_parts(self):
        self.env.context = self.with_context(skip_onchange=True).env.context

    @api.onchange('type')
    def _onchange_type(self):
        if self.type == 'invoice':
            self.customer = True
        elif self.type == 'delivery':
            self.customer = False

    @api.onchange('contact_type')
    def _onchange_contact_type(self):
        self.is_company = (self.contact_type == 'company')

    @api.onchange('partner_person_id')
    def _onchange_partner_person_id(self):
        if self.partner_person_id:
            self.date_of_birth = self.partner_person_id.date_of_birth
        else:
            self.date_of_birth = None

    @api.multi
    def name_get(self):
        result = []
        for partner in self:
            name = partner.name or ''
            if partner.company_name or partner.parent_id:
                if (partner.type in ['invoice', 'delivery']) or ((not name) and (partner.type in ['other'])):
                    name = dict(self.fields_get(['type'])['type']['selection'])[partner.type]
                if partner.is_company:
                    name = '%s, %s' % (partner.parent_id.name, name)
                else:
                    name = '%s, %s' % (partner.commercial_company_name or partner.parent_id.name, name)
            if self._context.get('show_address_only'):
                name = partner._display_address(without_company=True)
            if self._context.get('show_address'):
                name = name + '\n' + partner._display_address(without_company=True)
            name = name.replace('\n\n', '\n')
            name = name.replace('\n\n', '\n')
            if self._context.get('show_email') and partner.email:
                name = '%s <%s>' % (name, partner.email)
            if self._context.get('html_format'):
                name = name.replace('\n', '<br/>')
            result.append((partner.id, name))
        return result

    @api.model
    def create(self, values):
        context = dict(self.env.context)
        name = values.get('name', context.get('default_name'))
        lastname = values.get('lastname')
        firstname = values.get('firstname')
        is_company = values.get('is_company', self.default_get(['is_company'])['is_company'])
        if name and (not lastname) and (not firstname):
            name_parts = self._split_name_into_name_parts(name, is_company)
            for key, value in name_parts.items():
                if (not values.get(key)) or context.get('copy'):
                    values[key] = value
        elif name and (lastname or firstname):
            merge_name = self._merge_name_parts_into_name(lastname, firstname)
            if name != merge_name:
                values['name'] = merge_name
        if (not is_company) and ('default_name' in context):
            del context['default_name']

        return super(In4ContactsResPartner, self.with_context(context)).create(values)

    @api.multi
    def write(self, values):
        result = super(In4ContactsResPartner, self).write(values)

        for partner_id in self:
            if (    (partner_id.partner_person_id)
                and (  (not partner_id.company_type)
                    or (partner_id.company_type == 'company')
                    or ((partner_id.company_type == 'person') and (not partner_id.parent_id))
               )
            ):
                partner_id.partner_person_id = None
            if 'date_of_birth' in values:
                for partner_person_id in partner_id.partner_person_ids:
                    if partner_person_id.date_of_birth != partner_id.date_of_birth:
                        partner_person_id.write({'date_of_birth': partner_id.date_of_birth})

            users = self.env['res.users'].sudo().search([('partner_id', '=', partner_id.id)])
            for user in users:
                user._compute_display_name()

        return result

    @api.multi
    def copy(self, default=None):
        return super(In4ContactsResPartner, self.with_context(copy=True)).copy(default)

    @api.model
    def default_get(self, fields_list):
        result = super(In4ContactsResPartner, self).default_get(fields_list)

        name_parts = self._split_name_into_name_parts(result.get('name', ''), result.get('is_company', False))
        for field in list(name_parts.keys()):
            if field in fields_list:
                result[field] = name_parts.get(field)

        return result

    @api.multi
    def address_get(self, adr_pref=None):
        result = super(In4ContactsResPartner, self).address_get(adr_pref)

        if adr_pref and ('invoice' in adr_pref):
            if self.partner_invoice_id:
                result['invoice'] = self.partner_invoice_id.id
            if result['invoice'] == self.id:
                domain = [('parent_id', '=', self.id), ('type', '=', 'invoice')]
                invoice_partner_id = self.env['res.partner'].search(domain, limit=1)
                if invoice_partner_id:
                    result['invoice'] = invoice_partner_id.id

        if adr_pref and ('delivery' in adr_pref):
            if self.partner_shipping_id:
                result['delivery'] = self.partner_shipping_id.id
            if result['delivery'] == self.id:
                domain = [('parent_id', '=', self.id), ('type', '=', 'delivery')]
                delivery_partner_id = self.env['res.partner'].search(domain, limit=1)
                if delivery_partner_id:
                    result['delivery'] = delivery_partner_id.id

        return result

    @api.model
    def inverse_names(self):
        self._inverse_name()

    @api.model
    def install_contacts_names(self):
        self.search([('firstname', '=', False), ('lastname', '=', False)]).inverse_names()


class In4ContactsResPartnerCategory(models.Model):

    _inherit = 'res.partner.category'
    _order = 'full_name'

    @api.depends('name', 'parent_id')
    def _compute_full_name(self):
        for current_id in self:
            current_id.full_name = current_id.get_full_name()

    name = fields.Char(translate=False)
    full_name = fields.Char(string='Full Name', compute=_compute_full_name, store=True, index=True)

    _sql_constraints = [
        ('name_uniq', 'unique (full_name)', 'Tag name already exists !'),
    ]

    @api.model
    def get_full_name(self):
        result = self.name
        current_id = self.parent_id
        while current_id:
            result = current_id.name + ' / ' + result
            current_id = current_id.parent_id
        return result

    @api.multi
    def write(self, values):
        result = super(In4ContactsResPartnerCategory, self).write(values)
        if ('name' in values) or ('full_name' in values) or ('parent_id' in values):
            for child_id in self.child_ids:
                child_id.full_name = child_id.get_full_name()
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = [('full_name', operator, name)]
        ids = self.search(domain + args, limit=limit)
        return ids.name_get()


class In4ContactsResPartnerTitle(models.Model):

    _inherit = 'res.partner.title'

    salutation = fields.Char(string='Salutation', translate=True)
