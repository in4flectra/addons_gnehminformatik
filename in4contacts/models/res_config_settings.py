# -*- coding: utf-8 -*-

from flectra import api, fields, models, _


class In4ContactsResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    @api.model
    def _partner_names_order_selection(self):
        return [
            ('last_first', _('Lastname Firstname')),
            ('last_first_comma', _('Lastname, Firstname')),
            ('first_last', _('Firstname Lastname')),
            ('first_last_comma', _('Firstname, Lastname')),
        ]

    partner_names_order = fields.Selection(string='Partner Names Order', selection=_partner_names_order_selection, required=True, default='last_first')
    birthdays_prev_days = fields.Integer(string='Previous Days', default=0)
    birthdays_next_days = fields.Integer(string='Next Days', default=15)

    @api.model
    def get_values(self):
        result = super(In4ContactsResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        result.update(
            partner_names_order=get_param('partner_names_order', default='last_first'),
            birthdays_prev_days=int(get_param('birthdays_prev_days', default=0)),
            birthdays_next_days=int(get_param('birthdays_next_days', default=15)),
        )
        return result

    @api.multi
    def set_values(self):
        super(In4ContactsResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].sudo().set_param
        set_param('partner_names_order', self.partner_names_order)
        set_param('birthdays_prev_days', self.birthdays_prev_days)
        set_param('birthdays_next_days', self.birthdays_next_days)
        self.recalculate_partners_birthday()

    @api.multi
    def recalculate_partners_name(self):
        domain = [('is_company', '=', False), ('firstname', '!=', False), ('lastname', '!=', False)]
        return self.env['res.partner'].with_context(active_test=False).search(domain).inverse_names()

    @api.multi
    def recalculate_partners_birthday(self):
        domain = [('is_company', '=', False), ('date_of_birth', '!=', False)]
        return self.env['res.partner'].with_context(active_test=False).search(domain).compute_birthday()
