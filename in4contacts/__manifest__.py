# -*- coding: utf-8 -*-
{
    'name': 'Extensions of Addresses',
    'summary': 'Extends the Addresses with Fields and Functions',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Contacts',
    'version': '0.2',

    'depends': [
        'base',
        'base_setup',
        'contacts',
        'in4base',
    ],

    'data': [
        # views
        'views/assets.xml',
        'views/res_bank.xml',
        'views/res_company.xml',
        'views/res_config_settings.xml',
        'views/res_country.xml',
        'views/res_partner.xml',
        'views/res_users.xml',

        # data
        'data/res_partner.xml',
    ],

    'post_init_hook': 'post_init_hook',
}
