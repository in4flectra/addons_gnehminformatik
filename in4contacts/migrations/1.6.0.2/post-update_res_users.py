# -*- coding: utf-8 -*-

from flectra import api, SUPERUSER_ID


def migrate(cr, version):
    env = api.Environment(cr, SUPERUSER_ID, {})
    domain = [
        ('model', '=', 'res.groups'),
        ('module', '=', 'base'),
        ('name', '=', 'group_private_addresses'),
    ]
    model_data = env['ir.model.data'].search(domain, limit=1)
    if model_data:
        group = env['res.groups'].browse(model_data.res_id)
        if group:
            group.set_private_addresses_groups()
