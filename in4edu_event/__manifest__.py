# -*- coding: utf-8 -*-
{
    'name': 'Events Organization for Education',
    'summary': 'Extension of Events Organization for Education',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Education',
    'version': '0.1',

    'depends': [
        'event',
        'in4edu_base',
    ],

    'data': [
        'views/event.xml',
    ],
}
