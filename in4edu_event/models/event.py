# -*- coding: utf-8 -*-

from flectra import api, fields, models, _
from flectra.exceptions import UserError

from flectra.addons.mail.models.mail_template import format_tz

event_kinds = [
    ('event', 'Event'),
    ('course', 'Course'),
    ('module', 'Module'),
    ('continuous', 'Continuous Event'),
]


class In4EduEventEventEvent(models.Model):

    _inherit = 'event.event'

    @api.one
    @api.depends('event_ids')
    def compute_event_count(self):
        self.event_ids_count = len(self.event_ids)

    event_kind = fields.Selection(event_kinds, string='Kind', required=True, default='event')
    date_begin = fields.Datetime(required=False)
    date_end = fields.Datetime(required=False)
    event_ids = fields.Many2many(
        'event.event', 'event_event_course_module', 'event_id_self', 'event_id_other',
        string='Events',
        ondelete='cascade',
    )
    event_ids_count = fields.Integer(string='Count of Events', compute=compute_event_count, store=True)

    @api.multi
    def _compute_track_count(self):
        super(In4EduEventEventEvent, self)._compute_track_count()
        for event_id in self:
            delta = 0
            if event_id.event_kind == 'course':
                for module_event_id in event_id.event_ids:
                    delta += module_event_id.track_count
            if delta != 0:
                event_id.track_count -= delta

    @api.model
    def create(self, values):
        result = super(In4EduEventEventEvent, self).create(values)
        if len(result.event_ids) > 0:
            if result.event_ids[0].event_kind == 'course':
                if result.event_kind != 'module':
                    result.event_kind = 'module'
            else:
                if result.event_kind != 'course':
                    result.event_kind = 'course'
        for added_id in result.event_ids:
            domain = [('id', '=', added_id.id)]
            other_event_id = result.env['event.event'].search(domain, limit=1)
            if other_event_id:
                found = False
                for event_id in other_event_id.event_ids:
                    if event_id.id == result.id:
                        found = True
                if not found:
                    other_event_id.write({'event_ids': [(4, result.id)]})
        return result

    @api.multi
    def write(self, values):
        old_ids = []
        if 'event_ids' in values:
            for event_id in self.event_ids:
                old_ids.append(event_id.id)
        result = super(In4EduEventEventEvent, self).write(values)
        if len(self.event_ids) > 0:
            if self.event_ids[0].event_kind == 'course':
                if self.event_kind != 'module':
                    self.event_kind = 'module'
            else:
                if self.event_kind != 'course':
                    self.event_kind = 'course'
        if 'event_ids' in values:
            new_ids = []
            for event_id in self.event_ids:
                new_ids.append(event_id.id)
            deleted_ids = []
            for old_id in old_ids:
                found = False
                for new_id in new_ids:
                    if old_id == new_id:
                        found = True
                if not found:
                    deleted_ids.append(old_id)
            for deleted_id in deleted_ids:
                domain = [('id', '=', deleted_id)]
                other_event_id = self.env['event.event'].search(domain, limit=1)
                if other_event_id:
                    found = False
                    for event_id in other_event_id.event_ids:
                        if event_id.id == self.id:
                            found = True
                    if found:
                        other_event_id.write({'event_ids': [(3, self.id)]})
            added_ids = []
            for new_id in new_ids:
                found = False
                for old_id in old_ids:
                    if new_id == old_id:
                        found = True
                if not found:
                    added_ids.append(new_id)
            for added_id in added_ids:
                domain = [('id', '=', added_id)]
                other_event_id = self.env['event.event'].search(domain, limit=1)
                if other_event_id:
                    found = False
                    for event_id in other_event_id.event_ids:
                        if event_id.id == self.id:
                            found = True
                    if not found:
                        other_event_id.write({'event_ids': [(4, self.id)]})
        if 'name' in values:
            for event_id in self:
                for track_id in event_id.track_ids:
                    track_id.copy_to_course()
        return result

    @api.multi
    def unlink(self):
        for event_id in self:
            if event_id.event_kind == 'course':
                for event_module_id in event_id.event_ids:
                    if event_module_id.event_ids_count == 1:
                        event_module_id.unlink()
                    else:
                        event_module_id.write({'event_ids': [(3, event_id.id)]})
        return super(In4EduEventEventEvent, self).unlink()


class In4EduEventEventRegistration(models.Model):

    _inherit = 'event.registration'

    @api.one
    @api.depends('begin_of_participation', 'event_id', 'event_id.date_tz')
    def _compute_date_begin_located(self):
        if self.begin_of_participation:
            self.date_begin_located = format_tz(
                self.with_context(use_babel=True).env,
                self.begin_of_participation,
                tz=self.event_id.date_tz,
            )
        else:
            self.date_begin_located = False

    @api.one
    @api.depends('end_of_participation', 'event_id', 'event_id.date_tz')
    def _compute_date_end_located(self):
        if self.end_of_participation:
            self.date_end_located = format_tz(
                self.with_context(use_babel=True).env,
                self.end_of_participation,
                tz=self.event_id.date_tz,
            )
        else:
            self.date_end_located = False

    @api.one
    @api.depends('registration_ids')
    def compute_counts(self):
        count_course = len(self.registration_ids.filtered(lambda c: c.event_id.event_kind == 'course'))
        count_module = len(self.registration_ids.filtered(lambda c: c.event_id.event_kind == 'module'))
        self.registration_ids_count_course = count_course
        self.registration_ids_count_module = count_module

    event_kind = fields.Selection(event_kinds, string='Kind', related='event_id.event_kind', readonly=True)
    begin_of_participation = fields.Date(
        string='Start Date of Participation', readonly=True, states={'draft': [('readonly', False)]},
    )
    count_of_participations = fields.Integer(
        string='Count of Participations', readonly=True, states={'draft': [('readonly', False)]},
    )
    end_of_participation = fields.Date(
        string='End Date of Participation', readonly=True, states={'draft': [('readonly', False)]},
    )
    date_begin_located = fields.Char(string='Start Date Located', compute=_compute_date_begin_located)
    date_end_located = fields.Char(string='End Date Located', compute=_compute_date_end_located)

    registration_ids = fields.Many2many(
        'event.registration', 'event_registration_course_module', 'registration_id_self', 'registration_id_other',
        string='Registrations',
        ondelete='cascade',
    )
    registration_ids_count_course = fields.Integer(string='Count of Courses', compute=compute_counts, store=True)
    registration_ids_count_module = fields.Integer(string='Count of Modules', compute=compute_counts, store=True)

    @api.model
    def create(self, values):
        result = super(In4EduEventEventRegistration, self).create(values)
        if result.event_id.event_kind == 'course':
            for event_id in result.event_id.event_ids:
                found = False
                for registration_id in event_id.registration_ids:
                    if registration_id.partner_id.id == result.partner_id.id:
                        found = True
                        registration_id.write({'registration_ids': [(4, result.id)]})
                        result.write({'registration_ids': [(4, registration_id.id)]})
                if not found:
                    registration_id_copy = result.copy(default={'event_id': event_id.id, 'registration_ids': None})
                    registration_id_copy.write({'registration_ids': [(4, result.id)]})
                    result.write({'registration_ids': [(4, registration_id_copy.id)]})
        return result

    @api.multi
    def write(self, values):
        result = super(In4EduEventEventRegistration, self).write(values)
        for rec in self:
            if rec.event_id.event_kind == 'course':
                for registration_id in self.registration_ids:
                    if registration_id.partner_id != rec.partner_id:
                        registration_id.partner_id = rec.partner_id
                    if registration_id.name != rec.name:
                        registration_id.name = rec.name
                    if registration_id.phone != rec.phone:
                        registration_id.phone = rec.phone
                    if registration_id.email != rec.email:
                        registration_id.email = rec.email
        return result

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.event_id.event_kind == 'course':
                for registration_id in rec.registration_ids:
                    if registration_id.registration_ids_count_course == 1:
                        registration_id.unlink()
                    else:
                        registration_id.write({'registration_ids': [(3, rec.id)]})
        return super(In4EduEventEventRegistration, self).unlink()

    @api.one
    def confirm_registration(self):
        result = super(In4EduEventEventRegistration, self).confirm_registration()
        if self.event_id.event_kind == 'course':
            for registration_id in self.registration_ids:
                registration_id.confirm_registration()
        return result

    @api.one
    def button_reg_close(self):
        result = super(In4EduEventEventRegistration, self).button_reg_close()
        if self.event_id.event_kind == 'course':
            for registration_id in self.registration_ids:
                registration_id.button_reg_close()
        return result

    @api.one
    def do_draft(self):
        result = super(In4EduEventEventRegistration, self).do_draft()
        if self.event_id.event_kind == 'course':
            for registration_id in self.registration_ids:
                registration_id.do_draft()
        return result

    @api.one
    def button_reg_close(self):
        if self.event_id.event_kind == 'continuous':
            today = fields.Datetime.now()
            if self.begin_of_participation <= today and self.event_id.state == 'confirm':
                self.write({'state': 'done', 'date_closed': today})
            elif self.event_id.state == 'draft':
                raise UserError(_("You must wait the event confirmation before doing this action."))
            else:
                raise UserError(_("You must wait the event starting day before doing this action."))
        else:
            super(In4EduEventEventRegistration, self).button_reg_close()

    @api.one
    def button_reg_cancel(self):
        result = super(In4EduEventEventRegistration, self).button_reg_cancel()
        if self.event_id.event_kind == 'course':
            for registration_id in self.registration_ids:
                registration_id.button_reg_cancel()
        return result
