# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4edu_event
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4edu_event
#: model:ir.model,name:in4edu_event.model_event_registration
msgid "Attendee"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "Begin Date"
msgstr ""

#. module: in4edu_event
#: selection:event.event,event_kind:0
msgid "Continuous Event"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "Count"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_registration_ids_count_course
msgid "Count of Courses"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_event_event_ids_count
msgid "Count of Events"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_registration_ids_count_module
msgid "Count of Modules"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_count_of_participations
msgid "Count of Participations"
msgstr ""

#. module: in4edu_event
#: selection:event.event,event_kind:0
msgid "Course"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_event_form
#: model:ir.ui.view,arch_db:in4edu_event.event_event_search
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "Courses"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "End Date"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_date_end_located
msgid "End Date Located"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_end_of_participation
msgid "End Date of Participation"
msgstr ""

#. module: in4edu_event
#: selection:event.event,event_kind:0
#: model:ir.model,name:in4edu_event.model_event_event
msgid "Event"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_event_event_ids
#: model:ir.ui.view,arch_db:in4edu_event.event_event_search
msgid "Events"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_event_event_kind
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_event_kind
#: model:ir.ui.view,arch_db:in4edu_event.event_event_search
msgid "Kind"
msgstr ""

#. module: in4edu_event
#: selection:event.event,event_kind:0
msgid "Module"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_event_form
#: model:ir.ui.view,arch_db:in4edu_event.event_event_search
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "Modules"
msgstr ""

#. module: in4edu_event
#: model:ir.ui.view,arch_db:in4edu_event.event_registration_form
msgid "Participation"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_registration_ids
msgid "Registrations"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_date_begin_located
msgid "Start Date Located"
msgstr ""

#. module: in4edu_event
#: model:ir.model.fields,field_description:in4edu_event.field_event_registration_begin_of_participation
msgid "Start Date of Participation"
msgstr ""

#. module: in4edu_event
#: code:addons/in4edu_event/models/event.py:272
#, python-format
msgid "You must wait the event confirmation before doing this action."
msgstr ""

#. module: in4edu_event
#: code:addons/in4edu_event/models/event.py:274
#, python-format
msgid "You must wait the event starting day before doing this action."
msgstr ""

