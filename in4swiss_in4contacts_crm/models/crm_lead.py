# -*- coding: utf-8 -*-

from flectra import api, models


class In4SwissIn4ContactsCRMCRMLead(models.Model):

    _inherit = 'crm.lead'

    @api.depends('zip', 'city', 'state_id')
    def _compute_city_line(self):
        for lead_id in self:
            city_line = ''
            if lead_id.zip:
                city_line += lead_id.zip
                if lead_id.city or lead_id.state_id:
                    city_line += ' '
            if lead_id.city:
                city_line += lead_id.city
                if lead_id.state_id:
                    city_line += ' '
            if lead_id.state_id and lead_id.display_state_on_views:
                city_line += lead_id.state_id.name
            lead_id.city_line = city_line
