# -*- coding: utf-8 -*-
{
    'name': 'CRM: Extensions of Addresses (Swiss Localization)',
    'summary': 'Swiss Localization of the Extensions of Addresses for Leads and Opportunities',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4contacts_crm',
        'in4swiss',
    ],

    'data': [
        'views/crm_lead.xml',
    ],

    'auto_install': True,
}
