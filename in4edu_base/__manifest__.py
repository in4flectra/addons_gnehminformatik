# -*- coding: utf-8 -*-
{
    'name': 'Base for Education',
    'summary': 'Base of Extension for Education',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/base.xml',
    ],
}
