# -*- coding: utf-8 -*-
{
    'name': 'eCommerce (extended)',
    'summary': 'Extension of eCommerce',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Website',
    'version': '0.1',

    'depends': [
        'account',
        'base',
        'payment',
        'sale',
        'sale_payment',
        'sales_team',
        'website',
        'website_sale',

        'in4website',
    ],

    'data': [
        'views/payment.xml',
        'views/product.xml',
        'views/templates.xml',
        'views/website.xml',
        'views/website_sale.xml',
    ],

    'auto_install': True,
}
