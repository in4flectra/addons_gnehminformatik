# -*- coding: utf-8 -*-

from flectra import http
from flectra.http import request

from flectra.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSale(WebsiteSale):

    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth='public', website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        response = super(WebsiteSale, self).shop(page, category, search, ppg, **post)

        categs_count = {}
        for categ in response.qcontext['categories'].ids + response.qcontext['categories_with_child']:
            if categ not in categs_count:
                categs_count[categ] = 0
        domain = self._get_search_domain(search, None, None, None, None)
        products = request.env['product.template'].search(domain)
        categs_count[0] = len(products)
        for product in products:
            for categ in product.public_categ_ids:
                while categ:
                    if categ.id in categs_count:
                        categs_count[categ.id] += 1
                    else:
                        categs_count[categ.id] = 1
                    categ = categ.parent_id
        response.qcontext['categories_count'] = categs_count

        return response
