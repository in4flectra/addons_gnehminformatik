# -*- coding: utf-8 -*-

import logging

from flectra import fields, models
from flectra.exceptions import UserError

_logger = logging.getLogger(__name__)


class In4WebsiteSalePaymentAcquirer(models.Model):

    _inherit = 'payment.acquirer'

    invoice_handling = fields.Selection(
        [
            ('none', 'Send Quotation'),
            ('create', 'Confirm Quotation and create Invoice'),
            ('validate', 'Confirm Quotation and validate Invoice'),
            ('send', 'Confirm Quotation and send Invoice')
        ],
        string='Invoice Handling',
        default='none',
        required=True,
    )


class In4WebsiteSalePaymentTransaction(models.Model):

    _inherit = 'payment.transaction'

    def generate_and_send_invoice(self):
        self.sale_order_id._force_lines_to_invoice_policy_order()

        # force company to ensure journals/accounts etc. are correct
        # company_id needed for default_get on account.journal
        # force_company needed for company_dependent fields
        ctx_company = {
            'company_id': self.sale_order_id.company_id.id,
            'force_company': self.sale_order_id.company_id.id
        }

        # We might fail to create the invoice because there is no invoiceable lines. This will
        # raise a UserError and break the workflow. Better catch the error.
        try:
            created_invoice = self.sale_order_id.with_context(**ctx_company).action_invoice_create()
            created_invoice = self.env['account.invoice'].browse(created_invoice).with_context(**ctx_company)
        except UserError:
            _logger.warning(
                '<%s> transaction completed, could not auto-generate invoice for %s (ID %s)',
                self.acquirer_id.provider,
                self.sale_order_id.name,
                self.sale_order_id.id,
                exc_info=True,
            )
            return

        if created_invoice:
            _logger.info(
                '<%s> transaction completed, auto-generated invoice %s (ID %s) for %s (ID %s)',
                self.acquirer_id.provider,
                created_invoice.name,
                created_invoice.id,
                self.sale_order_id.name,
                self.sale_order_id.id,
            )
            if self.acquirer_id.invoice_handling in ['validate', 'send']:
                created_invoice.action_invoice_open()
                if self.acquirer_id.invoice_handling == 'send':
                    created_invoice.force_invoice_send()
        else:
            _logger.warning(
                '<%s> transaction completed, could not auto-generate invoice for %s (ID %s)',
                self.acquirer_id.provider,
                self.sale_order_id.name,
                self.sale_order_id.id,
            )
