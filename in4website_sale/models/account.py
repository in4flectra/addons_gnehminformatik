# -*- coding: utf-8 -*-

from flectra import api, models


class In4WebsiteSaleAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.multi
    def force_invoice_send(self):
        for invoice in self:
            email_act = invoice.action_invoice_sent()
            if email_act and email_act.get('context'):
                email_ctx = email_act['context']
                email_ctx.update(default_email_from=invoice.company_id.email)
                invoice.with_context(email_ctx).message_post_with_template(email_ctx.get('default_template_id'))
        return True

    @api.multi
    def action_invoice_paid(self):
        result = super(In4WebsiteSaleAccountInvoice, self).action_invoice_paid()
        for invoice in self:
            lines = invoice.invoice_line_ids.mapped('sale_line_ids')
            if len(lines) > 0:
                order = lines[0].order_id
                if order and order.payment_tx_id:
                    order.payment_tx_id.state = 'done'
        return result
