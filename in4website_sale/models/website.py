# -*- coding: utf-8 -*-

from flectra import fields, models


class In4WebsiteSaleWebsite(models.Model):

    _inherit = 'website'

    product_meta_title_format = fields.Char(
        string='Product meta title format',
        default='[name]',
    )
    product_meta_description_format = fields.Text(
        string='Product meta description format',
        default='[name] | [brand_id.name] | [website_price,{0:,.2f}] | [description_sale]',
    )
