# -*- coding: utf-8 -*-

from flectra import api, fields, models

from addons_gnehminformatik.in4base.models.in4base import format_data


class In4WebsiteSaleProductTemplate(models.Model):

    _inherit = 'product.template'

    @api.multi
    def _compute_website_meta_data(self):
        lang_id = self.env.user.lang_id
        title_format = '[name]'
        description_format = '[name] | [brand_id.name] | [website_price,{0:,.2f}] | [description_sale]'
        website_id = self.env.context.get('website_id')
        if website_id:
            website = self.env['website'].browse(self.env.context['website_id'])
            if website:
                title_format = website.product_meta_title_format or title_format
                description_format = website.product_meta_description_format or description_format
        for product in self:
            product.website_meta_title_calculated = format_data(product, title_format, lang_id)
            product.website_meta_description_calculated = format_data(product, description_format, lang_id)

    website_meta_title_calculated = fields.Char(
        string='Calculated website meta title',
        compute=_compute_website_meta_data,
    )
    website_meta_description_calculated = fields.Text(
        string='Calculated website meta description',
        compute=_compute_website_meta_data,
    )
