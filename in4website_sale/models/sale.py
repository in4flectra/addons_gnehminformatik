# -*- coding: utf-8 -*-

from flectra import api, models


class In4WebsiteSaleSaleOrder(models.Model):

    _inherit = 'sale.order'

    @api.multi
    def force_quotation_send(self):
        for order in self:
            if (self.env.context.get('send_email')
                    or (not order.payment_tx_id)
                    or (not order.payment_tx_id.acquirer_id)
                    or (order.payment_tx_id.acquirer_id.provider != 'transfer')
                    or (order.payment_tx_id.acquirer_id.invoice_handling == 'none')):
                super(In4WebsiteSaleSaleOrder, order).force_quotation_send()
            else:
                order.generate_and_send_invoice()
        return True

    def generate_and_send_invoice(self):
        self.ensure_one()
        self.action_confirm()
        self.payment_tx_id.generate_and_send_invoice()
