# -*- coding: utf-8 -*-
{
    'name': 'Tax (extended): Invoicing with Discount and Bonus',
    'summary': 'Extension of Tax: Discounts and Bonus for Sales, Invoices and Payments',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'in4invoice_discount',
        'in4tax',
    ],

    'data': [
        # views
        'views/account_invoice.xml',
        'views/sale_order.xml',

        # reports
        'views/report_invoice.xml',
        'views/report_saleorder.xml',
    ],

    'auto_install': True,
}
