# -*- coding: utf-8 -*-
{
    'name': 'XML Interface',
    'summary': 'Import and Export of XML-Data',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'in4base',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/xml_interface.xml',
    ],
}
