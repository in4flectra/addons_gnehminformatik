# -*- coding: utf-8 -*-

import base64
import fnmatch
import io
import logging
import os
import uuid
import xml.dom.minidom

from flectra import api, fields, models, tools, _
from pathlib import Path
from xml.dom.minidom import getDOMImplementation, parse

_logger = logging.getLogger(__name__)


class In4ToolsXMLInterfaceImport(models.TransientModel):

    _name = 'xml_interface.import'
    _description = 'XML Import'

    file = fields.Binary(string='File to import')
    full_name = fields.Char(string='Filename to import')

    @api.model
    def _split_xml_id(self, xml_id):
        id = xml_id.split('.')
        if len(id) > 1:
            xml_id_name = id[1]
            xml_id_module = id[0]
        else:
            xml_id_name = id[0]
            xml_id_module = ''
        return xml_id_module, xml_id_name

    @api.model
    def _import_xml_node(self, import_parent, start_transaction=True):
        for import_data in import_parent.childNodes:
            if start_transaction:
                self.env.cr.autocommit(False)
            try:
                if import_data.nodeType == 1:
                    if import_data.hasAttribute('table'):
                        table_name = import_data.getAttribute('table')
                    else:
                        table_name = import_data.nodeName
                    try:
                        field_objects = self.env[table_name].fields_get()
                    except:
                        field_objects = None
                        pass

                    if field_objects:
                        values = {}
                        translated_values = []
                        table_domain = []
                        xml_id_name = None
                        xml_id_module = ''

                        if import_data.hasAttribute('domain'):
                            attr_domain = eval(import_data.getAttribute('domain'))
                            for attr_domain_part in attr_domain:
                                if isinstance(attr_domain_part[0], str) and (attr_domain_part[0] == 'xml_id'):
                                    id_list = []
                                    xml_id_list = []
                                    if (str(attr_domain_part[2])[:1] == '[') and (str(attr_domain_part[2])[-1:] == ']'):
                                        for xml_id in attr_domain_part[2]:
                                            xml_id_list.append(xml_id)
                                    else:
                                        xml_id_list.append(attr_domain_part[0])
                                    for xml_id in xml_id_list:
                                        xml_id_module, xml_id_name = self._split_xml_id(xml_id)
                                        domain = [
                                            ('model', '=', table_name),
                                            ('name', '=', xml_id_name),
                                            ('module', '=', xml_id_module),
                                        ]
                                        model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                        if model_data_id:
                                            id_list.append(model_data_id.res_id)
                                    if id_list:
                                        if (str(attr_domain_part[2])[:1] == '[') and (str(attr_domain_part[2])[-1:] == ']'):
                                            table_domain.append(('id', attr_domain_part[1], id_list))
                                        else:
                                            table_domain.append(('id', attr_domain_part[1], id_list[0]))
                                elif isinstance(attr_domain_part[2], str) and (attr_domain_part[2][:7] == 'xml_id='):
                                    relation_name = field_objects[attr_domain_part[0]]['relation']
                                    domain_xml_id_module, domain_xml_id_name = self._split_xml_id(attr_domain_part[2][7:])
                                    domain = [
                                        ('model', '=', relation_name),
                                        ('name', '=', domain_xml_id_name),
                                        ('module', '=', domain_xml_id_module),
                                    ]
                                    model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                    if model_data_id:
                                        table_domain.append((attr_domain_part[0], attr_domain_part[1], model_data_id.res_id))
                                    else:
                                        table_domain.append(attr_domain_part)
                                else:
                                    table_domain.append(attr_domain_part)
                        elif import_data.hasAttribute('xml_id'):
                            xml_id_module, xml_id_name = self._split_xml_id(import_data.getAttribute('xml_id'))
                            domain = [
                                ('model', '=', table_name),
                                ('name', '=', xml_id_name),
                                ('module', '=', xml_id_module),
                            ]
                            model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                            if model_data_id:
                                table_domain.append(('id', '=', model_data_id.res_id))
                        elif import_data.hasAttribute('id'):
                            table_domain.append(('id', '=', import_data.getAttribute('id')))

                        import_type = 'create' # 'create', 'write', 'unlink'
                        if import_data.hasAttribute('type'):
                            import_type = import_data.getAttribute('type')

                        if import_type != 'unlink':
                            for import_child in import_data.childNodes:
                                import_text = import_child.firstChild
                                if (   (import_text and (import_text.nodeType == 3))
                                    or (    (import_child.nodeType == 1)
                                        and (import_child.hasAttribute('none') or import_child.hasAttribute('domain'))
                                       )
                                ):
                                    field_name = import_child.nodeName
                                    if import_text:
                                        value = import_text.nodeValue
                                    else:
                                        value = None
                                    if import_child.hasAttribute('none') and eval(import_child.getAttribute('none')):
                                        value = None
                                    if import_child.hasAttribute('lang'):
                                        lang_attr = import_child.getAttribute('lang')
                                    else:
                                        lang_attr = self.env.context.get('lang') or 'en_US'
                                    if field_name == 'xml_id':
                                        xml_id_module, xml_id_name = self._split_xml_id(value)
                                    elif field_name != 'id':
                                        for field_object in field_objects:
                                            if field_object == field_name:
                                                field_type = field_objects[field_name]['type']
                                                if field_type in ('char', 'text'):
                                                    field_translate = field_objects[field_name].get('translate') or False
                                                else:
                                                    field_translate = False
                                                if field_translate:
                                                    translated_values.append({
                                                        'table': table_name,
                                                        'field': field_name,
                                                        'lang': lang_attr,
                                                        'value': value,
                                                    })
                                                else:
                                                    if field_type == 'boolean':
                                                        if value:
                                                            value = eval(value)
                                                    elif field_type == 'many2many':
                                                        if value:
                                                            value = eval(value)
                                                            many2many_values = []
                                                            relation_name = field_objects[field_name]['relation']
                                                            for val in value:
                                                                if isinstance(val[1], str):
                                                                    rel_xml_id_module, rel_xml_id_name = self._split_xml_id(val[1])
                                                                    domain = [
                                                                        ('model', '=', relation_name),
                                                                        ('name', '=', rel_xml_id_name),
                                                                        ('module', '=', rel_xml_id_module),
                                                                    ]
                                                                    model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                                                    if model_data_id:
                                                                        many2many_values.append((int(val[0]), model_data_id.res_id))
                                                                else:
                                                                    many2many_values.append((val[0], val[1]))
                                                            value = many2many_values
                                                    elif field_type == 'many2one':
                                                        relation_name = field_objects[field_name]['relation']
                                                        if import_data.hasAttribute(field_name):
                                                            if value:
                                                                search_field = import_data.getAttribute(field_name)
                                                                domain = [(search_field, '=', value)]
                                                                relation_id = self.env[relation_name].search(domain, limit=1)
                                                                if not relation_id:
                                                                    _logger.warning('Create in %s: "%s"' % (relation_name, value))
                                                                    relation_id = self.env[relation_name].create({search_field: value})
                                                                if relation_id:
                                                                    value = relation_id.id
                                                        elif import_child.hasAttribute('domain'):
                                                            domain = eval(import_child.getAttribute('domain'))
                                                            relation_id = self.env[relation_name].search(domain, limit=1)
                                                            if relation_id:
                                                                value = relation_id.id
                                                            else:
                                                                value = None
                                                        elif value:
                                                            rel_xml_id_module, rel_xml_id_name = self._split_xml_id(value)
                                                            domain = [
                                                                ('model', '=', relation_name),
                                                                ('name', '=', rel_xml_id_name),
                                                                ('module', '=', rel_xml_id_module),
                                                            ]
                                                            model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                                            if not model_data_id:
                                                                domain = [
                                                                    ('model', '=', relation_name),
                                                                    ('name', '=', relation_name.replace('.', '_') + '_xml_import_' + value),
                                                                ]
                                                                model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                                            if model_data_id:
                                                                value = model_data_id.res_id
                                                    values[field_name] = value

                        if import_type == 'unlink':
                            table_ids = self.env[table_name].with_context(active_test=False).search(table_domain)
                            for table_id in table_ids:
                                self.env.cr.autocommit(False)
                                try:
                                    table_id.unlink()
                                    self.env.cr.commit()
                                    self.env.cr.autocommit(True)
                                except:
                                    self.env.cr.rollback()
                                    self.env.cr.autocommit(True)
                                    table_id.write({'active': False})
                                    pass
                        elif xml_id_name or values or translated_values:
                            table_ids = None
                            if table_domain:
                                if import_type == 'write':
                                    table_ids = self.env[table_name].with_context(active_test=False).search(table_domain)
                                else:
                                    table_ids = self.env[table_name].with_context(active_test=False).search(table_domain, limit=1)
                            if table_ids:
                                for table_id in table_ids:
                                    for translated_value in translated_values:
                                        domain = [
                                            ('type', '=', 'model'),
                                            ('name', '=', ('%s,%s' % (translated_value['table'], translated_value['field']))),
                                            ('lang', '=', translated_value['lang']),
                                            ('res_id', '=', table_id.id),
                                        ]
                                        translation_id = self.env['ir.translation'].search(domain, limit=1)
                                        if translation_id:
                                            translation_id.write({
                                                'value': translated_value['value']
                                            })
                                        else:
                                            values[translated_value['field']] = translated_value['value']
                                    if values:
                                        table_id.write(values)
                                    if xml_id_name:
                                        domain = [
                                            ('model', '=', table_name),
                                            ('name', '=', xml_id_name),
                                            ('module', '=', xml_id_module),
                                        ]
                                        model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                        if model_data_id:
                                            if model_data_id.res_id != table_id.id:
                                                model_data_id.write({'res_id': table_id.id})
                                        else:
                                            self.env['ir.model.data'].create({
                                                'model': table_name,
                                                'name': xml_id_name,
                                                'module': xml_id_module,
                                                'res_id': table_id.id,
                                            })
                            elif import_type == 'create':
                                for translated_value in translated_values:
                                    values[translated_value['field']] = translated_value['value']
                                table_id = self.env[table_name].create(values)
                                if xml_id_name:
                                    self.env['ir.model.data'].create({
                                        'model': table_name,
                                        'name': xml_id_name,
                                        'module': xml_id_module,
                                        'res_id': table_id.id,
                                    })
                                else:
                                    self.env['ir.model.data'].create({
                                        'model': table_name,
                                        'name': 'import_' + str(table_id.id),
                                        'module': 'xml_interface',
                                        'res_id': table_id.id,
                                    })

                    self._import_xml_node(import_data, False)
                if start_transaction:
                    self.env.cr.commit()
            except Exception as e:
                if start_transaction:
                    self.env.cr.rollback()
                msg = 'Exception: %s' % (tools.ustr(e))
                _logger.exception(msg)
            if start_transaction:
                self.env.cr.autocommit(True)

    @api.multi
    def import_xml_data(self):
        if self.full_name:
            file = tools.misc.file_open(self.full_name, 'rb')
        else:
            file = io.StringIO(base64.decodebytes(self.file).decode('utf-8'))
        import_xml = parse(file)
        import_root = import_xml.documentElement

        self._import_xml_node(import_root)

    @api.model_cr
    def import_update_data(self):
        module_ids = self.env['ir.module.module'].sudo().search([('state', '=', 'installed')])
        for module_id in module_ids:
            for ad in tools.config['addons_path'].split(','):
                path = ad + '/' + module_id.name + '/data/update'
                if os.path.isdir(path):
                    fn_list = fnmatch.filter(os.listdir(path), '*.xml')
                    if len(fn_list) > 0:
                        fn_list.sort(key=lambda s: s.lower())
                        for fn in fn_list:
                            full_name = path + '/' + fn
                            import_id = self.env['xml_interface.import'].create({
                                'full_name': full_name,
                            })
                            import_id.import_xml_data()
                            _logger.info('File "%s" imported' % (full_name))

    @api.model_cr
    def _register_hook(self):
        if tools.config.options['stop_after_init']:
            domain = [('name', '=', 'in4base_language'), ('state', '=', 'installed')]
            in4base_language_installed = self.env['ir.module.module'].sudo().search_count(domain)
            if in4base_language_installed == 0:
                self.import_update_data()


class In4ToolsXMLInterfaceExport(models.Model):

    _name = 'xml_interface.export'
    _description = 'XML Export'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    description = fields.Char(string='Description')
    file = fields.Char(string='Export to File', required='True', default=lambda self: self._default_file())
    schema = fields.Text(string='Schema', default='<?xml version="1.0" encoding="utf-8"?>')

    @api.model
    def _default_file(self):
        return os.path.join(tools.config['data_dir'], 'exports', 'export.xml')

    @api.model
    def _split_xml_id(self, xml_id):
        id = xml_id.split('.')
        if len(id) > 1:
            xml_id_name = id[1]
            xml_id_module = id[0]
        else:
            xml_id_name = id[0]
            xml_id_module = ''
        return xml_id_module, xml_id_name

    @api.model
    def _table_to_xml(self, xml_doc, xml_parent, object_name, object_ids, schema_parent):
        field_names = []
        child_indexes = {}
        field_objects = self.env[object_name].fields_get()

        field_selection = 'exclude'
        if schema_parent.hasAttribute('fields'):
            field_selection = schema_parent.getAttribute('fields')
        if field_selection == 'include':
            child_idx = 0
            for schema_child in schema_parent.childNodes:
                if schema_child.nodeName == 'field':
                    if schema_child.getAttribute('name') == 'xml_id':
                        field_names.append(schema_child.getAttribute('name'))
                        child_indexes[schema_child.getAttribute('name')] = child_idx
                    else:
                        for field_name in field_objects:
                            if schema_child.getAttribute('name') == field_name:
                                field_names.append(field_name)
                                child_indexes[field_name] = child_idx
                child_idx += 1
        elif field_selection == 'exclude':
            for field_name in field_objects:
                field_names.append(field_name)
            for schema_child in schema_parent.childNodes:
                if schema_child.nodeName == 'field':
                    field_names.remove(schema_child.getAttribute('name'))
            field_names.sort()

        domain = []
        if isinstance(object_ids, list):
            domain.append(('id', 'in', object_ids))
        if schema_parent.hasAttribute('domain'):
            for part in eval(schema_parent.getAttribute('domain')):
                if part[0] == 'xml_id':
                    res_ids = []
                    if isinstance(part[2], list):
                        xml_ids = part[2]
                    else:
                        xml_ids = [part[2]]
                    for xml_id in xml_ids:
                        xml_id_module, xml_id_name = self._split_xml_id(xml_id)
                        xml_domain = [
                            ('model', '=', object_name),
                            ('name', '=', xml_id_name),
                            ('module', '=', xml_id_module),
                        ]
                        model_data_id = self.env['ir.model.data'].search(xml_domain, limit=1)
                        if model_data_id:
                            res_ids.append(model_data_id.res_id)
                    if res_ids:
                        if isinstance(part[2], list):
                            domain.append(('id', part[1], res_ids))
                        else:
                            domain.append(('id', part[1], res_ids[0]))
                    else:
                        if isinstance(part[2], list):
                            domain.append(('id', part[1], [0]))
                        else:
                            domain.append(('id', part[1], 0))
                else:
                    domain.append(part)
        for schema_child in schema_parent.childNodes:
            if schema_child.nodeName == 'domain':
                domain_text = schema_child.firstChild
                if domain_text and (domain_text.nodeType == 3):
                    domain.append(eval(domain_text.nodeValue))

        order = 'id asc'
        if schema_parent.hasAttribute('order'):
            order = schema_parent.getAttribute('order')

        table_ids = self.env[object_name].with_context(active_test=False).search(domain, order=order)
        for table_id in table_ids:
            export_table = xml_doc.createElement(object_name)
            xml_parent.appendChild(export_table)

            domain = [
                ('model', '=', object_name),
                ('res_id', '=', table_id.id),
            ]
            model_data_id = self.env['ir.model.data'].search(domain, limit=1)
            if model_data_id:
                xml_id = str(model_data_id.name)
            else:
                xml_id = str(uuid.uuid1())
                model_data_id = self.env['ir.model.data'].create({
                    'model': object_name,
                    'name': xml_id,
                    'module': 'xml_interface',
                    'res_id': table_id.id,
                })
            if model_data_id.module:
                xml_id = model_data_id.module + '.' + xml_id

            if schema_parent.hasAttribute('id'):
                id_attr = schema_parent.getAttribute('id')
                if id_attr == 'id':
                    export_table.setAttribute('id', str(table_id.id))
                elif id_attr == 'xml_id':
                    export_table.setAttribute('xml_id', xml_id)

            lang_attr = None
            languages = None
            if schema_parent.hasAttribute('lang'):
                lang_attr = schema_parent.getAttribute('lang')
            if lang_attr:
                if lang_attr == 'all':
                    languages = self.env['res.lang'].search([('translatable', '=', True)])
                else:
                    languages = self.env['res.lang'].search([('code', '=', lang_attr)])

            for schema_child in schema_parent.childNodes:
                if schema_child.nodeName == 'attribute':
                    attribute_text = schema_child.firstChild
                    if attribute_text and (attribute_text.nodeType == 3):
                        export_table.setAttribute(schema_child.getAttribute('name'), attribute_text.nodeValue)

            for field_name in field_names:
                if field_name == 'xml_id':
                    if xml_id:
                        export_field = xml_doc.createElement(field_name)
                        export_table.appendChild(export_field)
                        export_text = xml_doc.createTextNode(xml_id)
                        export_field.appendChild(export_text)
                elif field_name in table_id:
                    field_type = field_objects[field_name]['type']
                    field_translate = False
                    translated_values = {}
                    if languages and (field_type in ('char', 'text')):
                        field_translate = field_objects[field_name].get('translate') or False
                    if field_translate:
                        for lang in languages:
                            domain = [
                                ('type', '=', 'model'),
                                ('name', '=', ('%s,%s' % (object_name, field_name))),
                                ('res_id', '=', table_id.id),
                                ('lang', '=', lang.code),
                            ]
                            translation_id = self.env['ir.translation'].search(domain, limit=1)
                            if translation_id and translation_id.value:
                                translated_values[lang.code] = translation_id.value

                    export_field = xml_doc.createElement(field_name)
                    export_table.appendChild(export_field)
                    export_field.setAttribute('type', field_type)
                    if (   translated_values
                        or table_id[field_name]
                        or ((field_type == 'boolean') and isinstance(table_id[field_name], bool))
                    ):
                        if field_type == 'boolean':
                            if table_id[field_name]:
                                export_text = xml_doc.createTextNode('True')
                            else:
                                export_text = xml_doc.createTextNode('False')
                            export_field.appendChild(export_text)
                        elif field_type in ['float']:
                            val = '{:.10f}'.format(table_id[field_name]).rstrip('0').rstrip('.')
                            if val.find('.') < 0:
                                val += '.0'
                            export_text = xml_doc.createTextNode(val)
                            export_field.appendChild(export_text)
                        elif field_type in ['integer', 'monetary']:
                            export_text = xml_doc.createTextNode(str(table_id[field_name]))
                            export_field.appendChild(export_text)
                        elif field_type in ['date', 'datetime']:
                            export_text = xml_doc.createTextNode(str(table_id[field_name]))
                            export_field.appendChild(export_text)
                        elif field_type in ['char', 'text', 'selection', 'html']:
                            val = table_id[field_name]
                            if val and isinstance(val, int):
                                val = str(val)
                            if val and ((lang_attr == 'all') or (not translated_values)):
                                export_text = xml_doc.createTextNode(val)
                                export_field.appendChild(export_text)
                                for key in translated_values:
                                    export_field_translated = xml_doc.createElement(field_name)
                                    export_table.appendChild(export_field_translated)
                                    export_field_translated.setAttribute('type', field_type)
                                    export_field_translated.setAttribute('lang', key)
                                    export_text = xml_doc.createTextNode(translated_values[key])
                                    export_field_translated.appendChild(export_text)
                            else:
                                for key in translated_values:
                                    export_text = xml_doc.createTextNode(translated_values[key])
                                    export_field.appendChild(export_text)
                        elif field_type == 'binary':
                            export_text = xml_doc.createTextNode(str(table_id[field_name])[2:-1])
                            export_field.appendChild(export_text)
                        elif field_type == 'one2many':
                            relation_name = field_objects[field_name]['relation']
                            export_field.setAttribute('relation', relation_name)
                            for schema_child in schema_parent.childNodes:
                                if schema_child.nodeName == 'field':
                                    ids = table_id[field_name].ids
                                    self._table_to_xml(xml_doc, export_field, relation_name, ids, schema_child)
                        elif field_type == 'many2one':
                            id = str(table_id[field_name].id)
                            if child_indexes:
                                schema_child = schema_parent.childNodes[child_indexes[field_name]]
                                if schema_child.hasAttribute('id'):
                                    id_attr = schema_parent.getAttribute('id')
                                    if id_attr == 'xml_id':
                                        domain = [
                                            ('model', '=', field_objects[field_name]['relation']),
                                            ('res_id', '=', id),
                                        ]
                                        model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                        if model_data_id:
                                            id = str(model_data_id.name)
                                            if model_data_id.module:
                                                id = model_data_id.module + '.' + id
                            export_text = xml_doc.createTextNode(id)
                            export_field.appendChild(export_text)
                        elif field_type == 'many2many':
                            vals = []
                            id_attr = ''
                            if child_indexes:
                                schema_child = schema_parent.childNodes[child_indexes[field_name]]
                                if schema_child.hasAttribute('id'):
                                    id_attr = schema_parent.getAttribute('id')
                            for id in table_id[field_name].ids:
                                id = str(id)
                                if id_attr == 'xml_id':
                                    domain = [
                                        ('model', '=', field_objects[field_name]['relation']),
                                        ('res_id', '=', id),
                                    ]
                                    model_data_id = self.env['ir.model.data'].search(domain, limit=1)
                                    if model_data_id:
                                        id = str(model_data_id.name)
                                        if model_data_id.module:
                                            id = model_data_id.module + '.' + id
                                vals.append('(4, \'' + id + '\')')
                            export_text = xml_doc.createTextNode('[' + ','.join(vals) + ']')
                            export_field.appendChild(export_text)
                    else:
                        export_field.setAttribute('none', 'True')

    @api.multi
    def export_xml_data(self, context=None):
        self.ensure_one()

        content = xml.dom.minidom.parseString(self.schema)
        schema_root = content.documentElement

        impl = getDOMImplementation()
        export_xml = impl.createDocument(None, schema_root.nodeName, None)
        export_root = export_xml.documentElement

        comment = '\n'
        if self.name:
            comment += _('Name:') + ' ' + self.name.strip() + '\n'
        if self.description:
            comment += _('Description:') + '\n'
            comment += self.description.strip() + '\n'
        if self.file:
            comment += _('File:') + ' ' + self.file.strip() + '\n'
        if self.schema:
            comment += _('Schema:') + '\n'
            comment += self.schema.strip() + '\n'
        comment_node = export_xml.createComment(comment.replace('--', '-|-'))
        export_root.appendChild(comment_node)

        for schema_data in schema_root.childNodes:
            if schema_data.nodeName == 'table':
                table_name = schema_data.getAttribute('name')
                if self.env.registry.models.get(table_name):
                    self._table_to_xml(export_xml, export_root, table_name, None, schema_data)

        file = self.file
        file = file.replace('~', str(Path.home())).strip()
        dir = os.path.dirname(file)
        if not os.path.isdir(dir):
            os.makedirs(dir)
        file_handle = open(file, 'wb')
        file_handle.write(export_xml.toprettyxml(indent='    ', encoding='utf-8'))
        file_handle.close()
