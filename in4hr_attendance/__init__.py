# -*- coding: utf-8 -*-

from . import models

from flectra import api, SUPERUSER_ID


def pre_init_hook(cr):
    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'hr_attendance'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'hr_attendance'
               ;
    """)

    cr.execute("""
        DELETE
          FROM res_groups
         WHERE id = (SELECT res_id
                       FROM ir_model_data
                      WHERE model = 'res.groups'
                        AND name = 'group_hr_attendance_change'
                    )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'res.groups'
           AND name = 'group_hr_attendance_change'
               ;
    """)


def post_init_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    attendance_ids = env['hr.attendance'].search([])
    for attendance_id in attendance_ids:
        attendance_id.check_in_set = True
        if attendance_id.check_out:
            attendance_id.check_out_set = True
