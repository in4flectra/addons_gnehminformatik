# -*- coding: utf-8 -*-

import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, exceptions, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4HRAttendanceHREmployee(models.Model):

    _inherit = 'hr.employee'

    @api.multi
    def attendance_action_change(self):
        # Override default function
        if len(self) > 1:
            raise exceptions.UserError(_('Cannot perform check in or check out on multiple employees.'))
        action_date = fields.Datetime.now()

        if self.attendance_state != 'checked_in':
            vals = {
                'employee_id': self.id,
                'check_in_set': True,
                'check_in': action_date,
            }
            return self.env['hr.attendance'].sudo().create(vals)
        else:
            attendance = self.env['hr.attendance'].search([('employee_id', '=', self.id), ('check_out', '=', False)], limit=1)
            if attendance:
                vals = {
                    'check_out_set': True,
                    'check_out': action_date,
                }
                attendance.sudo().write(vals)
            else:
                raise exceptions.UserError(_('Cannot perform check out on %(empl_name)s, could not find corresponding check in. '
                    'Your attendances have probably been modified manually by human resources.') % {'empl_name': self.name, })
            return attendance
