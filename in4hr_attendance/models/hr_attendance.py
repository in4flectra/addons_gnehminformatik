# -*- coding: utf-8 -*-

import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class In4HRAttendanceHRAttendance(models.Model):

    _inherit = 'hr.attendance'

    check_in_date = fields.Date(string='Date', compute='_compute_check_in_date', inverse='_inverse_check_in_date', default=lambda self: fields.Date.today())
    check_in_set = fields.Boolean(string='Check In', default=False)
    check_in_time = fields.Float(string='Start', compute='_compute_check_in_time', inverse='_inverse_check_in_time')
    check_out_set = fields.Boolean(string='Check Out', default=False)
    check_out_time = fields.Float(string='Stop', compute='_compute_check_out_time', inverse='_inverse_check_out_time')

    def _compute_check_in_date(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_in:
                date_time = datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
                attendance_id.check_in_date = date_time
            else:
                attendance_id.check_in_date = None

    def _inverse_check_in_date(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_in_date:
                values = {}
                if attendance_id.check_in:
                    date_time = datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                    date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
                    time_str = datetime.strftime(date_time, DEFAULT_SERVER_DATETIME_FORMAT)
                    time_str = time_str[11:]
                    values['check_in'] = attendance_id.check_in_date + ' ' + time_str
                if attendance_id.check_out:
                    date_time = datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT)
                    date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
                    time_str = datetime.strftime(date_time, DEFAULT_SERVER_DATETIME_FORMAT)
                    time_str = time_str[11:]
                    values['check_out'] = attendance_id.check_in_date + ' ' + time_str
                if values:
                    attendance_id.write(values)

    def _compute_check_in_time(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_in:
                date_time = datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
                attendance_id.check_in_time = date_time.hour + date_time.minute / 60.0
            else:
                attendance_id.check_in_time = 0.0

    def _inverse_check_in_time(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_in_time:
                values = {}
                if attendance_id.check_in and attendance_id.check_in_set:
                    date_str = attendance_id.check_in_date
                    time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(attendance_id.check_in_time * 60, 60))
                    check_in = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
                    values['check_in'] = pytz.timezone(tz).localize(check_in, is_dst=None).astimezone(pytz.utc)

                    if attendance_id.check_out_set:
                        if divmod(attendance_id.check_out_time * 60, 60) < divmod(attendance_id.check_in_time * 60, 60):
                            check_in_date = datetime.strptime(attendance_id.check_in_date, DEFAULT_SERVER_DATE_FORMAT)
                            check_out_date = check_in_date + relativedelta(days=1)
                            date_str = datetime.strftime(check_out_date, DEFAULT_SERVER_DATE_FORMAT)
                        else:
                            date_str = attendance_id.check_in_date
                        time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(attendance_id.check_out_time * 60, 60))
                        check_out = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
                        values['check_out'] = pytz.timezone(tz).localize(check_out, is_dst=None).astimezone(pytz.utc)
                else:
                    values['check_in'] = None
                    values['check_out'] = None
                if values:
                    attendance_id.write(values)

    def _compute_check_out_time(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_out:
                date_time = datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT)
                date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
                attendance_id.check_out_time = date_time.hour + date_time.minute / 60.0
            else:
                attendance_id.check_out_time = 0.0

    def _inverse_check_out_time(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        for attendance_id in self:
            if attendance_id.check_out_time:
                values = {}
                if attendance_id.check_in and attendance_id.check_out_set:
                    if divmod(attendance_id.check_out_time * 60, 60) < divmod(attendance_id.check_in_time * 60, 60):
                        check_in_date = datetime.strptime(attendance_id.check_in_date, DEFAULT_SERVER_DATE_FORMAT)
                        check_out_date = check_in_date + relativedelta(days=1)
                        date_str = datetime.strftime(check_out_date, DEFAULT_SERVER_DATE_FORMAT)
                    else:
                        date_str = attendance_id.check_in_date
                    time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(attendance_id.check_out_time * 60, 60))
                    check_out = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
                    values['check_out'] = pytz.timezone(tz).localize(check_out, is_dst=None).astimezone(pytz.utc)
                else:
                    values['check_out'] = None
                if values:
                    attendance_id.write(values)

    @api.onchange('check_in_set')
    def _onchange_check_in_set(self):
        if not self.check_in_set:
            self.check_in_set = True
        if self.check_in_date and self.check_in_set:
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            date_time = pytz.UTC.localize(datetime.now()).astimezone(pytz.timezone(tz))
            self.check_in_time = date_time.hour + date_time.minute / 60.0

            time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(self.check_in_time * 60, 60))
            self.check_in = self.check_in_date + ' ' + time_str + ':00'
            self._onchange_check_out_set()

    @api.onchange('check_out_set')
    def _onchange_check_out_set(self):
        if self.check_in_date and self.check_out_set:
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            date_time = pytz.UTC.localize(datetime.now()).astimezone(pytz.timezone(tz))
            self.check_out_time = date_time.hour + date_time.minute / 60.0

            time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(self.check_in_time * 60, 60))
            self.check_in = self.check_in_date + ' ' + time_str + ':00'

            time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(self.check_out_time * 60, 60))
            if divmod(self.check_out_time * 60, 60) < divmod(self.check_in_time * 60, 60):
                check_in_date = datetime.strptime(self.check_in_date, DEFAULT_SERVER_DATE_FORMAT)
                check_out_date = check_in_date + relativedelta(days=1)
                date_str = datetime.strftime(check_out_date, DEFAULT_SERVER_DATE_FORMAT)
            else:
                date_str = self.check_in_date
            self.check_out = date_str + ' ' + time_str + ':00'
        else:
            self.check_out = None
            self.check_out_time = 0.0

    @api.model
    def create(self, values):
        if (    (values['check_in_set']) and ('check_in_time' in values)
            and ('check_in_date' in values) and ('check_in_set' in values)
        ):
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            date_str = values['check_in_date']
            time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(values['check_in_time'] * 60, 60))
            check_in = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
            values['check_in'] = pytz.timezone(tz).localize(check_in, is_dst=None).astimezone(pytz.utc)
            if ('check_out_set' in values) and (values['check_out_set']) and ('check_out_time' in values):
                if divmod(values['check_out_time'] * 60, 60) < divmod(values['check_in_time'] * 60, 60):
                    check_in_date = datetime.strptime(date_str, DEFAULT_SERVER_DATE_FORMAT)
                    check_out_date = check_in_date + relativedelta(days=1)
                    date_str = datetime.strftime(check_out_date, DEFAULT_SERVER_DATE_FORMAT)
                time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(values['check_out_time'] * 60, 60))
                check_out = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
                values['check_out'] = pytz.timezone(tz).localize(check_out, is_dst=None).astimezone(pytz.utc)
        return super(In4HRAttendanceHRAttendance, self).create(values)

    @api.multi
    def write(self, values):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        if (    (('check_out' in values) and (values['check_out']))
            and (('check_out_set' in values) and (values['check_out_set']))
            and (('check_out_time' not in values) or (not values['check_out']))
        ):
            date_time = datetime.strptime(values['check_out'], DEFAULT_SERVER_DATETIME_FORMAT)
            date_time = pytz.UTC.localize(date_time).astimezone(pytz.timezone(tz))
            values['check_out_time'] = date_time.hour + date_time.minute / 60
        if (   (('check_in_set' not in values) and self.check_in_set)
            or (('check_in_set' in values) and (values['check_in_set']))
        ):
            if 'check_in_date' in values:
                date_str = values['check_in_date']
            else:
                date_str = self.check_in_date
            if 'check_in_time' in values:
                check_in_time = values['check_in_time']
            else:
                check_in_time = self.check_in_time
            time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(check_in_time * 60, 60))
            check_in = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
            values['check_in'] = pytz.timezone(tz).localize(check_in, is_dst=None).astimezone(pytz.utc)
            if (   (('check_out_set' not in values) and self.check_out_set)
                or (('check_out_set' in values) and (values['check_out_set']))
            ):
                if 'check_out_time' in values:
                    check_out_time = values['check_out_time']
                else:
                    check_out_time = self.check_out_time
                if divmod(check_out_time * 60, 60) < divmod(check_in_time * 60, 60):
                    check_in_date = datetime.strptime(date_str, DEFAULT_SERVER_DATE_FORMAT)
                    check_out_date = check_in_date + relativedelta(days=1)
                    date_str = datetime.strftime(check_out_date, DEFAULT_SERVER_DATE_FORMAT)
                time_str = '{0:02.0f}:{1:02.0f}'.format(*divmod(check_out_time * 60, 60))
                check_out = datetime.strptime(date_str + ' ' + time_str + ':00', DEFAULT_SERVER_DATETIME_FORMAT)
                values['check_out'] = pytz.timezone(tz).localize(check_out, is_dst=None).astimezone(pytz.utc)
        return super(In4HRAttendanceHRAttendance, self).write(values)
