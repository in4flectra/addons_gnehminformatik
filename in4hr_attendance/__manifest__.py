# -*- coding: utf-8 -*-
{
    'name': 'Attendances (extended)',
    'summary': 'Extension of manage employee attendances',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.2',

    'depends': [
        'base',
        'hr_attendance',
    ],

    'data': [
        # security
        'security/in4hr_attendance.xml',
        'security/ir.model.access.csv',

        # views
        'views/hr_attendance.xml',
    ],

    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
}
