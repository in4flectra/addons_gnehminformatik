# -*- coding: utf-8 -*-

from flectra import api, SUPERUSER_ID


def migrate(cr, version):
    env = api.Environment(cr, SUPERUSER_ID, {})
    attendance_ids = env['hr.attendance'].search([])
    for attendance_id in attendance_ids:
        attendance_id.check_in_set = True
        if attendance_id.check_out:
            attendance_id.check_out_set = True
