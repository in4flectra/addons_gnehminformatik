# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM res_groups
         WHERE id = (SELECT res_id
                       FROM ir_model_data
                      WHERE model = 'res.groups'
                        AND name = 'group_hr_attendance_change'
                    )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'res.groups'
           AND name = 'group_hr_attendance_change'
               ;
    """)
