# -*- coding: utf-8 -*-

import logging
import pytz
import sqlanydb
import time

from datetime import datetime

from flectra import api, fields, models, tools, _
from flectra.exceptions import UserError
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class ConnectionFailedError(UserError):
    pass


class ConnectionSuccessError(UserError):
    pass


class In4ToolsSyncSQLAnyDatabase(models.Model):

    _name = 'sync_sqlany.database'
    _description = 'Database to synchronize'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True, copy=False)

    uid = fields.Char(string='User', required=True)
    pwd = fields.Char(string='Password', required=True)
    engine_name = fields.Char(string='Engine Name', required=True)
    database_name = fields.Char(string='Database Name', required=True)
    host = fields.Char(string='Host (Name or IP)', required=True)
    port = fields.Integer(string='Port', required=True, default=2638)

    test_query = fields.Text(string='Test Query')
    query_result = fields.Text(string='Query Result', readonly=True, copy=False)

    table_ids = fields.One2many('sync_sqlany.table', 'database_id', string='Tables', copy=True)

    @api.multi
    def clear_query_result(self):
        for database_id in self:
            database_id.write({
                'query_result': None,
            })

    @api.multi
    def connection_test(self):
        for database_id in self:
            connection = database_id.connection_open(is_test=True)
            if (connection):
                try:
                    query_result = ''
                    if (database_id.test_query):
                        rows, cols = database_id.remote_execute(
                            connection=connection,
                            query=database_id.test_query,
                            metadata = True,
                        )
                        query_result = database_id.rows2csv(rows, cols)
                    database_id.write({
                        'query_result': query_result,
                    })
                    self.env.cr.commit()
                finally:
                    database_id.connection_close(connection)
        raise ConnectionSuccessError(_('Connection test succeeded!\nEverything seems properly set up.'))

    @api.multi
    def connection_open(self, is_test=False):
        try:
            # isql -v -k "Driver=/opt/sqlanywhere12/lib64/libdbodbc12.so;ENG=W10DEV - PerformX - SAQQ - 13.1 - A12;UID=PerformX;PWD=WagudE10.;DBN=SAQQ;LINKS=TCPIP(HOST=192.168.178.70:2638)"
            connection = sqlanydb.connect(
                UID=self.uid,  # 'PerformX',
                PWD=self.pwd,  # 'WagudE10.',
                EngineName=self.engine_name,  # 'W10DEV - PerformX - SAQQ - 13.1 - A12',
                DatabaseName=self.database_name,  # 'SAQQ',
                LINKS='TCPIP(HOST=' + self.host + ':' + str(self.port) + ')'
            )
            return connection
        except Exception as e:
            _logger.exception('Connection open failure: %s' % (tools.ustr(e)))
            if (is_test):
                raise ConnectionFailedError(_('Connection test failed!\nHere is what we got instead:\n%s') % (tools.ustr(e)))

    @api.multi
    def connection_close(self, connection):
        try:
            result = connection.close()
            return result
        except Exception as e:
            _logger.exception('Connection close failure: %s' % (tools.ustr(e)))

    @api.multi
    def remote_execute(self, connection=None, query=None, params=None, metadata=False):
        rows = []
        cols = []

        if (connection):
            conn = self.connection_open()
        else:
            conn = connection
        if (conn):
            cur = conn.cursor()
            cur.execute(query)
            if metadata:
                cols = [d[0] for d in cur.description]
            rows = cur.fetchall()
            if (not connection):
                self.connection_close(conn)

        return rows, cols

    @api.multi
    def remote_search(self, query, *args, **kwargs):
        """
        :param query:
        :param args:
        :param kwargs:
        :return:
        """

    @api.multi
    def remote_browse(self, record_ids, *args, **kwargs):
        """
        :param record_ids:
        :param args:
        :param kwargs:
        :return:
        """

    @api.multi
    def remote_create(self, vals, *args, **kwargs):
        """
        :param vals:
        :param args:
        :param kwargs:
        :return:
        """

    @api.multi
    def remote_update(self, record_ids, vals, *args, **kwargs):
        """
        :param record_ids:
        :param vals:
        :param args:
        :param kwargs:
        :return:
        """

    @api.multi
    def remote_delete(self, record_ids, *args, **kwargs):
        """
        :param record_ids:
        :param args:
        :param kwargs:
        :return:
        """

    @api.model
    def rows2csv(self, rows, cols=[]):
        result = ''
        if (len(cols) > 0):
            for col in cols:
                result += '"' + col.replace('"', '""') + '",'
            result = result[:-1] + '\n'
        for row in rows:
            for cell in row:
                if (cell):
                    if (   (isinstance(cell, int))
                        or (isinstance(cell, float))
                    ):
                        result += str(cell) + ','
                    else:
                        try:
                            result += '"' + cell.replace('"', '""') + '",'
                        except:
                            pass
                else:
                    result += ','
            result = result[:-1] + '\n'
        return result

    @api.multi
    def remote_to_local(self):
        for database_id in self:
            for table_id in database_id.table_ids:
                table_id.remote_to_local()


class In4ToolsSyncSQLAnyTable(models.Model):

    _name = 'sync_sqlany.table'
    _description = 'Table'
    _order = 'database_id, sequence, name'

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True, copy=False)
    sequence = fields.Integer(string='Sequence')

    database_id = fields.Many2one('sync_sqlany.database', string='Database', required=True, ondelete='cascade')
    display_name = fields.Char(compute='compute_display_name', store=True)

    model_id = fields.Many2one('ir.model', string='Model', required=True, ondelete='cascade')
    timestamp_begin = fields.Datetime(string='Timestamp Begin', copy=False)
    timestamp_end = fields.Datetime(string='Timestamp End', copy=False)
    record_ids = fields.One2many('sync_sqlany.record', 'table_id', string='Records')

    remote_query = fields.Text(string='Remote Query')
    query_result = fields.Text(string='Query Result', readonly=True, copy=False)

    @api.depends('name', 'database_id')
    def compute_display_name(self):
        for table_id in self:
            table_id.display_name = '{0} - {1}'.format(table_id.database_id.name, table_id.name)

    @api.multi
    def clear_query_result(self):
        for table_id in self:
            table_id.write({
                'query_result': None,
            })

    @api.multi
    def test_remote_query(self):
        for table_id in self:
            query_result = ''
            if (table_id.remote_query):
                connection = table_id.database_id.connection_open()
                if (connection):
                    try:
                        msg = _('Remote Query Test started on %s') % (time.strftime('%Y-%m-%d %H:%M:%S'))
                        _logger.info(msg)
                        query_result += msg + '\n'
                        rows, cols = table_id.database_id.remote_execute(
                            connection=connection,
                            query=table_id.remote_query,
                            metadata=True,
                        )
                        msg = _('Remote Query Test executed on %s (%s rows)') % (time.strftime('%Y-%m-%d %H:%M:%S'), str(len(rows)))
                        _logger.info(msg)
                        query_result += msg + '\n'
                        query_result += table_id.database_id.rows2csv(rows, cols)
                    finally:
                        table_id.database_id.connection_close(connection)
            table_id.write({
                'query_result': query_result,
            })

    @api.multi
    def test_search_query(self):
        for table_id in self:
            query_result = ''
            if (table_id.remote_query):
                connection = table_id.database_id.connection_open()
                if (connection):
                    try:
                        rows, cols = table_id.database_id.remote_execute(
                            connection=connection,
                            query=table_id.remote_query,
                            metadata=True,
                        )
                        local_table = self.env[table_id.model_id.model]
                        for row in rows:
                            col_idx = 0
                            search_domain = []
                            for col in cols:
                                if (col == '__search_domain__'):
                                    search_domain = eval(row[col_idx])
                                col_idx += 1
                            if (search_domain):
                                local_table_ids = local_table.search(search_domain)
                                if (len(local_table_ids) == 0):
                                    query_result += 'Not found: ' + str(search_domain) + '\n'
                                elif (len(local_table_ids) > 1):
                                    query_result += 'More then one found:\n'
                                    for local_table_id in local_table_ids:
                                        query_result += '- ' + local_table_id.name + '\n'
                    finally:
                        table_id.database_id.connection_close(connection)
            table_id.write({
                'query_result': query_result,
            })

    @api.model
    def _get_local_id(self, search_field, record_table, local_fields, local_name, row, col_idx, values):
        result = values

        local_field_type = local_fields[local_name]['type']
        if (local_field_type.lower() == 'many2one'):
            local_field_relation = local_fields[local_name]['relation']
            domain = [
                ('table_id.model_id.model', '=', local_field_relation),
                (search_field, '=', row[col_idx]),
            ]
            record_id = record_table.search(domain, limit=1)
            if (record_id):
                result[local_name] = record_id.local_id
        elif (local_field_type.lower() == 'many2many'):
            local_field_relation = local_fields[local_name]['relation']
            domain = [
                ('table_id.model_id.model', '=', local_field_relation),
                (search_field, 'in', eval(row[col_idx])),
            ]
            value = []
            record_ids = record_table.search(domain)
            for record_id in record_ids:
                value.append((4, record_id.local_id))
            result[local_name] = value

        return result

    @api.model
    def _update_many2many(self, local_table_id, local_fields, values):
        result = values

        for local_name in result:
            local_field_type = local_fields[local_name]['type']
            if (local_field_type.lower() == 'many2many'):
                changed_ids = []
                for remote_id in result[local_name]:
                    found = False
                    for local_id in local_table_id[local_name]:
                        if (local_id.id == remote_id[1]):
                            found = True
                    if (not found):
                        changed_ids.append((4, remote_id[1]))
                for local_id in local_table_id[local_name]:
                    found = False
                    for remote_id in result[local_name]:
                        if (remote_id[1] == local_id.id):
                            found = True
                    if (not found):
                        changed_ids.append((3, local_id.id))
                result[local_name] = changed_ids

        return result

    @api.model
    def _create_local_table(self, model, local_table, values):
        result = None
        if values:
            if model == 'res.partner':
                install_mode = values.get('image') # install_mode=True --> no default image
                result = local_table.with_context(install_mode=install_mode).create(values)
            else:
                result = local_table.create(values)
                if model == 'event.registration':
                    result._onchange_partner()
        return result

    @api.model
    def _write_values_to_local_table(self, model, local_table_id, local_fields, values):
        result = False

        if (values):
            values = self._update_many2many(local_table_id, local_fields, values)
            local_table_id.write(values)
            result = True
            if (model == 'event.registration'):
                local_table_id._onchange_partner()

        return result

    @api.multi
    def remote_to_local(self):
        for table_id in self:
            begin = time.strftime('%Y-%m-%d %H:%M:%S')
            query_result = ''
            if (table_id.remote_query):
                connection = table_id.database_id.connection_open()
                if (connection):
                    try:
                        msg = _('Remote Query started on %s') % (time.strftime('%Y-%m-%d %H:%M:%S'))
                        _logger.info(msg)
                        query_result += msg + '\n'
                        rows, cols = table_id.database_id.remote_execute(
                            connection=connection,
                            query=table_id.remote_query,
                            metadata=True,
                        )
                        msg = _('Remote Query executed on %s (%s rows)') % (time.strftime('%Y-%m-%d %H:%M:%S'), str(len(rows)))
                        _logger.info(msg)
                        query_result += msg + '\n'

                        row_idx = 0
                        local_table = self.env[table_id.model_id.model]
                        local_fields = local_table.fields_get()
                        record_table = self.env['sync_sqlany.record']
                        for row in rows:
                            self.env.cr.autocommit(False)
                            try:
                                col_idx = 0
                                values = {}
                                remote_name = None
                                remote_record_id = None
                                remote_search_id = None
                                search_domain = []
                                local_timezone = pytz.timezone(self.env.user.tz or pytz.utc)
                                for col in cols:
                                    if (col == '__remote_name__'):
                                        remote_name = row[col_idx]
                                    elif (col == '__remote_record_id__'):
                                        remote_record_id = row[col_idx]
                                    elif (col == '__remote_search_id__'):
                                        remote_search_id = row[col_idx]
                                    elif (col == '__search_domain__'):
                                        search_domain = eval(row[col_idx])
                                    elif (col[:len('__remote_record_id__')] == '__remote_record_id__'):
                                        local_name = col[len('__remote_record_id__'):]
                                        if (local_name in local_fields):
                                            values = self._get_local_id(
                                                'remote_record_id',
                                                record_table,
                                                local_fields,
                                                local_name,
                                                row,
                                                col_idx,
                                                values
                                            )
                                    elif (col[:len('__remote_search_id__')] == '__remote_search_id__'):
                                        local_name = col[len('__remote_search_id__'):]
                                        if (local_name in local_fields):
                                            values = self._get_local_id(
                                                'remote_search_id',
                                                record_table,
                                                local_fields,
                                                local_name,
                                                row,
                                                col_idx,
                                                values
                                            )
                                    elif (col == '__timezone__'):
                                        local_timezone = pytz.timezone(row[col_idx])
                                    elif (col in local_fields):
                                        value = row[col_idx]
                                        local_field_type = local_fields[col]['type']
                                        if (local_field_type.lower() == 'boolean'):
                                            value = eval(value)
                                        elif (local_field_type.lower() == 'datetime'):
                                            if (value):
                                                if (value.find('.')):
                                                    value = value.split('.')[0]
                                                value = datetime.strptime(value, DEFAULT_SERVER_DATETIME_FORMAT)
                                                value = local_timezone.localize(value).astimezone(pytz.utc)
                                        values[col] = value
                                    col_idx += 1
                                if (remote_search_id):
                                    local_table_id = None
                                    domain = [('table_id', '=', table_id.id), ('remote_search_id', '=', remote_search_id)]
                                    record_id = record_table.search(domain, limit=1)
                                    if (record_id):
                                        domain = [('id', '=', record_id.local_id)]
                                        local_table_id = local_table.with_context(active_test=False).search(domain, limit=1)
                                    if (local_table_id):
                                        self._write_values_to_local_table(
                                            table_id.model_id.model,
                                            local_table_id,
                                            local_fields,
                                            values,
                                        )
                                    else:
                                        if (search_domain):
                                            local_table_ids = local_table.with_context(active_test=False).search(search_domain)
                                            if (len(local_table_ids) == 1):
                                                local_table_id = local_table_ids[0]
                                                self._write_values_to_local_table(
                                                    table_id.model_id.model,
                                                    local_table_id,
                                                    local_fields,
                                                    values,
                                                )
                                        if not local_table_id:
                                            local_table_id = self._create_local_table(
                                                table_id.model_id.model,
                                                local_table,
                                                values,
                                            )
                                        if local_table_id:
                                            record_table.create({
                                                'name': remote_name or local_table_id.name,
                                                'table_id': table_id.id,
                                                'local_id': local_table_id.id,
                                                'remote_record_id': remote_record_id,
                                                'remote_search_id': remote_search_id,
                                            })
                                self.env.cr.commit()
                            except Exception as e:
                                self.env.cr.rollback()
                                msg = _('Remote -> Local failure on: %s\nRow: %s') % (tools.ustr(e), str(row))
                                _logger.exception(msg)
                                query_result += msg + '\n'
                                #pass

                            row_idx += 1
                            if (str(row_idx)[-2:] == '00'):
                                _logger.info(_('Remote -> Local: %s of %s done') % (str(row_idx), str(len(rows))))

                        self.env.cr.autocommit(True)
                        msg = _('Remote -> Local finished on %s') % (time.strftime('%Y-%m-%d %H:%M:%S'))
                        _logger.info(msg)
                        query_result += msg + '\n'
                    finally:
                        table_id.database_id.connection_close(connection)
                        table_id.end_timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
            table_id.write({
                'timestamp_begin': begin,
                'timestamp_end': time.strftime('%Y-%m-%d %H:%M:%S'),
                'query_result': query_result,
            })


class In4ToolsSyncSQLAnyRecord(models.Model):

    _name = 'sync_sqlany.record'
    _description = 'Record'
    _order = 'table_id, name'

    name = fields.Char(string='Name', store=True)
    active = fields.Boolean(string='Active', default=True, copy=False)

    table_id = fields.Many2one('sync_sqlany.table', string='Table', required=True, ondelete='cascade')
    display_name = fields.Char(compute='compute_display_name', store=True)

    local_id = fields.Integer(string='Local Record ID', readonly=True)
    remote_record_id = fields.Char(string='Remote Record ID', readonly=True)
    remote_search_id = fields.Char(string='Remote Search ID', readonly=True)

    @api.multi
    @api.depends('name', 'table_id')
    def compute_display_name(self):
        for record_id in self:
            record_id.display_name = '{0} - {1}'.format(record_id.table_id.display_name, record_id.name)


class In4ToolsSyncSQLAnyRequest(models.Model):

    _name = 'sync_sqlany.request'
    _description = 'Request'
    _order = 'table_id, name'

    name = fields.Char('Name', required=True)

    table_id = fields.Many2one('sync_sqlany.table', string='Table', required=True, ondelete='cascade')
    display_name = fields.Char(compute='_compute_display_name', store=True)

    body = fields.Text('Request', readonly=True)

    @api.multi
    @api.depends('name', 'table_id')
    def _compute_display_name(self):
        for request_id in self:
            request_id.display_name = '{0} - {1}'.format(request_id.table_id.display_name, request_id.name)
