# -*- coding: utf-8 -*-
{
    'name': 'SQLAny Sync',
    'summary': 'Synchronization with Sybase SQL Anywhere',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/sync_sqlany.xml',
    ],
}
