# -*- coding: utf-8 -*-
{
    'name': 'Project with Key (extended)',
    'summary': 'Extension of Projects and Tasks with Keys',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Project',
    'version': '0.1',

    'depends': [
        'project',
        'project_key',
    ],

    'data': [
        'views/project_task.xml',
    ],

    'auto_install': True
}
