# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM res_groups
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'res.groups'
                         AND module = 'hr_timesheet'
                         AND name IN ('group_hr_timesheet_user', 'group_timesheet_manager')
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'res.groups'
           AND module = 'hr_timesheet'
           AND name IN ('group_hr_timesheet_user', 'group_timesheet_manager')
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module IN ('hr_timesheet', 'sale_timesheet')
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module IN ('hr_timesheet', 'sale_timesheet')
               ;
    """)
