# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        UPDATE account_analytic_line
           SET duration = unit_amount
               ;
    """)

    cr.execute("""
            UPDATE account_analytic_line AS analytic_line
               SET price_unit = order_line.price_unit
              FROM project_task AS task
        INNER JOIN sale_order_line AS order_line ON order_line.id = task.sale_line_id
             WHERE task.id = analytic_line.task_id
               AND NOT order_line.price_unit IS NULL
                   ;
    """)

    cr.execute("""
            UPDATE account_analytic_line
               SET price_unit = account_invoice_line.price_unit
              FROM account_invoice_line
        INNER JOIN account_invoice ON account_invoice.id = account_invoice_line.invoice_id
             WHERE account_invoice_line.account_analytic_id = account_analytic_line.account_id
               AND account_invoice.id = account_analytic_line.timesheet_invoice_id
               AND NOT account_invoice_line.price_unit IS NULL
                   ;
    """)

    cr.execute("""
        UPDATE account_analytic_line
           SET price_amount = unit_amount * price_unit
               ;
    """)
