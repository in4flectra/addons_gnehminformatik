# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (extended): Sales Timesheet',
    'summary': 'Extension of Sales, Invoices and Payments: Sell based on timesheets',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Hidden',
    'version': '0.2',

    'depends': [
        'base',
        'hr_timesheet',
        'in4invoice',
        'project',
        'sale',
        'sale_timesheet',
    ],

    'data': [
        # security
        'security/in4invoice_sale_timesheet.xml',

        # views
        'views/account_analytic.xml',
        'views/account_invoice.xml',
        'views/product_template.xml',
        'views/project_project.xml',
        'views/project_task.xml',
        'views/res_config_settings.xml',
        'views/sale_order.xml',

        # wizards
        'views/sale_advance_payment_inv.xml',

        # reports
        'views/report_timesheet.xml',
    ],

    'auto_install': True,
    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
}
