# -*- coding: utf-8 -*-

from flectra import fields, models


class In4InvoiceSaleTimesheetProductTemplate(models.Model):

    _inherit = 'product.template'

    periodical_invoicing = fields.Boolean(string='Periodical Invoicing', default=False)
    interval_number = fields.Integer(string='Interval Qty', default=1)
    interval_type = fields.Selection(
        [('days', 'Days'), ('weeks', 'Weeks'), ('months', 'Months'), ('years', 'Years')],
        string='Interval Unit',
        default='months'
    )
