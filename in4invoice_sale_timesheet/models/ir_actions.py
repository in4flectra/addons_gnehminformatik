# -*- coding: utf-8 -*-

import base64
import logging
import time

from flectra import api, models
from flectra.exceptions import AccessError
from flectra.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)


class In4InvoiceSaleTimesheetIrActionsReport(models.Model):

    _inherit = 'ir.actions.report'

    @api.multi
    def postprocess_pdf_report(self, record, buffer):
        if self.xml_id == 'hr_timesheet.timesheet_report':
            if record.timesheet_invoice_id:
                attachment_name = safe_eval(self.attachment, {'object': record, 'time': time})
                if not attachment_name:
                    return None
                attachment_vals = {
                    'name': attachment_name,
                    'datas': base64.encodebytes(buffer.getvalue()),
                    'datas_fname': attachment_name,
                    'res_model': 'account.invoice',
                    'res_id': record.timesheet_invoice_id.id,
                }
                attachment = None
                try:
                    attachment = self.env['ir.attachment'].create(attachment_vals)
                except AccessError:
                    _logger.info("Cannot save PDF report %r as attachment", attachment_vals['name'])
                else:
                    _logger.info('The PDF document %s is now saved in the database', attachment_vals['name'])
                return attachment
            else:
                return None
        else:
            return super(In4InvoiceSaleTimesheetIrActionsReport, self).postprocess_pdf_report(record, buffer)
