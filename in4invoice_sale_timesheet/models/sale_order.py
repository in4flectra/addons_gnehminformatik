# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models
from flectra.osv import expression
from flectra.tools import float_is_zero, DEFAULT_SERVER_DATE_FORMAT


class In4InvoiceSaleTimesheetSaleOrder(models.Model):

    _inherit = 'sale.order'

    @api.multi
    def _compute_nextcall_visible(self):
        for order in self:
            visible = False
            for line in order.order_line:
                if (line.product_id.service_policy == 'delivered_manual') and line.product_id.periodical_invoicing:
                    visible = True
            order.nextcall_visible = visible

    task_project_id = fields.Many2one('project.project', string='Project for Tasks')
    invoice_date_to = fields.Date(string='Invoice To')
    nextcall_visible = fields.Boolean(string='Display Next Invoicing', compute=_compute_nextcall_visible, readonly=True, default=False)

    @api.model
    def update_periodical_invoicing(self):
        orders = self.search(['&', ('state', '=', 'sale'), ('nextcall_visible', '=', 'true')])
        for order in orders:
            for line in order.order_line:
                if line.nextcall_updateable:
                    nextcall = datetime.strptime(line.nextcall, DEFAULT_SERVER_DATE_FORMAT)
                    if nextcall <= datetime.today():
                        if line.product_id.interval_type == 'days':
                            nextcall = nextcall + relativedelta(days=line.product_id.interval_number)
                        elif line.product_id.interval_type == 'weeks':
                            nextcall = nextcall + relativedelta(weeks=line.product_id.interval_number)
                        elif line.product_id.interval_type == 'months':
                            nextcall = nextcall + relativedelta(months=line.product_id.interval_number)
                        elif line.product_id.interval_type == 'years':
                            nextcall = nextcall + relativedelta(years=line.product_id.interval_number)
                        line.write({
                            'qty_delivered': line.qty_delivered + line.product_uom_qty,
                            'nextcall': nextcall,
                        })
        return True

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        result = super(In4InvoiceSaleTimesheetSaleOrder, self).action_invoice_create(grouped, final)
        if self.invoice_date_to:
            for id in result:
                invoice = self.env['account.invoice'].browse(id)
                if invoice:
                    invoice.write({'invoice_date_to': self.invoice_date_to})
        return result


class In4InvoiceSaleTimesheetSaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    @api.multi
    @api.depends('product_id')
    def _compute_nextcall_updateable(self):
        for line in self:
            line.nextcall_updateable =  (line.product_id.service_policy == 'delivered_manual') and line.product_id.periodical_invoicing

    nextcall = fields.Date(string='Next Invoicing')
    nextcall_updateable = fields.Boolean(string='Can Edit Next Invoicing', compute=_compute_nextcall_updateable, readonly=True, default=False)

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        result = super(In4InvoiceSaleTimesheetSaleOrderLine, self).product_id_change()
        if (self.product_id.service_policy == 'delivered_manual') and self.product_id.periodical_invoicing:
            self.nextcall = datetime.today()
        else:
            self.nextcall = False
        return result

    def _timesheet_find_project(self):
        self.ensure_one()
        project_id = self.order_id.task_project_id
        if project_id:
            project_id.write({'sale_line_id': False})
            return project_id
        else:
            return super(In4InvoiceSaleTimesheetSaleOrderLine, self)._timesheet_find_project()

    def _analytic_compute_delivered_quantity_domain(self):
        domain = super(In4InvoiceSaleTimesheetSaleOrderLine, self)._analytic_compute_delivered_quantity_domain()
        invoice_date_to = None
        for line_id in self:
            if line_id.order_id.invoice_date_to and (not invoice_date_to):
                invoice_date_to = line_id.order_id.invoice_date_to
        if invoice_date_to:
            domain = expression.AND([domain, [('date', '<=', invoice_date_to)]])
        return domain

    @api.multi
    def _timesheet_compute_delivered_quantity_domain(self):
        domain = super(In4InvoiceSaleTimesheetSaleOrderLine, self)._timesheet_compute_delivered_quantity_domain()
        invoice_date_to = None
        for line_id in self:
            if line_id.order_id.invoice_date_to and (not invoice_date_to):
                invoice_date_to = line_id.order_id.invoice_date_to
        if invoice_date_to:
            domain = expression.AND([domain, [('date', '<=', invoice_date_to)]])
        return domain

    @api.multi
    def invoice_line_create(self, invoice_id, qty):
        invoice_lines = self.env['account.invoice.line']
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for line in self:
            if not float_is_zero(qty, precision_digits=precision):
                has_prices = False
                if self.env.user.has_group('in4invoice_sale_timesheet.group_timesheet_display_price_amount'):
                    self.env.cr.execute('''
                            SELECT   line.price_unit
                                   , SUM(line.unit_amount)
                              FROM account_analytic_line AS line
                        INNER JOIN project_task AS task on task.id = line.task_id
                             WHERE timesheet_invoice_id IS NULL
                               AND task.sale_line_id = %s
                               AND date <= %s
                          GROUP BY line.price_unit
                    ''', (line.id, line.order_id.invoice_date_to,))
                    analytic_lines = self.env.cr.fetchall()
                    if analytic_lines:
                        has_prices = True
                        for prices in analytic_lines:
                            vals = line._prepare_invoice_line(qty=prices[1])
                            vals.update({
                                'invoice_id': invoice_id,
                                'sale_line_ids': [(6, 0, [line.id])],
                                'price_unit': prices[0],
                            })
                            invoice_lines |= self.env['account.invoice.line'].create(vals)
                if not has_prices:
                    vals = line._prepare_invoice_line(qty=qty)
                    if vals:
                        vals.update({'invoice_id': invoice_id, 'sale_line_ids': [(6, 0, [line.id])]})
                        invoice_lines |= self.env['account.invoice.line'].create(vals)
        return invoice_lines
