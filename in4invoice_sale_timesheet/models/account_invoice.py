# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


class In4InvoiceSaleTimesheetAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.multi
    @api.depends('invoice_date_to', 'timesheet_ids', 'timesheet_ids.date')
    def _compute_invoice_date_from(self):
        for invoice in self:
            if invoice.invoice_date_to:
                invoice_date_from = datetime.strptime(invoice.invoice_date_to, DEFAULT_SERVER_DATE_FORMAT).date()
                invoice_date_from = invoice_date_from.replace(day=1)
                domain = [('id', 'in', invoice.timesheet_ids.ids)]
                lines = self.env['account.analytic.line'].search(domain)
                for line in lines:
                    line_date = datetime.strptime(line.date, DEFAULT_SERVER_DATE_FORMAT).date()
                    if line_date < invoice_date_from:
                        invoice_date_from = line_date
                invoice.invoice_date_from = invoice_date_from
            else:
                invoice.invoice_date_from = None

    invoice_date_from = fields.Date(
        string='Invoiced From',
        compute=_compute_invoice_date_from,
        store=True,
        readonly=True,
    )
    invoice_date_to = fields.Date(string='Invoiced To')
    timesheet_report_order = fields.Char(
        string='Timesheet Report Order',
        default='date asc',
        readonly=True,
        states={'draft': [('readonly', False)]}
    )

    def _get_compute_timesheet_revenue_domain(self, invoice_line):
        result = super(In4InvoiceSaleTimesheetAccountInvoice, self)._get_compute_timesheet_revenue_domain(invoice_line)
        if invoice_line and invoice_line.invoice_id and invoice_line.invoice_id.invoice_date_to:
            result.append(('date', '<=', invoice_line.invoice_id.invoice_date_to))
        return result

    @api.model
    def report_timesheet_ids(self):
        domain = [('id', 'in', self.timesheet_ids.ids)]
        order = self.timesheet_report_order
        return self.env['account.analytic.line'].search(domain, order=order)

    @api.multi
    def print_timesheet(self):
        self.ensure_one()
        self.sent = True
        return self.env.ref('hr_timesheet.timesheet_report').report_action(self.report_timesheet_ids())
