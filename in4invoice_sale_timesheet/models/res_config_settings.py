# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceSaleTimesheetResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    timesheet_number_of_factors = fields.Integer(string='Timesheet: Number of Factors', default=0)
    group_timesheet_display_unit_amount = fields.Boolean(string='Timesheet: Display Unit', default=False, implied_group='in4invoice_sale_timesheet.group_timesheet_display_unit_amount')
    group_timesheet_display_price_amount = fields.Boolean(string='Timesheet: Display Amount', default=False, implied_group='in4invoice_sale_timesheet.group_timesheet_display_price_amount')

    def set_values(self):
        super(In4InvoiceSaleTimesheetResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].set_param
        set_param('in4invoice_sale_timesheet.timesheet_number_of_factors', self.timesheet_number_of_factors)

    @api.model
    def get_values(self):
        result = super(In4InvoiceSaleTimesheetResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        result.update(
            timesheet_number_of_factors=int(get_param('in4invoice_sale_timesheet.timesheet_number_of_factors', default=0)),
        )
        return result
