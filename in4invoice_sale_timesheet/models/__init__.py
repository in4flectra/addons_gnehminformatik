# -*- coding: utf-8 -*-

from . import account_analytic
from . import account_invoice
from . import ir_actions
from . import mail_template
from . import product_template
from . import project_task
from . import res_config_settings
from . import sale_advance_payment_inv
from . import sale_order
