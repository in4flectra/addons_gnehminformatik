# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceSaleTimesheetProjectTask(models.Model):

    _inherit = 'project.task'

    @api.one
    def _compute_number_of_factors(self):
        get_param = self.env['ir.config_parameter'].sudo().get_param
        self.number_of_factors = int(get_param('in4invoice_sale_timesheet.timesheet_number_of_factors'))

    @api.model
    def _default_number_of_factors(self):
        get_param = self.env['ir.config_parameter'].sudo().get_param
        return int(get_param('in4invoice_sale_timesheet.timesheet_number_of_factors'))

    number_of_factors = fields.Integer(string='Number of Factors', compute=_compute_number_of_factors, default=_default_number_of_factors)

    @api.model
    def get_field_for_effective_hours(self):
        return 'duration'

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        super(In4InvoiceSaleTimesheetProjectTask, self)._onchange_partner_id()
        if (    self.sale_line_id
            and (not self.parent_id)
            and ((not self.partner_id) or (self.sale_line_id.order_id.partner_id != self.partner_id))
        ):
            self.sale_line_id = None

    @api.multi
    def write(self, values):
        result = super(In4InvoiceSaleTimesheetProjectTask, self).write(values)

        if ('sale_line_id' in values) and (self.sale_line_id):
            for child_task_id in self.child_ids:
                child_task_id.sale_line_id = self.sale_line_id

        return result
