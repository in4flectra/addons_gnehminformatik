# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceSaleTimesheetSaleAdvancePaymentInv(models.TransientModel):

    _inherit = 'sale.advance.payment.inv'

    invoice_date_to = fields.Date(string='Invoice To')

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        result = super(In4InvoiceSaleTimesheetSaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        result.write({'invoice_date_to': order.invoice_date_to})
        return result

    @api.multi
    def create_invoices(self):
        sale_order_ids = self.env['sale.order'].browse(self._context.get('active_ids', []))
        for sale_order_id in sale_order_ids:
            sale_order_id.invoice_date_to = self.invoice_date_to
            for line_id in sale_order_id.order_line:
                line_id.sudo()._analytic_compute_delivered_quantity()

        result = super(In4InvoiceSaleTimesheetSaleAdvancePaymentInv, self).create_invoices()

        for sale_order_id in sale_order_ids:
            sale_order_id.invoice_date_to = None
            for line_id in sale_order_id.order_line:
                line_id.sudo()._analytic_compute_delivered_quantity()

        return result
