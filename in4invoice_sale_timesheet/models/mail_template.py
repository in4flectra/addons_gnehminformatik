# -*- coding: utf-8 -*-

import base64

from flectra import api, models, _
from flectra.tools import pycompat


class In4InvoiceSaleTimesheetMailTemplate(models.Model):

    _inherit = 'mail.template'

    @api.multi
    def generate_email(self, res_ids, fields=None):
        rslt = super(In4InvoiceSaleTimesheetMailTemplate, self).generate_email(res_ids, fields)

        multi_mode = True
        if isinstance(res_ids, pycompat.integer_types):
            res_ids = [res_ids]
            multi_mode = False

        for res_id in res_ids:
            related_model = self.env[self.model_id.model].browse(res_id)

            if (related_model._name == 'account.invoice') and (len(related_model.timesheet_ids) > 0):
                report_name = _('Timesheet Entries - %s') % related_model.number
                domain = [
                    ('res_model_id', '=', self.model_id.id),
                    ('res_id', '=', res_id),
                    ('name', '=', report_name),
                    ('datas', '!=', False)
                ]
                attachment = self.env['ir.attachment'].sudo().search(domain, order='create_date desc', limit=1)
                if attachment:
                    pdf = attachment.datas
                else:
                    ids = related_model.report_timesheet_ids().ids
                    pdf = self.env.ref('hr_timesheet.timesheet_report').render_qweb_pdf(ids)[0]
                    pdf = base64.b64encode(pdf)

                if multi_mode and rslt[res_id].get('attachments'):
                    attachments_list = rslt[res_id]['attachments']
                elif rslt.get('attachments'):
                    attachments_list = rslt['attachments']
                else:
                    attachments_list = []
                attachments_list.append((report_name, pdf))
                if multi_mode:
                    if not rslt[res_id].get('attachments'):
                        rslt[res_id]['attachments'] = attachments_list
                else:
                    if not rslt.get('attachments'):
                        rslt['attachments'] = attachments_list
        return rslt
