# -*- coding: utf-8 -*-

from flectra import api, fields, models, _

from addons import decimal_precision as dp


class In4InvoiceSaleTimesheetAccountAnalyticLine(models.Model):

    _inherit = 'account.analytic.line'
    _description = 'Time Report'

    @api.one
    def _compute_number_of_factors(self):
        get_param = self.env['ir.config_parameter'].sudo().get_param
        self.number_of_factors = int(get_param('in4invoice_sale_timesheet.timesheet_number_of_factors'))

    @api.model
    def _default_number_of_factors(self):
        get_param = self.env['ir.config_parameter'].sudo().get_param
        return int(get_param('in4invoice_sale_timesheet.timesheet_number_of_factors'))

    def get_unit_amount(self, duration, factor01, factor02, factor03, factor04, factor05):
        return duration * (factor01 / 100) * (factor02 / 100) * (factor03 / 100) * (factor04 / 100) * (factor05 / 100)

    @api.multi
    @api.depends('duration', 'factor01', 'factor02', 'factor03', 'factor04', 'factor05')
    def _compute_unit_amount(self):
        for line in self:
            line.unit_amount = line.get_unit_amount(line.duration, line.factor01, line.factor02, line.factor03, line.factor04, line.factor05)

    @api.model
    def _default_price_unit(self):
        result = 0.0
        if self._context.get('default_sale_line_id'):
            result = self.env['sale.order.line'].browse(self._context['default_sale_line_id']).price_unit
        return result

    @api.multi
    @api.depends('unit_amount', 'price_unit')
    def _compute_price_amount(self):
        for line in self:
            line.price_amount = line.unit_amount * line.price_unit

    duration = fields.Float(string='Duration', default=0.0)
    factor01 = fields.Float(string='Factor 1 (%)', default=100.00)
    factor02 = fields.Float(string='Factor 2 (%)', default=100.00)
    factor03 = fields.Float(string='Factor 3 (%)', default=100.00)
    factor04 = fields.Float(string='Factor 4 (%)', default=100.00)
    factor05 = fields.Float(string='Factor 5 (%)', default=100.00)
    number_of_factors = fields.Integer(string='Number of Factors', compute=_compute_number_of_factors, default=_default_number_of_factors)
    unit_amount = fields.Float(compute=_compute_unit_amount, store=True)
    price_unit = fields.Float(string='Price', default=_default_price_unit, digits=dp.get_precision('Product Price'), required=True)
    price_amount = fields.Monetary(string='Amount', compute=_compute_price_amount, store=True)

    @api.onchange('task_id')
    def _onchange_task_id(self):
        price_unit = 0.0
        if self.task_id and self.task_id.sale_line_id and self.task_id.sale_line_id.price_unit:
            price_unit = self.task_id.sale_line_id.price_unit
        self.price_unit = price_unit

    @api.model
    def create(self, values):
        if (   ('duration' in values)
            or ('factor01' in values)
            or ('factor02' in values)
            or ('factor03' in values)
            or ('factor04' in values)
            or ('factor05' in values)
        ):
            duration = values['duration'] if 'duration' in values else 0.0
            factor01 = values['factor01'] if 'factor01' in values else 100.00
            factor02 = values['factor02'] if 'factor02' in values else 100.00
            factor03 = values['factor03'] if 'factor03' in values else 100.00
            factor04 = values['factor04'] if 'factor04' in values else 100.00
            factor05 = values['factor05'] if 'factor05' in values else 100.00
            values['unit_amount'] = self.get_unit_amount(duration, factor01, factor02, factor03, factor04, factor05)
        return super(In4InvoiceSaleTimesheetAccountAnalyticLine, self).create(values)

    @api.multi
    def write(self, values):
        if (   ('duration' in values)
            or ('factor01' in values)
            or ('factor02' in values)
            or ('factor03' in values)
            or ('factor04' in values)
            or ('factor05' in values)
        ):
            duration = values['duration'] if 'duration' in values else self.duration
            factor01 = values['factor01'] if 'factor01' in values else self.factor01
            factor02 = values['factor02'] if 'factor02' in values else self.factor02
            factor03 = values['factor03'] if 'factor03' in values else self.factor03
            factor04 = values['factor04'] if 'factor04' in values else self.factor04
            factor05 = values['factor05'] if 'factor05' in values else self.factor05
            values['unit_amount'] = self.get_unit_amount(duration, factor01, factor02, factor03, factor04, factor05)
        return super(In4InvoiceSaleTimesheetAccountAnalyticLine, self).write(values)

    @api.model
    def _sale_get_fields_delivered_qty(self):
        result = super(In4InvoiceSaleTimesheetAccountAnalyticLine, self)._sale_get_fields_delivered_qty()
        result.extend(['duration', 'factor01', 'factor02', 'factor03', 'factor04', 'factor05'])
        return result

    @api.multi
    def _get_printed_timesheet_report_name(self):
        self.ensure_one()
        if self.timesheet_invoice_id:
            if ((self.timesheet_invoice_id.type == 'out_invoice') and
                    (self.timesheet_invoice_id.state in ('open', 'paid'))):
                result = _('Timesheet Entries - %s') % self.timesheet_invoice_id.number
            else:
                result = _('Draft Timesheet Entries')
        else:
            result = _('Timesheet Entries')
        return result
