# -*- coding: utf-8 -*-
{
    'name': 'Opportunity to Quotation (extended)',
    'summary': 'Extension of Opportunity to Quotation',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'crm',
        'sale',
        'sale_crm',
    ],

    'data': [
        'views/crm_lead.xml',
        'views/sale_order.xml',
    ],

    'auto_install': True,
}
