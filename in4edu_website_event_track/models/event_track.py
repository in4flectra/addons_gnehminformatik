# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4EduEventEventTrack(models.Model):

    _inherit = 'event.track'

    track_id = fields.Many2one('event.track', string='Track', copy=False, ondelete='cascade')
    track_ids = fields.One2many('event.track', 'track_id', string='Tracks', copy=False)

    @api.model
    def copy_to_course(self):
        if ((self.event_id) and (self.event_id.event_kind == 'module')):
            for track_course_id in self.track_ids:
                track_course_id.unlink()
            for event_id in self.event_id.event_ids:
                track_name = self.event_id.name
                if (self.event_id.name != self.name):
                    track_name += ': ' + self.name
                self.copy(default={
                    'name': track_name,
                    'event_id': event_id.id,
                    'track_id': self.id,
                    'website_published': self.website_published
                })

    @api.model
    def create(self, vals):
        track_id = super(In4EduEventEventTrack, self).create(vals)

        if ('track_id' not in vals):
            track_id.copy_to_course()

        return track_id

    @api.multi
    def write(self, vals):

        res = super(In4EduEventEventTrack, self).write(vals)

        if ('track_id' not in vals):
            for track_id in self:
                track_id.copy_to_course()

        return res