# -*- coding: utf-8 -*-
{
    'name': 'Advanced Events for Education',
    'summary': 'Extension of Sponsors, Tracks, Agenda, Event News for Education',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Education',
    'version': '0.1',

    'depends': [
        'in4edu_event',
        'website_event_track',
    ],

    'data': [
        'views/event_track.xml',
    ],

    'auto_install': True
}
