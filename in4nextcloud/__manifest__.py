# -*- coding: utf-8 -*-
{
    'name': 'Sync with Nextcloud (Base)',
    'summary': 'Base for the Synchronization from Flectra with Nextcloud',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/nextcloud.xml',
        'views/res_partner.xml',
    ],

}
