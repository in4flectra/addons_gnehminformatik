# -*- coding: utf-8 -*-

import pytz

from datetime import datetime

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class In4CalendarCalendarEvent(models.Model):

    _inherit = 'calendar.event'

    @api.multi
    @api.depends('allday', 'start_date', 'start_datetime')
    def _compute_start_display(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        lang_code = self._context.get('lang') or 'en_US'
        lang_id = self.env['res.lang']._lang_get(lang_code)
        for meeting in self:
            if meeting.allday:
                start_date = datetime.strptime(meeting.start_date, DEFAULT_SERVER_DATE_FORMAT)
                display_format = lang_id.date_format
            else:
                start_date = datetime.strptime(meeting.start_datetime, DEFAULT_SERVER_DATETIME_FORMAT)
                display_format = lang_id.date_format + ' ' + lang_id.time_format
            start_date = pytz.UTC.localize(start_date).astimezone(pytz.timezone(tz))
            meeting.start_display = datetime.strftime(start_date, display_format)

    @api.multi
    @api.depends('allday', 'stop_date', 'stop_datetime')
    def _compute_stop_display(self):
        tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
        lang_code = self._context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        for meeting in self:
            if meeting.allday:
                stop_date = datetime.strptime(meeting.stop_date, DEFAULT_SERVER_DATE_FORMAT)
                display_format = lang_id.date_format
            else:
                stop_date = datetime.strptime(meeting.stop_datetime, DEFAULT_SERVER_DATETIME_FORMAT)
                display_format = lang_id.date_format + ' ' + lang_id.time_format
            stop_date = pytz.UTC.localize(stop_date).astimezone(pytz.timezone(tz))
            meeting.stop_display = datetime.strftime(stop_date, display_format)

    start_display = fields.Char('Start Date for Display', compute=_compute_start_display)
    stop_display = fields.Char('End Date for Display', compute=_compute_stop_display)

    @api.onchange('stop_datetime')
    def _onchange_stop_datetime(self):
        if self.stop_datetime:
            start = fields.Datetime.from_string(self.start_datetime)
            stop = fields.Datetime.from_string(self.stop_datetime)
            self.stop = self.stop_datetime
            self.duration = (stop - start).total_seconds() / 3600
