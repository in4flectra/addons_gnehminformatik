# -*- coding: utf-8 -*-
{
    'name': 'Calendar (extended)',
    'summary': 'Extension of Personal & Shared Calendar',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Extra Tools',
    'version': '0.1',

    'depends': [
        'calendar',
    ],

    'data': [
        'views/calendar_event.xml',
    ],
}
