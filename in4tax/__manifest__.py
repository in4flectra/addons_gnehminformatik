# -*- coding: utf-8 -*-
{
    'name': 'Tax (extended)',
    'summary': 'Extension of Tax',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.2',

    'depends': [
        'account',
        'base',
        'sale',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/account_tax.xml',
        'views/account_invoice.xml',
        'views/product_pricelist.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',

        # reports
        'views/report_invoice.xml',
        'views/report_saleorder.xml',
    ],

}
