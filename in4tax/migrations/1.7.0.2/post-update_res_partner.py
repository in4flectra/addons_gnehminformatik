# -*- coding: utf-8 -*-

from flectra import api, SUPERUSER_ID


def migrate(cr, version):
    env = api.Environment(cr, SUPERUSER_ID, {})
    domain = [('parent_id', '!=', False), ('sale_show_tax', '!=', 'parent_id.sale_show_tax')]
    contacts = env['res.partner'].search(domain)
    for contact in contacts:
        contact.sale_show_tax = contact.parent_id.sale_show_tax
