# -*- coding: utf-8 -*-

from flectra import fields, models


class In4TaxAccountTaxTemplate(models.Model):

    _inherit = 'account.tax.template'

    description_extended = fields.Char(string='Extended Label on Invoices', translate=True)

    def _get_tax_vals(self, company, tax_template_to_tax):
        self.ensure_one()
        result = super(In4TaxAccountTaxTemplate, self)._get_tax_vals(company, tax_template_to_tax)
        result['description_extended'] = self.description_extended
        return result


class In4TaxAccountTax(models.Model):

    _inherit = 'account.tax'

    description_extended = fields.Char(string='Extended Label on Invoices', translate=True)
