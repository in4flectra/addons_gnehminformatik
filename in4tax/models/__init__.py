# -*- coding: utf-8 -*-

from . import account_invoice
from . import account_tax
from . import product_pricelist
from . import res_partner
from . import sale_order
