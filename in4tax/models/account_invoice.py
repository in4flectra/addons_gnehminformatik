# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4TaxAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.model
    def _default_sale_show_tax(self):
        return self.env['ir.config_parameter'].sudo().get_param('sale.sale_show_tax', default='subtotal')

    sale_show_tax = fields.Selection(
        [('subtotal', 'Tax-Excluded Prices'), ('total', 'Tax-Included Prices')],
        string='Tax Display',
        default=_default_sale_show_tax,
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
    )

    def _is_tax_excluded(self):
        return self.sale_show_tax == 'subtotal'

    @api.onchange('sale_show_tax')
    def _onchange_sale_show_tax(self):
        for invoice_line_id in self.invoice_line_ids:
            invoice_line_id._compute_price()

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        result = super(In4TaxAccountInvoice, self)._onchange_partner_id()
        if self.partner_id:
            self.sale_show_tax = self.partner_id.sale_show_tax
        return result


class In4TaxAccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.multi
    def _compute_tax_id(self):
        result = super(In4TaxAccountInvoiceLine, self)._compute_tax_id()
        for line in self:
            if line.invoice_id.pricelist_id:
                for tax_replacement_id in line.invoice_id.pricelist_id.tax_replacement_ids:
                    if tax_replacement_id.source_tax_id in line.invoice_line_tax_ids:
                        line.invoice_line_tax_ids -= tax_replacement_id.source_tax_id
                        if tax_replacement_id.target_tax_id:
                            line.invoice_line_tax_ids += tax_replacement_id.target_tax_id
        return result
