# -*- coding: utf-8 -*-

from flectra import api, fields, models, _


class In4TaxProductPricelist(models.Model):

    _inherit = 'product.pricelist'

    tax_replacement_ids = fields.One2many('product.pricelist.tax_replacement', 'pricelist_id', 'Tax Replacements', copy=True)


class In4TaxProductPricelistTaxReplacement(models.Model):

    _name = 'product.pricelist.tax_replacement'
    _description = 'Pricelist Tax Replacement'
    _order = 'name'

    name = fields.Char(string='Tax Name', compute='_compute_tax_replacement_name', store=True, readonly=True)
    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, ondelete='cascade')
    source_tax_id = fields.Many2one('account.tax', string='Source Tax', required=True)
    target_tax_id = fields.Many2one('account.tax', string='Target Tax')

    _sql_constraints = [
        ('source_tax_unique', 'unique (pricelist_id, source_tax_id)', 'This Source Tax already exists !'),
    ]

    @api.depends('source_tax_id', 'target_tax_id')
    def _compute_tax_replacement_name(self):
        for tax_replacement_id in self:
            name = tax_replacement_id.source_tax_id.name + _(' --> ')
            if tax_replacement_id.target_tax_id:
                name += tax_replacement_id.target_tax_id.name
            else:
                name += _('Nothing')
            tax_replacement_id.name = name
