# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4TaxResPartner(models.Model):

    _inherit = 'res.partner'

    @api.model
    def _default_sale_show_tax(self):
        return self.env['ir.config_parameter'].sudo().get_param('sale.sale_show_tax', default='subtotal')

    sale_show_tax = fields.Selection(
        [('subtotal', 'Tax-Excluded Prices'), ('total', 'Tax-Included Prices')],
        string='Tax Display',
        default=_default_sale_show_tax,
        required=True,
    )

    @api.multi
    def write(self, values):
        result = super(In4TaxResPartner, self).write(values)
        if 'sale_show_tax' in values:
            for child in self.child_ids:
                if child.sale_show_tax != self.sale_show_tax:
                    child.sale_show_tax = self.sale_show_tax
        return result
