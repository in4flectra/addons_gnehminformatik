# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4TaxSaleOrder(models.Model):

    _inherit = 'sale.order'

    @api.model
    def _default_sale_show_tax(self):
        return self.env['ir.config_parameter'].sudo().get_param('sale.sale_show_tax', default='subtotal')

    sale_show_tax = fields.Selection(
        [('subtotal', 'Tax-Excluded Prices'), ('total', 'Tax-Included Prices')],
        string='Tax Display',
        default=_default_sale_show_tax,
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
    )

    def _is_tax_excluded(self):
        return self.sale_show_tax == 'subtotal'

    @api.onchange('sale_show_tax')
    def _onchange_sale_show_tax(self):
        for order_line_id in self.order_line:
            order_line_id._compute_amount()

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        result = super(In4TaxSaleOrder, self).onchange_partner_id()
        if self.partner_id and (self.sale_show_tax != self.partner_id.sale_show_tax):
            self.sale_show_tax = self.partner_id.sale_show_tax
        return result

    @api.multi
    def _prepare_invoice(self):
        invoice_vals = super(In4TaxSaleOrder, self)._prepare_invoice()
        invoice_vals['sale_show_tax'] = 'subtotal'
        if self.partner_id and (invoice_vals['sale_show_tax'] != self.partner_id.sale_show_tax):
            invoice_vals['sale_show_tax'] = self.partner_id.sale_show_tax
        return invoice_vals


class In4TaxSaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    @api.multi
    def _compute_tax_id(self):
        result = super(In4TaxSaleOrderLine, self)._compute_tax_id()
        for line in self:
            if line.order_id.pricelist_id:
                for tax_replacement_id in line.order_id.pricelist_id.tax_replacement_ids:
                    if tax_replacement_id.source_tax_id in line.tax_id:
                        line.tax_id -= tax_replacement_id.source_tax_id
                        if tax_replacement_id.target_tax_id:
                            line.tax_id += tax_replacement_id.target_tax_id
        return result
