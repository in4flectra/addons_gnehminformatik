# -*- coding: utf-8 -*-

from flectra import fields, models


class In4SwissHRContractHRContract(models.Model):

    _inherit = 'hr.contract'

    occupational_provision = fields.Monetary(string='Occupational Provision', digits=(16, 2))
    special_payments_01 = fields.Monetary(string='Special Payments 1', digits=(16, 2))
    special_payments_02 = fields.Monetary(string='Special Payments 2', digits=(16, 2))
