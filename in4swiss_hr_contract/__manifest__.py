# -*- coding: utf-8 -*-
{
    'name': 'Employee Contracts (Swiss Localization)',
    'summary': 'Swiss Localization of Employee Contracts',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'hr_contract',
        'in4swiss',
    ],

    'data': [
        'views/hr_contract.xml',
    ],

    'auto_install': True
}
