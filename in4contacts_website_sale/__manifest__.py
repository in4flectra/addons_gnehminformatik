# -*- coding: utf-8 -*-
{
    'name': 'eCommerce: First and Last Name',
    'summary': 'First and Last Name for eCommerce',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Website',
    'version': '0.1',

    'depends': [
        'in4contacts',
        'in4website_sale',
        'website_sale',
    ],

    'data': [
        'views/templates.xml',
    ],

    'auto_install': True,
}
