# -*- coding: utf-8 -*-

from flectra.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSale(WebsiteSale):

    def _get_mandatory_billing_fields(self):
        return ['firstname', 'lastname', 'email', 'street', 'zip', 'city', 'country_id']

    def _get_mandatory_shipping_fields(self):
        return ['firstname', 'lastname', 'street', 'zip', 'city', 'country_id']
