# -*- coding: utf-8 -*-
{
    'name': 'Employee Contracts (extended)',
    'summary': 'Extension of Employee Contracts',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.1',

    'depends': [
        'hr',
        'hr_contract',
    ],

    'data': [
        'views/hr_contract.xml',
        'views/hr_employee.xml',
    ],
}
