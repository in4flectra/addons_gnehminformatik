# -*- coding: utf-8 -*-

from flectra import api, models


class In4WebsiteWebsite(models.Model):

    _inherit = 'website'

    @api.model
    def action_dashboard_redirect(self):
        return self.env.ref('website.backend_dashboard').read()[0]
