# -*- coding: utf-8 -*-
{
    'name': 'Website (extended)',
    'summary': 'Extension of Website',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Website',
    'version': '0.1',

    'depends': [
        'website',
    ],

    'data': [
        # security
        'security/in4website.xml',

        # views
        'views/website.xml',
    ],

    'auto_install': True,
}
