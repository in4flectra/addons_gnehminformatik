# -*- coding: utf-8 -*-
{
    'name': 'Sync Contacts with Nextcloud',
    'summary': 'Synchronize the Contacts from Flectra with Contacts from Nextcloud',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'contacts',
        'in4contacts',
        'in4nextcloud',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/contacts.xml',
        'views/res_partner.xml',
    ],

}
