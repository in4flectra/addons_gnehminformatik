# -*- coding: utf-8 -*-

import base64
import uuid
import vobject
import webdav3.client

from flectra import api, fields, models, _


class In4NextcloudContactsResPartnerCategory(models.Model):

    _inherit = 'res.partner.category'

    nextcloud_group = fields.Char(string='Nextcloud Group')


class In4NextcloudContactsResPartner(models.Model):

    _inherit = 'res.partner'

    nextcloud_contacts_settings_id = fields.Many2one('in4nextcloud_contacts.settings', string='Nextcloud Contacts: Settings', copy=False)
    nextcloud_contacts_resource_href = fields.Char(string='Nextcloud Contacts: Resource Name', copy=False)
    nextcloud_contacts_timestamp = fields.Datetime(string='Nextcloud Contacts: Last Download', copy=False)

    @api.model
    def _get_nextcloud_address_and_pobox(self):
        extended_address = ''
        pobox = ''
        if self.street2:
            extended_address = self.street2
            if self.nextcloud_contacts_settings_id and self.nextcloud_contacts_settings_id.po_box_list:
                street2_lower = self.street2.lower()
                po_box_lines = self.nextcloud_contacts_settings_id.po_box_list.split('\n')
                for po_box_line in po_box_lines:
                    if po_box_line.lower() in street2_lower:
                        pobox = self.street2
                        extended_address = ''
                        break
        return extended_address, pobox

    @api.model
    def _update_nextcloud_names(self, vcard, lastname, firstname, values=None):
        if (   (not values)
            or ('name' in values)
            or ('lastname' in values)
            or ('firstname' in values)
            or ('parent_id' in values)
        ):
            result = ''
            if self.display_name:
                if not vcard.contents.get('fn'):
                    vcard.add('fn')
                vcard.fn.value = self.display_name
                result += 'fn,'
            if lastname or firstname:
                if not vcard.contents.get('n'):
                    vcard.add('n')
                vcard.n.value.family = lastname or ''
                vcard.n.value.given = firstname or ''
                result += 'n,'
            return result
        return ''

    @api.model
    def _update_nextcloud_orgs(self, vcard, organization, function, values=None):
        if (not values) or ('name' in values) or ('function' in values) or ('parent_id' in values):
            result = ''
            if self.display_name:
                if not vcard.contents.get('fn'):
                    vcard.add('fn')
                vcard.fn.value = self.display_name
                result += 'fn,'
            if organization:
                if not vcard.contents.get('org'):
                    vcard.add('org')
                vcard.org.value = [organization]
                result += 'org,'
            elif vcard.contents.get('org'):
                vcard.org.value = []
                result += 'org,'
            if function:
                if not vcard.contents.get('title'):
                    vcard.add('title')
                vcard.title.value = function
                result += 'title,'
            elif vcard.contents.get('title'):
                vcard.title.value = ''
                result += 'title,'
            return result
        return ''

    @api.model
    def _update_nextcloud_addresses(self, vcard, type, type_to_remove, values=None):
        result = ''
        if (   (not values)
            or ('street' in values)
            or ('street2' in values)
            or ('zip' in values)
            or ('city' in values)
            or ('state_id' in values)
            or ('country_id' in values)
        ):
            address_new = None
            extended_address, pobox = self._get_nextcloud_address_and_pobox()
            if self.street or extended_address or pobox or self.zip or self.city:
                address_new = vobject.vcard.Address(
                    street=self.street or '',
                    extended=extended_address,
                    box=pobox,
                    code=self.zip or '',
                    city=self.city or '',
                    region=self.state_id.name or '',
                    country=self.country_id.name or '',
                )
            address_found = False
            addresses_old = vcard.contents.get('adr', [])
            for address_old in addresses_old:
                address_type = address_old.params['TYPE'][0]
                if address_type and (address_type == type) and (not address_found):
                    address_found = True
                    if address_new:
                        address_old.value = address_new
                    else:
                        addresses_old.remove(address_old)
            if address_new and (not address_found):
                address = vcard.add('adr')
                address.type_param = type
                address.value = address_new
            result = 'adr,'
        if type_to_remove:
            addresses = vcard.contents.get('adr', [])
            for address in addresses:
                address_type = address.params['TYPE'][0]
                if address_type and (address_type == type_to_remove):
                    addresses.remove(address)
                    result = 'adr,'
        return result

    @api.model
    def _update_nextcloud_phones(self, vcard, type, type_to_remove, values=None):
        result = ''
        if (not values) or ('phone' in values) or ('mobile' in values):
            phone_voice_found = False
            phone_cell_found = False
            phones_old = vcard.contents.get('tel', [])
            for phone_old in phones_old:
                phone_type = phone_old.params['TYPE'][0]
                if phone_type and (phone_type == type + ',VOICE') and (not phone_voice_found):
                    phone_voice_found = True
                    if self.phone:
                        phone_old.value = self.phone
                    else:
                        phones_old.remove(phone_old)
                if phone_type and (phone_type == type + ',CELL') and (not phone_cell_found):
                    phone_cell_found = True
                    if self.mobile:
                        phone_old.value = self.mobile
                    else:
                        phones_old.remove(phone_old)
            if (not phone_voice_found) and self.phone:
                phone = vcard.add('tel')
                phone.type_param = type + ',VOICE'
                phone.value = self.phone
            if (not phone_cell_found) and self.mobile:
                phone = vcard.add('tel')
                phone.type_param = type + ',CELL'
                phone.value = self.mobile
            result = 'tel,'
        if type_to_remove:
            phones = vcard.contents.get('tel', [])
            for phone in phones:
                phone_type = phone.params['TYPE'][0]
                if phone_type and ((phone_type in [type_to_remove + ',VOICE', type_to_remove + ',CELL'])):
                    phones.remove(phone)
                    result = 'tel,'
        return result

    @api.model
    def _update_nextcloud_emails(self, vcard, type, type_to_remove, values=None):
        result = ''
        if (not values) or ('email' in values):
            email_found = False
            emails_old = vcard.contents.get('email', [])
            for email_old in emails_old:
                email_type = email_old.params['TYPE'][0]
                if email_type and (email_type == type) and (not email_found):
                    email_found = True
                    if self.email:
                        email_old.value = self.email
                    else:
                        emails_old.remove(email_old)
            if (not email_found) and self.email:
                email = vcard.add('email')
                email.type_param = type
                email.value = self.email
            result = 'email,'
        if type_to_remove:
            emails = vcard.contents.get('email', [])
            for email in emails:
                email_type = email.params['TYPE'][0]
                if email_type and (email_type == type_to_remove):
                    emails.remove(email)
                    result = 'email,'
        return result

    @api.model
    def _update_nextcloud_websites(self, vcard, values=None):
        if (not values) or ('website' in values):
            website_found = False
            websites_old = vcard.contents.get('url', [])
            for website_old in websites_old:
                if not website_found:
                    website_found = True
                    if self.website:
                        website_old.value = self.website
                    else:
                        websites_old.remove(website_old)
            if (not website_found) and self.website:
                website = vcard.add('url')
                website.value = self.website
            return 'url,'
        return ''

    @api.model
    def _update_nextcloud_birthdays(self, vcard, values=None):
        if (not values) or ('date_of_birth' in values):
            birthday_found = False
            birthdays_old = vcard.contents.get('bday', [])
            for birthday_old in birthdays_old:
                if not birthday_found:
                    birthday_found = True
                    if self.date_of_birth:
                        birthday_old.value = self.date_of_birth
                    else:
                        birthdays_old.remove(birthday_old)
            if (not birthday_found) and self.date_of_birth:
                birthday = vcard.add('bday')
                birthday.value = self.date_of_birth
            return 'bday,'
        return ''

    @api.model
    def _update_nextcloud_comments(self, vcard, values=None):
        if (not values) or ('comment' in values):
            comment_found = False
            comments_old = vcard.contents.get('note', [])
            for comment_old in comments_old:
                if not comment_found:
                    comment_found = True
                    if self.comment:
                        comment_old.value = self.comment
                    else:
                        comments_old.remove(comment_old)
            if (not comment_found) and self.comment:
                comment = vcard.add('note')
                comment.value = self.comment
            return 'note,'
        return ''

    @api.model
    def _update_nextcloud_images(self, vcard, values=None):
        if (not values) or ('image' in values):
            image_found = False
            images_old = vcard.contents.get('photo', [])
            for image_old in images_old:
                if not image_found:
                    image_found = True
                    if self.image:
                        image_old.params['ENCODING'] = ['b']
                        image_old.params['TYPE'] = ['JPEG']
                        image_old.value = base64.b64decode(self.image_medium)
                    else:
                        images_old.remove(image_old)
            if (not image_found) and self.image:
                image = vcard.add('photo')
                image.params['ENCODING'] = ['b']
                image.params['TYPE'] = ['JPEG']
                image.value = base64.b64decode(self.image_medium)
            return 'photo,'
        return ''

    @api.model
    def _update_nextcloud_groups(self, vcard, flectra_group, values=None):
        if (not values) or ('category_id' in values):
            groups_new = []
            groups_old = vcard.contents.get('categories', [])
            for group_old in groups_old:
                groups_new.extend(group_old.value)
                for value in group_old.value:
                    domain = [('nextcloud_group', '=', value)]
                    category_id = self.env['res.partner.category'].search(domain, limit=1)
                    if category_id and (category_id not in self.category_id):
                        groups_new.remove(value)
            for category_id in self.category_id:
                if category_id.nextcloud_group and (category_id.nextcloud_group not in groups_new):
                    groups_new.append(category_id.nextcloud_group)
            if flectra_group and (flectra_group not in groups_new):
                groups_new.append(flectra_group)
            if not vcard.contents.get('categories', []):
                vcard.add('categories')
            vcard.categories.value = groups_new
            return 'categories,'
        return ''

    @api.model
    def create(self, values):
        result = super(In4NextcloudContactsResPartner, self).create(values)

        contacts_settings = self.env['in4nextcloud_contacts.settings'].search([], limit=1)
        if contacts_settings and (contacts_settings.upload_selection == 'auto_upload'):
            result.upload_nextcloud_contact_ok(contacts_settings)
        return result

    @api.multi
    def write(self, values):
        result = super(In4NextcloudContactsResPartner, self).write(values)

        try:
            if (    (self.nextcloud_contacts_settings_id)
                and (self.nextcloud_contacts_settings_id.active)
                and (self.nextcloud_contacts_settings_id.upload_selection in ['auto_write', 'auto_upload'])
                and (self.nextcloud_contacts_resource_href)
                and (not 'nextcloud_contacts_timestamp' in values)
                and (  ('name' in values)
                    or ('street' in values)
                    or ('street2' in values)
                    or ('zip' in values)
                    or ('city' in values)
                    or ('state_id' in values)
                    or ('country_id' in values)
                    or ('function' in values)
                    or ('image' in values)
                    or ('phone' in values)
                    or ('mobile' in values)
                    or ('email' in values)
                    or ('website' in values)
                    or ('date_of_birth' in values)
                    or ('comment' in values)
                    or ('category_id' in values)
                    or ('parent_id' in values)
                    or ('child_ids' in values)
                )
            ):
                contacts_settings = self.nextcloud_contacts_settings_id
                client = webdav3.client.Client(contacts_settings._get_options()[0])
                if client:
                    client.verify = False
                    vcard = vobject.readOne(client.execute_request('download', self.nextcloud_contacts_resource_href).text)

                    # Company
                    if self.is_company:
                        update_fields = ''
                        update_fields += self._update_nextcloud_orgs(vcard, self.name, '', values)
                        update_fields += self._update_nextcloud_addresses(vcard, 'WORK', None, values)
                        update_fields += self._update_nextcloud_phones(vcard, 'WORK', None, values)
                        update_fields += self._update_nextcloud_emails(vcard, 'WORK', None, values)
                        update_fields += self._update_nextcloud_websites(vcard, values)
                        update_fields += self._update_nextcloud_comments(vcard, values)
                        update_fields += self._update_nextcloud_images(vcard, values)
                        update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group, values)
                        if update_fields:
                            client.execute_request(
                                'upload',
                                self.nextcloud_contacts_resource_href,
                                data=vcard.serialize(),
                            )

                        for child_id in self.child_ids:
                            if child_id.nextcloud_contacts_resource_href:
                                vcard = vobject.readOne(
                                    client.execute_request('download', child_id.nextcloud_contacts_resource_href).text
                                )
                                update_fields = ''
                                update_fields += child_id._update_nextcloud_orgs(vcard, self.name, child_id.function, values)
                                update_fields += child_id._update_nextcloud_addresses(vcard, 'WORK', 'HOME', values)
                                if update_fields:
                                    client.execute_request(
                                        'upload',
                                        child_id.nextcloud_contacts_resource_href,
                                        data=vcard.serialize(),
                                    )

                    # Person
                    else:
                        if self.parent_id:
                            update_fields = ''
                            update_fields += self._update_nextcloud_names(vcard, self.lastname, self.firstname, values)
                            update_fields += self._update_nextcloud_orgs(vcard, self.parent_id.name, self.function, values)
                            update_fields += self._update_nextcloud_addresses(vcard, 'WORK', 'HOME', values)
                            update_fields += self._update_nextcloud_phones(vcard, 'WORK', 'HOME', values)
                            update_fields += self._update_nextcloud_emails(vcard, 'WORK', 'HOME', values)
                            update_fields += self._update_nextcloud_websites(vcard, values)
                            update_fields += self._update_nextcloud_birthdays(vcard, values)
                            update_fields += self._update_nextcloud_comments(vcard, values)
                            update_fields += self._update_nextcloud_images(vcard, values)
                            update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group, values)
                            if update_fields:
                                client.execute_request(
                                    'upload',
                                    self.nextcloud_contacts_resource_href,
                                    data=vcard.serialize(),
                                )
                        else:
                            update_fields = ''
                            update_fields += self._update_nextcloud_names(vcard, self.lastname, self.firstname, values)
                            update_fields += self._update_nextcloud_orgs(vcard, '', '', values)
                            update_fields += self._update_nextcloud_addresses(vcard, 'HOME', 'WORK', values)
                            update_fields += self._update_nextcloud_phones(vcard, 'HOME', 'WORK', values)
                            update_fields += self._update_nextcloud_emails(vcard, 'HOME', 'WORK', values)
                            update_fields += self._update_nextcloud_websites(vcard, values)
                            update_fields += self._update_nextcloud_birthdays(vcard, values)
                            update_fields += self._update_nextcloud_comments(vcard, values)
                            update_fields += self._update_nextcloud_images(vcard, values)
                            update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group, values)
                            if update_fields:
                                client.execute_request(
                                    'upload',
                                    self.nextcloud_contacts_resource_href,
                                    data=vcard.serialize(),
                                )

        except Exception as e:
            pass
        return result

    @api.multi
    def unlink(self):
        for partner in self:
            if (    partner.nextcloud_contacts_settings_id
                and partner.nextcloud_contacts_resource_href
                and partner.nextcloud_contacts_settings_id.active
                and (partner.nextcloud_contacts_settings_id.remote_delete_type in ['delete', 'remove'])
            ):
                client = webdav3.client.Client(partner.nextcloud_contacts_settings_id._get_options()[0])
                if client:
                    client.verify = False
                if client:
                    if partner.nextcloud_contacts_settings_id.remote_delete_type == 'delete':
                        try:
                            client.execute_request('clean', partner.nextcloud_contacts_resource_href)
                        except Exception as e:
                            pass
                    else:
                        try:
                            obj = client.execute_request('download', partner.nextcloud_contacts_resource_href)
                        except Exception as e:
                            obj = None
                            pass
                        if obj:
                            vcard = vobject.readOne(obj.text)
                            groups = vcard.contents.get('categories', [])
                            if partner.nextcloud_contacts_settings_id.flectra_group:
                                for group in groups:
                                    for value in group.value:
                                        if value == partner.nextcloud_contacts_settings_id.flectra_group:
                                            group.value.remove(value)
                            else:
                                found = False
                                for group in groups:
                                    if partner.nextcloud_contacts_settings_id.private_group in group.value:
                                        found = True
                                if not found:
                                    if not groups:
                                        vcard.add('categories')
                                    if not vcard.categories.value:
                                        vcard.categories.value = []
                                    vcard.categories.value.append(partner.nextcloud_contacts_settings_id.private_group)
                            client.execute_request('upload', partner.nextcloud_contacts_resource_href, data=vcard.serialize())

        return super(In4NextcloudContactsResPartner, self).unlink()

    @api.multi
    def upload_nextcloud_contacts(self):
        contacts_settings = self.env['in4nextcloud_contacts.settings'].search([])
        if contacts_settings:
            if len(contacts_settings) == 1:
                active_ids = self._context.get('active_ids')
                partners = self.env['res.partner'].browse(active_ids)
                for partner in partners:
                    partner.upload_nextcloud_contact_ok(contacts_settings)
            else:
                view_id = self.env['in4nextcloud_contacts.upload_contact']
                new = view_id.create({'nextcloud_contacts_settings_id': contacts_settings[0].id})
                ctx = {'partner_ids': self._context.get('active_ids')}
                return {
                    'type': 'ir.actions.act_window',
                    'name': _('Confirmation'),
                    'res_model': 'in4nextcloud_contacts.upload_contact',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_id': new.id,
                    'view_id': self.env.ref('in4nextcloud_contacts.upload_contact_form', False).id,
                    'target': 'new',
                    'context': ctx,
                }

    @api.model
    def upload_nextcloud_contact_ok(self, contacts_settings):
        if (    contacts_settings
            and contacts_settings.active
            and (  (not self.nextcloud_contacts_settings_id)
                or (self.nextcloud_contacts_settings_id.id == contacts_settings.id)
                )
        ):
            client = webdav3.client.Client(contacts_settings._get_options()[0])
            if client:
                href = None
                vcard = None
                update_fields = ''
                client.verify = False
                if self.nextcloud_contacts_settings_id and self.nextcloud_contacts_resource_href:
                    try:
                        href = self.nextcloud_contacts_resource_href
                        vcard = vobject.readOne(client.execute_request('download', href).text)
                    except Exception as e:
                        href = None
                        vcard = None
                        pass
                if not vcard:
                    href = contacts_settings.url_path + str(uuid.uuid4()).upper() + '.vcf'
                    vcard = vobject.vCard()
                if self.is_company:
                    update_fields += self._update_nextcloud_orgs(vcard, self.name, '')
                    update_fields += self._update_nextcloud_addresses(vcard, 'WORK', None)
                    update_fields += self._update_nextcloud_phones(vcard, 'WORK', None)
                    update_fields += self._update_nextcloud_emails(vcard, 'WORK', None)
                    update_fields += self._update_nextcloud_websites(vcard)
                    update_fields += self._update_nextcloud_comments(vcard)
                    update_fields += self._update_nextcloud_images(vcard)
                    update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group)
                else:
                    if self.parent_id:
                        update_fields += self._update_nextcloud_names(vcard, self.lastname, self.firstname)
                        update_fields += self._update_nextcloud_orgs(vcard, self.parent_id.name, self.function)
                        update_fields += self._update_nextcloud_addresses(vcard, 'WORK', 'HOME')
                        update_fields += self._update_nextcloud_phones(vcard, 'WORK', 'HOME')
                        update_fields += self._update_nextcloud_emails(vcard, 'WORK', 'HOME')
                        update_fields += self._update_nextcloud_websites(vcard)
                        update_fields += self._update_nextcloud_birthdays(vcard)
                        update_fields += self._update_nextcloud_comments(vcard)
                        update_fields += self._update_nextcloud_images(vcard)
                        update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group)
                    else:
                        update_fields += self._update_nextcloud_names(vcard, self.lastname, self.firstname)
                        update_fields += self._update_nextcloud_orgs(vcard, '', '')
                        update_fields += self._update_nextcloud_addresses(vcard, 'HOME', 'WORK')
                        update_fields += self._update_nextcloud_phones(vcard, 'HOME', 'WORK')
                        update_fields += self._update_nextcloud_emails(vcard, 'HOME', 'WORK')
                        update_fields += self._update_nextcloud_websites(vcard)
                        update_fields += self._update_nextcloud_birthdays(vcard)
                        update_fields += self._update_nextcloud_comments(vcard)
                        update_fields += self._update_nextcloud_images(vcard)
                        update_fields += self._update_nextcloud_groups(vcard, contacts_settings.flectra_group)
                if update_fields:
                    self._update_nextcloud_groups(vcard, contacts_settings.flectra_group)
                    client.execute_request('upload', href, data=vcard.serialize())
                    self.write({
                        'nextcloud_contacts_settings_id': contacts_settings.id,
                        'nextcloud_contacts_resource_href': href,
                    })


class In4NextcloudContactsUploadContact(models.TransientModel):

    _name = 'in4nextcloud_contacts.upload_contact'
    _description = 'Upload Contact'

    nextcloud_contacts_settings_id = fields.Many2one('in4nextcloud_contacts.settings', string='Contacts Settings', required=True)

    @api.multi
    def upload_contact_ok(self):
        partners = self._context.get('partner_ids')
        for partner in self.env['res.partner'].search([('id', 'in', partners)]):
            partner.upload_nextcloud_contact_ok(self.nextcloud_contacts_settings_id)
