# -*- coding: utf-8 -*-

import base64
import pytz
import time
import vobject
import webdav3.client
import xml.etree
import xml.etree.ElementTree

from datetime import datetime
from dateutil import parser
from webdav3.exceptions import RemoteResourceNotFound

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4NextcloudContactsSettings(models.Model):

    _name = 'in4nextcloud_contacts.settings'
    _description = 'Contacts Settings'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True)
    hostname = fields.Char(string='Hostname', required=True, default='https://nextcloud.example.com')
    login = fields.Char(string='Login', required=True, default='nextcloud')
    password = fields.Char(string='Password', required=True)
    url_path = fields.Char(string='URL Path', required=True, default='/remote.php/dav/addressbooks/users/nextcloud/contacts/')
    po_box_list = fields.Text(
        string='P.O. Box - Names',
        default='Postfach\nCase Postale\nCasella Postale\nPost Office Box\nPost-Office Box\nP.O. Box\nP.O.B\nPOB',
    )
    download_selection = fields.Selection(
        [
            ('individuals', 'Individuals'),
            ('companies', 'Companies'),
            ('individuals_companies', 'Individuals and Companies'),
            ('individuals_companies_contacts', 'Individuals, Companies and Contacts'),
        ],
        string='Download',
        default='individuals_companies_contacts',
        required=True,
    )
    upload_selection = fields.Selection(
        [
            ('manually_write', 'Save changes and upload new contacts manually'),
            ('auto_upload', 'Save changes and upload new contacts automatic'),
            ('auto_write', 'Save changes automatic and upload new contacts manually'),
        ],
        string='Upload',
        default='auto_write',
        required=True,
    )
    local_delete_type = fields.Selection(
        [
            ('none', 'Delete only the Nextcloud Contact'),
            ('delete', 'Delete Flectra Contact also'),
        ],
        string='Delete on Nextcloud',
        default='none',
        required=True,
    )
    remote_delete_type = fields.Selection(
        [
            ('none', 'Delete only in Flectra'),
            ('delete', 'Delete Nextcloud Contact also'),
            ('remove', 'Remove from the Sync Group / Add to the Private Group'),
        ],
        string='Delete on Flectra',
        default='remove',
        required=True,
    )

    flectra_group = fields.Char(string='Sync Group')
    private_group = fields.Char(string='Private Group', required=True, default='Private')

    download_timestamp = fields.Datetime(string='Last Download')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Setting name already exists !'),
    ]

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.update(
            name=_('%s (copy)') % (self.name or ''))
        return super(In4NextcloudContactsSettings, self).copy(default)

    @api.one
    def _get_options(self):
        return {
            'webdav_hostname': self.hostname,
            'webdav_login': self.login,
            'webdav_password': self.password,
        }

    @api.model
    def _set_address(self, address_type, address, values, is_company):
        has_values = False
        if (   (address_type == 'UNKNOWN')
            or ((is_company) and (address_type == 'WORK'))
            or ((not is_company) and (address_type == 'HOME'))
        ):
            value = address.value.street
            if values['street'] != value:
                values['street'] = value
                has_values = True
            value = address.value.extended
            if not value:
                value = address.value.box
            if values['street2'] != value:
                values['street2'] = value
                has_values = True
            value = address.value.code
            if values['zip'] != value:
                values['zip'] = value
                has_values = True
            value = address.value.city
            if values['city'] != value:
                values['city'] = value
                has_values = True
            country_id = None
            if not country_id:
                country_name = address.value.country
                if country_name:
                    country_id = self.env['res.country'].search([('name', '=', country_name)], limit=1).id
            if not country_id:
                country_id = self.env.user.company_id.country_id.id
            if values['country_id'] != country_id:
                values['country_id'] = country_id
                has_values = True
        return has_values

    @api.model
    def _set_function(self, function_type, function, values, is_company_contact):
        has_values = False
        if (function_type == 'UNKNOWN') and is_company_contact:
            value = function.value
            if values['function'] != value:
                values['function'] = value
        return has_values

    @api.model
    def _set_phone(self, phone_type, phone_number, values, is_company_or_contact):
        has_values = False
        if (   (phone_type == 'UNKNOWN')
            or ((is_company_or_contact) and (phone_type == 'WORK,VOICE'))
            or ((not is_company_or_contact) and (phone_type == 'HOME,VOICE'))
        ):
            value = phone_number.value
            if values['phone'] != value:
                values['phone'] = value
                has_values = True
        if (   (phone_type == 'CELL')
            or ((is_company_or_contact) and (phone_type == 'WORK,CELL'))
            or ((not is_company_or_contact) and (phone_type == 'HOME,CELL'))
        ):
            value = phone_number.value
            if values['mobile'] != value:
                values['mobile'] = value
                has_values = True
        return has_values

    @api.model
    def _set_email(self, email_type, email_address, values, is_company_or_contact):
        has_values = False
        if (   (email_type == 'UNKNOWN')
            or ((is_company_or_contact) and (email_type == 'WORK'))
            or ((not is_company_or_contact) and (email_type == 'HOME'))
        ):
            value = email_address.value
            if values['email'] != value:
                values['email'] = value
                has_values = True
        return has_values

    @api.model
    def _set_webseite(self, webseite_type, webseite, values, is_company_or_address):
        has_values = False
        if (webseite_type == 'UNKNOWN') and is_company_or_address:
            value = webseite.value
            if value and (not value.startswith('http://')) and (not value.startswith('https://')):
                value = 'http://' + value
            if values['website'] != value:
                values['website'] = value
                has_values = True
        return has_values

    @api.model
    def _set_image(self, image_type, image, values):
        has_values = False
        if image_type == 'UNKNOWN':
            value = image.value
            if value:
                value = base64.encodebytes(value)
            if values['image'] != value:
                values['image'] = value
                has_values = True
        return has_values

    @api.model
    def _set_birthday(self, birthday_type, birthday, values):
        has_values = False
        if birthday_type == 'UNKNOWN':
            value = birthday.value
            if value:
                try:
                    value = datetime.strptime(value, '%Y%m%d')
                except:
                    pass
            if values['date_of_birth'] != value:
                values['date_of_birth'] = value
                has_values = True
        return has_values

    @api.model
    def _set_comment(self, comment_type, comment, values):
        has_values = False
        if comment_type == 'UNKNOWN':
            value = comment.value
            if values['comment'] != value:
                values['comment'] = value
                has_values = True
        return has_values

    @api.model
    def _set_group(self, group, values):
        has_values = False
        for value in group.value:
            domain = [('nextcloud_group', '=', value)]
            category_id = self.env['res.partner.category'].search(domain, limit=1)
            if category_id:
                values['category_id'].append((4, category_id.id))
                has_values = True
        return has_values

    @api.model
    def _update_nextcloud_contact(self, settings, href, vcard, values, with_empty_contacts):
        log_create = None
        log_ignore = None
        log_write = None

        values['nextcloud_contacts_timestamp'] = fields.datetime.now()

        is_company = values['is_company']
        is_contact = values['parent_id']
        is_company_contact = (not is_company) and is_contact
        is_company_or_contact = is_company or is_contact

        has_additional_values = False
        if values['parent_id']:
            domain = [
                ('parent_id', '=', False),
                ('nextcloud_contacts_settings_id', '=', settings.id),
                ('nextcloud_contacts_resource_href', '=', href),
            ]
            partner_person_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
            if partner_person_id:
                values['partner_person_id'] = partner_person_id.id
        else:
            address_types = []
            addresses = vcard.contents.get('adr', [])
            for address in addresses:
                address_type = address.params['TYPE'][0] or 'UNKNOWN'
                if (    (address_type not in address_types)
                    and (   (address_type != 'UNKNOWN')
                         or ((is_company) and ('WORK' not in address_types))
                         or ((not is_company) and ('HOME' not in address_types))
                        )
                ):
                    address_types.append(address_type)
                    if self._set_address(address_type, address, values, is_company):
                        has_additional_values = True

        function_types = []
        functions = vcard.contents.get('title', [])
        for function in functions:
            function_type = 'UNKNOWN'
            if function_type not in function_types:
                function_types.append(function_type)
                self._set_function(function_type, function, values, is_company_contact)

        phone_types = []
        phone_numbers = vcard.contents.get('tel', [])
        for phone_number in phone_numbers:
            phone_type = phone_number.params['TYPE'][0] or 'UNKNOWN'
            if (    (phone_type not in phone_types)
                and (   (phone_type != 'UNKNOWN')
                     or ((is_company_or_contact) and ('WORK,VOICE' not in phone_types))
                     or ((not is_company_or_contact) and ('HOME,VOICE' not in phone_types))
                    )
                and (   (phone_type != 'CELL')
                     or ((is_company_or_contact) and ('WORK,CELL' not in phone_types))
                     or ((not is_company_or_contact) and ('HOME,CELL' not in phone_types))
                    )
            ):
                phone_types.append(phone_type)
                if self._set_phone(phone_type, phone_number, values, is_company_or_contact):
                    has_additional_values = True

        email_types = []
        email_addresses = vcard.contents.get('email', [])
        for email_address in email_addresses:
            email_type = email_address.params['TYPE'][0] or 'UNKNOWN'
            if (    (email_type not in email_types)
                and (   (email_type != 'UNKNOWN')
                     or ((is_company_or_contact) and ('WORK' not in email_types))
                     or ((not is_company_or_contact) and ('HOME' not in email_types))
                    )
            ):
                email_types.append(email_type)
                if self._set_email(email_type, email_address, values, is_company_or_contact):
                    has_additional_values = True

        webseite_types = []
        webseites = vcard.contents.get('url', [])
        for webseite in webseites:
            webseite_type = 'UNKNOWN'
            if webseite_type not in webseite_types:
                webseite_types.append(webseite_type)
            self._set_webseite(webseite_type, webseite, values, is_company or (not is_contact))

        image_types = []
        images = vcard.contents.get('photo', [])
        for image in images:
            image_type = 'UNKNOWN'
            if image_type not in image_types:
                image_types.append(image_type)
                self._set_image(image_type, image, values)

        birthday_types = []
        birthdays = vcard.contents.get('bday', [])
        for birthday in birthdays:
            birthday_type = 'UNKNOWN'
            if birthday_type not in birthday_types:
                birthday_types.append(birthday_type)
                self._set_birthday(birthday_type, birthday, values)

        comment_types = []
        comments = vcard.contents.get('note', [])
        for comment in comments:
            comment_type = 'UNKNOWN'
            if comment_type not in comment_types:
                comment_types.append(comment_type)
                self._set_comment(comment_type, comment, values)

        groups = vcard.contents.get('categories', [])
        for group in groups:
            self._set_group(group, values)

        if values['parent_id']:
            domain = [
                '&',
                    ('parent_id', '=', values['parent_id']),
                    '|',
                        '&',
                            ('nextcloud_contacts_settings_id', '=', settings.id),
                            ('nextcloud_contacts_resource_href', '=', href),
                        '&',
                            ('partner_person_id.nextcloud_contacts_settings_id', '=', settings.id),
                            ('partner_person_id.nextcloud_contacts_resource_href', '=', href),
            ]
        else:
            domain = [
                ('parent_id', '=', False),
                ('nextcloud_contacts_settings_id', '=', settings.id),
                ('nextcloud_contacts_resource_href', '=', href),
            ]
        local_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
        if local_partner_id:
            for category_id in local_partner_id.category_id:
                if category_id.nextcloud_group:
                    found = False
                    for new_category_id in values['category_id']:
                        if new_category_id[1] == category_id.id:
                            found = True
                    if not found:
                        values['category_id'].append((3, category_id.id))
            local_partner_id.write(values)
            log_write = local_partner_id.display_name
        else:
            if with_empty_contacts or has_additional_values:
                domain = [
                    ('parent_id', '!=', False),
                    ('nextcloud_contacts_settings_id', '=', settings.id),
                    ('nextcloud_contacts_resource_href', '=', href),
                ]
                local_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
                if local_partner_id:
                    for category_id in local_partner_id.category_id:
                        if category_id.nextcloud_group:
                            found = False
                            for new_category_id in values['category_id']:
                                if new_category_id[1] == category_id.id:
                                    found = True
                            if not found:
                                values['category_id'].append((3, category_id.id))
                    values.pop('parent_id')
                    values.pop('is_company')
                    values.pop('lastname')
                    values.pop('firstname')
                    values.pop('image')
                    local_partner_id.write(values)
                    log_write = local_partner_id.display_name
                else:
                    values['nextcloud_contacts_settings_id'] = settings.id
                    values['nextcloud_contacts_resource_href'] = href
                    install_mode = values.get('image') # install_mode=True --> no default image
                    local_partner_id = self.env['res.partner'].with_context(install_mode=install_mode).create(values)
                    log_create = local_partner_id.display_name
            else:
                if values['lastname'] and values['firstname']:
                    log_ignore = values['lastname'] + ' ' + values['firstname']
                elif values['lastname']:
                    log_ignore = values['lastname']
                elif values['firstname']:
                    log_ignore = values['firstname']
                else:
                    log_ignore = '? (unknown)'
        return log_create, log_ignore, log_write

    @api.model
    def _update_nextcloud_individual(self, settings, href, vcard, person, has_organization, organization_partner_id):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': False,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            'image': None,
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': self.env.user.company_id.country_id.id,
            'function': None,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'date_of_birth': None,
            'comment': None,
            'category_id': [],
        }
        display_name = vcard.contents['fn'][0].value
        if display_name:
            value = person.value.family
            if value:
                values['lastname'] = value
            value = person.value.given
            if value:
                values['firstname'] = value
            if organization_partner_id:
                values['parent_id'] = organization_partner_id.id
                if values['lastname'] and (not values['firstname']):
                    selection = dict(organization_partner_id.fields_get(['type'])['type']['selection'])
                    if values['lastname'] == selection['invoice']:
                        values['type'] = 'invoice'
                        values['lastname'] = None
                    elif values['lastname'] == selection['delivery']:
                        values['type'] = 'delivery'
                        values['lastname'] = None
                    elif values['lastname'] == selection['other']:
                        values['type'] = 'other'
                        values['lastname'] = None
            with_empty_contacts = True
            if (    (has_organization)
                and (not organization_partner_id)
                and (settings.download_selection == 'individuals_companies_contacts')
            ):
                with_empty_contacts = False
            log_create, log_ignore, log_write = self._update_nextcloud_contact(settings, href, vcard, values, with_empty_contacts)
            # if (display_name == 'Simon Gnehm'):
            #     _logger.info(display_name)
        return log_create, log_ignore, log_write

    @api.model
    def _update_nextcloud_company(self, settings, href, vcard):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': True,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            'image': None,
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': self.env.user.company_id.country_id.id,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'date_of_birth': None,
            'comment': None,
            'category_id': [],
        }
        display_name = vcard.org.value[0]
        if display_name:
            values['lastname'] = display_name
            log_create, log_ignore, log_write = self._update_nextcloud_contact(settings, href, vcard, values, True)
            # if (display_name == 'Gnehm Informatik GmbH'):
            #     _logger.info(display_name)
        return log_create, log_ignore, log_write

    @api.model
    def _download_contacts(self, settings, client, only_contacts):
        added_list = []
        ignored_list = []
        changed_list = []
        missing_list = []

        list = xml.etree.ElementTree.fromstring(client.execute_request('list', settings.url_path).text)
        for root_node in list:
            href = ''
            etag = ''
            update_time = None
            for child_node in root_node:
                if child_node.tag == '{DAV:}href':
                    href = child_node.text
                elif child_node.tag == '{DAV:}propstat':
                    for propstat_node in child_node:
                        if propstat_node.tag == '{DAV:}prop':
                            for prop_node in propstat_node:
                                if prop_node.tag == '{DAV:}getetag':
                                    etag = prop_node.text
                                elif prop_node.tag == '{DAV:}getlastmodified':
                                    update_time = prop_node.text
            if href and etag:
                need_update = True
                vcard = vobject.readOne(client.execute_request('download', href).text)

                if need_update:
                    if update_time and settings.download_timestamp:
                        last_download_str = settings.download_timestamp
                        last_download_datetime = datetime.strptime(last_download_str, DEFAULT_SERVER_DATETIME_FORMAT)
                        if parser.parse(update_time) < pytz.UTC.localize(last_download_datetime):
                            need_update = False

                if need_update:
                    flectra_group = settings.flectra_group
                    in_flectra_group = True
                    if flectra_group:
                        in_flectra_group = False
                    private_group = settings.private_group
                    for groups in vcard.contents.get('categories', []):
                        for group in groups.value:
                            if need_update and private_group and (group == private_group):
                                need_update = False
                            if (not in_flectra_group) and flectra_group and (group == flectra_group):
                                in_flectra_group = True
                    if not in_flectra_group:
                        need_update = False

                if need_update:
                    people_count = 0
                    if (    (people_count == 0)
                        and (not only_contacts)
                        and (settings.download_selection in [
                                'individuals',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        has_organization = False
                        organizations = vcard.contents.get('org', [])
                        for organization in organizations:
                            if organization.value:
                                has_organization = True
                                break
                        persons = vcard.contents.get('n', [])
                        for person in persons:
                            if person.value.family or person.value.given:
                                log_create, log_ignore, log_write = self._update_nextcloud_individual(
                                    settings,
                                    href,
                                    vcard,
                                    person,
                                    has_organization,
                                    None,
                                )
                                if log_create:
                                    people_count += 1
                                    added_list.append(log_create)
                                if log_ignore:
                                    people_count += 1
                                    ignored_list.append(log_ignore)
                                if log_write:
                                    people_count += 1
                                    changed_list.append(log_write)
                            if people_count > 0:
                                break
                    if (    (people_count == 0)
                        and (settings.download_selection in [
                                'companies',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        organizations = vcard.contents.get('org', [])
                        for organization in organizations:
                            if organization.value:
                                if only_contacts:
                                    persons = vcard.contents.get('n', [])
                                    organization_name = organization.value[0]
                                    domain = [
                                        ('is_company', '=', True),
                                        ('parent_id', '=', False),
                                        ('name', '=', organization_name),
                                    ]
                                    organization_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
                                    if organization_partner_id:
                                        for person in persons:
                                            if person.value.family or person.value.given:
                                                log_create, log_ignore, log_write = self._update_nextcloud_individual(
                                                    settings,
                                                    href,
                                                    vcard,
                                                    person,
                                                    True,
                                                    organization_partner_id,
                                                )
                                                if log_create:
                                                    people_count += 1
                                                    added_list.append(log_create)
                                                if log_ignore:
                                                    people_count += 1
                                                    ignored_list.append(log_ignore)
                                                if log_write:
                                                    people_count += 1
                                                    changed_list.append(log_write)
                                            if people_count > 0:
                                                break
                                    else:
                                        for person in persons:
                                            if person.value.family or person.value.given:
                                                msg = organization_name + ' (' + vcard.contents['fn'][0].value + ')'
                                                missing_list.append(msg)
                                else:
                                    log_create, log_ignore, log_write = self._update_nextcloud_company(
                                        settings,
                                        href,
                                        vcard
                                    )
                                    if log_create:
                                        people_count += 1
                                        added_list.append(log_create)
                                    if log_ignore:
                                        people_count += 1
                                        ignored_list.append(log_ignore)
                                    if log_write:
                                        people_count += 1
                                        changed_list.append(log_write)
                            if people_count > 0:
                                break

        return added_list, ignored_list, changed_list, missing_list

    @api.model
    def download_contacts_from_settings(self, id=None):
        settings = None
        client = None

        if id:
            settings = id
        else:
            context = dict(self._context or {})
            active_id = context.get('active_id', False)
            if active_id:
                settings = self.env['in4nextcloud_contacts.settings'].browse(active_id)
        if settings and settings.active:
            client = webdav3.client.Client(settings._get_options()[0])
            if client:
                client.verify = False
        if settings and settings.active and client:
            start_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            added_list = []
            ignored_list = []
            changed_list = []
            missing_list = []
            deleted_list = []
            archived_list = []

            # Private Adressen und Unternehmen
            log_create, log_ignore, log_write, log_missing = self._download_contacts(settings, client, False)
            added_list.extend(log_create)
            ignored_list.extend(log_ignore)
            changed_list.extend(log_write)
            missing_list.extend(log_missing)

            # Geschäftliche Adressen
            if settings.download_selection == 'individuals_companies_contacts':
                log_create, log_ignore, log_write, log_missing = self._download_contacts(settings, client, True)
                added_list.extend(log_create)
                ignored_list.extend(log_ignore)
                changed_list.extend(log_write)
                missing_list.extend(log_missing)

            domain = [
                ('parent_id', '!=', False),
                ('partner_person_id', '!=', False),
                ('nextcloud_contacts_settings_id', '!=', False),
                ('nextcloud_contacts_resource_href', '!=', False),
                ('nextcloud_contacts_timestamp', '>=', start_date),
            ]
            partners = self.env['res.partner'].with_context(active_test=False).search(domain)
            for partner in partners:
                if (    (partner.nextcloud_contacts_settings_id == partner.partner_person_id.nextcloud_contacts_settings_id)
                    and (partner.nextcloud_contacts_resource_href == partner.partner_person_id.nextcloud_contacts_resource_href)
                ):
                    partner.write({
                        'nextcloud_contacts_settings_id': None,
                        'nextcloud_contacts_resource_href': None,
                    })
                    partner.upload_nextcloud_contact_ok(partner.partner_person_id.nextcloud_contacts_settings_id)
                    partner.partner_person_id.upload_nextcloud_contact_ok(partner.partner_person_id.nextcloud_contacts_settings_id)

            domain = [
                '|',
                    '&',
                        ('nextcloud_contacts_timestamp', '=', False),
                        '|',
                            ('nextcloud_contacts_settings_id', '!=', False),
                            ('nextcloud_contacts_resource_href', '!=', False),
                    '&',
                        ('nextcloud_contacts_timestamp', '!=', False),
                        ('nextcloud_contacts_timestamp', '<', start_date),
            ]
            partners = self.env['res.partner'].with_context(active_test=False).search(domain)
            for partner in partners:
                if partner.nextcloud_contacts_settings_id and partner.nextcloud_contacts_resource_href:
                    delete_local_contact = False
                    try:
                        vcard = vobject.readOne(
                            client.execute_request('download', partner.nextcloud_contacts_resource_href).text
                        )
                    except RemoteResourceNotFound as e:
                        vcard = None
                        delete_local_contact = True
                        pass
                    if (not delete_local_contact) and vcard:
                        private_group = settings.private_group
                        for groups in vcard.contents.get('categories', []):
                            for group in groups.value:
                                if group == private_group:
                                    delete_local_contact = True
                    if (not delete_local_contact) and vcard and settings.flectra_group:
                        delete_local_contact = True
                        flectra_group = settings.flectra_group
                        for groups in vcard.contents.get('categories', []):
                            for group in groups.value:
                                if group == flectra_group:
                                    delete_local_contact = False
                    if delete_local_contact:
                        if settings.local_delete_type == 'delete':
                            current_partner_name = partner.name
                            self.env.cr.autocommit(False)
                            try:
                                partner.unlink()
                                deleted_list.append(current_partner_name)
                            except:
                                self.env.cr.rollback()
                                values = {
                                    'active': False,
                                    'nextcloud_contacts_settings_id': None,
                                    'nextcloud_contacts_resource_href': None,
                                    'nextcloud_contacts_timestamp': None,
                                }
                                partner.write(values)
                                archived_list.append(current_partner_name)
                                pass
                        else:
                            partner.write({
                                'nextcloud_contacts_settings_id': None,
                                'nextcloud_contacts_resource_href': None,
                                'nextcloud_contacts_timestamp': None,
                            })

            added_list.sort()
            added_list_text = ''
            for log_create_text in added_list:
                added_list_text += log_create_text + '\n'

            ignored_list.sort()
            ignored_list_text = ''
            for log_ignore_text in ignored_list:
                ignored_list_text += log_ignore_text + '\n'

            changed_list.sort()
            changed_list_text = ''
            for log_write_text in changed_list:
                changed_list_text += log_write_text + '\n'

            missing_list.sort()
            missing_list_text = ''
            for log_missing_text in missing_list:
                missing_list_text += log_missing_text + '\n'

            deleted_list.sort()
            deleted_list_text = ''
            for log_deleted_text in deleted_list:
                deleted_list_text += log_deleted_text + '\n'

            archived_list.sort()
            archived_list_text = ''
            for log_archived_text in archived_list:
                archived_list_text += log_archived_text + '\n'

            settings.sudo().write({
                'download_timestamp': fields.datetime.now(),
            })

            end_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            summary = '''
Download started: %s
Download finished: %s

Added records: %d
Ignored records: %d
Changed records: %d
Missing companies: %d
Deleted records: %d
Archived records: %d
            ''' % (
                start_date,
                end_date,
                len(added_list),
                len(ignored_list),
                len(changed_list),
                len(missing_list),
                len(deleted_list),
                len(archived_list),
            )
            summary = summary.strip() + '\n\n'
            summary += 'Added records:\n' + added_list_text
            summary += '\n'
            summary += 'Ignored records:\n' + ignored_list_text
            summary += '\n'
            summary += 'Changed records:\n' + changed_list_text
            summary += '\n'
            summary += 'Missing companies:\n' + missing_list_text
            summary += '\n'
            summary += 'Deleted records:\n' + deleted_list_text
            summary += '\n'
            summary += 'Archived records:\n' + archived_list_text
            self.env['in4nextcloud_contacts.request'].sudo().create({
                'name': 'Download report',
                'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'body': summary,
            })

    @api.model
    def download_all_contacts(self):
        settings_list = self.env['in4nextcloud_contacts.settings'].search([('active', '=', True)])
        for settings in settings_list:
            self.download_contacts_from_settings(settings)


class In4NextcloudContactsRequest(models.Model):

    _name = 'in4nextcloud_contacts.request'
    _description = 'Contacts Request'
    _order = 'date desc'

    name = fields.Char(string='Name', required=True)
    date = fields.Datetime(string='Date', required=True)
    body = fields.Text(string='Request', readonly=True)
