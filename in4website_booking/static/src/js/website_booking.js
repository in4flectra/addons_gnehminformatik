flectra.define('in4website_booking.website', function (require) {
    'use strict';

    var ajax = require('web.ajax');
    var core = require('web.core');
    var session = require('web.session');

    var qweb = core.qweb;
    var _t = core._t;

    ajax.loadXML('/in4website_booking/static/src/xml/website_booking.xml', qweb);

    $(document).ready(function() {

        var calendarID = $('#calendar_id').val();
        var slotDuration = convert_float_time($('#calendar_slot_duration').val());
        var slotDurationInMS = moment.duration(slotDuration)._milliseconds;
        var maxSlotCount = $('#calendar_max_slot_count').val();
        var minTime = $('#calendar_min_time').val();
        var maxTime = $('#calendar_max_time').val();
        var bookingRange = $('#calendar_booking_range').val();
        var userID = $('#calendar_user_id').val();
        var events = JSON.parse($('#booking_events').val());
        var email = $('#booking_email').val();
        var defaultDate = $('#default_date').val();
        var todayText = $('#today_text').val();
        var locale = 'de-CH'; // TODO: Load dynamic
        var datesFormat = [{year: 'numeric', month: '2-digit', day: '2-digit', weekday: 'short', hour: '2-digit', minute: '2-digit'}];

        $('#booking_calendar').fullCalendar({
            minTime: minTime,
            maxTime: maxTime,
            slotDuration: slotDuration + ':00',
            slotLabelInterval: '00:' + slotDuration + ':00',
            slotLabelFormat: 'hh:mma',
            defaultView: 'agendaWeek',
            defaultDate: defaultDate,
            firstDay: 1,
            timezone: 'local',
            locale: locale,
            colHeadFormat: [{weekday: 'short', month: 'short', day: '2-digit'}],
            rowLabelFormat: [{hour: '2-digit', minute: '2-digit'}],
            allDaySlot: false,
            buttonText: {
                today: todayText,
            },
            eventSources: [
                {
                    url: '/book/calendar/timeframe/' + calendarID,
                    rendering: 'background',
                    className: 'booking_calendar_book_time',
                },
                {
                    url: '/book/calendar/events/' + userID,
                    rendering: 'background',
                    backgroundColor: '#ff0000',
                }
            ],
            eventRender: function (event, element) {
			    if (event.className[1] == 'fc-bgevent-taken-user') {
				    element.append(event.title);
				}
            },
            dayClick: function(date, jsEvent, view) {
                var nowTime = new Date();
                var baseTimezoneOffset = new Date('1970-01-01').getTimezoneOffset();
                var deltaTimezoneOffset = 1000 * 60 * (date._d.getTimezoneOffset() - baseTimezoneOffset);

                var allEvents = $('#booking_calendar').fullCalendar('clientEvents');
                var dateStart = new Date(date._d.getTime() + deltaTimezoneOffset);
                if (bookingRange != 'slots') {
                    for (var eventIdx = 0; eventIdx < allEvents.length; eventIdx++) {
                        var event = allEvents[eventIdx];
                        if (event.source.className[0] == 'booking_calendar_book_time') {
                            if ((event.start._d <= dateStart) && (event.end._d >= dateStart)) {
                                dateStart = event.start._d;
                            }
                        }
                    }
                }
                var dateStartIso = dateStart.toISOString();

                if (dateStart < nowTime) {
                    alert(_t("You cannot book for past."));
                } else {
                    var isLastOpt = false;
                    var isFirstOpt = false;
                    var isOutOfBooking = true;
                    var isAlreadyBooked = false;
                    var dateEnd = new Date(dateStart.getTime() + slotDurationInMS);
                    for (var eventIdx = 0; eventIdx < allEvents.length; eventIdx++) {
                        var event = allEvents[eventIdx];
                        if (event.source.className[0] == 'booking_calendar_book_time') {
                            if ((event.start._d <= dateStart) && (event.end._d >= dateEnd)) {
                                isOutOfBooking = false;
                            }
                        } else {
                            if ((dateStart < event.end._d) && (dateEnd > event.start._d)) {
                                isAlreadyBooked = true;
                            }
                        }
                    }
                    if (isAlreadyBooked) {
                        alert(_t("This timeslot has already been booked."));
                    } else if (isOutOfBooking) {
                        alert(_t("You cannot book this timeslot."));
                    } else {
                        var self = this;
                        self.template = 'in4website_booking.website_booking_create';
                        self.$modal = $(qweb.render(this.template, {}));
                        $('body').append(self.$modal);
                        $('#oe_website_calendar_modal').modal('show');
                        $('#booking_form_calendar_id').val(calendarID);
                        $('#booking_form_email').val(email);
                        $('#booking_form_start').val(dateStartIso.substr(0, 10) + ' ' + dateStartIso.substr(11, 8));
                        $('#booking_form_start_text').val(dateStart.toLocaleDateString(locale, datesFormat[0]));

                        // Mitglieder-Flag nur für Kalender 1 (Lastenvelo) sichtbar
                        if (calendarID != 1) {
                            // $('#booking_form_agb_member').css('display', 'none');
                            // $('#booking_form_agb').removeAttr('required');
                            $('#booking_form_member_group').css('display', 'none');
                            $('#booking_form_extendedaddress_group').css('display', 'none');
                            $('#booking_form_street').removeAttr('required');
                            $('#booking_form_city').removeAttr('required');
                            $('#booking_form_phone').removeAttr('required');
                            $('#booking_form_idcard').removeAttr('required');
                        }

                        var select = $('#booking_form_stop');
                        var optCnt = select[0].options.length;
                        for (var optIdx = optCnt - 1; optIdx >= 0; optIdx--) {
                            select[0].remove(optIdx);
                        }
                        var optIdx = 0;
                        var optCnt = 0;
                        isAlreadyBooked = false;
                        while ((!isAlreadyBooked) && (optCnt < maxSlotCount)) {

                            var optStart = new Date(dateStart);
                            optStart = new Date(optStart.setTime(optStart.getTime() + (slotDurationInMS * optIdx)));
                            var optEnd = new Date(optStart.getTime() + slotDurationInMS);

                            isLastOpt = false;
                            isFirstOpt = false;
                            isOutOfBooking = true;
                            for (var eventIdx = 0; eventIdx < allEvents.length; eventIdx++) {
                                var event = allEvents[eventIdx];
                                if (event.source.className[0] == 'booking_calendar_book_time') {
                                    var eventStart = new Date(event.start._d);
                                    var eventEnd = new Date(event.end._d);
                                    do {
                                        if ((eventStart <= optEnd) && (eventEnd >= optEnd)) {
                                            isOutOfBooking = false;
                                            if (eventEnd.toLocaleDateString(locale, datesFormat[0]) == optEnd.toLocaleDateString(locale, datesFormat[0])) {
                                                isLastOpt = true;
                                            }
                                            if (eventStart.toLocaleDateString(locale, datesFormat[0]) == optEnd.toLocaleDateString(locale, datesFormat[0])) {
                                                isFirstOpt = true;
                                            }
                                        }
                                        eventStart = new Date(eventStart.setDate(eventStart.getDate() + 7));
                                        eventEnd = new Date(eventEnd.setDate(eventEnd.getDate() + 7));
                                    } while ((isOutOfBooking) && (eventStart <= optEnd));
                                } else {
                                    if ((optStart < event.end._d) && (optEnd > event.start._d)) {
                                        isAlreadyBooked = true;
                                        break;
                                    }
                                }
                            }
                            if (
                                   (!isOutOfBooking)
                                && (!isAlreadyBooked)
                                && (
                                       (bookingRange == 'slots')
                                    || ((bookingRange != 'slots') && (isLastOpt == true))
                                    || ((bookingRange == 'full_timeframes_and_timeframe_begin') && (isFirstOpt == true))
                                )
                            ) {
                                var val = optEnd.toISOString().split('T');
                                val = val[0] + ' ' + val[1].split('.')[0];
                                select.append($('<option>').val(val).html(optEnd.toLocaleDateString(locale, datesFormat[0])));
                                optCnt++;
                            }
                            optIdx++;
                        }
                        self.$modal.find('#submit_calendar_booking').on('click', function () {
                            self.$modal.modal('hide');
                        });
                    }
                }
            }
        });

        function convert_float_time(float_time) {
            var formatTime = ""
            var decimal = float_time % 1
            formatTime = Math.floor(float_time) + ":" + (60 * decimal)
            return formatTime
        }
    });
});