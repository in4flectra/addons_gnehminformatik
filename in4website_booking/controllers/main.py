# -*- coding: utf-8 -*-

import json
import werkzeug

from datetime import date, datetime, timedelta

from flectra import http, _
from flectra.http import request
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class WebsiteBookingController(http.Controller):

    @http.route('/book/calendar/create', type='http', auth='public', website=True)
    def book_calendar_create(self, **kw):
        values = {}
        for field_name, field_value in kw.items():
            values[field_name] = field_value

        calendar = request.env['website.calendar'].sudo().browse(int(values['calendar_id']))

        start = datetime.strptime(values['start'], '%Y-%m-%d %H:%M:%S')
        start = start.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        stop = datetime.strptime(values['stop'], '%Y-%m-%d %H:%M:%S')
        stop = stop.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        description = values['comment']
        if values.get('agb'):
            if description:
                description += "\n\n"
            description += _("Agree AGB")
        if values.get('member'):
            if description:
                if values.get('agb'):
                    description += "\n"
                else:
                    description += "\n\n"
            description += _("Is Member")
        if description:
            description += "\n\n"
        description += values.get('name') + "\n"
        description += values.get('street') + "\n"
        description += values.get('city') + "\n"
        description += values.get('phone') + "\n"
        description += "ID: " + values.get('idcard')


        request.env['calendar.event'].sudo().create({
            'user_id': calendar.user_id.id,
            'name': values['name'],
            'start': start,
            'stop': stop,
            'booking_email': values['email'],
            'description': description,
        })
        return werkzeug.utils.redirect('/book/calendar/' + str(calendar.id) + '?date=' + start[:10])

    @http.route('/book/calendar/<calendar_id>', type='http', auth='public', website=True)
    def book_calendar(self, calendar_id, **kw):
        calendar = request.env['website.calendar'].sudo().browse(int(calendar_id))
        events = request.env['calendar.event'].sudo().search([('user_id', '=', calendar.user_id.id)])
        event_list = []
        for event in events:
            event_list.append({
                'name': event.name,
                'start': event.start_datetime + '+00:00',
                'end': event.stop_datetime + '+00:00',
            })
        if kw.get('date'):
            default_date = kw['date']
        else:
            default_date = date.today().strftime(DEFAULT_SERVER_DATE_FORMAT)
        return request.render('in4website_booking.website_booking', {
            'calendar': calendar,
            'booking_events': json.dumps(event_list),
            'booking_email': request.env.user.email,
            'default_date': default_date,
            'translation': {'today_text': _('today')},
        })

    @http.route('/book/calendar/timeframe/<calendar_id>', type='http', auth='public', website=True)
    def book_calendar_timeframe(self, calendar_id, **kw):
        calendar = request.env['website.calendar'].sudo().browse(int(calendar_id))
        start = datetime.strptime(kw['start'], '%Y-%m-%d')
        return_string = ''
        for time_frame in calendar.time_frame_ids:
            if time_frame.day == '0':
                frame_date = start + timedelta(days=7)
            else:
                frame_date = start + timedelta(days=int(time_frame.day) - 1)
            frame_date = frame_date.strftime(DEFAULT_SERVER_DATE_FORMAT)
            frame_start_time = calendar.frame_time_to_str(frame_date, time_frame.start_time)
            frame_end_time = calendar.frame_time_to_str(frame_date, time_frame.end_time)
            return_string += '{'
            return_string += '"start": "' + frame_start_time + '",'
            return_string += '"end": "' + frame_end_time + '",'
            return_string += '"dow": [' + str(time_frame.day) + ']'
            return_string += '},'
        return_string = return_string[:-1]
        return '[' + return_string + ']'

    @http.route('/book/calendar/events/<user_id>', type='http', auth='public', website=True)
    def book_calendar_events(self, user_id, **kw):
        values = {}
        for field_name, field_value in kw.items():
            values[field_name] = field_value

        return_string = ''
        for event in request.env['calendar.event'].sudo().search([('user_id', '=', int(user_id))]):
            return_string += '{'
            return_string += '"id": "' + str(event.id) + '",'
            if (request.env.user.id == int(user_id)) or (request.env.user.email == event.booking_email):
                return_string += '"title": "' + event.name + '",'
                return_string += '"className": "fc-bgevent-taken fc-bgevent-taken-user",'
            else:
                return_string += '"className": "fc-bgevent-taken",'
            return_string += '"start": "' + event.start_datetime + '+00:00",'
            return_string += '"end": "' + event.stop_datetime + '+00:00"'
            return_string += '},'
        return_string = return_string[:-1]
        return '[' + return_string + ']'
