# -*- coding: utf-8 -*-
{
    'name': 'Website Booking',
    'summary': 'Allow website users to book meetings from the website',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Website',
    'version': '0.1',

    'depends': [
        'calendar',
        'website',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/assets.xml',
        'views/calendar_event.xml',
        'views/website_booking.xml',
        'views/website_calendar.xml',
    ],
}
