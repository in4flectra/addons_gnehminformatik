# -*- coding: utf-8 -*-

import pytz

from datetime import datetime

from flectra.http import request
from flectra import api, fields, models


class In4WebsiteBookingWebsiteCalendar(models.Model):

    _name = 'website.calendar'
    _description = 'Website Booking Calendar'

    def frame_time_to_str(self, date, time):
        result = ''
        result += date
        result += ' '
        if time < 10:
            result += '0'
        result += str(int(time))
        result += ':'
        if (time - int(time)) * 60 < 10:
            result += '0'
        result += str(int((time - int(time)) * 60))
        result += ':'
        result += '00'
        result = datetime.strptime(result, '%Y-%m-%d %H:%M:%S')
        result = pytz.timezone(self.user_id.tz or 'UTC').localize(result, is_dst=None).astimezone(pytz.utc)
        result = datetime.strftime(result, '%H:%M')
        return result

    @api.depends('time_frame_ids')
    def _compute_booking_min_time(self):
        min_time = 25.0
        for time_frame in self.time_frame_ids:
            if time_frame.start_time < min_time:
                min_time = time_frame.start_time
        self.booking_min_time = self.frame_time_to_str('1970-01-01', min_time)

    @api.depends('time_frame_ids')
    def _compute_booking_max_time(self):
        max_time = 0.0
        for time_frame in self.time_frame_ids:
            if time_frame.end_time > max_time:
                max_time = time_frame.end_time
        self.booking_max_time = self.frame_time_to_str('1970-01-01', max_time)

    def _compute_url(self):
        for calendar in self:
            calendar.url = request.httprequest.host_url + 'book/calendar/' + str(calendar.id)

    name = fields.Char(string='Name', required=True)
    user_id = fields.Many2one('res.users', string='User', required=True, help="The user the bookings are for")
    booking_slot_duration = fields.Float(string='Booking Slot Duration', default=0.5)
    booking_max_slot_count = fields.Integer(string='Max. Booking Slot Count', default=200)
    booking_min_time = fields.Char(
        string='Min. Booking Time',
        compute=_compute_booking_min_time,
        help="Time before this is cut off the calendar to reduce it's size",
    )
    booking_max_time = fields.Char(
        string='Max. Booking Time',
        compute=_compute_booking_max_time,
        help="Time after this is cut off the calendar to reduce it's size",
    )
    booking_range = fields.Selection(
        [
            ('slots', 'Slots'),
            ('only_full_timeframes', 'Only full Timeframes'),
            ('full_timeframes_and_timeframe_begin', 'Full Timeframes and to Begin of Timeframes'),
        ],
        string='Booking Range',
        default='slots',
        required=True,
    )
    url = fields.Char(string='URL', compute=_compute_url)
    header = fields.Html(string='Header')
    footer = fields.Html(string='Footer')
    time_frame_ids = fields.One2many(
        'website.calendar.timeframe',
        'calendar_id',
        string='Timeframes',
        copy=True,
        ondelete='cascade',
        help="Timeframes that bookings are allowed for this user, can be multiple per day and different on each day",
    )


class In4WebsiteBookingWebsiteCalendarTimeframe(models.Model):

    _name = 'website.calendar.timeframe'

    @api.multi
    @api.depends('day')
    def _compute_day_sort(self):
        for timeframe in self:
            if timeframe.day == '0':
                timeframe.day_sort = '7'
            else:
                timeframe.day_sort = timeframe.day

    calendar_id = fields.Many2one('website.calendar', string='Calendar')
    day = fields.Selection(
        [
            ('1', 'Monday'),
            ('2', 'Tuesday'),
            ('3', 'Wednesday'),
            ('4', 'Thursday'),
            ('5', 'Friday'),
            ('6', 'Saturday'),
            ('0', 'Sunday')
        ],
        string='Day',
        required=True,
    )
    day_sort = fields.Char(string='Day (Sort)', compute=_compute_day_sort, store=True, readonly=True)
    start_time = fields.Float(string='Start Time', required=True)
    end_time = fields.Float(string='End Time', required=True)
