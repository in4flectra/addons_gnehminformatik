# -*- coding: utf-8 -*-

from flectra import fields, models


class In4WebsiteBookingCalendarEvent(models.Model):

    _inherit = 'calendar.event'

    booking_email = fields.Char(string='Booking Email')
