# -*- coding: utf-8 -*-

from flectra import api, fields, models

from flectra.addons.base.ir.ir_mail_server import encode_rfc2822_address_header


class In4BaseIRMailServerIRMailServer(models.Model):

    _inherit = 'ir.mail_server'

    use_server_login_as_email_from = fields.Boolean(string='Use Mail Server Login as "Email From"', default=False)

    @api.model
    def send_email(self, message, mail_server_id=None, smtp_server=None, smtp_port=None, smtp_user=None,
                   smtp_password=None, smtp_encryption=None, smtp_debug=False, smtp_session=None):
        mail_server = None
        if mail_server_id:
            mail_server = self.sudo().browse(mail_server_id)
        elif not smtp_server:
            mail_server = self.sudo().search([], order='sequence', limit=1)
        if (mail_server) and (mail_server.use_server_login_as_email_from):
            if message['From']:
                if not message['Reply-To']:
                    message['Reply-To'] = message['From']
                del message['From']
            message['From'] = encode_rfc2822_address_header(mail_server.smtp_user)
            if message['Return-Path']:
                del message['Return-Path']
            message['Return-Path'] = mail_server.smtp_user
        return super(In4BaseIRMailServerIRMailServer, self).send_email(message, mail_server_id, smtp_server, smtp_port,
                                                                       smtp_user, smtp_password, smtp_encryption,
                                                                       smtp_debug, smtp_session)
