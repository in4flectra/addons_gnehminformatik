# -*- coding: utf-8 -*-
{
    'name': 'Outgoing Mail Servers (extended)',
    'summary': 'Extension of Outgoing Mail Servers',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Extra Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        'views/ir_mail_server.xml',
    ],
}
