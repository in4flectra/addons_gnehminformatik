# -*- coding: utf-8 -*-
{
    'name': 'Sync Contacts with Google',
    'summary': 'Synchronize the Contacts from Flectra with Peoples from Google',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Contacts',
    'version': '0.1',

    'depends': [
        'base',
        'contacts',
        'in4contacts',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/google_people_sync.xml',
        'views/res_partner.xml',
    ],

}
