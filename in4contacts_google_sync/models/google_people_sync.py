# -*- coding: utf-8 -*-

import base64
import os
import pickle
import pytz
import time
import urllib
import urllib.request

from datetime import datetime
from dateutil import parser
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from flectra import api, fields, models
from flectra.tools import config, DEFAULT_SERVER_DATETIME_FORMAT

# import logging
# _logger = logging.getLogger(__name__)

# cd ~/Flectra/flectra.gnehminformatik/google/people
# mkdir -p /home/sgn/.local/share/Flectra/filestore/gnehm-informatik.ch/in4contacts_google_sync/1
# cp token.pickle /home/sgn/.local/share/Flectra/filestore/gnehm-informatik.ch/in4contacts_google_sync/1/token.pickle

# Link to enable the People API: https://developers.google.com/people/quickstart/python
#SCOPES = 'https://www.googleapis.com/auth/contacts.readonly'
#SCOPES = 'https://www.googleapis.com/auth/contacts'

# If modifying these scopes, delete the file token.pickle.
SCOPES = 'https://www.googleapis.com/auth/contacts'


class In4ContactsGoogleSyncSettings(models.Model):

    _name = 'in4contacts_google_sync.settings'
    _description = 'Settings'
    _order = 'name'

    @api.depends('name')
    def compute_token(self):
        filestore = config.filestore(self._cr.dbname)
        for settings in self:
            token = filestore + '/in4contacts_google_sync/' + str(settings.id) + '/token.pickle'
            settings.token = token

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True)
    po_box_list = fields.Text(
        string='P.O. Box - Names',
        default='Postfach\nCase Postale\nCasella Postale\nPost Office Box\nPost-Office Box\nP.O. Box\nP.O.B\nPOB',
    )
    download_selection = fields.Selection(
        [
            ('individuals', 'Individuals'),
            ('companies', 'Companies'),
            ('individuals_companies', 'Individuals and Companies'),
            ('individuals_companies_contacts', 'Individuals, Companies and Contacts'),
        ],
        string='Download',
        default='individuals_companies_contacts',
        required=True,
    )
    upload_selection = fields.Selection(
        [
            ('manually_write', 'Save changes and upload new contacts manually'),
            ('auto_upload', 'Save changes and upload new contacts automatic'),
            ('auto_write', 'Save changes automatic and upload new contacts manually'),
        ],
        string='Upload',
        default='auto_write',
        required=True,
    )
    local_delete_type = fields.Selection(
        [
            ('none', 'Delete only the Google Contact'),
            ('delete', 'Delete Flectra Contact also'),
        ],
        string='Delete on Google',
        default='none',
        required=True,
    )
    remote_delete_type = fields.Selection(
        [
            ('none', 'Delete only in Flectra'),
            ('delete', 'Delete Google Contact also'),
            ('remove', 'Remove from the Sync Group / Add to the Private Group'),
        ],
        string='Delete on Flectra',
        default='remove',
        required=True,
    )

    token = fields.Char(string='Save Token to', compute=compute_token, store=True)

    flectra_group = fields.Char(string='Sync Group')
    private_group = fields.Char(string='Private Group', required=True, default='contactGroups/???')
    group_list = fields.Text(string='Existing Groups', readonly=True)

    download_timestamp = fields.Datetime(string='Last Download')

    @api.model
    def load_group_list(self):
        settings = None
        client = None

        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            settings = self.env['in4contacts_google_sync.settings'].browse(active_id)
            if settings and settings.token and os.path.exists(settings.token):
                with open(settings.token, 'rb') as token:
                    creds = pickle.load(token)
                if creds:
                    client = build('people', 'v1', credentials=creds)

        if settings and client:
            group_list = []
            next_page_token = ''
            while True:
                result = client.contactGroups().list(pageToken=next_page_token).execute()
                contact_groups = result.get('contactGroups', [])
                if not contact_groups:
                    break
                for contact_group in contact_groups:
                    if contact_group.get('groupType') == 'USER_CONTACT_GROUP':
                        group_list.append(contact_group.get('name') + ': ' + contact_group.get('resourceName'))
                next_page_token = result.get('nextPageToken', [])
                if not next_page_token:
                    break
            group_list.sort()
            group_list_text = ''
            for group in group_list:
                group_list_text += group + '\n'
            settings.sudo().write({'group_list': group_list_text})

    @api.model
    def _set_address(self, address_type, address, values, is_company):
        has_values = False
        if (   (address_type == 'unknown')
            or ((is_company) and (address_type == 'work'))
            or ((not is_company) and (address_type == 'home'))
        ):
            value = address.get('streetAddress')
            if values['street'] != value:
                values['street'] = value
                has_values = True
            value = address.get('extendedAddress')
            if not value:
                value = address.get('poBox')
            if values['street2'] != value:
                values['street2'] = value
                has_values = True
            value = address.get('postalCode')
            if values['zip'] != value:
                values['zip'] = value
                has_values = True
            value = address.get('city')
            if values['city'] != value:
                values['city'] = value
                has_values = True
            country_id = None
            if not country_id:
                country_code = address.get('countryCode')
                if country_code:
                    country_id = self.env['res.country'].search([('code', '=', country_code)], limit=1).id
            if not country_id:
                country_name = address.get('country')
                if country_name:
                    country_id = self.env['res.country'].search([('name', '=', country_name)], limit=1).id
            if not country_id:
                country_id = self.env.user.company_id.country_id.id
            if values['country_id'] != country_id:
                values['country_id'] = country_id
                has_values = True
        return has_values

    @api.model
    def _set_function(self, function_type, function, values, is_company_contact):
        has_values = False
        if (function_type == 'unknown') and is_company_contact:
            value = function.get('title')
            if values['function'] != value:
                values['function'] = value
        return has_values

    @api.model
    def _set_phone(self, phone_type, phone_number, values, is_company_or_contact):
        has_values = False
        if (   (phone_type == 'unknown')
            or ((is_company_or_contact) and (phone_type == 'work'))
            or ((not is_company_or_contact) and (phone_type == 'home'))
        ):
            value = phone_number.get('value')
            if values['phone'] != value:
                values['phone'] = value
                has_values = True
        if phone_type == 'mobile':
            value = phone_number.get('value')
            if values['mobile'] != value:
                values['mobile'] = value
                has_values = True
        return has_values

    @api.model
    def _set_email(self, email_type, email_address, values, is_company_or_contact):
        has_values = False
        if (   (email_type == 'unknown')
            or ((is_company_or_contact) and (email_type == 'work'))
            or ((not is_company_or_contact) and (email_type == 'home'))
        ):
            value = email_address.get('value')
            if values['email'] != value:
                values['email'] = value
                has_values = True
        return has_values

    @api.model
    def _set_webseite(self, webseite_type, webseite, values, is_company_or_contact):
        has_values = False
        if (   (webseite_type == 'unknown')
            or ((is_company_or_contact) and (webseite_type == 'work'))
            or ((not is_company_or_contact) and (webseite_type == 'homePage'))
        ):
            value = webseite.get('value')
            if value and (not value.startswith('http://')) and (not value.startswith('https://')):
                value = 'http://' + value
            if values['website'] != value:
                values['website'] = value
                has_values = True
        return has_values

    @api.model
    def _set_image(self, image_type, image, values):
        has_values = False
        if image_type == 'unknown':
            value = image.get('url')
            if value:
                value = base64.encodebytes(urllib.request.urlopen(value).read())
            if values['image'] != value:
                values['image'] = value
                has_values = True
        return has_values

    @api.model
    def _set_birthday(self, birthday_type, birthday, values):
        has_values = False
        if birthday_type == 'unknown':
            value = birthday.get('date')
            if value:
                try:
                    value = str(value.get('year')) + '-' + str(value.get('month')) + '-' + str(value.get('day'))
                    value = datetime.strptime(value, '%Y-%m-%d')
                except:
                    pass
            if values['date_of_birth'] != value:
                values['date_of_birth'] = value
                has_values = True
        return has_values

    @api.model
    def _set_comment(self, comment_type, comment, values):
        has_values = False
        if comment_type == 'unknown':
            value = comment.get('value')
            if values['comment'] != value:
                values['comment'] = value
                has_values = True
        return has_values

    @api.model
    def _set_group(self, group, values):
        has_values = False
        contact_group_id = group.get('contactGroupMembership').get('contactGroupId')
        if contact_group_id:
            domain = [('google_group', '=', 'contactGroups/' + contact_group_id)]
            category_id = self.env['res.partner.category'].search(domain, limit=1)
            if category_id:
                values['category_id'].append((4, category_id.id))
                has_values = True
        return has_values

    @api.model
    def _update_google_contact(self, settings, connection, values, with_empty_contacts):
        log_create = None
        log_ignore = None
        log_write = None

        values['google_download_timestamp'] = fields.datetime.now()

        is_company = values['is_company']
        is_contact = values['parent_id']
        is_company_contact = (not is_company) and is_contact
        is_company_or_contact = is_company or is_contact

        has_additional_values = False
        if values['parent_id']:
            domain = [
                ('parent_id', '=', False),
                ('google_settings_id', '=', settings.id),
                ('google_resource_name', '=', connection.get('resourceName')),
            ]
            partner_person_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
            if partner_person_id:
                values['partner_person_id'] = partner_person_id.id
        else:
            address_types = []
            addresses = connection.get('addresses', [])
            for address in addresses:
                address_type = address.get('type', 'unknown')
                if (    (address_type not in address_types)
                    and (   (address_type != 'unknown')
                         or ((is_company) and ('work' not in address_types))
                         or ((not is_company) and ('home' not in address_types))
                        )
                ):
                    address_types.append(address_type)
                    if self._set_address(address_type, address, values, is_company):
                        has_additional_values = True

        function_types = []
        functions = connection.get('organizations', [])
        for function in functions:
            function_type = 'unknown'
            if function_type not in function_types:
                function_types.append(function_type)
                self._set_function(function_type, function, values, is_company_contact)

        phone_types = []
        phone_numbers = connection.get('phoneNumbers', [])
        for phone_number in phone_numbers:
            phone_type = phone_number.get('type', 'unknown')
            if (    (phone_type not in phone_types)
                and (   (phone_type != 'unknown')
                     or ((is_company_or_contact) and ('work' not in phone_types))
                     or ((not is_company_or_contact) and ('home' not in phone_types))
                    )
            ):
                phone_types.append(phone_type)
                if self._set_phone(phone_type, phone_number, values, is_company_or_contact):
                    has_additional_values = True

        email_types = []
        email_addresses = connection.get('emailAddresses', [])
        for email_address in email_addresses:
            email_type = email_address.get('type', 'unknown')
            if (    (email_type not in email_types)
                and (   (email_type != 'unknown')
                     or ((is_company_or_contact) and ('work' not in email_types))
                     or ((not is_company_or_contact) and ('home' not in email_types))
                    )
            ):
                email_types.append(email_type)
                if self._set_email(email_type, email_address, values, is_company_or_contact):
                    has_additional_values = True

        webseite_types = []
        webseites = connection.get('urls', [])
        for webseite in webseites:
            webseite_type = webseite.get('type', 'unknown')
            if (    (webseite_type not in webseite_types)
                and (   (webseite_type != 'unknown')
                     or ((is_company_or_contact) and ('work' not in webseite_types))
                     or ((not is_company_or_contact) and ('homePage' not in webseite_types))
                    )
                and (webseite.get('metadata').get('source').get('type') == 'CONTACT')
            ):
                webseite_types.append(webseite_type)
                if self._set_webseite(webseite_type, webseite, values, is_company_or_contact):
                    has_additional_values = True

        image_types = []
        images = connection.get('photos', [])
        for image in images:
            image_type = 'unknown'
            if (    (image_type not in image_types)
                and (not image.get('default'))
                and (image.get('metadata').get('source').get('type') == 'CONTACT')
            ):
                image_types.append(image_type)
                self._set_image(image_type, image, values)

        birthday_types = []
        birthdays = connection.get('birthdays', [])
        for birthday in birthdays:
            birthday_type = 'unknown'
            if (    (birthday_type not in birthday_types)
                and (birthday.get('metadata').get('source').get('type') == 'CONTACT')
            ):
                birthday_types.append(birthday_type)
                self._set_birthday(birthday_type, birthday, values)

        comment_types = []
        comments = connection.get('biographies', [])
        for comment in comments:
            comment_type = 'unknown'
            if comment_type not in comment_types:
                comment_types.append(comment_type)
                self._set_comment(comment_type, comment, values)

        groups = connection.get('memberships', [])
        for group in groups:
            self._set_group(group, values)

        if values['parent_id']:
            domain = [
                '&',
                    ('parent_id', '=', values['parent_id']),
                    '|',
                        '&',
                            ('google_settings_id', '=', settings.id),
                            ('google_resource_name', '=', connection.get('resourceName')),
                        '&',
                            ('partner_person_id.google_settings_id', '=', settings.id),
                            ('partner_person_id.google_resource_name', '=', connection.get('resourceName')),
            ]
        else:
            domain = [
                ('parent_id', '=', False),
                ('google_settings_id', '=', settings.id),
                ('google_resource_name', '=', connection.get('resourceName')),
            ]
        local_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
        if local_partner_id:
            for category_id in local_partner_id.category_id:
                if category_id.google_group:
                    found = False
                    for new_category_id in values['category_id']:
                        if new_category_id[1] == category_id.id:
                            found = True
                    if not found:
                        values['category_id'].append((3, category_id.id))
            local_partner_id.write(values)
            log_write = local_partner_id.display_name
        else:
            if with_empty_contacts or has_additional_values:
                domain = [
                    ('parent_id', '!=', False),
                    ('google_settings_id', '=', settings.id),
                    ('google_resource_name', '=', connection.get('resourceName')),
                ]
                local_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
                if local_partner_id:
                    for category_id in local_partner_id.category_id:
                        if category_id.google_group:
                            found = False
                            for new_category_id in values['category_id']:
                                if new_category_id[1] == category_id.id:
                                    found = True
                            if not found:
                                values['category_id'].append((3, category_id.id))
                    values.pop('parent_id')
                    values.pop('is_company')
                    values.pop('lastname')
                    values.pop('firstname')
                    values.pop('image')
                    local_partner_id.write(values)
                    log_write = local_partner_id.display_name
                else:
                    values['google_settings_id'] = settings.id
                    values['google_resource_name'] = connection.get('resourceName')
                    install_mode = values.get('image') # install_mode=True --> no default image
                    local_partner_id = self.env['res.partner'].with_context(install_mode=install_mode).create(values)
                    log_create = local_partner_id.display_name
            else:
                if values['lastname'] and values['firstname']:
                    log_ignore = values['lastname'] + ' ' + values['firstname']
                elif values['lastname']:
                    log_ignore = values['lastname']
                elif values['firstname']:
                    log_ignore = values['firstname']
                else:
                    log_ignore = '? (unknown)'
        return log_create, log_ignore, log_write

    @api.model
    def _update_google_individual(self, settings, connection, person, has_organization, organization_partner_id):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': False,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            'image': None,
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': self.env.user.company_id.country_id.id,
            'function': None,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'date_of_birth': None,
            'comment': None,
            'category_id': [],
        }
        display_name = person.get('displayName')
        if display_name:
            value = person.get('familyName')
            if value:
                values['lastname'] = value
            value = person.get('givenName')
            if value:
                values['firstname'] = value
            if organization_partner_id:
                values['parent_id'] = organization_partner_id.id
                if values['lastname'] and (not values['firstname']):
                    selection = dict(organization_partner_id.fields_get(['type'])['type']['selection'])
                    if values['lastname'] == selection['invoice']:
                        values['type'] = 'invoice'
                        values['lastname'] = None
                    elif values['lastname'] == selection['delivery']:
                        values['type'] = 'delivery'
                        values['lastname'] = None
                    elif values['lastname'] == selection['other']:
                        values['type'] = 'other'
                        values['lastname'] = None
            with_empty_contacts = True
            if (    (has_organization)
                and (not organization_partner_id)
                and (settings.download_selection == 'individuals_companies_contacts')
            ):
                with_empty_contacts = False
            log_create, log_ignore, log_write = self._update_google_contact(settings, connection, values, with_empty_contacts)
            # if (display_name == 'Simon Gnehm'):
            #     _logger.info(display_name)
        return log_create, log_ignore, log_write

    @api.model
    def _update_google_company(self, settings, connection, organization):
        log_create = None
        log_ignore = None
        log_write = None

        values = {
            'is_company': True,
            'parent_id': None,
            'lastname': None,
            'firstname': None,
            'image': None,
            'street': None,
            'street2': None,
            'zip': None,
            'city': None,
            'country_id': self.env.user.company_id.country_id.id,
            'phone': None,
            'mobile': None,
            'email': None,
            'website': None,
            'date_of_birth': None,
            'comment': None,
            'category_id': [],
        }
        display_name = organization.get('name')
        if display_name:
            values['lastname'] = display_name
            log_create, log_ignore, log_write = self._update_google_contact(settings, connection, values, True)
            # if (display_name == 'Gnehm Informatik GmbH'):
            #     _logger.info(display_name)
        return log_create, log_ignore, log_write

    @api.model
    def _download_contacts(self, settings, client, only_contacts):
        added_list = []
        ignored_list = []
        changed_list = []
        missing_list = []

        next_page_token = ''
        while True:
            list = client.people().connections().list(
                resourceName='people/me',
                pageToken=next_page_token,
                personFields='addresses,biographies,birthdays,emailAddresses,memberships,metadata,names,organizations,phoneNumbers,photos,urls',
            ).execute()
            connections = list.get('connections', [])

            if not connections:
                break

            for connection in connections:
                need_update = True

                if need_update:
                    update_time = connection.get('metadata').get('sources')[0].get('updateTime')
                    if update_time and settings.download_timestamp:
                        last_download_str = settings.download_timestamp
                        last_download_datetime = datetime.strptime(last_download_str, DEFAULT_SERVER_DATETIME_FORMAT)
                        if parser.parse(update_time) < pytz.UTC.localize(last_download_datetime):
                            need_update = False

                if need_update:
                    flectra_group = None
                    in_flectra_group = True
                    if settings.flectra_group:
                        flectra_group = settings.flectra_group.split('/')[1]
                    if flectra_group:
                        in_flectra_group = False
                    private_group = settings.private_group.split('/')[1]
                    groups = connection.get('memberships', [])
                    for group in groups:
                        contact_group_id = group.get('contactGroupMembership').get('contactGroupId')
                        if contact_group_id:
                            if need_update and private_group and (contact_group_id == private_group):
                                need_update = False
                            if (not in_flectra_group) and flectra_group and (contact_group_id == flectra_group):
                                in_flectra_group = True
                    if not in_flectra_group:
                        need_update = False

                if need_update:
                    people_count = 0
                    if (    (people_count == 0)
                        and (not only_contacts)
                        and (settings.download_selection in [
                                'individuals',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        has_organization = False
                        organizations = connection.get('organizations', [])
                        for organization in organizations:
                            if organization.get('metadata').get('source').get('type') == 'CONTACT':
                                has_organization = True
                                break
                        persons = connection.get('names', [])
                        for person in persons:
                            if person.get('metadata').get('source').get('type') == 'CONTACT':
                                log_create, log_ignore, log_write = self._update_google_individual(
                                    settings,
                                    connection,
                                    person,
                                    has_organization,
                                    None,
                                )
                                if log_create:
                                    people_count += 1
                                    added_list.append(log_create)
                                if log_ignore:
                                    people_count += 1
                                    ignored_list.append(log_ignore)
                                if log_write:
                                    people_count += 1
                                    changed_list.append(log_write)
                            if people_count > 0:
                                break
                    if (    (people_count == 0)
                        and (settings.download_selection in [
                                'companies',
                                'individuals_companies',
                                'individuals_companies_contacts',
                            ])
                    ):
                        organizations = connection.get('organizations', [])
                        for organization in organizations:
                            if organization.get('metadata').get('source').get('type') == 'CONTACT':
                                if only_contacts:
                                    persons = connection.get('names', [])
                                    organization_name = organization.get('name')
                                    domain = [
                                        ('is_company', '=', True),
                                        ('parent_id', '=', False),
                                        ('name', '=', organization_name),
                                    ]
                                    organization_partner_id = self.env['res.partner'].with_context(active_test=False).search(domain, limit=1)
                                    if organization_partner_id:
                                        for person in persons:
                                            if person.get('metadata').get('source').get('type') == 'CONTACT':
                                                log_create, log_ignore, log_write = self._update_google_individual(
                                                    settings,
                                                    connection,
                                                    person,
                                                    True,
                                                    organization_partner_id,
                                                )
                                                if log_create:
                                                    people_count += 1
                                                    added_list.append(log_create)
                                                if log_ignore:
                                                    people_count += 1
                                                    ignored_list.append(log_ignore)
                                                if log_write:
                                                    people_count += 1
                                                    changed_list.append(log_write)
                                            if people_count > 0:
                                                break
                                    else:
                                        for person in persons:
                                            if person.get('metadata').get('source').get('type') == 'CONTACT':
                                                msg = person.get('displayName')
                                                missing_list.append(msg)
                                else:
                                    log_create, log_ignore, log_write = self._update_google_company(
                                        settings,
                                        connection,
                                        organization,
                                    )
                                    if log_create:
                                        people_count += 1
                                        added_list.append(log_create)
                                    if log_ignore:
                                        people_count += 1
                                        ignored_list.append(log_ignore)
                                    if log_write:
                                        people_count += 1
                                        changed_list.append(log_write)
                            if people_count > 0:
                                break

            next_page_token = list.get('nextPageToken', [])
            if not next_page_token:
                break

        return added_list, ignored_list, changed_list, missing_list

    @api.model
    def download_contacts_from_settings(self, id=None):
        settings = None
        client = None

        if id:
            settings = id
        else:
            context = dict(self._context or {})
            active_id = context.get('active_id', False)
            if active_id:
                settings = self.env['in4contacts_google_sync.settings'].browse(active_id)
        if settings and settings.token and os.path.exists(settings.token):
            with open(settings.token, 'rb') as token:
                creds = pickle.load(token)
            if creds:
                client = build('people', 'v1', credentials=creds)
        if settings and client:
            start_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            added_list = []
            ignored_list = []
            changed_list = []
            missing_list = []
            deleted_list = []
            archived_list = []

            # Private Adressen und Unternehmen
            log_create, log_ignore, log_write, log_missing = self._download_contacts(settings, client, False)
            added_list.extend(log_create)
            ignored_list.extend(log_ignore)
            changed_list.extend(log_write)
            missing_list.extend(log_missing)

            # Geschäftliche Adressen
            if settings.download_selection == 'individuals_companies_contacts':
                log_create, log_ignore, log_write, log_missing = self._download_contacts(settings, client, True)
                added_list.extend(log_create)
                ignored_list.extend(log_ignore)
                changed_list.extend(log_write)
                missing_list.extend(log_missing)

            domain = [
                ('parent_id', '!=', False),
                ('partner_person_id', '!=', False),
                ('google_settings_id', '!=', False),
                ('google_resource_name', '!=', False),
                ('google_download_timestamp', '>=', start_date),
            ]
            partners = self.env['res.partner'].with_context(active_test=False).search(domain)
            for partner in partners:
                if (    (partner.google_settings_id == partner.partner_person_id.google_settings_id)
                    and (partner.google_resource_name == partner.partner_person_id.google_resource_name)
                ):
                    partner.write({
                        'google_settings_id': None,
                        'google_resource_name': None,
                    })
                    partner.upload_google_contact_ok(partner.partner_person_id.google_settings_id)
                    partner.partner_person_id.upload_google_contact_ok(partner.partner_person_id.google_settings_id)

            domain = [
                '|',
                    '&',
                        ('google_download_timestamp', '=', False),
                        '|',
                            ('google_settings_id', '!=', False),
                            ('google_resource_name', '!=', False),
                    '&',
                        ('google_download_timestamp', '!=', False),
                        ('google_download_timestamp', '<', start_date),
            ]
            partners = self.env['res.partner'].with_context(active_test=False).search(domain)
            for partner in partners:
                if (    partner.google_settings_id
                    and partner.google_resource_name
                ):
                    delete_local_contact = False
                    try:
                        person = client.people().get(
                            resourceName = partner.google_resource_name,
                            personFields='memberships,metadata',
                        ).execute()
                    except HttpError as e:
                        person = None
                        delete_local_contact = e.resp.status == 404
                        pass
                    if (not delete_local_contact) and person:
                        private_group = settings.private_group.split('/')[1]
                        for group in person.get('memberships', []):
                            contact_group_id = group.get('contactGroupMembership').get('contactGroupId')
                            if contact_group_id and (contact_group_id == private_group):
                                delete_local_contact = True
                    if (not delete_local_contact) and person and settings.flectra_group:
                        delete_local_contact = True
                        flectra_group = settings.flectra_group.split('/')[1]
                        for group in person.get('memberships', []):
                            contact_group_id = group.get('contactGroupMembership').get('contactGroupId')
                            if contact_group_id and (contact_group_id == flectra_group):
                                delete_local_contact = False
                    if delete_local_contact:
                        if settings.local_delete_type == 'delete':
                            current_partner_name = partner.name
                            self.env.cr.autocommit(False)
                            try:
                                partner.unlink()
                                deleted_list.append(current_partner_name)
                            except:
                                self.env.cr.rollback()
                                values = {
                                    'active': False,
                                    'google_settings_id': None,
                                    'google_resource_name': None,
                                    'google_download_timestamp': None,
                                }
                                partner.write(values)
                                archived_list.append(current_partner_name)
                                pass
                        else:
                            partner.write({
                                'google_settings_id': None,
                                'google_resource_name': None,
                                'google_download_timestamp': None,
                            })

            added_list.sort()
            added_list_text = ''
            for log_create_text in added_list:
                added_list_text += log_create_text + '\n'

            ignored_list.sort()
            ignored_list_text = ''
            for log_ignore_text in ignored_list:
                ignored_list_text += log_ignore_text + '\n'

            changed_list.sort()
            changed_list_text = ''
            for log_write_text in changed_list:
                changed_list_text += log_write_text + '\n'

            missing_list.sort()
            missing_list_text = ''
            for log_missing_text in missing_list:
                missing_list_text += log_missing_text + '\n'

            deleted_list.sort()
            deleted_list_text = ''
            for log_deleted_text in deleted_list:
                deleted_list_text += log_deleted_text + '\n'

            archived_list.sort()
            archived_list_text = ''
            for log_archived_text in archived_list:
                archived_list_text += log_archived_text + '\n'

            settings.sudo().write({
                'download_timestamp': fields.datetime.now(),
            })

            end_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            summary = '''
Download started: %s
Download finished: %s

Added records: %d
Ignored records: %d
Changed records: %d
Missing companies: %d
Deleted records: %d
Archived records: %d
            ''' % (
                start_date,
                end_date,
                len(added_list),
                len(ignored_list),
                len(changed_list),
                len(missing_list),
                len(deleted_list),
                len(archived_list),
            )
            summary = summary.strip() + '\n\n'
            summary += 'Added records:\n' + added_list_text
            summary += '\n'
            summary += 'Ignored records:\n' + ignored_list_text
            summary += '\n'
            summary += 'Changed records:\n' + changed_list_text
            summary += '\n'
            summary += 'Missing companies:\n' + missing_list_text
            summary += '\n'
            summary += 'Deleted records:\n' + deleted_list_text
            summary += '\n'
            summary += 'Archived records:\n' + archived_list_text
            self.env['in4contacts_google_sync.request'].sudo().create({
                'name': 'Download report',
                'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'body': summary,
            })

    @api.model
    def download_contacts_all_settings(self):
        settings_list = self.env['in4contacts_google_sync.settings'].search([('active', '=', True)])
        for settings in settings_list:
            self.download_contacts_from_settings(settings)


class In4ContactsGoogleSyncRequest(models.Model):

    _name = 'in4contacts_google_sync.request'
    _description = 'Request'
    _order = 'date desc'

    name = fields.Char(string='Name', required=True)
    date = fields.Datetime(string='Date', required=True)
    body = fields.Text(string='Request', readonly=True)
