# -*- coding: utf-8 -*-

import os
import pickle

from datetime import datetime
from googleapiclient.discovery import build

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


class In4ContactsGoogleSyncResPartnerCategory(models.Model):

    _inherit = 'res.partner.category'

    google_group = fields.Char(string='Google Group')


class In4ContactsGoogleSyncResPartner(models.Model):

    _inherit = 'res.partner'

    google_settings_id = fields.Many2one('in4contacts_google_sync.settings', string='Settings', copy=False)
    google_resource_name = fields.Char(string='Resource Name', copy=False)
    google_download_timestamp = fields.Datetime(string='Last Download', copy=False)

    @api.model
    def _get_google_address_and_pobox(self):
        extended_address = ''
        pobox = ''
        if self.street2:
            extended_address = self.street2
            if self.google_settings_id and self.google_settings_id.po_box_list:
                street2_lower = self.street2.lower()
                po_box_lines = self.google_settings_id.po_box_list.split('\n')
                for po_box_line in po_box_lines:
                    if po_box_line.lower() in street2_lower:
                        pobox = self.street2
                        extended_address = ''
                        break
        return extended_address, pobox

    @api.model
    def _update_google_names(self, family_name, given_name, update_body, values=None):
        if (   (not values)
            or ('name' in values)
            or ('lastname' in values)
            or ('firstname' in values)
            or ('parent_id' in values)
        ):
            update_body.update({'names': [{
                'familyName': family_name or '',
                'givenName': given_name or '',
            }]})
            return 'names,'
        return ''

    @api.model
    def _update_google_orgs(self, organization, function, update_body, values=None):
        if (not values) or ('name' in values) or ('function' in values) or ('parent_id' in values):
            update_body.update({'organizations': [{
                'name': organization or '',
                'title': function or '',
            }]})
            return 'organizations,'
        return ''

    @api.model
    def _update_google_addresses(self, person, type, type_to_remove, update_body, values=None):
        result = ''
        if (   (not values)
            or ('street' in values)
            or ('street2' in values)
            or ('zip' in values)
            or ('city' in values)
            or ('country_id' in values)
        ):
            extended_address, pobox = self._get_google_address_and_pobox()
            addresses_new = [{
                'type': type,
                'streetAddress': self.street or '',
                'extendedAddress': extended_address,
                'poBox': pobox,
                'postalCode': self.zip or '',
                'city': self.city or '',
                'country': self.country_id.name or '',
                'countryCode': self.country_id.code or '',
            }]
            if person:
                addresses_old = person.get('addresses', [])
                for address_old in addresses_old:
                    address_type = address_old.get('type')
                    if (   (not address_type)
                        or (    (address_type != type)
                            and ((not type_to_remove) or (address_type != type_to_remove))
                           )
                    ):
                        addresses_new.append(address_old)
            update_body.update({'addresses': addresses_new})
            result = 'addresses,'
        elif person and type_to_remove:
            addresses_new = []
            addresses_old = person.get('addresses', [])
            for address_old in addresses_old:
                address_type = address_old.get('type')
                if (not address_type) or (address_type != type_to_remove):
                    addresses_new.append(address_old)
            if addresses_new != addresses_old:
                update_body.update({'addresses': addresses_new})
                result = 'addresses,'
        return result

    @api.model
    def _update_google_phones(self, person, type, type_to_remove, update_body, values=None):
        result = ''
        if (not values) or ('phone' in values) or ('mobile' in values):
            phones_new = [
                {'type': type, 'value': self.phone or ''},
                {'type': 'mobile', 'value': self.mobile or ''},
            ]
            if person:
                phones_old = person.get('phoneNumbers', [])
                for phone_old in phones_old:
                    phone_type = phone_old.get('type')
                    if (   (not phone_type)
                        or (    (phone_type != type)
                            and ((not type_to_remove) or (phone_type != type_to_remove))
                           )
                    ):
                        phones_new.append(phone_old)
            update_body.update({'phoneNumbers': phones_new})
            result = 'phoneNumbers,'
        elif person and type_to_remove:
            phones_new = []
            phones_old = person.get('phoneNumbers', [])
            for phone_old in phones_old:
                phone_type = phone_old.get('type')
                if (not phone_type) or (phone_type != type_to_remove):
                    phones_new.append(phone_old)
            if phones_new != phones_old:
                update_body.update({'phoneNumbers': phones_new})
                result = 'phoneNumbers,'
        return result

    @api.model
    def _update_google_emails(self, person, type, type_to_remove, update_body, values=None):
        result = ''
        if (not values) or ('email' in values):
            emails_new = [{'type': type, 'value': self.email or ''}]
            if person:
                emails_old = person.get('emailAddresses', [])
                for email_old in emails_old:
                    email_type = email_old.get('type')
                    if (   (not email_type)
                        or (    (email_type != type)
                            and ((not type_to_remove) or (email_type != type_to_remove))
                           )
                    ):
                        emails_new.append(email_old)
            update_body.update({'emailAddresses': emails_new})
            result = 'emailAddresses,'
        elif person and type_to_remove:
            emails_new = []
            emails_old = person.get('emailAddresses', [])
            for email_old in emails_old:
                email_type = email_old.get('type')
                if (not email_type) or (email_type != type_to_remove):
                    emails_new.append(email_old)
            if emails_new != emails_old:
                update_body.update({'emailAddresses': emails_new})
                result = 'emailAddresses,'
        return result

    @api.model
    def _update_google_websites(self, person, type, type_to_remove, update_body, values=None):
        result = ''
        if (not values) or ('website' in values):
            websites_new = [{'type': type, 'value': self.website or ''}]
            if person:
                websites_old = person.get('urls', [])
                for website_old in websites_old:
                    website_type = website_old.get('type')
                    if (   (not website_type)
                        or (    (website_type != type)
                            and ((not type_to_remove) or (website_type != type_to_remove))
                           )
                    ):
                        websites_new.append(website_old)
            update_body.update({'urls': websites_new})
            result = 'urls,'
        elif person and type_to_remove:
            websites_new = []
            websites_old = person.get('urls', [])
            for website_old in websites_old:
                website_type = website_old.get('type')
                if (not website_type) or (website_type != type_to_remove):
                    websites_new.append(website_old)
            if websites_new != websites_old:
                update_body.update({'urls': websites_new})
                result = 'urls,'
        return result

    @api.model
    def _update_google_birthdays(self, update_body, values=None):
        if (not values) or ('date_of_birth' in values):
            birthdays_new = []
            if self.date_of_birth:
                birthday = datetime.strptime(self.date_of_birth, DEFAULT_SERVER_DATE_FORMAT)
                birthdays_new = [{'date': {
                    'year': birthday.year,
                    'month': birthday.month,
                    'day': birthday.day,
                }}]
            update_body.update({'birthdays': birthdays_new})
            return 'birthdays,'
        return ''

    @api.model
    def _update_google_comments(self, update_body, values=None):
        if (not values) or ('comment' in values):
            update_body.update({'biographies': [{'value': self.comment or ''}]})
            return 'biographies,'
        return ''

    @api.model
    def _update_google_images(self, client, values=None):
        if (not values) or ('image' in values):
            try:
                if self.image:
                    client.people().updateContactPhoto(
                        resourceName=self.google_resource_name,
                        body={'photoBytes': self.image_medium.decode()},
                    ).execute()
                else:
                    client.people().deleteContactPhoto(
                        resourceName=self.google_resource_name,
                    ).execute()
            except Exception as e:
                pass
        return ''

    @api.model
    def _update_google_groups(self, client, person, flectra_group, values=None):
        if (not values) or ('category_id' in values):
            groups_new = []
            groups_old = person.get('memberships', [])
            for group_old in groups_old:
                contact_group_id = group_old.get('contactGroupMembership').get('contactGroupId')
                if contact_group_id:
                    domain = [('google_group', '=', 'contactGroups/' + contact_group_id)]
                    category_id = self.env['res.partner.category'].search(domain, limit=1)
                    if category_id and (category_id not in self.category_id):
                        client.contactGroups().members().modify(
                            resourceName='contactGroups/' + contact_group_id,
                            body={'resourceNamesToRemove': [self.google_resource_name]},
                        ).execute()
            for category_id in self.category_id:
                if category_id.google_group:
                    found = False
                    for group_old in groups_old:
                        contact_group_id = group_old.get('contactGroupMembership').get('contactGroupId')
                        if category_id.google_group == 'contactGroups/' + contact_group_id:
                            found = True
                    if not found:
                        groups_new.append(category_id.google_group)
                        client.contactGroups().members().modify(
                            resourceName=category_id.google_group,
                            body={'resourceNamesToAdd': [self.google_resource_name]},
                        ).execute()
            if flectra_group and (flectra_group not in groups_new):
                found = False
                for group_old in groups_old:
                    contact_group_id = group_old.get('contactGroupMembership').get('contactGroupId')
                    if flectra_group == 'contactGroups/' + contact_group_id:
                        found = True
                if not found:
                    client.contactGroups().members().modify(
                        resourceName=flectra_group,
                        body={'resourceNamesToAdd': [self.google_resource_name]},
                    ).execute()
        return ''

    @api.model
    def create(self, values):
        result = super(In4ContactsGoogleSyncResPartner, self).create(values)

        settings = self.env['in4contacts_google_sync.settings'].search([], limit=1)
        if settings and (settings.upload_selection == 'auto_upload'):
            result.upload_google_contact_ok(settings)
        return result

    @api.multi
    def write(self, values):
        result = super(In4ContactsGoogleSyncResPartner, self).write(values)

        try:
            if (    (self.google_settings_id)
                and (self.google_settings_id.upload_selection in ['auto_write', 'auto_upload'])
                and (self.google_resource_name)
                and (not 'google_download_timestamp' in values)
                and (  ('name' in values)
                    or ('street' in values)
                    or ('street2' in values)
                    or ('zip' in values)
                    or ('city' in values)
                    or ('state_id' in values)
                    or ('country_id' in values)
                    or ('function' in values)
                    or ('image' in values)
                    or ('phone' in values)
                    or ('mobile' in values)
                    or ('email' in values)
                    or ('website' in values)
                    or ('date_of_birth' in values)
                    or ('comment' in values)
                    or ('category_id' in values)
                    or ('parent_id' in values)
                    or ('child_ids' in values)
                )
            ):
                settings = self.google_settings_id
                client = None
                if settings.token and os.path.exists(settings.token):
                    with open(settings.token, 'rb') as token:
                        creds = pickle.load(token)
                    if creds:
                        client = build('people', 'v1', credentials=creds)
                if client:
                    person = client.people().get(
                        resourceName = self.google_resource_name,
                        personFields='addresses,biographies,birthdays,emailAddresses,memberships,metadata,names,organizations,phoneNumbers,urls',
                    ).execute()

                    # Company
                    if self.is_company:
                        update_fields = ''
                        update_body = {'etag': person.get('etag')}
                        update_fields += self._update_google_orgs(self.name, '', update_body, values)
                        update_fields += self._update_google_addresses(person, 'work', None, update_body, values)
                        update_fields += self._update_google_phones(person, 'work', None, update_body, values)
                        update_fields += self._update_google_emails(person, 'work', None, update_body, values)
                        update_fields += self._update_google_websites(person, 'work', None, update_body, values)
                        update_fields += self._update_google_comments(update_body, values)
                        update_fields += self._update_google_images(client, values)
                        update_fields += self._update_google_groups(client, person, settings.flectra_group, values)
                        if update_fields:
                            client.people().updateContact(
                                resourceName=self.google_resource_name,
                                updatePersonFields=update_fields,
                                body=update_body,
                            ).execute()

                        for child_id in self.child_ids:
                            if child_id.google_resource_name:
                                person = client.people().get(
                                    resourceName=child_id.google_resource_name,
                                    personFields='addresses,metadata',
                                ).execute()
                                update_fields = ''
                                update_body = {'etag': person.get('etag')}
                                update_fields += child_id._update_google_orgs(self.name, child_id.function, update_body, values)
                                update_fields += child_id._update_google_addresses(person, 'work', 'home', update_body, values)
                                if update_fields:
                                    client.people().updateContact(
                                        resourceName=child_id.google_resource_name,
                                        updatePersonFields=update_fields,
                                        body=update_body,
                                    ).execute()

                    # Person
                    else:
                        if self.parent_id:
                            update_fields = ''
                            update_body = {'etag': person.get('etag')}
                            update_fields += self._update_google_names(self.lastname, self.firstname, update_body, values)
                            update_fields += self._update_google_orgs(self.parent_id.name, self.function, update_body, values)
                            update_fields += self._update_google_addresses(person, 'work', 'home', update_body, values)
                            update_fields += self._update_google_phones(person, 'work', 'home', update_body, values)
                            update_fields += self._update_google_emails(person, 'work', 'home', update_body, values)
                            update_fields += self._update_google_websites(person, 'work', 'home', update_body, values)
                            update_fields += self._update_google_birthdays(update_body, values)
                            update_fields += self._update_google_comments(update_body, values)
                            update_fields += self._update_google_images(client, values)
                            update_fields += self._update_google_groups(client, person, settings.flectra_group, values)
                            if update_fields:
                                client.people().updateContact(
                                    resourceName=self.google_resource_name,
                                    updatePersonFields=update_fields,
                                    body=update_body,
                                ).execute()
                        else:
                            update_fields = ''
                            update_body = {'etag': person.get('etag')}
                            update_fields += self._update_google_names(self.lastname, self.firstname, update_body, values)
                            update_fields += self._update_google_orgs('', '', update_body, values)
                            update_fields += self._update_google_addresses(person, 'home', 'work', update_body, values)
                            update_fields += self._update_google_phones(person, 'home', 'work', update_body, values)
                            update_fields += self._update_google_emails(person, 'home', 'work', update_body, values)
                            update_fields += self._update_google_websites(person, 'home', 'work', update_body, values)
                            update_fields += self._update_google_birthdays(update_body, values)
                            update_fields += self._update_google_comments(update_body, values)
                            update_fields += self._update_google_images(client, values)
                            update_fields += self._update_google_groups(client, person, settings.flectra_group, values)
                            if update_fields:
                                client.people().updateContact(
                                    resourceName=self.google_resource_name,
                                    updatePersonFields=update_fields,
                                    body=update_body
                                ).execute()

        except Exception as e:
            pass
        return result

    @api.multi
    def unlink(self):
        for partner in self:
            if (    partner.google_settings_id
                and partner.google_resource_name
                and (partner.google_settings_id.remote_delete_type in ['delete', 'remove'])
            ):
                client = None
                if partner.google_settings_id.token and os.path.exists(partner.google_settings_id.token):
                    with open(partner.google_settings_id.token, 'rb') as token:
                        creds = pickle.load(token)
                    if creds:
                        client = build('people', 'v1', credentials=creds)
                if client:
                    if partner.google_settings_id.remote_delete_type == 'delete':
                        client.people().deleteContact(resourceName=partner.google_resource_name).execute()
                    else:
                        if partner.google_settings_id.flectra_group:
                            client.contactGroups().members().modify(
                                resourceName=partner.google_settings_id.flectra_group,
                                body={'resourceNamesToRemove': [partner.google_resource_name]},
                            ).execute()
                        else:
                            client.contactGroups().members().modify(
                                resourceName=partner.google_settings_id.private_group,
                                body={'resourceNamesToAdd': [partner.google_resource_name]},
                            ).execute()

        return super(In4ContactsGoogleSyncResPartner, self).unlink()

    @api.multi
    def upload_google_contacts(self):
        settings = self.env['in4contacts_google_sync.settings'].search([], limit=1)
        if settings:
            view_id = self.env['in4contacts_google_sync.upload_contact']
            new = view_id.create({'google_settings_id': settings.id})
            ctx = {'partner_ids': self._context.get('active_ids')}
            return {
                'type': 'ir.actions.act_window',
                'name': _('Confirmation'),
                'res_model': 'in4contacts_google_sync.upload_contact',
                'view_type': 'form',
                'view_mode': 'form',
                'res_id': new.id,
                'view_id': self.env.ref('in4contacts_google_sync.upload_contact_form', False).id,
                'target': 'new',
                'context': ctx,
            }

    @api.model
    def upload_google_contact_ok(self, settings):
        if (not self.google_settings_id) or (self.google_settings_id.id == settings.id):
            client = None
            if settings and settings.token and os.path.exists(settings.token):
                with open(settings.token, 'rb') as token:
                    creds = pickle.load(token)
                if creds:
                    client = build('people', 'v1', credentials=creds)
            if client:
                res = None
                person = None
                update_body = {}
                update_fields = ''
                if self.google_settings_id and self.google_resource_name:
                    try:
                        res = self.google_resource_name
                        person = client.people().get(
                            resourceName=res,
                            personFields='addresses,emailAddresses,metadata,memberships,phoneNumbers'
                        ).execute()
                        update_body = {'etag': person.get('etag')}
                    except Exception as e:
                        res = None
                        person = None
                        update_body = {}
                        pass
                if self.is_company:
                    update_fields += self._update_google_orgs(self.name, '', update_body)
                    update_fields += self._update_google_addresses(person, 'work', None, update_body)
                    update_fields += self._update_google_phones(person, 'work', None, update_body)
                    update_fields += self._update_google_emails(person, 'work', None, update_body)
                    update_fields += self._update_google_websites(person, 'work', None, update_body)
                    update_fields += self._update_google_comments(update_body)
                else:
                    if self.parent_id:
                        update_fields += self._update_google_names(self.lastname, self.firstname, update_body)
                        update_fields += self._update_google_orgs(self.parent_id.name, self.function, update_body)
                        update_fields += self._update_google_addresses(person, 'work', 'home', update_body)
                        update_fields += self._update_google_phones(person, 'work', 'home', update_body)
                        update_fields += self._update_google_emails(person, 'work', 'home', update_body)
                        update_fields += self._update_google_websites(person, 'work', 'home', update_body)
                        update_fields += self._update_google_birthdays(update_body)
                        update_fields += self._update_google_comments(update_body)
                    else:
                        update_fields += self._update_google_names(self.lastname, self.firstname, update_body)
                        update_fields += self._update_google_addresses(person, 'home', 'work', update_body)
                        update_fields += self._update_google_phones(person, 'home', 'work', update_body)
                        update_fields += self._update_google_emails(person, 'home', 'work', update_body)
                        update_fields += self._update_google_websites(person, 'home', 'work', update_body)
                        update_fields += self._update_google_birthdays(update_body)
                        update_fields += self._update_google_comments(update_body)
                if update_fields:
                    if person:
                        client.people().updateContact(
                            resourceName=res,
                            updatePersonFields=update_fields,
                            body=update_body,
                        ).execute()
                    else:
                        person = client.people().createContact(body=update_body).execute()
                        res = person.get('resourceName')
                    self.write({
                        'google_settings_id': settings.id,
                        'google_resource_name': res,
                    })
                    self._update_google_images(client)
                    self._update_google_groups(client, person, settings.flectra_group)


class In4ContactsGoogleSyncUploadContact(models.TransientModel):

    _name = 'in4contacts_google_sync.upload_contact'
    _description = 'Upload Contact'

    google_settings_id = fields.Many2one('in4contacts_google_sync.settings', string='Settings', required=True)

    @api.multi
    def upload_contact_ok(self):
        partners = self._context.get('partner_ids')
        for partner in self.env['res.partner'].search([('id', 'in', partners)]):
            partner.upload_google_contact_ok(self.google_settings_id)
