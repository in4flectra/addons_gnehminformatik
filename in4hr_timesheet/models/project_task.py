# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4HRTimesheetProjectTask(models.Model):

    _inherit = 'project.task'

    @api.depends('stage_id', 'planned_hours', 'timesheet_ids.unit_amount',
                 'child_ids.stage_id', 'child_ids.planned_hours', 'child_ids.timesheet_ids.unit_amount')
    def _hours_get(self):
        super(In4HRTimesheetProjectTask, self)._hours_get()
        for task in self.sorted(key='id', reverse=True):
            children_planned_hours = 0
            children_effective_hours = 0
            children_remaining_hours = 0
            children_exceeded_hours = 0
            for child_task in task.child_ids:
                children_planned_hours += child_task.planned_hours_total
                children_effective_hours += child_task.effective_hours_total
                children_remaining_hours += child_task.remaining_hours_task
                children_exceeded_hours += child_task.exceeded_hours_task

            task.planned_hours_task = task.planned_hours
            task.effective_hours_task = task.effective_hours
            if task.planned_hours_task >= task.effective_hours_task:
                task.remaining_hours_task = task.planned_hours_task - task.effective_hours_task
                task.exceeded_hours_task = 0
            else:
                task.remaining_hours_task = 0
                task.exceeded_hours_task = task.effective_hours_task - task.planned_hours_task

            task.planned_hours_subtasks = children_planned_hours
            task.effective_hours_subtasks = children_effective_hours
            task.remaining_hours_subtasks = children_remaining_hours
            task.exceeded_hours_subtasks = children_exceeded_hours

            task.planned_hours_total = task.planned_hours_task + task.planned_hours_subtasks
            task.effective_hours_total = task.effective_hours_task + task.effective_hours_subtasks
            task.remaining_hours_total = task.remaining_hours_task + task.remaining_hours_subtasks
            task.exceeded_hours_total = task.exceeded_hours_task + task.exceeded_hours_subtasks

            if task.stage_id and task.stage_id.fold:
                task.progress = 100.0
            elif task.planned_hours_total > 0.0:
                task.progress = round(100.0 * task.effective_hours_total / task.planned_hours_total, 2)
            else:
                task.progress = 0.0

    planned_hours_task = fields.Float(string='Remaining Hours Task', compute=_hours_get, store=True)
    effective_hours_task = fields.Float(string='Effective Hours Task', compute=_hours_get, store=True)
    remaining_hours_task = fields.Float(string='Remaining Hours Task', compute=_hours_get, store=True)
    exceeded_hours_task = fields.Float(string='Exceeded Hours Task', compute=_hours_get, store=True)

    planned_hours_subtasks = fields.Float(string='Remaining Hours Sub-Tasks', compute=_hours_get, store=True)
    effective_hours_subtasks = fields.Float(string='Effective Hours Sub-Tasks', compute=_hours_get, store=True)
    remaining_hours_subtasks = fields.Float(string='Remaining Hours Sub-Tasks', compute=_hours_get, store=True)
    exceeded_hours_subtasks = fields.Float(string='Exceeded Hours Sub-Tasks', compute=_hours_get, store=True)

    planned_hours_total = fields.Float(string='Remaining Hours Total', compute=_hours_get, store=True)
    effective_hours_total = fields.Float(string='Effective Hours Total', compute=_hours_get, store=True)
    remaining_hours_total = fields.Float(string='Remaining Hours Total', compute=_hours_get, store=True)
    exceeded_hours_total = fields.Float(string='Exceeded Hours Total', compute=_hours_get, store=True)
