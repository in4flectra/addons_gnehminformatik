# -*- coding: utf-8 -*-
{
    'name': 'Timesheets (extended)',
    'summary': 'Extension of review and approve employees time reports',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.1',

    'depends': [
        'hr_timesheet',
    ],

    'data': [
        'views/project_task.xml',
    ],
}
