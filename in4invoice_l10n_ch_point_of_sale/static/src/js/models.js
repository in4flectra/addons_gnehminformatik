flectra.define('in4invoice_l10n_ch_point_of_sale.models', function (require) {
    'use strict';

    var models = require('point_of_sale.models');

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({

        print_additional_reports: function(order, order_server_id) {
            var self = this;
            _super_posmodel.print_additional_reports.apply(this, arguments);
            if (order.get_total_with_tax() != 0.0) {
                setTimeout(function () {
                    self.chrome.do_action('in4invoice_l10n_ch_point_of_sale.pos_invoice_isr_report', {additional_context: {
                        active_ids: order_server_id,
                    }});
                }, 1000);
            }
        },

    });

});
