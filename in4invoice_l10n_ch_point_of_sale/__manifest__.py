# -*- coding: utf-8 -*-
{
    'name': 'Swiss Accounting and Point of Sale',
    'summary': 'Printing ISR from Point of Sale',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'account',
        'in4invoice_l10n_ch',
        'l10n_ch',
        'point_of_sale',
        'sale',
    ],

    'data': [
        'views/assets.xml',
        'views/account_invoice_view.xml',
        'views/point_of_sale_report.xml',
        'views/report_invoice.xml',
    ],

    'auto_install': True,
}
