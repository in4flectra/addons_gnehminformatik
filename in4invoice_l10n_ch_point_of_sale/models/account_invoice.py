# -*- coding: utf-8 -*-

from flectra import api, fields, models
from flectra.tools import float_is_zero


class In4InvoiceL10NCHPointOfSaleAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.depends('pos_order_ids.session_id.state')
    def _compute_pos_order_session_opened(self):
        for invoice in self:
            is_opened = False
            for pos_order_id in invoice.pos_order_ids:
                if pos_order_id.session_id.state == 'opened':
                    is_opened = True
            invoice.pos_order_session_opened = is_opened

    pos_order_ids = fields.One2many('pos.order', 'invoice_id', string='Point of Sale Orders')
    pos_order_session_opened = fields.Boolean(
        string='Point of Sale Session is open',
        compute=_compute_pos_order_session_opened,
    )

    def _get_pos_for_amount_residual(self):
        self.ensure_one()
        result = []
        for pos_order_id in self.sudo().pos_order_ids:
            if pos_order_id.session_id.state == 'opened':
                for statement_id in pos_order_id.statement_ids:
                    result.append(statement_id)
        return result

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.currency_id', 'move_id.line_ids.amount_residual',
        'pos_order_ids.statement_ids.currency_id', 'pos_order_ids.statement_ids.amount',
    )
    def _compute_residual(self):
        super(In4InvoiceL10NCHPointOfSaleAccountInvoice, self)._compute_residual()
        if (self.state == 'open') and (self.reconciled == False):
            residual = self.residual
            for line in self._get_pos_for_amount_residual():
                residual -= line.amount
            self.residual = residual
            digits_rounding_precision = self.currency_id.rounding
            if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
                self.reconciled = True
            else:
                self.reconciled = False
