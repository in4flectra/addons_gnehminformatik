# -*- coding: utf-8 -*-

import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4HRHREmployee(models.Model):

    _inherit = 'hr.employee'

    manager = fields.Boolean(string='Is a Manager')
