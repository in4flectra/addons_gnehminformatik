# -*- coding: utf-8 -*-
{
    'name': 'Employee Directory (extended)',
    'summary': 'Extension of Jobs, Departments, Employees Details',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.1',

    'depends': [
        'hr',
    ],

    'data': [
        'views/hr_employee.xml',
    ],

    'post_init_hook': 'post_init_hook',
}
