# -*- coding: utf-8 -*-

from flectra import http, _
from flectra.http import request
from flectra.osv.expression import OR

from flectra.addons.portal.controllers.portal import CustomerPortal


class CustomerPortal(CustomerPortal):

    def _get_project_domain(self):
        user = request.env.user
        return [
            '|',
                ('user_id', '=', user.id),
                '|',
                    ('message_partner_ids', '=', user.partner_id.id),
                    '|',
                        ('message_partner_ids', '=', user.partner_id.commercial_partner_id.id),
                        ('task_ids', 'child_of', request.env['project.task'].search(self._get_task_domain()).ids),
        ]

    def _get_task_domain(self):
        user = request.env.user
        return [
            '&',
                '|',
                    ('stage_id', '=', False),
                    ('stage_id.fold', '=', False),
                '|',
                    ('message_partner_ids', '=', user.partner_id.id),
                    '|',
                        ('message_partner_ids', '=', user.partner_id.commercial_partner_id.id),
                        '|',
                            ('user_id', '=', user.id),
                            '|',
                                ('parent_id.user_id', '=', user.id),
                                ('project_id.user_id', '=', user.id),
        ]

    def _get_project_inputs(self):
        values = super(CustomerPortal, self)._get_project_inputs()
        values.update({
            'description': {'input': 'description', 'label': _('Description'), 'sequence': 25},
        })
        return values

    def _get_project_search_domain(self, search_in, search):
        search_domain = super(CustomerPortal, self)._get_project_search_domain(search_in, search)
        if search_in and search:
            if search_in in ('all', 'description'):
                search_domain = OR([search_domain, [('description', 'ilike', search)]])
        return search_domain

    @http.route(['/my/task/<int:task_id>'], type='http', auth='user', website=True)
    def portal_my_task(self, task_id=None, kanban_state=None, **kw):
        task = request.env['project.task'].sudo().browse(task_id)
        if kanban_state and (task.kanban_state != kanban_state):
            task.sudo().write({'kanban_state': kanban_state})

        return super(CustomerPortal, self).portal_my_task(task_id, **kw)
