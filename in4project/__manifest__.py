# -*- coding: utf-8 -*-
{
    'name': 'Project (extended)',
    'summary': 'Extension of Projects and Tasks',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Project',
    'version': '0.3',

    'depends': [
        'in4base',
        'mail',
        'portal',
        'project',
    ],

    'data': [
        # security
        'security/in4project.xml',
        'security/ir.model.access.csv',

        # views
        'views/assets.xml',
        'views/mail_activity.xml',
        'views/project_portal.xml',
        'views/project_project.xml',
        'views/project_tags.xml',
        'views/project_task.xml',
    ],

    'pre_init_hook': 'pre_init_hook',
    'post_init_hook': 'post_init_hook',
}
