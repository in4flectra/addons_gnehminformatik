# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM res_groups
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'res.groups'
                         AND module = 'project'
                         AND name IN ('group_project_user', 'group_project_manager')
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'res.groups'
           AND module = 'project'
           AND name IN ('group_project_user', 'group_project_manager')
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'project'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'project'
               ;
    """)
