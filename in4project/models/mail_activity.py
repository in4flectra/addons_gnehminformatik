# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ProjectMailActivity(models.Model):

    _inherit = 'mail.activity'

    @api.depends('res_model', 'res_id')
    def _compute_project_ids(self):
        for activity_id in self:
            if activity_id.res_model == 'project.task':
                task_id = self.env['project.task'].with_context(active_test=False).browse(activity_id.res_id)
                if task_id:
                    activity_id.project_id = task_id.project_id
                    activity_id.task_id = task_id.id
                else:
                    activity_id.project_id = None
                    activity_id.task_id = None
            elif activity_id.res_model == 'project.project':
                activity_id.project_id = activity_id.res_id
                activity_id.task_id = None

    project_id = fields.Many2one('project.project', string='Project', compute=_compute_project_ids, store=True)
    task_id = fields.Many2one('project.task', string='Task', compute=_compute_project_ids, store=True)
