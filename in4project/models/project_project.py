# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ProjectProjectProject(models.Model):

    _name = 'project.project'
    _inherit = ['project.project', 'mail.activity.mixin']

    def _compute_attachment_ids(self):
        for project in self:
            domain = [
                '|',
                    '&', ('res_model', '=', 'project.project'), ('res_id', '=', project.id),
                    '&', ('res_model', '=', 'project.task'), ('res_id', 'in', project.task_ids.ids)
            ]
            project.attachment_ids = self.env['ir.attachment'].search(domain).ids

    description = fields.Html(string='Description')
    privacy_visibility = fields.Selection(default='followers')
    attachment_ids = fields.One2many('ir.attachment', string='Documents', compute=_compute_attachment_ids)

    @api.model
    def default_task_type_ids(self):
        return self.env['project.task.type'].search([('project_default', '=', True)]).ids

    task_type_ids = fields.Many2many('project.task.type', 'project_task_type_rel', 'project_id', 'type_id', string='Task Stages', default=default_task_type_ids)

    @api.model
    def _get_message_ids_changed(self, field_name, ids_message_old, ids_message_new, ids_message_subtype_old, ids_message_subtype_new):
        ids_message_add = []
        ids_message_changed = []
        ids_message_del = []

        for id_message_old in ids_message_old:
            if id_message_old not in ids_message_new:
                ids_message_del.append(id_message_old)
        for id_message_new in ids_message_new:
            if id_message_new not in ids_message_old:
                ids_message_add.append(id_message_new)

        for id_message_subtype_old in ids_message_subtype_old:
            for id_message_subtype_new in ids_message_subtype_new:
                if id_message_subtype_new[field_name] == id_message_subtype_old[field_name]:
                    for id_subtype_old in id_message_subtype_old['subtype_ids']:
                        if (    (id_subtype_old not in id_message_subtype_new['subtype_ids'])
                            and (id_message_subtype_old[field_name] not in ids_message_changed)
                        ):
                            ids_message_changed.append(id_message_subtype_old[field_name])
                    for id_subtype_new in id_message_subtype_new['subtype_ids']:
                        if (    (id_subtype_new not in id_message_subtype_old['subtype_ids'])
                            and (id_message_subtype_new[field_name] not in ids_message_changed)
                        ):
                            ids_message_changed.append(id_message_subtype_new[field_name])

        return ids_message_add, ids_message_changed, ids_message_del

    @api.multi
    def write(self, vals):
        ids_channel_old = []
        ids_channel_new = []
        ids_channel_subtype_old = []
        ids_channel_subtype_new = []
        ids_partner_old = []
        ids_partner_new = []
        ids_partner_subtype_old = []
        ids_partner_subtype_new = []
        if 'message_follower_ids' in vals:
            for follower_id in self.message_follower_ids:
                if follower_id.channel_id:
                    ids_channel_old.append(follower_id.channel_id.id)
                    ids_channel_subtype_old.append({
                        'channel_id': follower_id.channel_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })
                if follower_id.partner_id:
                    ids_partner_old.append(follower_id.partner_id.id)
                    ids_partner_subtype_old.append({
                        'partner_id': follower_id.partner_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })

        result = super(In4ProjectProjectProject, self).write(vals)

        if 'message_follower_ids' in vals:
            for follower_id in self.message_follower_ids:
                if follower_id.channel_id:
                    ids_channel_new.append(follower_id.channel_id.id)
                    ids_channel_subtype_new.append({
                        'channel_id': follower_id.channel_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })
                if follower_id.partner_id:
                    ids_partner_new.append(follower_id.partner_id.id)
                    ids_partner_subtype_new.append({
                        'partner_id': follower_id.partner_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })

        ids_channel_add, ids_channel_changed, ids_channel_del = self._get_message_ids_changed(
            'channel_id',
            ids_channel_old,
            ids_channel_new,
            ids_channel_subtype_old,
            ids_channel_subtype_new,
        )

        ids_partner_add, ids_partner_changed, ids_partner_del = self._get_message_ids_changed(
            'partner_id',
            ids_partner_old,
            ids_partner_new,
            ids_partner_subtype_old,
            ids_partner_subtype_new,
        )

        if (    ids_channel_add or ids_channel_changed or ids_channel_del
             or ids_partner_add or ids_partner_changed or ids_partner_del
        ):
            tasks = self.env['project.task'].with_context(active_test=False).search([('project_id', '=', self.id)])
            for task in tasks:
                if ids_channel_del:
                    task.message_unsubscribe(channel_ids=ids_channel_del)
                if ids_partner_del:
                    task.message_unsubscribe(partner_ids=ids_partner_del)

                if ids_channel_changed:
                    for follower_id in self.message_follower_ids:
                        if follower_id.channel_id.id in ids_channel_changed:
                            for task_follower_id in task.message_follower_ids:
                                if task_follower_id.channel_id.id == follower_id.channel_id.id:
                                    task.message_unsubscribe(channel_ids=[follower_id.channel_id.id])
                                    task.message_subscribe(
                                        channel_ids=[follower_id.channel_id.id],
                                        subtype_ids=follower_id.subtype_ids.ids,
                                    )
                if ids_partner_changed:
                    for follower_id in self.message_follower_ids:
                        if follower_id.partner_id.id in ids_partner_changed:
                            for task_follower_id in task.message_follower_ids:
                                if task_follower_id.partner_id.id == follower_id.partner_id.id:
                                    task.message_unsubscribe(partner_ids=[follower_id.partner_id.id])
                                    task.message_subscribe(
                                        partner_ids=[follower_id.partner_id.id],
                                        subtype_ids=follower_id.subtype_ids.ids,
                                    )

                if ids_channel_add:
                    for follower_id in self.message_follower_ids:
                        if follower_id.channel_id.id in ids_channel_add:
                            task.message_subscribe(
                                channel_ids=[follower_id.channel_id.id],
                                subtype_ids=follower_id.subtype_ids.ids,
                            )
                if ids_partner_add:
                    for follower_id in self.message_follower_ids:
                        if follower_id.partner_id.id in ids_partner_add:
                            task.message_subscribe(
                                partner_ids=[follower_id.partner_id.id],
                                subtype_ids=follower_id.subtype_ids.ids,
                            )

        return result
