# -*- coding: utf-8 -*-

from datetime import timedelta, date

from flectra import api, fields, models, _


class In4ProjectProjectTaskType(models.Model):

    _inherit = 'project.task.type'

    project_default = fields.Boolean(string='Default for Projects', default=True, oldname='projeckt_default')


class In4ProjectProjectTask(models.Model):

    _inherit = 'project.task'
    _order = 'date_deadline asc, priority_kanban desc, project_id, parent_id, name'

    @api.depends('priority')
    def _compute_priority_kanban(self):
        priority_dict = {
            'l': '1',
            'm': '2',
            'h': '3',
        }
        for task in self:
            if task.priority:
                task.priority_kanban = priority_dict[task.priority]
            else:
                task.priority_kanban = '0'

    @api.multi
    def _inverse_priority_kanban(self):
        priority_kanban_dict = {
            '0': None,
            '1': 'l',
            '2': 'm',
            '3': 'h'
        }
        for task in self:
            if task.priority_kanban:
                task.priority = priority_kanban_dict[task.priority_kanban]
            else:
                task.priority = None

    @api.one
    @api.depends('name', 'parent_id.name')
    def _compute_display_name(self):
        names = [self.parent_id.name, self.name]
        self.display_name = ': '.join(filter(None, names))

    def _get_attachment_domain(self):
        return [
            '|',
                '&', ('res_model', '=', 'project.task'), ('res_id', '=', self.id),
                '&', ('res_model', '=', 'project.task'), ('res_id', 'in', self.child_ids.ids)
        ]

    def _compute_attachment_ids(self):
        for task in self:
            task.attachment_ids = self.env['ir.attachment'].search(task._get_attachment_domain()).ids

    def _compute_attachment_count(self):
        for task in self:
            task.attachment_count = self.env['ir.attachment'].search_count(task._get_attachment_domain())

    @api.one
    def _compute_favorite_user_id(self):
        for task in self:
            if self.env.uid != task.user_id.id:
                task.favorite00_user_id = self.env.uid
            else:
                task.favorite00_user_id = None

            domain = [('res_model', '=', 'project.task'), ('res_id', '=', task.id)]
            follower_ids = self.env['mail.followers'].search(domain)
            ids_partner = []
            for follower_id in follower_ids:
                ids_partner.append(follower_id.partner_id.id)
            domain = [('id', 'not in', [self.env.uid, task.user_id.id]), ('partner_id', 'in', ids_partner)]
            user_ids = self.env['res.users'].search(domain)

            if len(user_ids) > 0:
                task.favorite01_user_id = user_ids[0].id
            else:
                task.favorite01_user_id = None
            if len(user_ids) > 1:
                task.favorite02_user_id = user_ids[1].id
            else:
                task.favorite02_user_id = None
            if len(user_ids) > 2:
                task.favorite03_user_id = user_ids[2].id
            else:
                task.favorite03_user_id = None
            if len(user_ids) > 3:
                task.favorite04_user_id = user_ids[3].id
            else:
                task.favorite04_user_id = None
            if len(user_ids) > 4:
                task.favorite05_user_id = user_ids[4].id
            else:
                task.favorite05_user_id = None
            if len(user_ids) > 5:
                task.favorite06_user_id = user_ids[5].id
            else:
                task.favorite06_user_id = None
            if len(user_ids) > 6:
                task.favorite07_user_id = user_ids[6].id
            else:
                task.favorite07_user_id = None
            if len(user_ids) > 7:
                task.favorite08_user_id = user_ids[7].id
            else:
                task.favorite08_user_id = None
            if len(user_ids) > 8:
                task.favorite09_user_id = user_ids[8].id
            else:
                task.favorite09_user_id = None
            if len(user_ids) > 9:
                task.favorite10_user_id = user_ids[9].id
            else:
                task.favorite10_user_id = None

    @api.multi
    def _compute_required_tasks_count(self):
        for task in self:
            task.required_tasks_count = self.search_count([('id', 'child_of', task.required_task_ids.ids)])

    @api.multi
    def _compute_dependent_tasks_count(self):
        for task in self:
            task.dependent_tasks_count = self.search_count([('id', 'child_of', task.dependent_task_ids.ids)])

    display_name = fields.Char(string='Display Name', compute=_compute_display_name, store=True, index=True)
    priority = fields.Selection(default=None)
    priority_kanban = fields.Selection([
        ('0', 'None'),
        ('1', 'Low'),
        ('2', 'Medium'),
        ('3', 'High')
        ], string='Priority for Kanban', compute=_compute_priority_kanban, store=True, inverse=_inverse_priority_kanban)

    attachment_ids = fields.One2many('ir.attachment', string='Documents', compute=_compute_attachment_ids)
    attachment_count = fields.Integer(string='Number of documents attached', compute=_compute_attachment_count)

    favorite00_user_id = fields.Many2one('res.users', string='Favorite User Self', compute=_compute_favorite_user_id)
    favorite01_user_id = fields.Many2one('res.users', string='Favorite User 1', compute=_compute_favorite_user_id)
    favorite02_user_id = fields.Many2one('res.users', string='Favorite User 2', compute=_compute_favorite_user_id)
    favorite03_user_id = fields.Many2one('res.users', string='Favorite User 3', compute=_compute_favorite_user_id)
    favorite04_user_id = fields.Many2one('res.users', string='Favorite User 4', compute=_compute_favorite_user_id)
    favorite05_user_id = fields.Many2one('res.users', string='Favorite User 5', compute=_compute_favorite_user_id)
    favorite06_user_id = fields.Many2one('res.users', string='Favorite User 6', compute=_compute_favorite_user_id)
    favorite07_user_id = fields.Many2one('res.users', string='Favorite User 7', compute=_compute_favorite_user_id)
    favorite08_user_id = fields.Many2one('res.users', string='Favorite User 8', compute=_compute_favorite_user_id)
    favorite09_user_id = fields.Many2one('res.users', string='Favorite User 9', compute=_compute_favorite_user_id)
    favorite10_user_id = fields.Many2one('res.users', string='Favorite User 10', compute=_compute_favorite_user_id)

    required_task_ids = fields.Many2many('project.task', 'project_task_required_to_dependent_rel', 'dependent_task_id', 'required_task_id', string='Required Tasks')
    required_tasks_count = fields.Integer(string='Required Tasks Count', compute=_compute_required_tasks_count)
    dependent_task_ids = fields.Many2many('project.task', 'project_task_required_to_dependent_rel', 'required_task_id', 'dependent_task_id', string='Dependent Tasks')
    dependent_tasks_count = fields.Integer(string='Dependent Tasks Count', compute=_compute_dependent_tasks_count)

    @api.onchange('project_id', 'priority')
    def task_deadline(self):
        date_deadline = None
        if self.project_id and self.priority:
            days = 0
            if self.priority == 'l':
                days = int(self.project_id.low)
            elif self.priority == 'm':
                days = int(self.project_id.medium)
            elif self.priority == 'h':
                days = int(self.project_id.high)
            if days > 0:
                date_deadline = date.today() + timedelta(days)
        self.update({'date_deadline': date_deadline})

    @api.onchange('parent_id')
    def _onchange_parent_id(self):
        if self.parent_id:
            self.partner_id = self.parent_id.partner_id
        else:
            self.partner_id = None

    @api.multi
    def name_get(self):
        result = []
        for task in self:
            task_name = []
            if task.sudo().parent_id:
                task_name.append(task.parent_id.name)
            task_name.append(task.sudo().name)
            result.append((task.id, ': '.join(task_name)))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if args is None:
            args = []
        tasks = self.browse()
        if name:
            tasks = self.search([('display_name', operator, name)] + args, limit=limit)
        if tasks:
            return tasks.name_get()
        return super(In4ProjectProjectTask, self).name_search(name, args=args, operator=operator, limit=limit)

    @api.model
    def _get_message_ids_changed(self, field_name, ids_message_old, ids_message_new, ids_message_subtype_old, ids_message_subtype_new):
        ids_message_add = []
        ids_message_changed = []
        ids_message_del = []

        for id_message_old in ids_message_old:
            if id_message_old not in ids_message_new:
                ids_message_del.append(id_message_old)
        for id_message_new in ids_message_new:
            if id_message_new not in ids_message_old:
                ids_message_add.append(id_message_new)

        for id_message_subtype_old in ids_message_subtype_old:
            for id_message_subtype_new in ids_message_subtype_new:
                if id_message_subtype_new[field_name] == id_message_subtype_old[field_name]:
                    for id_subtype_old in id_message_subtype_old['subtype_ids']:
                        if (    (id_subtype_old not in id_message_subtype_new['subtype_ids'])
                            and (id_message_subtype_old[field_name] not in ids_message_changed)
                        ):
                            ids_message_changed.append(id_message_subtype_old[field_name])
                    for id_subtype_new in id_message_subtype_new['subtype_ids']:
                        if (    (id_subtype_new not in id_message_subtype_old['subtype_ids'])
                            and (id_message_subtype_new[field_name] not in ids_message_changed)
                        ):
                            ids_message_changed.append(id_message_subtype_new[field_name])

        return ids_message_add, ids_message_changed, ids_message_del

    @api.model
    def create(self, values):
        result = super(In4ProjectProjectTask, self).create(values)

        if result.parent_id:
            for task_follower_id in result.message_follower_ids:
                if task_follower_id.channel_id:
                    result.message_unsubscribe(channel_ids=[task_follower_id.channel_id.id])
                if task_follower_id.partner_id:
                    result.message_unsubscribe(partner_ids=[task_follower_id.partner_id.id])
            user_is_follower = False
            for parent_follower_id in result.parent_id.message_follower_ids:
                if parent_follower_id.channel_id:
                    result.message_subscribe(
                        channel_ids=[parent_follower_id.channel_id.id],
                        subtype_ids=parent_follower_id.subtype_ids.ids,
                    )
                if parent_follower_id.partner_id:
                    result.message_subscribe(
                        partner_ids=[parent_follower_id.partner_id.id],
                        subtype_ids=parent_follower_id.subtype_ids.ids,
                    )
                    if parent_follower_id.partner_id.id == result.user_id.partner_id.id:
                        user_is_follower = True
            if not user_is_follower:
                result.message_subscribe(
                    partner_ids=[result.user_id.partner_id.id],
                )

        return result

    @api.multi
    def write(self, values):
        ids_channel_old = []
        ids_channel_new = []
        ids_channel_subtype_old = []
        ids_channel_subtype_new = []
        ids_partner_old = []
        ids_partner_new = []
        ids_partner_subtype_old = []
        ids_partner_subtype_new = []
        if (    ('parent_id' not in values)
            and ('project_id' not in values)
            and ('message_follower_ids' in values)
        ):
            for follower_id in self.message_follower_ids:
                if follower_id.channel_id:
                    ids_channel_old.append(follower_id.channel_id.id)
                    ids_channel_subtype_old.append({
                        'channel_id': follower_id.channel_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })
                if follower_id.partner_id:
                    ids_partner_old.append(follower_id.partner_id.id)
                    ids_partner_subtype_old.append({
                        'partner_id': follower_id.partner_id.id,
                        'subtype_ids': follower_id.subtype_ids.ids,
                    })

        result = super(In4ProjectProjectTask, self).write(values)

        if ('partner_id' in values) and (self.partner_id):
            for child_task_id in self.child_ids:
                child_task_id.partner_id = self.partner_id

        if 'project_id' in values:
            for activity_id in self.activity_ids:
                activity_id.project_id = self.project_id

        if ('parent_id' in values) or ('project_id' in values):
            if self.parent_id or self.project_id:
                for task_follower_id in self.message_follower_ids:
                    self.message_unsubscribe(partner_ids=[task_follower_id.partner_id.id])
            if self.parent_id:
                for parent_follower_id in self.parent_id.message_follower_ids:
                    self.message_subscribe(
                        partner_ids=[parent_follower_id.partner_id.id],
                        subtype_ids=parent_follower_id.subtype_ids.ids,
                    )
            elif self.project_id:
                for project_follower_id in self.project_id.message_follower_ids:
                    self.message_subscribe(
                        partner_ids=[project_follower_id.partner_id.id],
                        subtype_ids=project_follower_id.subtype_ids.ids,
                    )
        else:
            if 'message_follower_ids' in values:
                for follower_id in self.message_follower_ids:
                    if follower_id.channel_id:
                        ids_channel_new.append(follower_id.channel_id.id)
                        ids_channel_subtype_new.append({
                            'channel_id': follower_id.channel_id.id,
                            'subtype_ids': follower_id.subtype_ids.ids,
                        })
                    if follower_id.partner_id:
                        ids_partner_new.append(follower_id.partner_id.id)
                        ids_partner_subtype_new.append({
                            'partner_id': follower_id.partner_id.id,
                            'subtype_ids': follower_id.subtype_ids.ids,
                        })

            ids_channel_add, ids_channel_changed, ids_channel_del = self._get_message_ids_changed(
                'channel_id',
                ids_channel_old,
                ids_channel_new,
                ids_channel_subtype_old,
                ids_channel_subtype_new,
            )

            ids_partner_add, ids_partner_changed, ids_partner_del = self._get_message_ids_changed(
                'partner_id',
                ids_partner_old,
                ids_partner_new,
                ids_partner_subtype_old,
                ids_partner_subtype_new,
            )

            if (    ids_channel_add or ids_channel_changed or ids_channel_del
                 or ids_partner_add or ids_partner_changed or ids_partner_del
            ):
                tasks = self.env['project.task'].with_context(active_test=False).search([('parent_id', '=', self.id)])
                for task in tasks:
                    if ids_channel_del:
                        task.message_unsubscribe(channel_ids=ids_channel_del)
                    if ids_partner_del:
                        task.message_unsubscribe(partner_ids=ids_partner_del)

                    if ids_channel_changed:
                        for follower_id in self.message_follower_ids:
                            if follower_id.channel_id.id in ids_channel_changed:
                                for task_follower_id in task.message_follower_ids:
                                    if task_follower_id.channel_id.id == follower_id.channel_id.id:
                                        task.message_unsubscribe(channel_ids=[follower_id.channel_id.id])
                                        task.message_subscribe(
                                            channel_ids=[follower_id.channel_id.id],
                                            subtype_ids=follower_id.subtype_ids.ids,
                                        )
                    if ids_partner_changed:
                        for follower_id in self.message_follower_ids:
                            if follower_id.partner_id.id in ids_partner_changed:
                                for task_follower_id in task.message_follower_ids:
                                    if task_follower_id.partner_id.id == follower_id.partner_id.id:
                                        task.message_unsubscribe(partner_ids=[follower_id.partner_id.id])
                                        task.message_subscribe(
                                            partner_ids=[follower_id.partner_id.id],
                                            subtype_ids=follower_id.subtype_ids.ids,
                                        )

                    if ids_channel_add:
                        for follower_id in self.message_follower_ids:
                            if follower_id.channel_id.id in ids_channel_add:
                                task.message_subscribe(
                                    channel_ids=[follower_id.channel_id.id],
                                    subtype_ids=follower_id.subtype_ids.ids,
                                )
                    if ids_partner_add:
                        for follower_id in self.message_follower_ids:
                            if follower_id.partner_id.id in ids_partner_add:
                                task.message_subscribe(
                                    partner_ids=[follower_id.partner_id.id],
                                    subtype_ids=follower_id.subtype_ids.ids,
                                )

        return result

    @api.multi
    def project_task_required_tasks_action(self):
        self.ensure_one()
        action = self.env.ref('in4project.project_task_required_tasks_action').read()[0]
        action['domain'] = [('id', 'child_of', self.required_task_ids.ids)]
        return action

    @api.multi
    def project_task_dependent_tasks_action(self):
        self.ensure_one()
        action = self.env.ref('in4project.project_task_dependent_tasks_action').read()[0]
        action['domain'] = [('id', 'child_of', self.dependent_task_ids.ids)]
        return action

    @api.multi
    def attachment_tree_view(self):
        self.ensure_one()
        return {
            'name': _('Documents'),
            'domain': self._get_attachment_domain(),
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'kanban,tree,form',
            'view_type': 'form',
            'limit': 80,
            'context': "{'default_res_model': '%s','default_res_id': %d}" % (self._name, self.id)
        }

    def action_assign_to_favorite01(self):
        self.write({'user_id': self.favorite01_user_id.id})

    def action_assign_to_favorite02(self):
        self.write({'user_id': self.favorite02_user_id.id})

    def action_assign_to_favorite03(self):
        self.write({'user_id': self.favorite03_user_id.id})

    def action_assign_to_favorite04(self):
        self.write({'user_id': self.favorite04_user_id.id})

    def action_assign_to_favorite05(self):
        self.write({'user_id': self.favorite05_user_id.id})

    def action_assign_to_favorite06(self):
        self.write({'user_id': self.favorite06_user_id.id})

    def action_assign_to_favorite07(self):
        self.write({'user_id': self.favorite07_user_id.id})

    def action_assign_to_favorite08(self):
        self.write({'user_id': self.favorite08_user_id.id})

    def action_assign_to_favorite09(self):
        self.write({'user_id': self.favorite08_user_id.id})

    def action_assign_to_favorite10(self):
        self.write({'user_id': self.favorite10_user_id.id})
