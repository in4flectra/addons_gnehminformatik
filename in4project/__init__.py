# -*- coding: utf-8 -*-

from . import controllers
from . import models

from flectra import api, SUPERUSER_ID


def pre_init_hook(cr):
    cr.execute("""
        DELETE
          FROM res_groups
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'res.groups'
                         AND module = 'project'
                         AND name IN ('group_project_user', 'group_project_manager')
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'res.groups'
           AND module = 'project'
           AND name IN ('group_project_user', 'group_project_manager')
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'project'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'project'
               ;
    """)


def post_init_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    project_ids = env['project.project'].search([])
    for project_id in project_ids:
        if project_id.privacy_visibility != 'followers':
            project_id.privacy_visibility = 'followers'
