# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (extended): Switzerland - Accounting and Payment Slip',
    'summary': 'Extension of Sales, Invoices and Payments: Swiss Accounting and Print Payment Slip',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4invoice_l10n_ch',
        'l10n_ch_payment_slip',
    ],

    'data': [
        'views/account_invoice.xml',
        'views/res_partner.xml',
    ],

    'auto_install': True,
}
