# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_ui_menu_group_rel
         WHERE ir_ui_menu_group_rel.menu_id IN (   SELECT menu_item.id
                                                     FROM ir_ui_menu AS menu_item
                                                LEFT JOIN ir_ui_menu AS parent_menu ON parent_menu.id = menu_item.parent_id
                                                LEFT JOIN ir_ui_menu AS parent_parent_menu ON parent_parent_menu.id = parent_menu.parent_id
                                                    WHERE menu_item.name = 'Helpdesk'
                                                       OR parent_menu.name = 'Helpdesk'
                                                       OR parent_parent_menu.name = 'Helpdesk'
                                               )
               ;
    """)
