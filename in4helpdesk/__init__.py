# -*- coding: utf-8 -*-

from . import models

from flectra import api, SUPERUSER_ID, tools


def pre_init_hook(cr):
    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'helpdesk_basic'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'helpdesk_basic'
               ;
    """)

    env = api.Environment(cr, SUPERUSER_ID, {})

    issue_type_ids = env['issue.type'].search([('reporting_template', '!=', False)])
    for issue_type_id in issue_type_ids:
        issue_type_id.write({'reporting_template': tools.plaintext2html(issue_type_id.reporting_template)})

    helpdesk_ticket_ids = env['helpdesk.ticket'].search([('description', '!=', False)])
    for helpdesk_ticket_id in helpdesk_ticket_ids:
        helpdesk_ticket_id.write({'description': tools.plaintext2html(helpdesk_ticket_id.description)})

    helpdesk_ticket_ids = env['helpdesk.ticket'].search([('assigned_to_id', '!=', False)])
    for helpdesk_ticket_id in helpdesk_ticket_ids:
        helpdesk_ticket_id.write({'user_id': helpdesk_ticket_id.assigned_to_id.id})

    helpdesk_ticket_ids = env['helpdesk.ticket'].search([('partner_id', '!=', False)])
    for helpdesk_ticket_id in helpdesk_ticket_ids:
        helpdesk_ticket_id.write({'user_id': None})
