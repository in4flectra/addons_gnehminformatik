# -*- coding: utf-8 -*-
{
    'name': 'Helpdesk (extended)',
    'summary': 'Extension of Helpdesk',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Helpdesk',
    'version': '0.5',

    'depends': [
        'base',
        'helpdesk_basic',
        'in4base',
        'mail',
    ],

    'data': [
        # security
        'security/in4helpdesk.xml',
        'security/ir.model.access.csv',

        # views
        'views/helpdesk_report.xml',
        'views/helpdesk_stage.xml',
        'views/helpdesk_tag.xml',
        'views/helpdesk_team.xml',
        'views/helpdesk_ticket.xml',
        'views/issue_type.xml',
        'views/mail_activity.xml',
    ],

    'pre_init_hook': 'pre_init_hook',
}
