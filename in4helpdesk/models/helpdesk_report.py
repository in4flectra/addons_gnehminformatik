# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4HelpdeskHelpdeskReport(models.Model):

    _inherit = 'helpdesk.report'

    assigned_to_id = fields.Many2one(invisible=True, store=False)
    start_date = fields.Date()
    end_date = fields.Date()
    description = fields.Html()

    @api.model
    def fields_get(self, fields=None):
        fields_to_hide = ['assigned_to_id']
        res = super(In4HelpdeskHelpdeskReport, self).fields_get()
        for field in fields_to_hide:
            res[field]['selectable'] = False # funktioniert nicht für 'Group by', mit 'store=False' gelöst
            res[field]['exportable'] = False
        return res
