# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4HelpdeskMailActivity(models.Model):

    _inherit = 'mail.activity'

    @api.depends('res_model', 'res_id')
    def _compute_ticket_ids(self):
        for activity_id in self:
            if activity_id.res_model == 'helpdesk.ticket':
                activity_id.ticket_id = activity_id.res_id

    ticket_id = fields.Many2one('helpdesk.ticket', string='Ticket', compute=_compute_ticket_ids, store=True)
