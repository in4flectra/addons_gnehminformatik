# -*- coding: utf-8 -*-

from flectra import fields, models


class In4HelpdeskIssueType(models.Model):

    _inherit = 'issue.type'

    reporting_template = fields.Html(translate=True)
