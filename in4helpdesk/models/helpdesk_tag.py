# -*- coding: utf-8 -*-

from flectra import api, fields, models, _
from flectra.exceptions import ValidationError


class In4HelpdeskHelpdeskTag(models.Model):

    _inherit = 'helpdesk.tag'
    _order = 'full_name'

    @api.depends('name', 'parent_id')
    def _compute_full_name(self):
        for current_id in self:
            current_id.full_name = current_id.get_full_name()

    active = fields.Boolean(string='Active', default=True, copy=False)
    parent_id = fields.Many2one('helpdesk.tag', string='Parent Tag', index=True, ondelete='cascade')
    child_ids = fields.One2many('helpdesk.tag', 'parent_id', string='Child Tags')
    full_name = fields.Char(string='Full Name', compute=_compute_full_name, store=True, index=True)
    color = fields.Integer(string='Color Index', default=10)

    _sql_constraints = [
        ('name_uniq', 'unique (full_name)', 'Tag name already exists !'),
    ]

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('Error ! You can not create recursive tags.'))

    @api.multi
    def name_get(self):
        if self._context.get('helpdesk_tag_display') == 'short':
            return super(In4HelpdeskHelpdeskTag, self).name_get()

        res = []
        for tag_id in self:
            names = []
            current_id = tag_id
            while current_id:
                names.append(current_id.name)
                current_id = current_id.parent_id
            res.append((tag_id.id, ' / '.join(reversed(names))))
        return res

    @api.model
    def get_full_name(self):
        result = self.name
        current_id = self.parent_id
        while current_id:
            result = current_id.name + ' / ' + result
            current_id = current_id.parent_id
        return result

    @api.multi
    def write(self, values):
        result = super(In4HelpdeskHelpdeskTag, self).write(values)
        if ('name' in values) or ('full_name' in values) or ('parent_id' in values):
            for child_id in self.child_ids:
                child_id.full_name = child_id.get_full_name()
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = [('full_name', operator, name)]
        ids = self.search(domain + args, limit=limit)
        return ids.name_get()
