# -*- coding: utf-8 -*-

from flectra import fields, models


class In4HelpdeskHelpdeskStage(models.Model):

    _inherit = 'helpdesk.stage'

    team_ids = fields.Many2many('helpdesk.team', string='Teams')
