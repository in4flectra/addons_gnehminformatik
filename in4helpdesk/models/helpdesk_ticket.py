# -*- coding: utf-8 -*-

from datetime import date

from flectra import api, fields, models


class In4HelpdeskHelpdeskTicket(models.Model):

    _inherit = 'helpdesk.ticket'
    _order = 'date_deadline asc, priority_kanban desc, name'

    @api.depends('priority')
    def _compute_priority_kanban(self):
        priority_dict = {
            '1': '1',
            '2': '2',
            '3': '3',
        }
        for ticket_id in self:
            if ticket_id.priority:
                ticket_id.priority_kanban = priority_dict[ticket_id.priority]
            else:
                ticket_id.priority_kanban = '0'

    @api.multi
    def _inverse_priority_kanban(self):
        priority_kanban_dict = {
            '0': None,
            '1': '1',
            '2': '2',
            '3': '3'
        }
        for ticket_id in self:
            if ticket_id.priority_kanban:
                ticket_id.priority = priority_kanban_dict[ticket_id.priority_kanban]
            else:
                ticket_id.priority = None

    @api.one
    @api.depends('stage_id')
    def _compute_actual_end_date(self):
        if self.stage_id and self.stage_id.stage_type == 'done':
            self.actual_end_date = date.today()

    @api.one
    def _compute_favorite_user_id(self):
        for ticket_id in self:
            if self.env.uid != ticket_id.user_id.id:
                ticket_id.favorite00_user_id = self.env.uid
            else:
                ticket_id.favorite00_user_id = None

            domain = [('res_model', '=', 'helpdesk.ticket'), ('res_id', '=', ticket_id.id)]
            follower_ids = self.env['mail.followers'].search(domain)
            ids_partner = []
            for follower_id in follower_ids:
                ids_partner.append(follower_id.partner_id.id)
            if ticket_id.team_id:
                for member_id in ticket_id.team_id.member_ids:
                    ids_partner.append(member_id.partner_id.id)

            domain = [('id', 'not in', [self.env.uid, ticket_id.user_id.id]), ('partner_id', 'in', ids_partner)]
            user_ids = self.env['res.users'].search(domain)

            if len(user_ids) > 0:
                ticket_id.favorite01_user_id = user_ids[0].id
            else:
                ticket_id.favorite01_user_id = None
            if len(user_ids) > 1:
                ticket_id.favorite02_user_id = user_ids[1].id
            else:
                ticket_id.favorite02_user_id = None
            if len(user_ids) > 2:
                ticket_id.favorite03_user_id = user_ids[2].id
            else:
                ticket_id.favorite03_user_id = None
            if len(user_ids) > 3:
                ticket_id.favorite04_user_id = user_ids[3].id
            else:
                ticket_id.favorite04_user_id = None
            if len(user_ids) > 4:
                ticket_id.favorite05_user_id = user_ids[4].id
            else:
                ticket_id.favorite05_user_id = None
            if len(user_ids) > 5:
                ticket_id.favorite06_user_id = user_ids[5].id
            else:
                ticket_id.favorite06_user_id = None
            if len(user_ids) > 6:
                ticket_id.favorite07_user_id = user_ids[6].id
            else:
                ticket_id.favorite07_user_id = None
            if len(user_ids) > 7:
                ticket_id.favorite08_user_id = user_ids[7].id
            else:
                ticket_id.favorite08_user_id = None
            if len(user_ids) > 8:
                ticket_id.favorite09_user_id = user_ids[8].id
            else:
                ticket_id.favorite09_user_id = None
            if len(user_ids) > 9:
                ticket_id.favorite10_user_id = user_ids[9].id
            else:
                ticket_id.favorite10_user_id = None

    name = fields.Char(required=True, index=True, translate=False, track_visibility='always')
    ticket_seq = fields.Char(default=lambda self: self.env['ir.sequence'].next_by_code('helpdesk.ticket') or '/', track_visibility='onchange')
    priority = fields.Selection(default=None, index=True)
    priority_kanban = fields.Selection(
        [('0', 'None'), ('1', 'Low'), ('2', 'Medium'), ('3', 'High')],
        string='Priority for Kanban',
        compute=_compute_priority_kanban,
        inverse=_inverse_priority_kanban,
        store = True,
    )
    user_id = fields.Many2one(default=None, track_visibility='always')
    assigned_to_id = fields.Many2one(invisible=True, store=False)
    date_deadline = fields.Date(string='Deadline', index=True, copy=False)
    start_date = fields.Date(default=None)
    end_date = fields.Date(default=None)
    actual_end_date = fields.Date(
        string='Actual End Date',
        compute=_compute_actual_end_date,
        store=True,
        track_visibility='onchange'
    )
    description = fields.Html(translate=False)
    stage_id = fields.Many2one(track_visibility='always')

    favorite00_user_id = fields.Many2one('res.users', string='Favorite User Self', compute=_compute_favorite_user_id)
    favorite01_user_id = fields.Many2one('res.users', string='Favorite User 1', compute=_compute_favorite_user_id)
    favorite02_user_id = fields.Many2one('res.users', string='Favorite User 2', compute=_compute_favorite_user_id)
    favorite03_user_id = fields.Many2one('res.users', string='Favorite User 3', compute=_compute_favorite_user_id)
    favorite04_user_id = fields.Many2one('res.users', string='Favorite User 4', compute=_compute_favorite_user_id)
    favorite05_user_id = fields.Many2one('res.users', string='Favorite User 5', compute=_compute_favorite_user_id)
    favorite06_user_id = fields.Many2one('res.users', string='Favorite User 6', compute=_compute_favorite_user_id)
    favorite07_user_id = fields.Many2one('res.users', string='Favorite User 7', compute=_compute_favorite_user_id)
    favorite08_user_id = fields.Many2one('res.users', string='Favorite User 8', compute=_compute_favorite_user_id)
    favorite09_user_id = fields.Many2one('res.users', string='Favorite User 9', compute=_compute_favorite_user_id)
    favorite10_user_id = fields.Many2one('res.users', string='Favorite User 10', compute=_compute_favorite_user_id)

    @api.model
    def fields_get(self, fields=None):
        fields_to_hide = ['assigned_to_id']
        res = super(In4HelpdeskHelpdeskTicket, self).fields_get()
        for field in fields_to_hide:
            res[field]['selectable'] = False # funktioniert nicht für 'Group by', mit 'store=False' gelöst
            res[field]['exportable'] = False
        return res

    @api.onchange('user_id')
    def onchange_user_id(self):
        return None

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        return None

    def action_assign_to_me(self):
        self.write({'user_id': self.env.user.id})

    def action_assign_to_favorite01(self):
        self.write({'user_id': self.favorite01_user_id.id})

    def action_assign_to_favorite02(self):
        self.write({'user_id': self.favorite02_user_id.id})

    def action_assign_to_favorite03(self):
        self.write({'user_id': self.favorite03_user_id.id})

    def action_assign_to_favorite04(self):
        self.write({'user_id': self.favorite04_user_id.id})

    def action_assign_to_favorite05(self):
        self.write({'user_id': self.favorite05_user_id.id})

    def action_assign_to_favorite06(self):
        self.write({'user_id': self.favorite06_user_id.id})

    def action_assign_to_favorite07(self):
        self.write({'user_id': self.favorite07_user_id.id})

    def action_assign_to_favorite08(self):
        self.write({'user_id': self.favorite08_user_id.id})

    def action_assign_to_favorite09(self):
        self.write({'user_id': self.favorite08_user_id.id})

    def action_assign_to_favorite10(self):
        self.write({'user_id': self.favorite10_user_id.id})
