# -*- coding: utf-8 -*-
{
    'name': 'Swiss Localization',
    'summary': 'Extension for Swiss Localization',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'base',
        'base_address_city',
        'in4base',
        'l10n_ch',
        'web',
    ],

    'data': [
        # views
        'views/res_bank.xml',
        'views/res_city.xml',
        'views/res_company.xml',
        'views/res_country.xml',
        'views/res_partner.xml',

        # reports
        'views/report_templates.xml',
    ],
}
