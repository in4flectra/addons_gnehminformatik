# -*- coding: utf-8 -*-

import base64
import csv
import io
import sys

from flectra import api, fields, models

swiss_bank_kind = [
    (1, 'Headquarters'),
    (2, 'Head-End'),
    (3, 'Branch')
]


class In4SwissResBank(models.Model):

    _inherit = 'res.bank'

    @api.depends('zip', 'city', 'state')
    def _compute_city_line(self):
        for bank_id in self:
            city_line = ''
            if bank_id.zip:
                city_line += bank_id.zip
                if bank_id.city or bank_id.state:
                    city_line += ' '
            if bank_id.city:
                city_line += bank_id.city
                if bank_id.state:
                    city_line += ' '
            if bank_id.state:
                city_line += bank_id.state.name
            bank_id.city_line = city_line

    city_line = fields.Char(string='City Line', compute=_compute_city_line)
    swiss_kind = fields.Selection(swiss_bank_kind, string='Kind')
    headquarters_bank_id = fields.Many2one('res.bank', string='Headquarters')
    bank_no = fields.Char(string='Bank No')
    branch_no = fields.Char(string='Branch No')

    @api.multi
    @api.depends('name', 'city')
    def name_get(self):
        result = []
        for bank in self:
            name = bank.name + (bank.city and (', ' + bank.city) or '')
            result.append((bank.id, name))
        return result


class In4SwissResBankSwissBankImport(models.TransientModel):

    _name = 'res.bank.swiss_bank_import'
    _description = 'Import Swiss Banks'

    file = fields.Binary(string='File to import', required=True)

    @api.multi
    def import_swiss_banks(self):
        file = io.StringIO(base64.decodebytes(self.file).decode('iso-8859-1'))
        reader = csv.reader(file)
        csv.field_size_limit(sys.maxsize)

        domain = [('code', '=', 'CH')]
        ch_country_id = self.env['res.country'].search(domain, limit=1)

        domain = [('country', '=', ch_country_id.id)]
        bank_ids = self.env['res.bank'].search(domain)
        for bank_id in bank_ids:
            bank_id.write({'active': False})

        for row in reader:
            bank_no = row[0][2:7].strip()
            branch_no = row[0][7:11].strip()
            bank_no_new = row[0][11:16].strip()
            bank_name = row[0][54:114].strip()
            if ((len(bank_no_new) == 0) and (len(bank_name) > 0)):
                # bank_sic_no = row[0][16:22].strip()
                bank_no_head = row[0][22:27].strip()
                swiss_kind = int(row[0][27:28])

                id_headquarters_bank = None
                if (len(bank_no_head) > 0):
                    domain = [('swiss_kind', '=', 1), ('bank_no', '=', bank_no_head)]
                    headquarters_bank_id = self.env['res.bank'].search(domain, limit=1)
                    if (headquarters_bank_id):
                        id_headquarters_bank = headquarters_bank_id.id

                country_id = None
                country_code = row[0][270:272].strip()
                if (len(country_code) > 0):
                    domain = [('code', '=', country_code)]
                    country_id = self.env['res.country'].search(domain, limit=1)
                if (not country_id):
                    country_id = ch_country_id

                postal_chf = row[0][272:284].strip().replace('*', '').replace('-', '')
                if (len(postal_chf) >= 4): # xx-x-x => xxxx
                    postal_chf = postal_chf[:2] + '0' * (9 - len(postal_chf)) + postal_chf[2:]

                domain = [('bank_no', '=', bank_no), ('branch_no', '=', branch_no)]
                bank_id = self.env['res.bank'].with_context(active_test=False).search(domain, limit=1)
                if (bank_id):
                    if (   (bank_id.name != bank_name)
                        or (bank_id.swiss_kind != swiss_kind)
                        or (bank_id.headquarters_bank_id != id_headquarters_bank)
                        or (bank_id.street != row[0][114:149].strip())
                        or (bank_id.street2 != row[0][149:184].strip())
                        or (bank_id.zip != row[0][184:194].strip())
                        or (bank_id.city != row[0][194:229].strip())
                        or (bank_id.phone != (row[0][265:270].strip() + ' ' + row[0][229:247].strip()).strip())
                        or (bank_id.country != country_id.id)
                        or (bank_id.l10n_ch_postal_chf != postal_chf)
                        or (bank_id.bic != row[0][284:298].strip())
                    ):
                        bank_id.write({
                            'name': bank_name,
                            'swiss_kind': swiss_kind,
                            'headquarters_bank_id': id_headquarters_bank,
                            'street': row[0][114:149].strip(),
                            'street2': row[0][149:184].strip(),
                            'zip': row[0][184:194].strip(),
                            'city': row[0][194:229].strip(),
                            'phone': (row[0][265:270].strip() + ' ' + row[0][229:247].strip()).strip(),
                            'country': country_id.id,
                            'l10n_ch_postal_chf': postal_chf,
                            'bic': row[0][284:298].strip(),
                            'active': True,
                        })
                    else:
                        bank_id.write({
                            'active': True,
                        })
                else:
                    self.env['res.bank'].create({
                        'name': bank_name,
                        'swiss_kind': swiss_kind,
                        'headquarters_bank_id': id_headquarters_bank,
                        'bank_no': bank_no,
                        'branch_no': branch_no,
                        'street': row[0][114:149].strip(),
                        'street2': row[0][149:184].strip(),
                        'zip': row[0][184:194].strip(),
                        'city': row[0][194:229].strip(),
                        # 'state'
                        # 'email'
                        'phone': (row[0][265:270].strip() + ' ' + row[0][229:247].strip()).strip(),
                        # 'fax': (row[0][265:270].strip() + ' ' + row[0][247:265].strip()).strip(),
                        'country': country_id.id, # row[0][270:272]
                        'l10n_ch_postal_chf': postal_chf, # row[0][272:284]
                        # 'l10n_ch_postal_eur'
                        'bic': row[0][284:298].strip(),
                        'active': True,
                    })

        domain = [('active', '=', False)]
        bank_ids = self.env['res.bank'].with_context(active_test=False).search(domain)
        for bank_id in bank_ids:
            try:
                bank_id.unlink()
            except:
                pass
