# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissResCompany(models.Model):

    _inherit = 'res.company'

    @api.depends('zip', 'city', 'state_id')
    def _compute_city_line(self):
        for company_id in self:
            city_line = ''
            if company_id.zip:
                city_line += company_id.zip
                if company_id.city or company_id.state_id:
                    city_line += ' '
            if company_id.city:
                city_line += company_id.city
                if company_id.state_id:
                    city_line += ' '
            if company_id.state_id:
                city_line += company_id.state_id.name
            company_id.city_line = city_line

    city_line = fields.Char(string='City Line', compute=_compute_city_line)
