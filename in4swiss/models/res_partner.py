# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissResPartner(models.Model):

    _inherit = 'res.partner'

    @api.depends('zip', 'city', 'state_id')
    def _compute_city_line(self):
        for partner_id in self:
            city_line = ''
            if partner_id.zip:
                city_line += partner_id.zip
                if partner_id.city or partner_id.state_id:
                    city_line += ' '
            if partner_id.city:
                city_line += partner_id.city
                if partner_id.state_id:
                    city_line += ' '
            if partner_id.state_id:
                city_line += partner_id.state_id.name
            partner_id.city_line = city_line

    city_line = fields.Char(string='City Line', compute=_compute_city_line)
