# -*- coding: utf-8 -*-

import base64
import csv
import io
import sys

from flectra import api, fields, models

swiss_zip_type = [
    ('10', 'Domizil- und Fachadressen'),
    ('20', 'Nur Domiziladressen'),
    ('30', 'Nur Fach-PLZ'),
    ('40', 'Firmen-PLZ'),
    ('80', 'Postinterne PLZ'),
]

swiss_zip_lang = [
    ('1', 'German'),
    ('2', 'French'),
    ('3', 'Italian'),
]


class In4SwissCityResCity(models.Model):

    _inherit = 'res.city'

    swiss_zip_onrp = fields.Char(string='Post No.', size=5)
    swiss_zip_bfsnr = fields.Char(string='BFS No.', size=5)
    swiss_zip_type = fields.Selection(swiss_zip_type, string='ZIP Type')
    swiss_zip_digit = fields.Char(string='Additional Digits', size=2)
    swiss_zip_lang = fields.Selection(swiss_zip_lang, string='Language code')
    swiss_zip_delivery = fields.Char(string='Mail Delivery', size=5)
    swiss_zip_valid = fields.Date(string='Valid From')

    @api.multi
    @api.depends('name', 'zipcode', 'state_id', 'country_id')
    def name_get(self): #Override
        result = []
        for rec in self:
            names = []
            if rec.zipcode:
                names.append(rec.zipcode)
            if rec.name:
                names.append(rec.name)
            if rec.state_id:
                names.append(rec.state_id.name)
            result.append((rec.id, ', '.join(names)))
        return result


class In4SwissResCityImportSwiss(models.TransientModel):

    _name = 'res.city.swiss_zip_import'
    _description = 'Import Swiss ZIP'

    file = fields.Binary(string='File to import', required=True)

    def update_swiss_zip(self, ch_country_id):
        domain = [('country_id', '=', ch_country_id.id), ('city_id', '=', False)]
        partner_ids = self.env['res.partner'].search(domain)
        for partner_id in partner_ids:
            partner_id.write({
                'city': partner_id.city,
                'zip': partner_id.zip,
                'country_id': partner_id.country_id.id,
            })

        domain = [('country', '=', ch_country_id.id), ('city_id', '=', False)]
        bank_ids = self.env['res.bank'].search(domain)
        for bank_id in bank_ids:
            bank_id.write({
                'city': bank_id.city,
                'zip': bank_id.zip,
                'country_id': bank_id.country.id,
            })

    @api.multi
    def import_swiss_zip(self):
        file = io.StringIO(base64.decodebytes(self.file).decode('iso-8859-1'))
        reader = csv.reader(file, delimiter=';')
        csv.field_size_limit(sys.maxsize)

        domain = [('code', '=', 'CH')]
        ch_country_id = self.env['res.country'].search(domain, limit=1)

        domain = [('country_id', '=', ch_country_id.id)]
        city_ids = self.env['res.city'].search(domain)
        for city_id in city_ids:
            city_id.write({'active': False})

        for row in reader:
            rec_art = row[0]
            if rec_art and (rec_art == '01'):
                zip_type = row[3]

                domain = [('country_id', '=', ch_country_id.id), ('code', '=', row[9])]  # KANTON
                state_id = self.env['res.country.state'].search(domain, limit=1)

                if zip_type and (zip_type in ['10', '20', '30', '40']):  # 10 = Domizil- und Fachadressen, 20 = Nur Domiziladressen, 30 = Nur Fach-PLZ, 40 = Firmen-PLZ, 80 = Postinterne PLZ
                    zip_onrp = row[1]
                    zip_digit = str(int(row[5]))  # PLZ_ZZ
                    valid_from = row[13][:4] + '-' + row[13][4:6] + '-' + row[13][6:8]  # GILT_AB_DAT

                    domain = [
                        '|', ('swiss_zip_onrp', '=', zip_onrp),
                             '&', ('name', '=', row[8]),
                                  '&', ('zipcode', '=', row[4]),
                                       '&', ('state_id', '=', state_id.id),
                                            ('country_id', '=', ch_country_id.id)
                    ]
                    city_id = self.env['res.city'].with_context(active_test=False).search(domain, limit=1)
                    if city_id:
                        if (   (city_id.name != row[8])
                            or (city_id.zipcode != row[4])
                            or (city_id.state_id.id != state_id.id)
                            or (city_id.country_id.id != ch_country_id.id)
                            or (city_id.swiss_zip_bfsnr != int(row[2]))
                            or (city_id.swiss_zip_type != zip_type)
                            or (city_id.swiss_zip_digit != zip_digit)
                            or (city_id.swiss_zip_lang != row[10])
                            or (city_id.swiss_zip_delivery != int(row[12]))
                            or (city_id.swiss_zip_valid != valid_from)
                        ):
                            city_id.write({
                                'name': row[8],  # ORTBEZ27
                                'zipcode': row[4],  # POSTLEITZAHL
                                'state_id': state_id.id,
                                'country_id': ch_country_id.id,

                                'swiss_zip_bfsnr': int(row[2]),  # BFSNR
                                'swiss_zip_type': zip_type,
                                'swiss_zip_digit': zip_digit,
                                'swiss_zip_lang': row[10],  # SPRACHCODE
                                'swiss_zip_delivery': int(row[12]),  # BRIEFZ_DURCH
                                'swiss_zip_valid': valid_from,

                                'active': True,
                            })
                        else:
                            city_id.write({
                                'active': True,
                            })
                    else:
                        self.env['res.city'].create({
                            'name': row[8],  # ORTBEZ27
                            'zipcode': row[4],  # POSTLEITZAHL
                            'state_id': state_id.id,
                            'country_id': ch_country_id.id,

                            'swiss_zip_onrp': zip_onrp,
                            'swiss_zip_bfsnr': int(row[2]),  # BFSNR
                            'swiss_zip_type': zip_type,
                            'swiss_zip_digit': zip_digit,
                            'swiss_zip_lang': row[10],  # SPRACHCODE
                            'swiss_zip_delivery': int(row[12]),  # BRIEFZ_DURCH
                            'swiss_zip_valid': valid_from,

                            'active': True,
                        })

        self.update_swiss_zip(ch_country_id)

        domain = [('country_id', '=', ch_country_id.id), ('active', '=', False)]
        city_ids = self.env['res.city'].with_context(active_test=False).search(domain)
        for city_id in city_ids:
            try:
                city_id.unlink()
            except:
                pass
