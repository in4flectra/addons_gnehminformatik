# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4CRMResPartner(models.Model):

    _inherit = 'res.partner'

    lead_ids = fields.One2many('crm.lead', 'partner_id', string='Leads', domain=[('type', '=', 'lead')])
    leads_and_opportunities_ids = fields.One2many('crm.lead', 'partner_id', string='Leads & Opportunities')

    @api.one
    def update_lead(self, lead_id):
        partner_name = False
        if self.is_company:
            partner_name = self.name
        elif self.parent_id and self.parent_id.is_company:
            partner_name = self.parent_id.name
        lead_id.sudo().write({
            'partner_name': partner_name,
            'contact_name': self.name if not self.is_company else False,

            'street': self.street,
            'street2': self.street2,
            'zip': self.zip,
            'city': self.city,
            'state_id': self.state_id.id,
            'country_id': self.country_id.id,
            'website': self.website if not self.parent_id else self.parent_id.website,

            'title': self.title.id if not self.is_company else False,
            'function': self.function if not self.is_company else False,
            'phone': self.phone,
            'mobile': self.mobile,
            'email_from': self.email,
        })

    @api.multi
    def write(self, vals):
        result = super(In4CRMResPartner, self).write(vals)

        for partner_id in self:
            for lead_id in partner_id.leads_and_opportunities_ids:
                partner_id.update_lead(lead_id)
            for child_id in partner_id.child_ids:
                for lead_id in child_id.leads_and_opportunities_ids:
                    child_id.update_lead(lead_id)

        return result
