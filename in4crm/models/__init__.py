# -*- coding: utf-8 -*-

from . import crm_customer_to_partner
from . import crm_lead_tag
from . import mail_activity
from . import res_partner
