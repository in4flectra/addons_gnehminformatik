# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4CRMCRMCustomerToPartner(models.TransientModel):

    _name = 'crm.customer_to_partner'
    _description = 'Customer To Partner'
    _inherit = 'crm.partner.binding'

    action = fields.Selection([('create', 'Create a new customer'), ('exist', 'Link to an existing customer')])

    @api.onchange('action')
    def onchange_action(self):
        if self.action == 'exist':
            self.partner_id = self._find_matching_partner()
        else:
            self.partner_id = False

    @api.multi
    def apply_action(self):
        self.ensure_one()

        if self._context.get('active_model') != 'crm.lead' or not self._context.get('active_id'):
            return False
        lead_id = self.env['crm.lead'].browse(self._context.get('active_id'))

        partner_id = False
        if self.partner_id:
            partner_id = self.partner_id.id

        return lead_id.handle_partner_assignation(self.action, partner_id).get(lead_id)
