# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4CRMMailActivity(models.Model):

    _inherit = 'mail.activity'

    @api.depends('res_model', 'res_id')
    def _compute_lead_ids(self):
        for activity_id in self:
            if activity_id.res_model == 'crm.lead':
                activity_id.lead_id = activity_id.res_id

    lead_id = fields.Many2one('crm.lead', string='Lead/Opportunity', compute=_compute_lead_ids, store=True)
