# -*- coding: utf-8 -*-
{
    'name': 'CRM (extended)',
    'summary': 'Extension of Leads and Opportunities',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'crm',
        'sales_team',
    ],

    'data': [
        'views/crm_customer_to_partner.xml',
        'views/crm_lead.xml',
        'views/crm_lead_tag.xml',
        'views/mail_activity.xml',
    ],
}
