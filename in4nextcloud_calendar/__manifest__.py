# -*- coding: utf-8 -*-
{
    'name': 'Sync Calendars with Nextcloud',
    'summary': 'Synchronize the Calendars from Flectra with Calendars from Nextcloud',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'calendar',
        'in4nextcloud',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/calendar.xml',
        'views/res_users.xml',
    ],

}
