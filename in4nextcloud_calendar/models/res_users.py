# -*- coding: utf-8 -*-

import pytz
import vobject
import webdav3.client
import xml.etree.ElementTree

from datetime import date, datetime, timedelta
from dateutil import parser
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

flectra_states = {
    'NEEDS-ACTION': 'needsAction',
    'TENTATIVE': 'tentative',
    'DECLINED': 'declined',
    'ACCEPTED': 'accepted',
}


class In4NextcloudCalendarResUsers(models.Model):

    _inherit = 'res.users'

    nextcloud_calendar_settings_id = fields.Many2one('in4nextcloud_calendar.settings', string='Nextcloud Calendar: Settings', copy=False)
    nextcloud_calendar_hostname = fields.Char(string='Nextcloud Calendar: Hostname')
    nextcloud_calendar_login = fields.Char(string='Nextcloud Calendar: Login')
    nextcloud_calendar_password = fields.Char(string='Nextcloud Calendar: Password')
    nextcloud_calendar_url_path_personal = fields.Char(string='Nextcloud Calendar: URL Path to Personal')
    nextcloud_calendar_download_timestamp = fields.Datetime(string='Nextcloud Calendar: Last Download')

    @api.multi
    def get_nextcloud_calendar_options(self):
        self.ensure_one()
        return {
            'webdav_hostname': self.nextcloud_calendar_hostname,
            'webdav_login': self.nextcloud_calendar_login,
            'webdav_password': self.nextcloud_calendar_password,
        }

    @api.multi
    def sync_calendar_with_nextcloud(self):
        for user in self:
            if (    user.nextcloud_calendar_settings_id
                and user.nextcloud_calendar_settings_id.active
                and user.nextcloud_calendar_hostname
                and user.nextcloud_calendar_login
                and user.nextcloud_calendar_url_path_personal
            ):
                download_timestamp = fields.datetime.now()
                client = webdav3.client.Client(user.get_nextcloud_calendar_options())
                if client:
                    client.verify = False
                    request = client.execute_request('list', user.nextcloud_calendar_url_path_personal)
                    list = xml.etree.ElementTree.fromstring(request.text)
                    default_partner_id = user.nextcloud_calendar_settings_id.user_id.partner_id.id
                    for root_node in list:
                        href = ''
                        etag = ''
                        update_time = None
                        for child_node in root_node:
                            if child_node.tag == '{DAV:}href':
                                href = child_node.text
                            elif child_node.tag == '{DAV:}propstat':
                                for propstat_node in child_node:
                                    if propstat_node.tag == '{DAV:}prop':
                                        for prop_node in propstat_node:
                                            if prop_node.tag == '{DAV:}getetag':
                                                etag = prop_node.text
                                            elif prop_node.tag == '{DAV:}getlastmodified':
                                                update_time = prop_node.text
                        if href and etag:
                            need_update = True
                            if update_time and user.nextcloud_calendar_download_timestamp:
                                last_download_str = user.nextcloud_calendar_download_timestamp
                                last_download_datetime = datetime.strptime(last_download_str, DEFAULT_SERVER_DATETIME_FORMAT)
                                if parser.parse(update_time) < pytz.UTC.localize(last_download_datetime):
                                    need_update = False
                            if need_update:
                                ical = vobject.readOne(client.execute_request('download', href).text)
                                event = None
                                values = {}
                                nextcloud_calendar_uid = None
                                for vevent in ical.contents.get('vevent', []):
                                    for uid in vevent.contents.get('uid', []):
                                        nextcloud_calendar_uid = uid.value
                                if nextcloud_calendar_uid:
                                    domain = [('nextcloud_calendar_uid', '=', nextcloud_calendar_uid)]
                                    event = self.env['calendar.event'].search(domain, limit=1)
                                allday = False
                                organizer = None
                                partner_ids = []
                                attendee_ids = []
                                for vevent in ical.contents.get('vevent', []):
                                    for summary in vevent.contents.get('summary', []):
                                        value = summary.value
                                        if (not event) or (event.name != value):
                                            values['name'] = value
                                    for location in vevent.contents.get('location', []):
                                        value = location.value.replace('\n', ', ')
                                        if (not event) or (event.location != value):
                                            values['location'] = value
                                    for description in vevent.contents.get('description', []):
                                        value = description.value
                                        if (not event) or (event.description != value):
                                            values['description'] = value
                                    for dtstart in vevent.contents.get('dtstart', []):
                                        value = dtstart.value
                                        check_value = value
                                        if isinstance(value, datetime):
                                            allday = False
                                            value = value.astimezone(pytz.timezone('UTC'))
                                            value = datetime.strftime(value, DEFAULT_SERVER_DATETIME_FORMAT)
                                            check_value = value
                                        elif isinstance(value, date):
                                            allday = True
                                            value = date.strftime(value, DEFAULT_SERVER_DATE_FORMAT)
                                            check_value = value
                                            value = value + ' 08:00:00'
                                        if (not event) or (event.start != check_value):
                                            values['start'] = value
                                    for dtend in vevent.contents.get('dtend', []):
                                        value = dtend.value
                                        check_value = value
                                        if isinstance(value, datetime):
                                            allday = False
                                            value = value.astimezone(pytz.timezone('UTC'))
                                            value = datetime.strftime(value, DEFAULT_SERVER_DATETIME_FORMAT)
                                            check_value = value
                                        elif isinstance(value, date):
                                            allday = True
                                            value = value - timedelta(days=1)
                                            value = date.strftime(value, DEFAULT_SERVER_DATE_FORMAT)
                                            check_value = value
                                            value = value + ' 18:00:00'
                                        if (not event) or (event.stop != check_value):
                                            values['stop'] = value
                                    for organizer in vevent.contents.get('organizer', []):
                                        email = organizer.value
                                        if email:
                                            email = email.split(':')[1]
                                            domain = [('partner_id.email', '=ilike', email)]
                                            organizer = self.env['res.users'].search(domain, limit=1)
                                            if organizer:
                                                partner_ids.append(organizer.partner_id.id)
                                                attendee_ids.append({
                                                    'partner_id': organizer.partner_id.id,
                                                    'state': 'accepted',
                                                    'email': organizer.partner_id.email,
                                                })
                                    for attendee in vevent.contents.get('attendee', []):
                                        email = attendee.value
                                        state = attendee.params.get('PARTSTAT', [])
                                        if email and state:
                                            email = email.split(':')[1]
                                            state = state[0]
                                            domain = [('email', '=ilike', email), ('user_ids', '!=', False)]
                                            attendee_partner = self.env['res.partner'].search(domain, limit=1)
                                            if not attendee_partner:
                                                domain = [('email', '=ilike', email)]
                                                attendee_partner = self.env['res.partner'].search(domain, limit=1)
                                            if not attendee_partner:
                                                name = attendee.params.get('CN', [])
                                                if name:
                                                    name = name[0]
                                                else:
                                                    name = email
                                                attendee_partner = self.env['res.partner'].create({
                                                    'name': name,
                                                    'email': email,
                                                    'customer': False,
                                                })
                                            partner_ids.append(attendee_partner.id)
                                            attendee_ids.append({
                                                'partner_id': attendee_partner.id,
                                                'state': flectra_states.get(state, 'needsAction'),
                                                'email': attendee_partner.email,
                                            })
                                if (not event) or (event.allday != allday):
                                    values['allday'] = allday
                                if not partner_ids:
                                    partner_ids.append(user.partner_id.id)
                                if not attendee_ids:
                                    attendee_ids.append({
                                        'partner_id': user.partner_id.id,
                                        'state': 'accepted',
                                        'email': user.partner_id.email,
                                    })
                                if (not organizer) or (organizer.id == user.id):
                                    if event:
                                        update_partner_ids = []
                                        for id in partner_ids:
                                            if (id not in event.partner_ids.ids) and (id != default_partner_id):
                                                update_partner_ids.append((4, id))
                                        for id in event.partner_ids.ids:
                                            if id not in partner_ids:
                                                update_partner_ids.append((3, id))
                                        if update_partner_ids:
                                            values['partner_ids'] = update_partner_ids
                                        if values:
                                            event.write(values)
                                        else:
                                            event.write({'nextcloud_calendar_timestamp': download_timestamp})
                                        for attendee_values in attendee_ids:
                                            domain = [
                                                ('event_id', '=', event.id),
                                                ('partner_id', '=', attendee_values['partner_id'])
                                            ]
                                            attendee = self.env['calendar.attendee'].search(domain, limit=1)
                                            if attendee:
                                                if attendee.partner_id.id != event.user_id.partner_id.id:
                                                    attendee.write({
                                                        'state': attendee_values['state'],
                                                        'email': attendee_values['email'],
                                                    })
                                            elif attendee.partner_id.id != default_partner_id:
                                                self.env['calendar.attendee'].create({
                                                    'partner_id': attendee_values['partner_id'],
                                                    'state': attendee_values['state'],
                                                    'email': attendee_values['email'],
                                                })
                                    else:
                                        if organizer:
                                            values['user_id'] = organizer.id
                                        else:
                                            values['user_id'] = user.id
                                        values['partner_ids'] = []
                                        for id in partner_ids:
                                            values['partner_ids'].append((4, id))
                                        values['attendee_ids'] = []
                                        for attendee_values in attendee_ids:
                                            values['attendee_ids'].append((0, 0, {
                                                'partner_id': attendee_values['partner_id'],
                                                'state': attendee_values['state'],
                                                'email': attendee_values['email'],
                                            }))
                                        values['nextcloud_calendar_uid'] = nextcloud_calendar_uid
                                        values['nextcloud_calendar_resource_href'] = href
                                        values['nextcloud_calendar_timestamp'] = download_timestamp
                                        self.env['calendar.event'].create(values)
                user.write({
                    'nextcloud_calendar_download_timestamp': download_timestamp,
                })

            domain = [
                ('user_id', '=', user.id),
                ('stop', '>=', date.strftime(date.today() - relativedelta(months=1), DEFAULT_SERVER_DATE_FORMAT))
            ]
            events = self.env['calendar.event'].search(domain)
            events.sync_with_nextcloud()
