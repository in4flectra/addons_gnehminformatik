# -*- coding: utf-8 -*-

import pytz
import time
import uuid
import vobject
import webdav3.client

from datetime import datetime, timedelta

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

nextcloud_states = {
    'needsAction': 'NEEDS-ACTION',
    'tentative': 'TENTATIVE',
    'declined': 'DECLINED',
    'accepted': 'ACCEPTED',
}


class In4NextcloudCalendarSettings(models.Model):

    _name = 'in4nextcloud_calendar.settings'
    _description = 'Calendar Settings'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean(string='Active', default=True)
    user_id = fields.Many2one('res.users', string='User', required=True)
    download_timestamp = fields.Datetime(string='Last Download')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Setting name already exists !'),
    ]

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.update(
            name=_('%s (copy)') % (self.name or ''))
        return super(In4NextcloudCalendarSettings, self).copy(default)

    @api.model
    def _download_calendar(self, settings):
        log = ''
        users = self.env['res.users'].search([('nextcloud_calendar_settings_id', '=', settings.id)])
        if not settings.download_timestamp:
            for user in users:
                user.sudo().write({'nextcloud_calendar_download_timestamp': None})
        users.sync_calendar_with_nextcloud()
        return log

    @api.model
    def download_calendar_from_settings(self, id=None):
        settings = None
        if id:
            settings = id
        else:
            context = dict(self._context or {})
            active_id = context.get('active_id', False)
            if active_id:
                settings = self.env['in4nextcloud_calendar.settings'].browse(active_id)
        if settings and settings.active:
            start_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            log = self._download_calendar(settings)
            settings.sudo().write({
                'download_timestamp': fields.datetime.now(),
            })
            end_date = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            summary = '''
Download started: %s
Download finished: %s
            ''' % (
                start_date,
                end_date,
            )
            summary = summary.strip() + '\n\n'
            summary += 'Log:\n' + log
            self.env['in4nextcloud_calendar.request'].sudo().create({
                'name': 'Download report',
                'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'body': summary,
            })

    @api.model
    def download_all_calendar(self):
        settings_list = self.env['in4nextcloud_calendar.settings'].search([('active', '=', True)])
        for settings in settings_list:
            self.download_calendar_from_settings(settings)


class In4NextcloudCalendarRequest(models.Model):

    _name = 'in4nextcloud_calendar.request'
    _description = 'Calendar Request'
    _order = 'date desc'

    name = fields.Char(string='Name', required=True)
    date = fields.Datetime(string='Date', required=True)
    body = fields.Text(string='Request', readonly=True)


class In4NextcloudCalendarCalendarEvent(models.Model):

    _inherit = 'calendar.event'

    nextcloud_calendar_uid = fields.Char(string='Nextcloud Calendar: Unique ID', copy=False)
    nextcloud_calendar_resource_href = fields.Char(string='Nextcloud Calendar: Resource Name', copy=False)
    nextcloud_calendar_timestamp = fields.Datetime(string='Nextcloud Calendar: Last Download', copy=False)

    @api.model
    def _update_nextcloud_summary(self, vevent):
        result = ''
        summary_found = False
        summaries_old = vevent.contents.get('summary', [])
        for summary_old in summaries_old:
            if not summary_found:
                summary_found = True
                if self.name:
                    if summary_old.value != self.name:
                        summary_old.value = self.name
                        result = 'summary,'
                else:
                    summaries_old.remove(summary_old)
                    result = 'summary,'
        if (not summary_found) and self.name:
            summary = vevent.add('summary')
            summary.value = self.name
            result = 'summary,'
        return result

    @api.model
    def _update_nextcloud_start(self, vevent):
        result = ''
        if self.allday:
            start_value = datetime.strptime(self.start_date, DEFAULT_SERVER_DATE_FORMAT)
            start_value = start_value.date()
        else:
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            start_value = datetime.strptime(self.start_datetime, DEFAULT_SERVER_DATETIME_FORMAT)
            start_value = pytz.UTC.localize(start_value).astimezone(pytz.timezone(tz))
        start_found = False
        starts_old = vevent.contents.get('dtstart', [])
        for start_old in starts_old:
            if not start_found:
                start_found = True
                if self.start:
                    if start_old.value != start_value:
                        start_old.value = start_value
                        result = 'dtstart,'
                    if self.allday:
                        if not start_old.params.get('VALUE'):
                            start_old.params['VALUE'] = ['DATE']
                            result = 'dtstart,'
                    else:
                        if start_old.params.get('VALUE'):
                            start_old.params.pop('VALUE')
                            result = 'dtstart,'
                else:
                    starts_old.remove(start_old)
                    result = 'dtstart,'
        if (not start_found) and self.start:
            start = vevent.add('dtstart')
            start.value = start_value
            if self.allday:
                start.params['VALUE'] = ['DATE']
            result = 'dtstart,'
        return result

    @api.model
    def _update_nextcloud_stop(self, vevent):
        result = ''
        if self.allday:
            stop_value = datetime.strptime(self.stop_date, DEFAULT_SERVER_DATE_FORMAT)
            stop_value = (stop_value + timedelta(days=1)).date()
        else:
            tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
            stop_value = datetime.strptime(self.stop_datetime, DEFAULT_SERVER_DATETIME_FORMAT)
            stop_value = pytz.UTC.localize(stop_value).astimezone(pytz.timezone(tz))
        stop_found = False
        stops_old = vevent.contents.get('dtend', [])
        for stop_old in stops_old:
            if not stop_found:
                stop_found = True
                if self.stop:
                    if stop_old.value != stop_value:
                        stop_old.value = stop_value
                        result = 'dtend,'
                    if self.allday:
                        if not stop_old.params.get('VALUE'):
                            stop_old.params['VALUE'] = ['DATE']
                            result = 'dtend,'
                    else:
                        if stop_old.params.get('VALUE'):
                            stop_old.params.pop('VALUE')
                            result = 'dtend,'
                else:
                    stops_old.remove(stop_old)
                    result = 'dtend,'
        if (not stop_found) and self.stop:
            stop = vevent.add('dtend')
            stop.value = stop_value
            if self.allday:
                stop.params['VALUE'] = ['DATE']
            result = 'dtend,'
        return result

    @api.model
    def _update_nextcloud_location(self, vevent):
        result = ''
        location_found = False
        locations_old = vevent.contents.get('location', [])
        for location_old in locations_old:
            if not location_found:
                location_found = True
                if self.location:
                    if location_old.value != self.location:
                        location_old.value = self.location
                        result = 'location,'
                else:
                    locations_old.remove(location_old)
                    result = 'location,'
        if (not location_found) and self.location:
            location = vevent.add('location')
            location.value = self.location
            result = 'location,'
        return result

    @api.model
    def _update_nextcloud_description(self, vevent):
        result = ''
        description_found = False
        descriptions_old = vevent.contents.get('description', [])
        for description_old in descriptions_old:
            if not description_found:
                description_found = True
                if self.description:
                    if description_old.value != self.description:
                        description_old.value = self.description
                        result = 'description,'
                else:
                    descriptions_old.remove(description_old)
                    result = 'description,'
        if (not description_found) and self.description:
            description = vevent.add('description')
            description.value = self.description
            result = 'description,'
        return result

    @api.model
    def _update_nextcloud_organizer(self, vevent, partner):
        result = ''
        organizers_new = []
        organizers_old = vevent.contents.get('organizer', [])
        if (   (len(self.attendee_ids) > 1)
            or ((len(self.attendee_ids) == 1) and (self.attendee_ids[0].partner_id.id != partner.id))
        ):
            if partner.email:
                email = 'mailto:' + partner.email
            else:
                email = partner.name
            organizers_new.append({
                'email': email,
                'name': [partner.name],
            })
        idx_old = len(organizers_old) - 1
        while idx_old >= 0:
            organizer_old = organizers_old[idx_old]
            organizer_found = False
            idx_new = len(organizers_new) - 1
            while idx_new >= 0:
                organizer_new = organizers_new[idx_new]
                if not organizer_found:
                    if organizer_old.value == organizer_new['email']:
                        organizer_found = True
                        if organizer_old.params['CN'] != organizer_new['name']:
                            organizer_old.params['CN'] = organizer_new['name']
                            result = 'organizer,'
                        organizers_new.remove(organizer_new)
                idx_new -= 1
            if not organizer_found:
                organizers_old.remove(organizer_old)
                result = 'organizer,'
            idx_old -= 1
        for organizer_new in organizers_new:
            organizer = vevent.add('organizer')
            organizer.value = organizer_new['email']
            organizer.params['CN'] = organizer_new['name']
            result = 'organizer,'
        return result

    @api.model
    def _update_nextcloud_attendee(self, vevent, partner):
        result = ''
        attendees_new = []
        attendees_old = vevent.contents.get('attendee', [])
        for attendee in self.attendee_ids:
            if attendee.partner_id.id != partner.id:
                attendees_new.append({
                    'email': 'mailto:' + attendee.email,
                    'name': [attendee.partner_id.name],
                    'state': [nextcloud_states.get(attendee.state, 'NEEDS-ACTION')],
                })
        idx_old = len(attendees_old) - 1
        while idx_old >= 0:
            attendee_old = attendees_old[idx_old]
            attendee_found = False
            idx_new = len(attendees_new) - 1
            while idx_new >= 0:
                attendee_new = attendees_new[idx_new]
                if not attendee_found:
                    if attendee_old.value == attendee_new['email']:
                        attendee_found = True
                        if attendee_old.params['CN'] != attendee_new['name']:
                            attendee_old.params['CN'] = attendee_new['name']
                            result = 'attendee,'
                        if attendee_old.params['PARTSTAT'] != attendee_new['state']:
                            attendee_old.params['PARTSTAT'] = attendee_new['state']
                            result = 'attendee,'
                        attendees_new.remove(attendee_new)
                idx_new -= 1
            if not attendee_found:
                attendees_old.remove(attendee_old)
                result = 'attendee,'
            idx_old -= 1
        for attendee_new in attendees_new:
            attendee = vevent.add('attendee')
            attendee.value = attendee_new['email']
            attendee.params['CN'] = attendee_new['name']
            attendee.params['PARTSTAT'] = attendee_new['state']
            result = 'attendee,'
        return result

    @api.multi
    def get_user_for_nextcloud(self, settings):
        self.ensure_one()
        result = None
        if (    not result
            and self.user_id.partner_id.id in self.partner_ids.ids
            and self.user_id.nextcloud_calendar_settings_id
            and self.user_id.nextcloud_calendar_hostname
            and self.user_id.nextcloud_calendar_login
            and self.user_id.nextcloud_calendar_url_path_personal
        ):
            result = self.user_id
        if (    not result
            and self.user_id.nextcloud_calendar_settings_id
            and self.user_id.nextcloud_calendar_settings_id.user_id
            and self.user_id.nextcloud_calendar_settings_id.user_id.nextcloud_calendar_settings_id
            and self.user_id.nextcloud_calendar_settings_id.user_id.nextcloud_calendar_hostname
            and self.user_id.nextcloud_calendar_settings_id.user_id.nextcloud_calendar_login
            and self.user_id.nextcloud_calendar_settings_id.user_id.nextcloud_calendar_url_path_personal
        ):
            result = self.user_id.nextcloud_calendar_settings_id.user_id
        if (    not result
            and settings.user_id
            and settings.user_id.nextcloud_calendar_settings_id
            and settings.user_id.nextcloud_calendar_hostname
            and settings.user_id.nextcloud_calendar_login
            and settings.user_id.nextcloud_calendar_url_path_personal
        ):
            result = settings.user_id
        return result

    @api.multi
    def sync_with_nextcloud(self):
        settings = self.env['in4nextcloud_calendar.settings'].search([], limit=1)
        if settings:
            for event in self:
                user = event.get_user_for_nextcloud(settings)
                if user:
                    client = webdav3.client.Client(user.get_nextcloud_calendar_options())
                    if client:
                        client.verify = False
                        ical = None
                        href = None
                        if event.nextcloud_calendar_resource_href:
                            href = event.nextcloud_calendar_resource_href
                            try:
                                ical = vobject.readOne(client.execute_request('download', href).text)
                            except Exception as e:
                                ical = None
                                href = None
                                pass
                        if ical:
                            vevent = ical.contents.get('vevent', [])[0]
                        else:
                            ical = vobject.iCalendar()
                            vevent = ical.add('vevent')
                            href = str(uuid.uuid4()).upper() + '.ics'
                            href = user.nextcloud_calendar_url_path_personal + href

                        update_fields = ''
                        update_fields += event._update_nextcloud_summary(vevent)
                        update_fields += event._update_nextcloud_start(vevent)
                        update_fields += event._update_nextcloud_stop(vevent)
                        update_fields += event._update_nextcloud_location(vevent)
                        update_fields += event._update_nextcloud_description(vevent)
                        update_fields += event._update_nextcloud_organizer(vevent, user.partner_id)
                        update_fields += event._update_nextcloud_attendee(vevent, user.partner_id)

                        if update_fields:
                            client.execute_request('upload', href, data=ical.serialize())
                            values = {
                                'nextcloud_calendar_resource_href': href,
                                'nextcloud_calendar_timestamp': fields.datetime.now(),
                            }
                            ical = vobject.readOne(client.execute_request('download', href).text)
                            for vevent in ical.contents.get('vevent', []):
                                for uid in vevent.contents.get('uid', []):
                                    values['nextcloud_calendar_uid'] = uid.value
                            event.write(values)

    @api.model
    def create(self, values):
        result = super(In4NextcloudCalendarCalendarEvent, self).create(values)
        self.sync_with_nextcloud()
        return result

    @api.multi
    def write(self, values):
        result = super(In4NextcloudCalendarCalendarEvent, self).write(values)
        try:
            if (    (not 'nextcloud_contacts_timestamp' in values)
                and (  ('name' in values)
                    or ('allday' in values)
                    or ('start' in values)
                    or ('stop' in values)
                    or ('location' in values)
                    or ('description' in values)
                    or ('user_id' in values)
                    or ('partner_ids' in values)
                    or ('attendee_ids' in values)
                )
            ):
                self.sync_with_nextcloud()
        except Exception as e:
            pass
        return result

    @api.multi
    def unlink(self):
        settings = self.env['in4nextcloud_calendar.settings'].search([], limit=1)
        if settings:
            for event in self:
                if event.nextcloud_calendar_resource_href:
                    user = event.get_user_for_nextcloud(settings)
                    if user:
                        client = webdav3.client.Client(user.get_nextcloud_calendar_options())
                        if client:
                            client.verify = False
                            client.execute_request('clean', event.nextcloud_calendar_resource_href)
        return super(In4NextcloudCalendarCalendarEvent, self).unlink()


class In4NextcloudCalendarCalendarAttendee(models.Model):

    _inherit = 'calendar.attendee'

    @api.multi
    def write(self, values):
        result = super(In4NextcloudCalendarCalendarAttendee, self).write(values)
        try:
            for attendee in self:
                if attendee.event_id:
                    attendee.event_id.sync_with_nextcloud()
        except Exception as e:
            pass
        return result
