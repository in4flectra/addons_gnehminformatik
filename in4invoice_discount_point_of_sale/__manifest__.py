# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale with Discount and Bonus',
    'summary': 'Discounts and Bonus for Point of Sale',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'in4invoice_discount',
        'point_of_sale',
        'pos_order_mgmt',
        'pos_restaurant',
        'web',
    ],

    'data': [
        # views
        'views/assets.xml',
        'views/pos_config.xml',
        'views/pos_order.xml',

        # reports
        'views/report_saledetails.xml',
    ],

    'qweb': [
        'static/src/xml/point_of_sale.xml',
        'static/src/xml/pos_restaurant.xml',
    ],

    'auto_install': True,
}
