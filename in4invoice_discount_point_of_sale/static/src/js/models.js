flectra.define('in4invoice_discount_point_of_sale.models', function (require) {
'use strict';

var utils = require('web.utils');
var field_utils = require('web.field_utils');
var models = require('point_of_sale.models');

var round_di = utils.round_decimals;
var round_pr = utils.round_precision;

models.load_fields('res.partner', 'bonus_ids');

models.load_models({
    model: 'res.partner.bonus',
    fields: ['name', 'product_id', 'partner_id', 'amount_used'],
    domain: null,
    loaded: function(self, bonus_ids) {
        self.bonus_ids = bonus_ids;
        self.bonus_by_id = [];
        self.bonus_calc_orders_by_id = [];
        self.discount_calc_orders_by_id = [];
        self.bonus_amount_by_partner_id_and_product_id = [];
        _.each(bonus_ids, function(bonus_id) {
            self.bonus_by_id[bonus_id.id] = bonus_id;
        });
    },
});

models.load_models({
    model: 'product.product',
    fields: ['name', 'bonus_discount'],
    domain: function(self) { return [['id', 'in', self.config.bonus_product_ids]]; },
    loaded: function(self, bonus_product_ids) {
        self.bonus_product_ids = bonus_product_ids;
        self.bonus_product_by_id = [];
        _.each(bonus_product_ids, function(bonus_product_id){
            self.bonus_product_by_id[bonus_product_id.id] = bonus_product_id;
        });
    },
});

models.load_models({
    model: 'product.product',
    fields: ['name'],
    domain: function(self) { return [['id', 'in', self.config.discount_product_ids]]; },
    loaded: function(self, discount_product_ids) {
        self.discount_product_ids = discount_product_ids;
        self.discount_product_by_id = [];
        _.each(discount_product_ids, function(discount_product_id){
            self.discount_product_by_id[discount_product_id.id] = discount_product_id;
        });
    },
});

var _super_posmodel = models.PosModel.prototype;
models.PosModel = models.PosModel.extend({

    initialize: function(session, attributes) {
        this.discount_product_ids = [];
        return _super_posmodel.initialize.call(this, session, attributes);
    },

    product_domain: function() {
        var rounding_or_tip = [this.config.cash_rounding_product_id[0], this.config.tip_product_id[0]];
        return ['|', '|', '&', ['sale_ok', '=', true], ['available_in_pos', '=', true], ['id', 'in', rounding_or_tip], ['bonus', '=', true]];
    },

});

var _super_orderline = models.Orderline.prototype;
models.Orderline = models.Orderline.extend({

    export_as_JSON: function() {
        var json = _super_orderline.export_as_JSON.apply(this, arguments);
        json.line_discount = this.get_line_discount();
        json.line_discount_text = this.get_line_discount_text();
        return json;
    },
    init_from_JSON: function(json) {
        _super_orderline.init_from_JSON.apply(this, arguments);
        this.discount = json.line_discount || 0;
        this.discount_text = json.line_discount_text || '';
    },
    export_for_printing: function() {
        var result = _super_orderline.export_for_printing.apply(this, arguments);
        result.line_price_display = this.get_line_display_price();
        result.line_discount = this.get_line_discount();
        result.line_discount_str = this.get_line_discount_str();
        result.line_discount_text = this.get_line_discount_text();
        return result;
    },
    get_line_discount: function() {
        return _super_orderline.get_discount.apply(this, arguments);
    },
    get_line_discount_str: function() {
		var decimals = this.pos.dp['Discount'];
        var disc = field_utils.format.float(this.get_line_discount(), {digits: [69, decimals]})
        return disc.replace(/0+$/, '').replace(/\.+$/, '').replace(/,+$/, '');
    },
    get_line_discount_text: function() {
        return _super_orderline.get_discount_text.apply(this, arguments);
    },
    get_order_discount: function() {
        return this.order.order_discount;
    },
    get_order_discount_str: function() {
		var decimals = this.pos.dp['Discount'];
        var disc = field_utils.format.float(this.get_order_discount(), {digits: [69, decimals]})
        return disc.replace(/0+$/, '').replace(/\.+$/, '').replace(/,+$/, '');
    },
    get_order_discount_text: function() {
        return this.order.order_discount_text;
    },
    get_order_discount_label: function() {
        return this.order.order_discount_label;
    },
    get_discount: function() {
        if (this.get_with_discount()) {
            var line_value = (100 - (this.get_line_discount() || 0.0)) / 100;
            var order_value = (100 - (this.get_order_discount() || 0.0)) / 100;
            return 100 * (1 - (line_value * order_value));
        } else {
            return 0.0;
        }
    },
    get_base_price: function() {
        var rounding = this.pos.currency.rounding;
        var amount = this.get_quantity() * this.get_unit_price();
        var discount = 0.0;
        if (this.get_with_discount()) {
            discount = round_pr(round_pr(amount * this.get_line_discount(), rounding) / 100, rounding);
            amount -= discount;
            discount = round_pr(round_pr(amount * this.get_order_discount(), rounding) / 100, rounding);
        }
        return round_pr(amount - discount, rounding);
    },
    get_price_unit_for_all_prices: function() {
        if (this.get_with_discount()) {
            var rounding = this.pos.currency.rounding;
            var amount = this.get_quantity() * this.get_unit_price();
            var discount = round_pr(round_pr(amount * this.get_line_discount(), rounding) / 100, rounding);
            amount -= discount;
            discount = round_pr(round_pr(amount * this.get_order_discount(), rounding) / 100, rounding);
            return (amount - discount) / this.get_quantity();
        } else {
            return this.get_unit_price();
        }
    },
    get_line_base_price: function() {
        var rounding = this.pos.currency.rounding;
        var amount = this.get_quantity() * this.get_unit_price();
        var discount = round_pr(round_pr(amount * this.get_line_discount(), rounding) / 100, rounding);
        return round_pr(amount - discount, rounding);
    },
    get_line_display_price: function() {
        if (this.pos.config.iface_tax_included === 'total') {
            return this.get_line_price_with_tax();
        } else {
            return this.get_line_base_price();
        }
    },
    get_line_price_without_tax: function() {
        return this.get_all_line_prices().priceWithoutTax;
    },
    get_line_price_with_tax: function() {
        return this.get_all_line_prices().priceWithTax;
    },
    get_all_line_prices: function() {
        var rounding = this.pos.currency.rounding;
        var amount = this.get_quantity() * this.get_unit_price();
        var discount = round_pr(round_pr(amount * this.get_line_discount(), rounding) / 100, rounding);
        var price_unit = (amount - discount) / this.get_quantity();
        var taxtotal = 0;
        var tax_codes = '';

        var product =  this.get_product();
        if (this.taxes_id) {
            var taxes_ids = this.taxes_id;
        } else if (product) {
            var taxes_ids = product.taxes_id;
        } else {
            var taxes_ids = [];
        }
        var taxes =  this.pos.taxes;
        var tax_details = {};
        var product_taxes = [];

        _(taxes_ids).each(function(el){
            product_taxes.push(_.detect(taxes, function(t){
                return t.id === el;
            }));
        });

        var all_taxes = this.compute_all(product_taxes, price_unit, this.get_quantity(), this.pos.currency.rounding);
        _(all_taxes.taxes).each(function(tax) {
            taxtotal += tax.amount;
            if (tax_codes.indexOf(tax.code) < 0) {
                tax_codes += tax.code;
            }
            tax_details[tax.id] = {
                'amount': tax.amount,
                'total_included': tax.total_included,
                'total_excluded': tax.total_excluded,
            };
        });
        return {
            'priceWithTax': all_taxes.total_included,
            'priceWithoutTax': all_taxes.total_excluded,
            'tax': taxtotal,
            'tax_codes': tax_codes,
            'tax_details': tax_details,
        };
    },
    get_with_discount: function() {
        if (!this.product) {
            return false;
        }
        if (   this.product.id == this.pos.config.tip_product_id[0]
            || this.product.id == this.pos.config.cash_rounding_product_id[0]
            || this.pos.discount_product_by_id[this.product.id]
        ) {
            return false;
        } else {
            return true;
        }
    },
    get_show_line: function() {
        if (_super_orderline.get_show_line.apply(this, arguments)) {
            if (this.pos.discount_product_by_id[this.product.id]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    },
    can_be_merged_with: function(orderline) {
        var product_price = this.pos.dp['Product Price'];
        var this_price = parseFloat(round_di(this.price || 0, product_price).toFixed(product_price));
        var line_price = orderline.get_product().get_price(orderline.order.pricelist, this.get_quantity());
        if (this.get_product().id !== orderline.get_product().id) {
            return false;
        } else if (!this.get_unit() || !this.get_unit().is_pos_groupable) {
            return false;
        } else if (this.get_product_type() !== orderline.get_product_type()) {
            return false;
        } else if (this.get_line_discount() > 0) {
            return false;
        } else if (!utils.float_is_zero(this_price - line_price, this.pos.currency.decimals)) {
            return false;
        } else if (this.product.tracking == 'lot') {
            return false;
        } else {
            return true;
        }
    },
});

var _super_order = models.Order.prototype;
models.Order = models.Order.extend({

    initialize: function() {
        _super_order.initialize.apply(this, arguments);
        this.order_discount = this.order_discount || 0.0;
        this.order_discount_text = this.order_discount_text || '';
        this.order_discount_label = this.order_discount_label || '';
        this.save_to_db();
    },
    init_from_JSON: function(json) {
        _super_order.init_from_JSON.apply(this, arguments);
        this.order_discount = json.order_discount || 0.0;
        this.order_discount_text = json.order_discount_text || '';
        this.order_discount_label = json.order_discount_label || '';
    },
    export_as_JSON: function() {
        var json = _super_order.export_as_JSON.apply(this, arguments);
        json.order_discount = this.order_discount;
        json.order_discount_text = this.order_discount_text;
        json.order_discount_label = this.order_discount_label;
        return json;
    },
    export_for_printing: function() {
        var receipt = _super_order.export_for_printing.apply(this, arguments);
        receipt.order_discount = this.get_order_discount();
        receipt.order_discount_str = this.get_order_discount_str();
        receipt.order_discount_text = this.get_order_discount_text();
        receipt.order_discount_label = this.get_order_discount_label();
        receipt.discount_price = this.get_price_discount();
        receipt.discount_tax_codes = this.get_discount_tax_codes();
        receipt.discount_details_text = this.get_discount_details_text();
        receipt.bonus_ids = this.get_bonus_ids_list();
        receipt.discount_ids = this.get_discount_ids_list();
        this.pos.set_header_and_footer(self.order, receipt);
        return receipt;
    },
    get_order_discount: function() {
        return this.order_discount;
    },
    get_order_discount_str: function() {
		var decimals = this.pos.dp['Discount'];
        var disc = field_utils.format.float(this.get_order_discount(), {digits: [69, decimals]})
        return disc.replace(/0+$/, '').replace(/\.+$/, '').replace(/,+$/, '');
    },
    get_order_discount_text: function() {
        return this.order_discount_text;
    },
    get_order_discount_label: function() {
        return this.order_discount_label;
    },
    get_price_discount: function() {
        var result = 0.0;
        var lines = this.get_orderlines();
        for (var i = 0; i < lines.length; i++) {
            result += lines[i].get_display_price() - lines[i].get_line_display_price();
        }
        return result;
    },
    get_total_discount: function() {
        var rounding = this.pos.currency.rounding;
        return this.orderlines.reduce((function(sum, orderLine) {
            var amount = orderLine.get_quantity() * orderLine.get_unit_price();
            var discount = 0.0;
            if (orderLine.get_with_discount()) {
                discount += round_pr(round_pr((amount - discount) * orderLine.get_line_discount(), rounding) / 100, rounding);
                discount += round_pr(round_pr((amount - discount) * orderLine.get_order_discount(), rounding) / 100, rounding);
            }
            return sum + discount;
        }), 0);
    },
    get_discount_tax_codes: function() {
        var result = '';
        var lines = this.get_orderlines();
        var cash_rounding_product_id = this.pos.config.cash_rounding_product_id[0];
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].product.id != cash_rounding_product_id) {
                if (!result) {
                    result = lines[i].get_tax_codes();
                } else if (result != lines[i].get_tax_codes()) {
                    result = '';
                    break;
                }
            }
        }
        return result;
    },
    get_discount_details_text: function() {
        var tax_codes = {};
        var lines = this.get_orderlines();
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].get_with_discount()) {
                if (!tax_codes[lines[i].get_tax_codes()]) {
                    tax_codes[lines[i].get_tax_codes()] = 0;
                }
                tax_codes[lines[i].get_tax_codes()] = tax_codes[lines[i].get_tax_codes()] + lines[i].get_line_display_price();
            }
        }
        var result = '';
        var discount = this.get_order_discount();
        var rounding = this.pos.currency.rounding;
        var decimals = this.pos.currency.decimals;
        result += Object.keys(tax_codes).map(function(key) {
            var amount = round_pr(tax_codes[key] * discount / 100, rounding);
            if (amount != 0.0) {
                if (key) {
                    return ' ' + key + ': ' + field_utils.format.float(amount, {digits: [69, decimals]});
                } else {
                    return ' ' + field_utils.format.float(amount, {digits: [69, decimals]});
                }
            }
        });
        result = result.trim();
        if (result.length > 0 && result[0] == ',') {
            result = result.substring(1);
        }
        if (result.length > 0 && result[result.length - 1] == ',') {
            result = result.substring(0, result.length - 1);
        }
        result = result.replace(',,', ',');
        return result.trim();
    },
    set_order_discount_amount: function(value) {
        this.order_discount = Math.max(0, Math.min(100, value));
        var lines = this.get_orderlines();
        for (var i = 0; i < lines.length; i++) {
            lines[i].trigger('change', lines[i]);
        }
        this.trigger('change');
    },
    set_order_discount_text: function(value) {
        this.order_discount_text = value;
        var lines = this.get_orderlines();
        for (var i = 0; i < lines.length; i++) {
            lines[i].trigger('change', lines[i]);
        }
        this.trigger('change');
    },
    set_order_discount_label: function(value) {
        this.order_discount_label = value;
        var lines = this.get_orderlines();
        for (var i = 0; i < lines.length; i++) {
            lines[i].trigger('change', lines[i]);
        }
        this.trigger('change');
    },

    bonus_is_calculated: function() {
        if (this.pos.reprint_order || this.pos.bonus_calc_orders_by_id[this.uid]) {
            return true;
        } else {
            return false;
        }
    },

    discount_is_calculated: function() {
        if (this.pos.reprint_order || this.pos.discount_calc_orders_by_id[this.uid]) {
            return true;
        } else {
            return false;
        }
    },

    get_bonus_ids_list: function() {
        if (!this.pos) {
            return undefined;
        }

        var client = this.get('client')
        if (!client) {
            return undefined;
        }
        if (client.parent_id) {
            client = this.pos.db.get_partner_by_id(client.id);
        }

        var bonus_is_calculated = this.bonus_is_calculated();
        if (!bonus_is_calculated) {
            this.pos.bonus_calc_orders_by_id[this.uid] = true;
        }

        var total = 0.0;
        var orderlines = this.get_orderlines();
        for (var i = 0; i < orderlines.length; i++) {
            total += orderlines[i].get_display_price();
        }

        var result = [];
        for (var i = 0; i < this.pos.bonus_product_ids.length; i++) {
            var total_old = 0.0;
            var product = this.pos.bonus_product_ids[i];
            for (var j = 0; j < client.bonus_ids.length; j++) {
                var bonus_id = this.pos.bonus_by_id[client.bonus_ids[j]];
                if (bonus_id.product_id[0] == product.id) {
                    total_old += bonus_id.amount_used;
                }
            }
            var id = client.id.toString() + '_' + product.id.toString();
            var bonus_amount = this.pos.bonus_amount_by_partner_id_and_product_id[id];
            if (!bonus_amount) {
                bonus_amount = 0.0;
            }
            total_old += bonus_amount;

            var amount = product.bonus_discount * total / 100;

            var total_new = total_old;
            if (bonus_is_calculated) {
                total_old -= amount;
            } else {
                this.pos.bonus_amount_by_partner_id_and_product_id[id] = bonus_amount + amount;
                total_new += amount;
            }
            if (total_new != 0.0) {
                result[i] = {'product': product.name, 'total_old': total_old, 'amount': amount, 'total_new': total_new};
            }
        }
        return result;
    },

    set_discount: function(product_id) {
        if (!this.pos) {
            return;
        }
        var client = this.get('client')
        if (!client) {
            return;
        }
        if (client.parent_id) {
            client = this.pos.db.get_partner_by_id(client.id);
        }

        var discount_is_calculated = this.discount_is_calculated();
        if (!discount_is_calculated) {
            this.pos.discount_calc_orders_by_id[this.uid] = true;
        }

        if (product_id) {

            var max_amount = 0.0;
            var orderlines = this.get_orderlines();
            for (var i = 0; i < orderlines.length; i++) {
                max_amount += orderlines[i].get_display_price();
            }

            var total_old = 0.0;
            var product = this.pos.discount_product_by_id[product_id];
            for (var i = 0; i < client.bonus_ids.length; i++) {
                var bonus_id = this.pos.bonus_by_id[client.bonus_ids[i]];
                if (bonus_id.product_id[0] == product.id) {
                    total_old += bonus_id.amount_used;
                }
            }
            var id = client.id.toString() + '_' + product.id.toString();
            var bonus_amount = this.pos.bonus_amount_by_partner_id_and_product_id[id];
            if (!bonus_amount) {
                bonus_amount = 0.0;
            }
            total_old -= bonus_amount;

            var amount = total_old;
            if (max_amount < amount) {
                amount = max_amount;
            }
            if (amount != 0.0) {
                var product = this.pos.db.get_product_by_id(product_id);
                if (product.taxes_id.length != 0) {
                    var price = amount;
                    this.add_product(product, {quantity: 1, price: - price});
                } else {
                    var total_amount = 0.0;
                    var rounding = this.pos.currency.rounding;
                    var tax_details = this.get_tax_details();
                    for (var i = 0; i < tax_details.length; i++) {
                        var tax_line = tax_details[i];
                        var tax_amount = tax_line.total_included;
                        var price_incl = amount * tax_amount / max_amount;
                        if (tax_line.tax.price_include) {
                            var price = round_pr(price_incl, rounding);
                        } else {
                            var price = 100 * price_incl / (100 + tax_line.tax.amount);
                        }
                        if (price != 0.0) {
                            this.add_product(product, {quantity: 1, price: - price, taxes_id: [tax_line.tax.id]});
                            total_amount = round_pr(total_amount + price_incl, rounding);
                        }
                    }
                    if (total_amount != amount) {
                        var price = amount - total_amount;
                        this.add_product(product, {quantity: 1, price: - price, taxes_id: []});
                    }
                }
            }
            this.pos.bonus_amount_by_partner_id_and_product_id[id] = bonus_amount + amount;

        } else {

            for (var i = 0; i < client.bonus_ids.length; i++) {
                var amount = 0.0;
                var bonus_id = this.pos.bonus_by_id[client.bonus_ids[i]];
                var product = this.pos.discount_product_by_id[bonus_id.product_id[0]];
                if (product) {
                    var orderlines = this.get_orderlines();
                    for (var j = orderlines.length - 1; j >= 0; j--) {
                        if (orderlines[j].product.id == product.id) {
                            amount += orderlines[j].get_display_price();
                            this.remove_orderline(orderlines[j]);
                        }
                    }
                    var id = client.id.toString() + '_' + product.id.toString();
                    var bonus_amount = this.pos.bonus_amount_by_partner_id_and_product_id[id];
                    this.pos.bonus_amount_by_partner_id_and_product_id[id] = bonus_amount + amount;
                }
            }
        }
    },

    get_discount_ids_list: function() {
        if (!this.pos) {
            return undefined;
        }

        var client = this.get('client')
        if (!client) {
            return undefined;
        }
        if (client.parent_id) {
            client = this.pos.db.get_partner_by_id(client.id);
        }

        var discount_is_calculated = this.discount_is_calculated();

        var result = [];
        for (var i = 0; i < this.pos.discount_product_ids.length; i++) {
            var total_old = 0.0;
            var product = this.pos.discount_product_ids[i];
            for (var j = 0; j < client.bonus_ids.length; j++) {
                var bonus_id = this.pos.bonus_by_id[client.bonus_ids[j]];
                if (bonus_id.product_id[0] == product.id) {
                    total_old += bonus_id.amount_used;
                }
            }
            var id = client.id.toString() + '_' + product.id.toString();
            var discount_amount = this.pos.bonus_amount_by_partner_id_and_product_id[id];
            if (!discount_amount) {
                discount_amount = 0.0;
            }
            total_old -= discount_amount;

            var amount = 0.0;
            var orderlines = this.get_orderlines();
            for (var j = 0; j < orderlines.length; j++) {
                if (orderlines[j].product.id == product.id) {
                    amount += orderlines[j].get_display_price();
                }
            }

            var total_new = total_old;
            if (discount_is_calculated) {
                total_old -= amount;
            } else {
                this.pos.bonus_amount_by_partner_id_and_product_id[id] = discount_amount - amount;
                total_new += amount;
            }
            if (total_old != 0.0 || amount != 0.0 || total_new != 0.0) {
                result.push({'product': product.name, 'total_old': total_old, 'amount': amount, 'total_new': total_new});
            }
        }
        return result;
    },

});

return models;

});
