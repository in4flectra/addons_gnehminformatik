flectra.define('in4invoice_discount_point_of_sale.screens', function (require) {
'use strict';

var core = require('web.core');
var screens = require('point_of_sale.screens');
var widgets = require('pos_order_mgmt.widgets');

var _t = core._t;


var OrderDiscountButton = screens.ActionButtonWidget.extend({
    template: 'OrderDiscountButton',
    discount: function() {
        var result = _t('% Discount');
		var order = this.pos.get_order();
        if (order) {
        	if (order.order_discount_label) {
            	result = order.order_discount_label;
			} else if (order.order_discount != 0.0) {
                result = order.order_discount + ' ' + result;
			}
        }
        return result;
    },
    button_click: function() {
        var self = this;
        this.gui.show_popup('number', {
            'title': _t('Discount (%)'),
            'type': 'discount',
            'value': this.pos.get_order().order_discount,
            'confirm': function(value) {
                console.log('confirm');
                console.log(value);
                var values = value.split(',');
                var amount = Math.max(0, Math.min(100, values[0]));
                var text = '';
                var label = '';
                if (values.length > 1) {
                    text = values[1]
                }
                if (values.length > 2) {
                    label = values[2]
                }
                self.pos.get_order().set_order_discount_amount(amount);
                self.pos.get_order().set_order_discount_text(text);
                self.pos.get_order().set_order_discount_label(label);
                self.renderElement();
            },
        });
    },
});

screens.OrderWidget.include({
    update_summary: function(){
        this._super();

        if (this.getParent().action_buttons &&
            this.getParent().action_buttons.discount) {
            this.getParent().action_buttons.discount.renderElement();
        }

        var discount_node = this.el.querySelector('.order-discount');
        if (discount_node) {
            var order = this.pos.get_order();
            if (order.get_order_discount() == 0.0) {
                discount_node.setAttribute('hidden', 'true');
            } else {
                var discount_text = order.get_order_discount_text();
                if (!discount_text) {
                    discount_text = order.get_order_discount() + ' ' + _t('% Discount');
                }

                var discount_amount = 0.0;
                var lines = order.get_orderlines();
                for (var i = 0; i < lines.length; i++) {
                    discount_amount = discount_amount + lines[i].get_display_price() - lines[i].get_line_display_price();
                }
                discount_node.removeAttribute('hidden');
                this.el.querySelector('.order-discount > .label').textContent = discount_text;
                this.el.querySelector('.order-discount > .value').textContent = this.format_currency(discount_amount);
            }
        }
    },
});

screens.define_action_button({
    'name': 'discount',
    'widget': OrderDiscountButton,
    'condition': function() {
        return this.pos.config.iface_show_order_discounts;
    },
});

var PaymentScreenWidget = screens.PaymentScreenWidget;

PaymentScreenWidget.include({

    click_discount_product: function(id) {
        var self  = this;
        var order = this.pos.get_order();

        order.set_discount(id);
        self.order_changes();
        self.render_paymentlines();
    },

    customer_changed: function() {
        this._super();

        var client = this.pos.get_client();
        if (client && client.parent_id) {
            client = this.pos.db.get_partner_by_id(client.id)
        }
        var discounts = this.$('.js_discount_product');
        for (var i = 0; i < discounts.length; i++) {
            discounts[i].hidden = true;
            var product_id = discounts[i].getAttribute('data-id');
            if (client && product_id) {
                for (var j = 0; j < client.bonus_ids.length; j++) {
                    var bonus_id = this.pos.bonus_by_id[client.bonus_ids[j]];
                    if (bonus_id.product_id[0] == product_id) {
                        var id = client.id.toString() + '_' + product_id.toString();
                        var discount_amount = this.pos.bonus_amount_by_partner_id_and_product_id[id];
                        if (!discount_amount) {
                            discount_amount = 0.0;
                        }
                        var total = bonus_id.amount_used - discount_amount;
                        if (total != 0.0) {
                            discounts[i].innerText = bonus_id.product_id[1] + ': ' + this.format_currency(total);
                            discounts[i].hidden = null;
                            break
                        }
                    }
                }
            }
        }
    },

    click_back: function() {
        this.click_discount_product();
        this._super();
    },

    render_paymentlines: function() {
        this._super();
        this.customer_changed();
    },

    renderElement: function() {
        var self = this;
        this._super();

        this.$('.js_discount_product').click(function() {
            self.click_discount_product($(this).data('id'));
        });
    },

});

widgets.OrderListScreenWidget.include({

    _prepare_order_from_order_data: function (order_data, action) {
        var order = this._super(order_data, action);
        if (order_data && order) {
            order.order_discount = order_data.order_discount || 0.0;
            order.order_discount_text = order_data.order_discount_text || '';
            order.order_discount_label = order_data.order_discount_label || '';
        }
        return order;
    },
    _get_orderline_options_from_order_data: function (qty, line) {
        var options = this._super(qty, line);
        options['discount'] = line.line_discount || 0.0;
        options['discount_text'] = line.line_discount_text || '';
        return options;
    },

});

return {
    OrderDiscountButton: OrderDiscountButton,
};

});
