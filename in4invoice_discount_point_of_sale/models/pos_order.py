# -*- coding: utf-8 -*-

from flectra import api, fields, models, _
from flectra.exceptions import UserError


class In4InvoiceDiscountPointOfSalePOSOrder(models.Model):

    _inherit = 'pos.order'

    @api.model
    def _amount_line_tax(self, line, fiscal_position_id):
        taxes = line.tax_ids.filtered(lambda t: t.company_id.id == line.order_id.company_id.id)
        if fiscal_position_id:
            taxes = fiscal_position_id.map_tax(taxes, line.product_id, line.order_id.partner_id)
        curr = line.order_id.pricelist_id.currency_id
        if (line.qty != 0.0) and line.with_discount:
            amount = line.qty * line.price_unit
            discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
            amount -= discount
            discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
            price_reduce = (amount - discount) / line.qty
        else:
            price_reduce = line.price_unit
        taxes = taxes.compute_all(
            price_reduce,
            curr,
            line.qty,
            product=line.product_id,
            partner=line.order_id.partner_id or False,
        )
        return sum(tax.get('amount', 0.0) for tax in taxes.get('taxes', []))

    order_discount = fields.Float(string='Order Discount (%)', readonly=True)
    order_discount_text = fields.Char(string='Order Discount Text', readonly=True)
    order_discount_label = fields.Char(string='Order Discount Label', readonly=True)

    @api.onchange('order_discount')
    def _onchange_order_discount(self):
        for line in self.lines:
            line._onchange_qty()

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(In4InvoiceDiscountPointOfSalePOSOrder, self)._order_fields(ui_order)
        order_fields['order_discount'] = ui_order.get('order_discount', 0)
        order_fields['order_discount_text'] = ui_order.get('order_discount_text', 0)
        order_fields['order_discount_label'] = ui_order.get('order_discount_label', 0)
        return order_fields

    def _prepare_invoice(self):
        result = super(In4InvoiceDiscountPointOfSalePOSOrder, self)._prepare_invoice()
        result.update({
            'order_discount': self.order_discount,
            'order_discount_text': self.order_discount_text,
            'order_discount_label': self.order_discount_label,
        })
        return result

    def _action_create_invoice_line(self, line, invoice_id):
        result = super(In4InvoiceDiscountPointOfSalePOSOrder, self)._action_create_invoice_line(line, invoice_id)
        if line:
            result.sudo().write({
                'line_discount': line.line_discount,
                'line_discount_text': line.line_discount_text,
            })
            if line.product_id.id in line.order_id.session_id.config_id.discount_product_ids.ids:
                result.sudo().write({
                    'sequence': 9900 + (line.tax_ids[0].sequence if line.tax_ids else 0),
                    'is_bonus_line': True,
                    'invoice_line_tax_ids': line.tax_ids.ids and [[6, 0, line.tax_ids.ids]] or False,
                })
        return result

    @api.multi
    def action_pos_order_paid(self, to_invoice=False):
        result = super(In4InvoiceDiscountPointOfSalePOSOrder, self).action_pos_order_paid(to_invoice)
        for order in self:
            if order.partner_id:
                bonus = self.env['res.partner.bonus']
                partner_id = order.partner_id
                if partner_id.parent_id:
                    partner_id = partner_id.parent_id

                total = 0.0
                for line in order.lines:
                    total += line.price_subtotal_incl
                if total != 0.0:
                    domain = [('id', 'in', order.session_id.config_id.bonus_product_ids.ids)]
                    product_ids = self.env['product.product'].search(domain)
                    for product_id in product_ids:
                        amount = product_id.bonus_discount * total / 100
                        domain = [('partner_id', '=', partner_id.id), ('product_id', '=', product_id.id)]
                        bonus_id = bonus.search(domain, limit=1)
                        if bonus_id:
                            bonus_id.write({'amount_calc': bonus_id.amount_calc + amount})
                        else:
                            bonus.create({
                                'partner_id': partner_id.id,
                                'product_id': product_id.id,
                                'amount_calc': amount,
                            })

                if not to_invoice:
                    domain = [('id', 'in', order.session_id.config_id.discount_product_ids.ids)]
                    product_ids = self.env['product.product'].search(domain)
                    for product_id in product_ids:
                        amount = 0.0
                        for line in order.lines:
                            if line.product_id.id == product_id.id:
                                amount += line.price_subtotal_incl
                        if amount != 0.0:
                            domain = [('partner_id', '=', partner_id.id), ('product_id', '=', product_id.id)]
                            bonus_id = bonus.search(domain, limit=1)
                            if bonus_id:
                                bonus_id.write({'amount_calc': bonus_id.amount_calc + amount})
                            else:
                                bonus.create({
                                    'partner_id': partner_id.id,
                                    'product_id': product_id.id,
                                    'amount_calc': amount,
                                })

            return result

    @api.multi
    def _prepare_done_order_for_pos(self):
        result = super(In4InvoiceDiscountPointOfSalePOSOrder, self)._prepare_done_order_for_pos()
        result.update({
            'order_discount': self.order_discount,
            'order_discount_text': self.order_discount_text,
            'order_discount_label': self.order_discount_label,
        })
        return result

    @api.multi
    def _prepare_done_order_line_for_pos(self, order_line):
        result = super(In4InvoiceDiscountPointOfSalePOSOrder, self)._prepare_done_order_line_for_pos(order_line)
        result.update({
            'line_discount': order_line.line_discount,
            'line_discount_text': order_line.line_discount_text,
        })
        return result

    @api.model
    def get_reduced_price_for_anglosaxon_lines(self, line):
        if line.qty != 0.0 and line.with_discount:
            curr = line.order_id.pricelist_id.currency_id
            amount = line.qty * line.price_unit
            discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
            amount -= discount
            discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
            return (amount - discount) / line.qty
        else:
            return line.price_unit


class In4InvoiceDiscountPointOfSalePOSOrderLine(models.Model):

    _inherit = 'pos.order.line'

    @api.depends('product_id')
    def _compute_with_discount(self):
        for line in self:
            if (   (line.product_id.id == line.order_id.session_id.config_id.tip_product_id.id)
                or (line.product_id.id == line.order_id.session_id.config_id.cash_rounding_product_id.id)
                or (line.product_id.id in line.order_id.session_id.config_id.discount_product_ids.ids)
            ):
                line.with_discount = False
            else:
                line.with_discount = True

    @api.depends('line_discount', 'order_id', 'order_id.order_discount')
    def _compute_discount(self):
        for line in self:
            if line.with_discount:
                line_value = (100 - (line.line_discount or 0.0)) / 100
                order_value = (100 - (line.order_id.order_discount or 0.0)) / 100
                line.discount = 100 * (1 - (line_value * order_value))
            else:
                line.discount = 0.0

    @api.depends('line_discount', 'line_discount_text', 'order_id', 'order_id.order_discount', 'order_id.order_discount_text')
    def _compute_discount_text(self):
        for line in self:
            line.discount_text = line.line_discount_text

    @api.multi
    def _inverse_discount_text(self):
        for line in self:
            line.line_discount_text = line.discount_text

    with_discount = fields.Boolean(string='With Discount', compute=_compute_with_discount, store=True, readonly=True)
    discount = fields.Float(compute=_compute_discount, store=True, readonly=True)
    discount_text = fields.Char(compute=_compute_discount_text, inverse=_inverse_discount_text, store=True, readonly=True)
    line_discount = fields.Float(string='Line Discount (%)')
    line_discount_text = fields.Char(string='Line Discount Text')

    @api.depends('product_id', 'qty', 'price_unit', 'tax_ids', 'line_discount', 'order_id', 'order_id.order_discount')
    def _compute_amount_line_all(self):
        for line in self:
            fpos = line.order_id.fiscal_position_id
            tax_ids_after_fiscal_position = fpos.map_tax(line.tax_ids, line.product_id, line.order_id.partner_id) if fpos else line.tax_ids
            curr = line.order_id.pricelist_id.currency_id
            if (line.qty != 0.0) and line.with_discount:
                amount = line.qty * line.price_unit
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.qty
            else:
                price_reduce = line.price_unit
            taxes = tax_ids_after_fiscal_position.compute_all(
                price_reduce,
                curr,
                line.qty,
                product=line.product_id,
                partner=line.order_id.partner_id,
            )
            line.update({
                'price_subtotal_incl': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    @api.onchange('qty', 'price_unit', 'tax_ids', 'line_discount', 'order_id')
    def _onchange_qty(self):
        if self.product_id:
            if not self.order_id.pricelist_id:
                raise UserError(_('You have to select a pricelist in the sale form !'))
            curr = self.order_id.pricelist_id.currency_id
            if (self.qty != 0.0) and self.with_discount:
                amount = self.qty * self.price_unit
                discount = curr.round(curr.round(amount * (self.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (self.order_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / self.qty
            else:
                price_reduce = self.price_unit
            self.price_subtotal = self.price_subtotal_incl = price_reduce * self.qty
            if self.product_id.taxes_id:
                taxes = self.product_id.taxes_id.compute_all(
                    price_reduce,
                    curr,
                    self.qty,
                    product=self.product_id,
                    partner=False,
                )
                self.price_subtotal = taxes['total_excluded']
                self.price_subtotal_incl = taxes['total_included']


class In4InvoiceDiscountPointOfSaleReportSaleDetails(models.AbstractModel):

    _inherit = 'report.point_of_sale.report_saledetails'

    @api.model
    def get_reduced_price(self, line):
        if line.qty != 0.0 and line.with_discount:
            curr = line.order_id.pricelist_id.currency_id
            amount = line.qty * line.price_unit
            discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
            amount -= discount
            discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
            return (amount - discount) / line.qty
        else:
            return line.price_unit

    @api.model
    def get_show_line(self, line):
        result = super(In4InvoiceDiscountPointOfSaleReportSaleDetails, self).get_show_line(line)
        if result:
            result = line.product_id.id not in line.order_id.config_id.discount_product_ids.ids
        return result

    @api.model
    def get_show_price(self, line):
        result = super(In4InvoiceDiscountPointOfSaleReportSaleDetails, self).get_show_price(line)
        if result:
            result = line.product_id.id not in line.order_id.config_id.discount_product_ids.ids
        return result
