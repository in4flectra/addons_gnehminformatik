# -*- coding: utf-8 -*-

from flectra import fields, models


class In4InvoiceDiscountPointOfSalePOSConfig(models.Model):

    _inherit = 'pos.config'

    iface_show_order_discounts = fields.Boolean(string='Show Order Discounts', default=False, help='Enables discounts over the whole order')
    bonus_product_ids = fields.Many2many(
        'product.product',
        'pos_config_product_product_bonus_rel',
        'pos_config_id',
        'product_product_id',
        string='Available Bonus Products',
        domain="[('bonus', '=', True)]",
    )
    discount_product_ids = fields.Many2many(
        'product.product',
        'pos_config_product_product_discount_rel',
        'pos_config_id',
        'product_product_id',
        string='Available Discount Products',
        domain="[('bonus', '=', True)]",
    )
