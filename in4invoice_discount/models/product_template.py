# -*- coding: utf-8 -*-

from flectra import fields, models


class In4InvoiceDiscountProductTemplate(models.Model):

    _inherit = 'product.template'

    bonus = fields.Boolean(string='Bonus', default=False)
    bonus_discount = fields.Float(string='Discount (%)')
