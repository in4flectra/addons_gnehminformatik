# -*- coding: utf-8 -*-

from functools import partial

from flectra import api, fields, models, _
from flectra.tools.misc import formatLang


class In4InvoiceDiscountSaleOrder(models.Model):

    _inherit = 'sale.order'

    def _compute_available_discount_product_ids(self):
        product = self.env['product.product'].sudo()
        get_param =self.env['ir.config_parameter'].sudo().get_param
        product_ids = product.search([('id', 'in', eval(get_param('sale_order_discount_product_ids', '[]')))]).ids
        for order in self:
            order.available_discount_product_ids = product_ids

    @api.model
    def _default_discount_product_ids(self):
        product = self.env['product.product'].sudo()
        get_param =self.env['ir.config_parameter'].sudo().get_param
        return product.search([('id', 'in', eval(get_param('sale_order_discount_product_ids', '[]')))]).ids

    @api.depends('order_line.price_total')
    def _amount_all(self):
        super(In4InvoiceDiscountSaleOrder, self)._amount_all()
        for order in self:
            discount_price_subtotal = sum((line.price_subtotal - line.line_price_subtotal) for line in order.order_line)
            discount_price_total = sum((line.price_total - line.line_price_total) for line in order.order_line)
            order.update({
                'discount_price_subtotal': discount_price_subtotal,
                'discount_price_total': discount_price_total,
            })

    @api.multi
    def _discount_taxes(self):
        precision = self.env['decimal.precision'].precision_get('Account')
        for order in self:
            codes = {}
            for line in self.order_line.filtered(lambda l: l.is_bonus_line == False):
                if line.tax_id:
                    for tax in line.tax_id:
                        codes.setdefault(tax, {
                            'name': tax.description or tax.name,
                            'amount': 0.0,
                        })
                        codes[tax]['amount'] += line.line_price_total - line.price_total
                else:
                    codes.setdefault(False, {
                        'name': '',
                        'amount': 0.0,
                    })
                    codes[False]['amount'] += line.line_price_total - line.price_total
            codes = sorted(codes.items(), key=lambda c: c[0].sequence if c[0] else -1)
            code_count = 0
            for id, values in codes:
                if values['amount'] != 0.0:
                    code_count += 1
            if code_count == 0:
                order.discount_tax_codes = None
                order.discount_details_text = None
            elif code_count == 1:
                for id, values in codes:
                    if values['amount'] != 0.0:
                        order.discount_tax_codes = values['name']
                        order.discount_details_text = None
            else:
                text = []
                for id, values in codes:
                    if values['amount'] != 0.0:
                        amount = '%.{0}f'.format(precision) % values['amount']
                        if id == 0:
                            text.append(_('%s without VAT') % (amount))
                        else:
                            text.append(_('%s with %s VAT') % (amount, values['name']))
                order.discount_tax_codes = None
                order.discount_details_text = '(' + (', '.join(text)) + ')'

    order_discount = fields.Float(string='Order Discount (%)', readonly=True, states={'draft': [('readonly', False)]})
    order_discount_text = fields.Char(string='Order Discount Text', readonly=True, states={'draft': [('readonly', False)]})
    discount_price_subtotal = fields.Monetary(string='Discount Amount', compute=_amount_all, store=True, readonly=True, help='Total discount amount without taxes')
    discount_price_total = fields.Monetary(string='Discount Amount', compute=_amount_all, store=True, readonly=True, help='Total discount amount with taxes')
    discount_tax_codes = fields.Char(string='Discount Tax Codes', compute=_discount_taxes)
    discount_details_text = fields.Char(string='Discount Tax Codes', compute=_discount_taxes)

    with_bonus = fields.Boolean(
        string='With Bonus',
        default=False,
        readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
    )
    available_discount_product_ids = fields.Many2many(
        'product.product',
        string='Available Discount Products',
        compute=_compute_available_discount_product_ids,
    )
    discount_product_ids = fields.Many2many(
        'product.product',
        'sale_order_product_product_discount_rel',
        'sale_order_id',
        'product_product_id',
        string='Discount Products',
        default=_default_discount_product_ids,
        domain="[('id', 'in', available_discount_product_ids)]",
        readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
    )

    def _prepare_tax_line_vals(self, tax):
        vals = {
            'order_id': self.id,
            'name': tax['name'],
            'tax_id': tax['id'],
            'amount': tax['amount'],
            'base': tax['base'],
            'base_calc': tax['base'],
            'manual': False,
            'sequence': tax['sequence'],
            'account_analytic_id': tax['analytic'] and self.analytic_account_id.id or False,
            'account_id': tax['account_id'],
        }
        if not vals.get('account_analytic_id') and self.analytic_account_id and vals['account_id'] == self.analytic_account_id.id:
            vals['account_analytic_id'] = self.analytic_account_id.id
        return vals

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        curr = self.currency_id or self.company_id.currency_id
        for line in self.order_line:
            if (line.product_uom_qty != 0.0) and line.with_discount:
                amount = line.product_uom_qty * line.price_unit
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.product_uom_qty
            else:
                price_reduce = line.price_unit
            taxes = line.tax_id.compute_all(
                price_reduce,
                quantity=line.product_uom_qty,
                product=line.product_id,
                partner=self.partner_shipping_id
            )
            for tax in taxes.get('taxes', []):
                val = self._prepare_tax_line_vals(tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = curr.round(val['base'])
                    tax_grouped[key]['base_calc'] = val['base']
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += curr.round(val['base'])
                    tax_grouped[key]['base_calc'] += val['base']
        return tax_grouped

    @api.onchange('partner_id', 'order_discount', 'with_bonus', 'discount_product_ids', 'order_line')
    def _onchange_lines_bonus(self):
        lines_to_remove = self.order_line.filtered(lambda l: l.is_bonus_line)
        if lines_to_remove:
            self.order_line -= lines_to_remove
            self._amount_all()
        if self.with_bonus and self.partner_id:
            partner_id = self.partner_id
            if self.partner_id.parent_id:
                partner_id = self.partner_id.parent_id
            max_amount = self.amount_total
            bonus = self.env['res.partner.bonus']
            domain = [('product_id', 'in', self.discount_product_ids.ids), ('partner_id', '=', partner_id.id)]
            bonus_ids = bonus.search(domain, order='amount_used asc')
            for bonus_id in bonus_ids:
                if max_amount > bonus_id.amount_used:
                    bonus_amount = bonus_id.amount_used
                else:
                    bonus_amount = max_amount
                if bonus_amount != 0.0:
                    product_id = bonus_id.product_id
                    account_id = product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id
                    if product_id.taxes_id:
                        price = bonus_amount
                        bonus_line = self.env['sale.order.line'].new({
                            'order_id': self.id,
                            'product_id': product_id.id,
                            'name': product_id.name,
                            'account_id': account_id.id,
                            'product_uom_qty': 1,
                            'product_uom': product_id.uom_id.id,
                            'price_unit': - price,
                            'tax_id': product_id.taxes_id,
                            'is_bonus_line': True,
                            'sequence': 9900
                        })
                        if bonus_line not in self.order_line:
                            self.order_line += bonus_line
                    else:
                        total_amount = 0.0
                        tax_line_ids = self.get_taxes_values()
                        for tax_line in tax_line_ids.values():
                            tax_id = self.env['account.tax'].browse(tax_line['tax_id'])
                            tax_amount = tax_line['base_calc'] + tax_line['amount']
                            price_incl = bonus_amount * tax_amount / max_amount
                            if tax_id.price_include:
                                price = self.currency_id.round(price_incl)
                            else:
                                price = 100 * price_incl / (100 + tax_id.amount)
                            bonus_line = self.env['sale.order.line'].new({
                                'order_id': self.id,
                                'product_id': product_id.id,
                                'name': product_id.name,
                                'account_id': account_id.id,
                                'product_uom_qty': 1,
                                'product_uom': product_id.uom_id.id,
                                'price_unit': - price,
                                'tax_id': [(6, 0, [tax_line['tax_id']])],
                                'is_bonus_line': True,
                                'sequence': 9900 + tax_id.sequence
                            })
                            if bonus_line not in self.order_line:
                                self.order_line += bonus_line
                            total_amount = self.currency_id.round(total_amount + price_incl)
                        if total_amount != bonus_amount:
                            price = bonus_amount - total_amount
                            bonus_line = self.env['sale.order.line'].new({
                                'order_id': self.id,
                                'product_id': product_id.id,
                                'name': product_id.name,
                                'account_id': account_id.id,
                                'product_uom_qty': 1,
                                'product_uom': product_id.uom_id.id,
                                'price_unit': - price,
                                'is_bonus_line': True,
                                'sequence': 9900
                            })
                            if bonus_line not in self.order_line:
                                self.order_line += bonus_line
                max_amount = self.currency_id.round(max_amount - bonus_amount)
            self._amount_all()

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        result = super(In4InvoiceDiscountSaleOrder, self).action_invoice_create(grouped, final)
        for id in result:
            invoice = self.env['account.invoice'].browse(id)
            if invoice:
                product_ids = []
                for product_id in invoice.discount_product_ids.ids:
                    if product_id not in self.discount_product_ids.ids:
                        product_ids.append((3, product_id))
                for product_id in self.discount_product_ids.ids:
                    if product_id not in invoice.discount_product_ids.ids:
                        product_ids.append((4, product_id))
                invoice.write({
                    'order_discount': self.order_discount,
                    'order_discount_text': self.order_discount_text,
                    'with_bonus': True,
                    'discount_product_ids': product_ids,
                })
                invoice._onchange_lines_bonus()
        return result

    @api.multi
    def _get_tax_amount_by_group(self):
        self.ensure_one()
        res = {}
        curr = self.currency_id or self.company_id.currency_id
        for line in self.order_line:
            if (line.product_uom_qty != 0.0) and line.with_discount:
                amount = line.product_uom_qty * line.price_unit
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.product_uom_qty
            else:
                price_reduce = line.price_unit
            taxes = line.tax_id.compute_all(
                price_reduce,
                quantity=line.product_uom_qty,
                product=line.product_id,
                partner=self.partner_shipping_id,
            )
            for tax in line.tax_id:
                group = tax.tax_group_id
                res.setdefault(group, {'amount': 0.0, 'base': 0.0})
                for t in taxes.get('taxes', []):
                    if t['id'] == tax.id or t['id'] in tax.children_tax_ids.ids:
                        res[group]['amount'] += t['amount']
                        if self._is_tax_excluded():
                            res[group]['base'] += t['base']
                        else:
                            res[group]['base'] += t['base'] + t['amount']
        res = sorted(res.items(), key=lambda r: r[0].sequence)
        curr_fmt = partial(formatLang, self.with_context(lang=self.partner_id.lang).env, currency_obj=curr)
        res_len = len(res)
        res = [(
            r[0].name,                # 0
            r[1]['amount'],           # 1
            r[1]['base'],             # 2
            res_len,                  # 3
            curr_fmt(r[1]['amount']), # 4
            curr_fmt(r[1]['base']),   # 5
        ) for r in res]
        return res


class In4InvoiceDiscountSaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    @api.depends('product_id', 'is_downpayment')
    def _compute_with_discount(self):
        for line in self:
            if line.product_id.id in line.order_id.discount_product_ids.ids:
                line.with_discount = False
            else:
                line.with_discount = True

    @api.depends('line_discount', 'order_id', 'order_id.order_discount')
    def _compute_discount(self):
        for line in self:
            if line.with_discount:
                line_value = (100 - (line.line_discount or 0.0)) / 100
                order_value = (100 - (line.order_id.order_discount or 0.0)) / 100
                line.discount = 100 * (1 - (line_value * order_value))
            else:
                line.discount = 0.0

    @api.depends('line_discount', 'line_discount_text', 'order_id', 'order_id.order_discount', 'order_id.order_discount_text')
    def _compute_discount_text(self):
        lang_id = self.env.user.lang_id
        for line in self:
            if line.with_discount:
                line_text = ''
                order_text = ''
                line_amount = line.line_discount
                order_amount = line.order_id.order_discount
                if (line_amount != 0.0) and ((order_amount != 100.0) or (line_amount == 100.0)):
                    line_text = line.line_discount_text
                    if not line_text:
                        line_text = _('With a %s %% discount') % (str('{:,f}'.format(line_amount)).rstrip('0').rstrip('.').replace(',', lang_id.thousands_sep).replace('.', lang_id.decimal_point))
                if (order_amount != 0.0) and (line_amount != 100.0):
                    order_text = line.order_id.order_discount_text
                    if not order_text:
                        order_text = _('With a %s %% discount') % (str('{:,f}'.format(order_amount)).rstrip('0').rstrip('.').replace(',', lang_id.thousands_sep).replace('.', lang_id.decimal_point))
                if line_text and order_text:
                    if line_amount > order_amount:
                        line.discount_text = order_text + ', ' + line_text
                    else:
                        line.discount_text = line_text + ', ' + order_text
                elif line_text:
                    line.discount_text = line_text
                elif order_text:
                    line.discount_text = order_text
                else:
                    line.discount_text = ''
            else:
                line.discount_text = line.line_discount_text

    @api.depends('product_uom_qty', 'price_unit', 'tax_id', 'line_discount', 'order_id', 'order_id.order_discount')
    def _compute_amount(self):
        for line in self:
            curr = line.order_id.currency_id or line.order_id.company_id.currency_id
            price_reduce = line.price_unit
            amount = line.product_uom_qty * line.price_unit
            discount = 0.0
            if (line.product_uom_qty != 0.0) and line.with_discount:
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.product_uom_qty
            taxes = line.tax_id.compute_all(
                price_reduce,
                currency=curr,
                quantity=line.product_uom_qty,
                product=line.product_id,
                partner=line.order_id.partner_shipping_id
            )
            if line.order_id._is_tax_excluded():
                price_total_display = taxes['total_excluded']
            else:
                price_total_display = taxes['total_included']
            line.update({
                'line_price_subtotal': taxes['total_excluded'],
                'line_price_total': taxes['total_included'],
                'price_total_display': price_total_display,
            })

            if (line.product_uom_qty != 0.0) and line.with_discount:
                amount -= discount
                discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.product_uom_qty
            taxes = line.tax_id.compute_all(
                price_reduce,
                currency=curr,
                quantity=line.product_uom_qty,
                product=line.product_id,
                partner=line.order_id.partner_shipping_id
            )
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_subtotal': taxes['total_excluded'],
                'price_total': taxes['total_included'],
            })

    with_discount = fields.Boolean(string='With Discount', compute=_compute_with_discount, store=True, readonly=True)
    discount = fields.Float(compute=_compute_discount, store=True, readonly=True)
    discount_text = fields.Char(compute=_compute_discount_text, store=True, readonly=True)
    line_discount = fields.Float(string='Line Discount (%)')
    line_discount_text = fields.Char(string='Line Discount Text')

    line_price_subtotal = fields.Monetary(string='Subtotal', compute=_compute_amount, store=True, readonly=True)
    line_price_total = fields.Monetary(string='Total', compute=_compute_amount, store=True, readonly=True)

    is_bonus_line = fields.Boolean(string='Is Bonus Line')

    @api.multi
    def _prepare_invoice_line(self, qty):
        result = {}
        if not self.is_bonus_line:
            result = super(In4InvoiceDiscountSaleOrderLine, self)._prepare_invoice_line(qty)
            result.update({
                'line_discount': self.line_discount,
                'line_discount_text': self.line_discount_text,
            })
        return result

    @api.depends('product_uom_qty', 'price_unit', 'line_discount', 'order_id', 'order_id.order_discount')
    def _get_price_reduce(self):
        for line in self:
            if (line.product_uom_qty != 0.0) and line.with_discount:
                curr = line.order_id.currency_id or line.order_id.company_id.currency_id
                amount = line.product_uom_qty * line.price_unit
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (line.order_id.order_discount or 0.0)) / 100)
                line.price_reduce = (amount - discount) / line.product_uom_qty
            else:
                line.price_reduce = line.price_unit


class In4InvoiceDiscountSaleAdvancePaymentInv(models.TransientModel):

    _inherit = 'sale.advance.payment.inv'

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        result = super(In4InvoiceDiscountSaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        if result:
            product_ids = []
            for product_id in result.discount_product_ids.ids:
                if product_id not in order.discount_product_ids.ids:
                    product_ids.append((3, product_id))
            for product_id in order.discount_product_ids.ids:
                if product_id not in result.discount_product_ids.ids:
                    product_ids.append((4, product_id))
            result.write({
                'order_discount': order.order_discount,
                'order_discount_text': order.order_discount_text,
                'with_bonus': True,
                'discount_product_ids': product_ids,
            })
            result._onchange_lines_bonus()
        return result
