# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceDiscountResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    account_invoice_bonus_product_ids = fields.Many2many(
        'product.product',
        'res_config_settings_product_product_invoice_bonus_rel',
        'res_config_settings_id',
        'product_product_id',
        string='Available Bonus Products for Invoices',
        domain="[('bonus', '=', True)]",
    )
    account_invoice_discount_product_ids = fields.Many2many(
        'product.product',
        'res_config_settings_product_product_invoice_discount_rel',
        'res_config_settings_id',
        'product_product_id',
        string='Available Discount Products for Invoices',
        domain="[('bonus', '=', True)]",
    )
    sale_order_bonus_product_ids = fields.Many2many(
        'product.product',
        'res_config_settings_product_product_sale_order_bonus_rel',
        'res_config_settings_id',
        'product_product_id',
        string='Available Bonus Products for Quotations and Orders',
        domain="[('bonus', '=', True)]",
    )
    sale_order_discount_product_ids = fields.Many2many(
        'product.product',
        'res_config_settings_product_product_sale_order_discount_rel',
        'res_config_settings_id',
        'product_product_id',
        string='Available Discount Products for Quotations and Orders',
        domain="[('bonus', '=', True)]",
    )

    @api.multi
    def set_values(self):
        super(In4InvoiceDiscountResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].sudo().set_param
        set_param('account_invoice_bonus_product_ids', self.account_invoice_bonus_product_ids.ids)
        set_param('account_invoice_discount_product_ids', self.account_invoice_discount_product_ids.ids)
        set_param('sale_order_bonus_product_ids', self.sale_order_bonus_product_ids.ids)
        set_param('sale_order_discount_product_ids', self.sale_order_discount_product_ids.ids)

    @api.model
    def get_values(self):
        result = super(In4InvoiceDiscountResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        product = self.env['product.product'].sudo()
        invoice_bonus = product.search([('id', 'in', eval(get_param('account_invoice_bonus_product_ids', '[]')))])
        invoice_discount = product.search([('id', 'in', eval(get_param('account_invoice_discount_product_ids', '[]')))])
        sale_order_bonus = product.search([('id', 'in', eval(get_param('sale_order_bonus_product_ids', '[]')))])
        sale_order_discount = product.search([('id', 'in', eval(get_param('sale_order_discount_product_ids', '[]')))])
        result.update(
            account_invoice_bonus_product_ids = invoice_bonus.ids,
            account_invoice_discount_product_ids = invoice_discount.ids,
            sale_order_bonus_product_ids = sale_order_bonus.ids,
            sale_order_discount_product_ids = sale_order_discount.ids,
        )
        return result
