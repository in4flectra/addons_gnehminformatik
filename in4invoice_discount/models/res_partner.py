# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceDiscountResPartner(models.Model):

    _inherit = 'res.partner'

    bonus_ids = fields.One2many('res.partner.bonus', 'partner_id', string='Bonus')


class In4InvoiceDiscountResPartnerBonus(models.Model):

    _name = 'res.partner.bonus'
    _description = 'Bonus'
    _order = 'name'

    @api.depends('partner_id', 'product_id')
    def _compute_name(self):
        for bonus in self:
            name = ''
            if bonus.partner_id:
                name = bonus.partner_id.name
                if bonus.product_id:
                    name += ': ' + bonus.product_id.name
            elif bonus.product_id:
                name = bonus.product_id.name
            bonus.name = name

    @api.depends('amount_calc', 'amount_corr')
    def _compute_amount_used(self):
        for bonus in self:
            bonus.amount_used = bonus.amount_calc + bonus.amount_corr

    name = fields.Char(string='Bonus', compute='_compute_name', store=True)
    partner_id = fields.Many2one('res.partner', string='Partner', required=True, index=True, readonly=True)
    product_id = fields.Many2one('product.product', string='Product', required=True, index=True)
    partner_currency_id = fields.Many2one(
        'res.currency',
        string='Partner Currency',
        related='partner_id.currency_id',
        readonly=True,
        store=True,
        help='Utility field to express amount currency',
    )
    amount_calc = fields.Monetary(string='Calculated', currency_field='partner_currency_id', readonly=True)
    amount_corr = fields.Monetary(string='Correction', currency_field='partner_currency_id')
    amount_used = fields.Monetary(string='Amount', currency_field='partner_currency_id', compute='_compute_amount_used', store=True)
    remarks = fields.Text(string='Remarks')

    _sql_constraints = [
        ('partner_product_unique', 'UNIQUE(partner_id, product_id)', 'This Bonus already exists !'),
    ]
