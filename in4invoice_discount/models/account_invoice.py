# -*- coding: utf-8 -*-

from flectra import api, fields, models, _


class In4InvoiceDiscountAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    def _compute_available_discount_product_ids(self):
        product = self.env['product.product'].sudo()
        get_param =self.env['ir.config_parameter'].sudo().get_param
        product_ids = product.search([('id', 'in', eval(get_param('account_invoice_discount_product_ids', '[]')))]).ids
        for order in self:
            order.available_discount_product_ids = product_ids

    @api.model
    def _default_discount_product_ids(self):
        product = self.env['product.product'].sudo()
        get_param =self.env['ir.config_parameter'].sudo().get_param
        return product.search([('id', 'in', eval(get_param('account_invoice_discount_product_ids', '[]')))]).ids

    @api.one
    def _compute_tax_amounts(self):
        super(In4InvoiceDiscountAccountInvoice, self)._compute_tax_amounts()
        self.discount_price_subtotal = sum((line.price_subtotal - line.line_price_subtotal) for line in self.invoice_line_ids)
        self.discount_price_total = sum((line.price_total - line.line_price_total) for line in self.invoice_line_ids)

    @api.one
    @api.depends(
        'type', 'date', 'date_invoice', 'company_id', 'partner_id', 'order_discount', 'currency_id',
        'invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
    )
    def _compute_amount(self):
        super(In4InvoiceDiscountAccountInvoice, self)._compute_amount()

    @api.multi
    def _discount_taxes(self):
        precision = self.env['decimal.precision'].precision_get('Account')
        for invoice in self:
            codes = {}
            for line in self.invoice_line_ids.filtered(lambda l: (l.is_bonus_line == False) and (l.is_tip_line == False) and (l.is_rounding_line == False)):
                if line.invoice_line_tax_ids:
                    for tax in line.invoice_line_tax_ids:
                        codes.setdefault(tax, {
                            'name': tax.description or tax.name,
                            'amount': 0.0,
                        })
                        codes[tax]['amount'] += line.line_price_total - line.price_total
                else:
                    codes.setdefault(False, {
                        'name': '',
                        'amount': 0.0,
                    })
                    codes[False]['amount'] += line.line_price_total - line.price_total
            codes = sorted(codes.items(), key=lambda c: c[0].sequence if c[0] else -1)
            code_count = 0
            for id, values in codes:
                if values['amount'] != 0.0:
                    code_count += 1
            if code_count == 0:
                invoice.discount_tax_codes = None
                invoice.discount_details_text = None
            elif code_count == 1:
                for id, values in codes:
                    if values['amount'] != 0.0:
                        invoice.discount_tax_codes = values['name']
                        invoice.discount_details_text = None
            else:
                text = []
                for id, values in codes:
                    if values['amount'] != 0.0:
                        amount = '%.{0}f'.format(precision) % values['amount']
                        if id == 0:
                            text.append(_('%s without VAT') % (amount))
                        else:
                            text.append(_('%s with %s VAT') % (amount, values['name']))
                invoice.discount_tax_codes = None
                invoice.discount_details_text = '(' + (', '.join(text)) + ')'

    order_discount = fields.Float(string='Order Discount (%)', readonly=True, states={'draft': [('readonly', False)]})
    order_discount_text = fields.Char(string='Order Discount Text', readonly=True, states={'draft': [('readonly', False)]})
    discount_price_subtotal = fields.Monetary(string='Discount Amount', compute=_compute_amount, store=True, readonly=True, help='Total discount amount without taxes')
    discount_price_total = fields.Monetary(string='Discount Amount', compute=_compute_amount, store=True, readonly=True, help='Total discount amount with taxes')
    discount_tax_codes = fields.Char(string='Discount Tax Codes', compute=_discount_taxes)
    discount_details_text = fields.Char(string='Discount Tax Codes', compute=_discount_taxes)

    with_bonus = fields.Boolean(
        string='With Bonus',
        default=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
    )
    available_discount_product_ids = fields.Many2many(
        'product.product',
        string='Available Discount Products',
        compute=_compute_available_discount_product_ids,
    )
    discount_product_ids = fields.Many2many(
        'product.product',
        'account_invoice_product_product_discount_rel',
        'account_invoice_id',
        'product_product_id',
        string='Discount Products',
        default=_default_discount_product_ids,
        domain="[('id', 'in', available_discount_product_ids)]",
        readonly=True,
        states={'draft': [('readonly', False)]},
    )

    @api.onchange('cash_rounding_id', 'invoice_line_ids', 'tax_line_ids')
    def _onchange_cash_rounding(self):
        lines_to_remove = self.invoice_line_ids.filtered(lambda l: l.is_bonus_line)
        if lines_to_remove:
            self.invoice_line_ids -= lines_to_remove
            self._onchange_invoice_line_ids()
        super(In4InvoiceDiscountAccountInvoice, self)._onchange_cash_rounding()
        self._onchange_lines_bonus()

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        curr = self.currency_id or self.company_id.currency_id
        for line in self.invoice_line_ids:
            if (line.quantity != 0.0) and line.with_discount:
                amount = line.quantity * line.price_unit
                discount = curr.round(curr.round(amount * (line.line_discount or 0.0)) / 100)
                amount -= discount
                discount = curr.round(curr.round(amount * (line.invoice_id.order_discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.quantity
            else:
                price_reduce = line.price_unit
            taxes = line.invoice_line_tax_ids.compute_all(
                price_reduce,
                curr,
                line.quantity,
                line.product_id,
                self.partner_id
            )
            for tax in taxes.get('taxes', []):
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = curr.round(val['base'])
                    tax_grouped[key]['base_calc'] = val['base']
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += curr.round(val['base'])
                    tax_grouped[key]['base_calc'] += val['base']
        return tax_grouped

    @api.onchange('partner_id', 'order_discount', 'with_bonus', 'discount_product_ids', 'invoice_line_ids', 'tax_line_ids')
    def _onchange_lines_bonus(self):
        if self.type == 'out_invoice':
            lines_to_remove = self.invoice_line_ids.filtered(lambda l: l.is_bonus_line)
            if lines_to_remove:
                self.invoice_line_ids -= lines_to_remove
                self._onchange_invoice_line_ids()
            if self.with_bonus and self.partner_id:
                partner_id = self.partner_id
                if self.partner_id.parent_id:
                    partner_id = self.partner_id.parent_id
                max_amount = self.amount_total
                bonus = self.env['res.partner.bonus']
                domain = [('product_id', 'in', self.discount_product_ids.ids), ('partner_id', '=', partner_id.id)]
                bonus_ids = bonus.search(domain, order='amount_used asc')
                for bonus_id in bonus_ids:
                    if max_amount > bonus_id.amount_used:
                        bonus_amount = bonus_id.amount_used
                    else:
                        bonus_amount = max_amount
                    if bonus_amount != 0.0:
                        product_id = bonus_id.product_id
                        account_id = product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id
                        if product_id.taxes_id:
                            price = bonus_amount
                            bonus_line = self.env['account.invoice.line'].new({
                                'invoice_id': self.id,
                                'product_id': product_id.id,
                                'name': product_id.name,
                                'account_id': account_id.id,
                                'quantity': 1,
                                'uom_id': product_id.uom_id.id,
                                'price_unit': - price,
                                'invoice_line_tax_ids': product_id.taxes_id,
                                'is_bonus_line': True,
                                'sequence': 9900
                            })
                            if bonus_line not in self.invoice_line_ids:
                                self.invoice_line_ids += bonus_line
                        else:
                            total_amount = 0.0
                            tax_line_ids = self.get_taxes_values()
                            for tax_line in tax_line_ids.values():
                                tax_id = self.env['account.tax'].browse(tax_line['tax_id'])
                                tax_amount = tax_line['base_calc'] + tax_line['amount']
                                price_incl = bonus_amount * tax_amount / max_amount
                                if tax_id.price_include:
                                    price = self.currency_id.round(price_incl)
                                else:
                                    price = 100 * price_incl / (100 + tax_id.amount)
                                bonus_line = self.env['account.invoice.line'].new({
                                    'invoice_id': self.id,
                                    'product_id': product_id.id,
                                    'name': product_id.name,
                                    'account_id': account_id.id,
                                    'quantity': 1,
                                    'uom_id': product_id.uom_id.id,
                                    'price_unit': - price,
                                    'invoice_line_tax_ids': [(6, 0, [tax_line['tax_id']])],
                                    'is_bonus_line': True,
                                    'sequence': 9900 + tax_id.sequence
                                })
                                if bonus_line not in self.invoice_line_ids:
                                    self.invoice_line_ids += bonus_line
                                total_amount = self.currency_id.round(total_amount + price_incl)
                            if total_amount != bonus_amount:
                                price = bonus_amount - total_amount
                                bonus_line = self.env['account.invoice.line'].new({
                                    'invoice_id': self.id,
                                    'product_id': product_id.id,
                                    'name': product_id.name,
                                    'account_id': account_id.id,
                                    'quantity': 1,
                                    'uom_id': product_id.uom_id.id,
                                    'price_unit': - price,
                                    'is_bonus_line': True,
                                    'sequence': 9900
                                })
                                if bonus_line not in self.invoice_line_ids:
                                    self.invoice_line_ids += bonus_line
                    max_amount = self.currency_id.round(max_amount - bonus_amount)
                self._onchange_invoice_line_ids()

    @api.multi
    def action_invoice_open(self):
        result = super(In4InvoiceDiscountAccountInvoice, self).action_invoice_open()
        for invoice in self:
            if (    invoice.partner_id
                and (   (invoice.state == 'open')
                     or ((invoice.state == 'paid') and (invoice.amount_total == 0.0))
                    )
            ):
                partner_id = invoice.partner_id
                if invoice.partner_id.parent_id:
                    partner_id = invoice.partner_id.parent_id
                bonus = self.env['res.partner.bonus']
                lines = invoice.invoice_line_ids.filtered(lambda l: l.is_bonus_line)
                for line in lines:
                    domain = [('partner_id', '=', partner_id.id), ('product_id', '=', line.product_id.id)]
                    bonus_id = bonus.search(domain, limit=1)
                    if bonus_id:
                        amount_calc = bonus_id.amount_calc
                        if invoice.type == 'out_invoice':
                            amount_calc = amount_calc + line.price_unit
                        if invoice.type == 'out_refund':
                            amount_calc = amount_calc - line.price_unit
                        bonus_id.write({'amount_calc': amount_calc})
        return result


class In4InvoiceDiscountAccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.depends('is_tip_line', 'is_rounding_line', 'product_id')
    def _compute_with_discount(self):
        for line in self:
            if (   (line.is_tip_line)
                or (line.is_rounding_line)
                or (line.product_id.id in line.invoice_id.discount_product_ids.ids)
            ):
                line.with_discount = False
            else:
                line.with_discount = True

    @api.depends('line_discount', 'invoice_id', 'invoice_id.order_discount')
    def _compute_discount(self):
        for line in self:
            if line.with_discount:
                line_value = (100 - (line.line_discount or 0.0)) / 100
                order_value = (100 - (line.invoice_id.order_discount or 0.0)) / 100
                line.discount = 100 * (1 - (line_value * order_value))
            else:
                line.discount = 0.0

    @api.depends('line_discount', 'line_discount_text', 'invoice_id', 'invoice_id.order_discount', 'invoice_id.order_discount_text')
    def _compute_discount_text(self):
        lang_id = self.env.user.lang_id
        for line in self:
            if line.with_discount:
                line_text = ''
                order_text = ''
                line_amount = line.line_discount
                order_amount = line.invoice_id.order_discount
                if (line_amount != 0.0) and ((order_amount != 100.0) or (line_amount == 100.0)):
                    line_text = line.line_discount_text
                    if not line_text:
                        line_text = _('With a %s %% discount') % (str('{:,f}'.format(line_amount)).rstrip('0').rstrip('.').replace(',', lang_id.thousands_sep).replace('.', lang_id.decimal_point))
                if (order_amount != 0.0) and (line_amount != 100.0):
                    order_text = line.invoice_id.order_discount_text
                    if not order_text:
                        order_text = _('With a %s %% discount') % (str('{:,f}'.format(order_amount)).rstrip('0').rstrip('.').replace(',', lang_id.thousands_sep).replace('.', lang_id.decimal_point))
                if line_text and order_text:
                    if line_amount > order_amount:
                        line.discount_text = order_text + ', ' + line_text
                    else:
                        line.discount_text = line_text + ', ' + order_text
                elif line_text:
                    line.discount_text = line_text
                elif order_text:
                    line.discount_text = order_text
                else:
                    line.discount_text = ''
            else:
                line.discount_text = line.line_discount_text

    @api.one
    @api.depends(
        'product_id', 'quantity', 'price_unit', 'invoice_line_tax_ids', 'line_discount',
        'invoice_id', 'invoice_id.date', 'invoice_id.date_invoice', 'invoice_id.company_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.order_discount',
    )
    def _compute_price(self):
        super(In4InvoiceDiscountAccountInvoiceLine, self)._compute_price()

    @api.one
    def _set_price_values(self):
        curr = self.invoice_id.currency_id or self.invoice_id.company_id.currency_id
        price_reduce = self.price_unit
        amount = self.quantity * self.price_unit
        discount = 0.0
        if (self.quantity != 0.0) and self.with_discount:
            discount = curr.round(curr.round(amount * (self.line_discount or 0.0)) / 100)
            price_reduce = (amount - discount) / self.quantity
        taxes = None
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(
                price_reduce,
                currency=curr,
                quantity=self.quantity,
                product=self.product_id,
                partner=self.invoice_id.partner_id
            )
        self.line_price_subtotal = taxes['total_excluded'] if taxes else self.quantity * price_reduce
        self.line_price_total = taxes['total_included'] if taxes else self.line_price_subtotal
        if self.invoice_id._is_tax_excluded():
            self.price_total_display = self.line_price_subtotal
        else:
            self.price_total_display = self.line_price_total

        if (self.quantity != 0.0) and self.with_discount:
            amount -= discount
            discount = curr.round(curr.round(amount * (self.invoice_id.order_discount or 0.0)) / 100)
            price_reduce = (amount - discount) / self.quantity
        taxes = None
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(
                price_reduce,
                currency=curr,
                quantity=self.quantity,
                product=self.product_id,
                partner=self.invoice_id.partner_id
            )
        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price_reduce
        self.price_total = taxes['total_included'] if taxes else self.price_subtotal
        if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            price_subtotal_signed = self.invoice_id.currency_id.with_context(
                date=self.invoice_id._get_currency_rate_date()
            ).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign

    with_discount = fields.Boolean(string='With Discount', compute=_compute_with_discount, store=True, readonly=True)
    discount = fields.Float(compute=_compute_discount, store=True, readonly=True)
    discount_text = fields.Char(compute=_compute_discount_text, store=True, readonly=True)
    line_discount = fields.Float(string='Line Discount (%)')
    line_discount_text = fields.Char(string='Line Discount Text')

    line_price_subtotal = fields.Monetary(string='Line Amount', compute=_compute_price, store=True, readonly=True, help='Total line amount without taxes')
    line_price_total = fields.Monetary(string='Line Amount', compute=_compute_price, store=True, readonly=True, help='Total line amount with taxes')

    is_bonus_line = fields.Boolean(string='Is Bonus Line')
