# -*- coding: utf-8 -*-

from . import account_invoice
from . import product_template
from . import res_config_settings
from . import res_partner
from . import sale_order
