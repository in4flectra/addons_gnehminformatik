# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_ui_view
         WHERE name = 'in4invoice_discount.report_invoice_document'
               ;
    """)
