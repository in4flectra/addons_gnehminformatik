# -*- coding: utf-8 -*-
{
    'name': 'Invoicing with Discount and Bonus',
    'summary': 'Discounts and Bonus for Sales, Invoices and Payments',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.2',

    'depends': [
        'account',
        'base',
        'base_setup',
        'in4invoice',
        'sale',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/account_invoice.xml',
        'views/product_template.xml',
        'views/res_config_settings.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',

        # reports
        'views/report_invoice.xml',
        'views/report_saleorder.xml',
    ],
}
