# -*- coding: utf-8 -*-
{
    'name': 'Helpdesk Project Extension (extended)',
    'summary': 'Extension of Helpdesk Project Extension',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Helpdesk',
    'version': '0.1',

    'depends': [
        'helpdesk_basic',
        'helpdesk_project_ext',
        'in4helpdesk',
        'in4project',
        'project',
    ],

    'data': [
        'views/helpdesk_team.xml',
    ],

    'auto_install': True,
}
