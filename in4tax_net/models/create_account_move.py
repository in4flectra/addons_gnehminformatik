# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


class In4TaxNetCreateAccountMove(models.TransientModel):

    _name = 'create.account.move'
    _description = 'Create Net Tax Debt Rates'

    def _get_default_ref(self):
        return _('Net Tax Debt Rate')

    def _get_default_journal_id(self):
        return self.env['account.journal'].search([('type', '=', 'general')], limit=1).id

    date = fields.Date(string='Date', required=True, default=fields.Date.context_today)
    ref = fields.Char(string='Reference', default=_get_default_ref)
    journal_id = fields.Many2one('account.journal', string='Journal', required=True, default=_get_default_journal_id)
    company_id = fields.Many2one('res.company', string='Company', related='journal_id.company_id', readonly=True)
    branch_id = fields.Many2one(string='Branch', related='journal_id.branch_id', readonly=True)

    date_from = fields.Date(string='Start Date', required=True)
    date_to = fields.Date(string='End Date', required=True)

    @api.multi
    def _create_move(self):
        lines = []

        lang_code = self._context.get('lang') or 'en_US'
        lang_id = self.env['res.lang']._lang_get(lang_code)
        display_format = lang_id.date_format
        date_from = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT)
        date_from_display = datetime.strftime(date_from, display_format)
        date_to = datetime.strptime(self.date_to, DEFAULT_SERVER_DATE_FORMAT)
        date_to_display = datetime.strftime(date_to, display_format)

        params = {
            'company_id': self.company_id.id,
            'date_from': self.date_from,
            'date_to': self.date_to,
        }

        query = '''
              SELECT   t.id AS tax_id
                     , l.account_id AS account_id 
                     , SUM(l.debit * (t.amount - COALESCE(t.net_rate_amount, 0.0)) / 100) + SUM(l.credit * (COALESCE(t.net_rate_amount, 0.0) - t.amount) / 100) AS balance
                FROM account_move_line AS l
                JOIN account_move_line_account_tax_rel AS r ON r.account_move_line_id = l.id
                JOIN account_tax AS t ON t.id = r.account_tax_id
               WHERE l.company_id = %(company_id)s
                 AND l.date >= %(date_from)s
                 AND l.date <= %(date_to)s
            GROUP BY   t.id
                     , l.account_id
                     ;
        '''
        self.env.cr.execute(query, params)
        rows = self.env.cr.dictfetchall()
        for row in rows:
            tax = self.env['account.tax'].browse(row.get('tax_id'))
            account_id = row.get('account_id')
            balance = row.get('balance')
            if balance > 0.0:
                vals = {
                    'name': tax.name,
                    'debit': balance,
                    'credit': 0.0,
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))
            if balance < 0.0:
                vals = {
                    'name': tax.name,
                    'debit': 0.0,
                    'credit': abs(balance),
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))

        query = '''
              SELECT   t.account_id AS account_id
                     , SUM(l.debit * (t.amount - COALESCE(t.net_rate_amount, 0.0)) / 100) + SUM(l.credit * (COALESCE(t.net_rate_amount, 0.0) - t.amount) / 100) AS balance
                FROM account_move_line AS l
                JOIN account_move_line_account_tax_rel AS r ON r.account_move_line_id = l.id
                JOIN account_tax AS t ON t.id = r.account_tax_id
                JOIN account_invoice AS i ON i.id = l.invoice_id
               WHERE i.type IN ('in_invoice', 'out_invoice')
                 AND l.company_id = %(company_id)s
                 AND l.date >= %(date_from)s
                 AND l.date <= %(date_to)s
            GROUP BY   t.account_id
                     ;
        '''
        self.env.cr.execute(query, params)
        rows = self.env.cr.dictfetchall()
        for row in rows:
            name = _('Net Tax Debt Rate: %s - %s') % (date_from_display, date_to_display)
            account_id = row.get('account_id')
            balance = row.get('balance')
            if balance > 0.0:
                vals = {
                    'name': name,
                    'debit': 0.0,
                    'credit': balance,
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))
            if balance < 0.0:
                vals = {
                    'name': name,
                    'debit': abs(balance),
                    'credit': 0.0,
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))

        query = '''
              SELECT   t.refund_account_id AS account_id
                     , SUM(l.debit * (t.amount - COALESCE(t.net_rate_amount, 0.0)) / 100) + SUM(l.credit * (COALESCE(t.net_rate_amount, 0.0) - t.amount) / 100) AS balance
                FROM account_move_line AS l
                JOIN account_move_line_account_tax_rel AS r ON r.account_move_line_id = l.id
                JOIN account_tax AS t ON t.id = r.account_tax_id
                JOIN account_invoice AS i ON i.id = l.invoice_id
               WHERE i.type IN ('in_refund', 'out_refund')
                 AND l.company_id = %(company_id)s
                 AND l.date >= %(date_from)s
                 AND l.date <= %(date_to)s
            GROUP BY   t.refund_account_id
                     ;
        '''
        self.env.cr.execute(query, params)
        rows = self.env.cr.dictfetchall()
        for row in rows:
            name = _('Net Tax Debt Rate: %s - %s') % (date_from_display, date_to_display)
            account_id = row.get('account_id')
            balance = row.get('balance')
            if balance > 0.0:
                vals = {
                    'name': name,
                    'debit': 0.0,
                    'credit': balance,
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))
            if balance < 0.0:
                vals = {
                    'name': name,
                    'debit': abs(balance),
                    'credit': 0.0,
                    'account_id': account_id,
                }
                lines.append((0, 0, vals))

        vals = {
            'date': self.date,
            'ref': self.ref,
            'journal_id': self.journal_id.id,
            'state': 'draft',
            'line_ids': lines,
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    def create_account_move(self):
        # create the account move
        move_id = self._create_move()

        # return an action showing the created move
        action = self.env.ref(self.env.context.get('action', 'account.action_move_line_form'))
        result = action.read()[0]
        result['views'] = [(False, 'form')]
        result['res_id'] = move_id
        return result
