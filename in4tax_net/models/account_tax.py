# -*- coding: utf-8 -*-

from flectra import fields, models


class In4TaxNetAccountTax(models.Model):

    _inherit = 'account.tax'

    net_rate_amount = fields.Float(string='Net Tax Debt Rate/Fixed Rate Amount', digits=(16, 4), default=0.0, required=True)
