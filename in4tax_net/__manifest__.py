# -*- coding: utf-8 -*-
{
    'name': 'Net Tax Debt Rate / Fixed Rate',
    'summary': 'Extension of Tax with Net Tax Debt Rate / Fixed Rate',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Accounting',
    'version': '0.1',

    'depends': [
        'account',
    ],

    'data': [
        'views/account_tax.xml',
        'views/create_account_move.xml',
    ],
}
