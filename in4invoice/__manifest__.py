# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (extended)',
    'summary': 'Extension of Sales, Invoices and Payments',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.4',

    'depends': [
        'account',
        'product',
        'sale',
    ],

    'data': [
        # views
        'views/account_invoice.xml',
        'views/account_move.xml',
        'views/product_uom.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',

        # reports
        'views/report_invoice.xml',
        'views/report_saleorder.xml',
    ],
}
