# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_ui_view
         WHERE name IN ( 'in4invoice.report_invoice_document'
                       , 'in4invoice_discount.report_invoice_document'
                       , 'in4swiss_invoice.report_invoice_document'
                       , 'in4tax.report_invoice_document'
                       , 'in4tax.report_invoice_document_with_payments'
                       )
               ;
    """)
