# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        UPDATE mail_template
           SET   report_template = NULL
               , report_name = NULL
         WHERE name = 'Invoicing: Invoice email'
               ;
    """)
