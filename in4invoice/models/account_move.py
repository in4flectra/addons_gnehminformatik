# -*- coding: utf-8 -*-

from flectra import api, models


class In4InvoiceAccountMove(models.Model):

    _inherit = 'account.move'

    @api.model
    def create(self, vals):
        if not self._context.get('invoice'):
            result = super(In4InvoiceAccountMove, self.with_context(apply_taxes=True)).create(vals)
        else:
            result = super(In4InvoiceAccountMove, self).create(vals)
        return result

    @api.multi
    def write(self, vals):
        if not self._context.get('invoice'):
            result = super(In4InvoiceAccountMove, self.with_context(apply_taxes=True)).write(vals)
        else:
            result = super(In4InvoiceAccountMove, self).write(vals)
        return result


class In4InvoiceAccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    @api.onchange('account_id')
    def onchange_account_id(self):
        if self.account_id and self.account_id.tax_ids and (not self.tax_ids):
            self.tax_ids = self.account_id.tax_ids
