# -*- coding: utf-8 -*-

from flectra import api, models


class In4InvoiceIrActionsReport(models.Model):

    _inherit = 'ir.actions.report'

    @api.multi
    def postprocess_pdf_report(self, record, buffer):
        if (self.model == 'account.invoice') and (record.state == 'draft'):
            return None
        else:
            return super(In4InvoiceIrActionsReport, self).postprocess_pdf_report(record, buffer)
