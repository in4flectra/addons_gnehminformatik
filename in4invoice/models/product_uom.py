# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4InvoiceProductUoM(models.Model):

    _inherit = 'product.uom'

    print_on_invoice = fields.Boolean(string='Print on Invoice', default=True)
    prefix = fields.Char(string='Prefix')

    @api.multi
    def _compute_quantity(self, qty, to_unit, round=True, rounding_method='HALF-UP'):
        return super(In4InvoiceProductUoM, self)._compute_quantity(qty, to_unit, round, rounding_method)
