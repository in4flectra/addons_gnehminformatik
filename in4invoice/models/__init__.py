# -*- coding: utf-8 -*-

from . import account_invoice
from . import account_move
from . import ir_actions
from . import mail_template
from . import product_uom
from . import res_partner
from . import sale_order
