# -*- coding: utf-8 -*-

from functools import partial

from flectra import api, fields, models
from flectra.tools.misc import formatLang


class In4InvoiceSaleOrder(models.Model):

    _inherit = 'sale.order'

    def _is_tax_excluded(self):
        return self.env['ir.config_parameter'].sudo().get_param('sale.sale_show_tax', default='subtotal') == 'subtotal'

    @api.depends('order_line.price_total')
    def _amount_all(self):
        for order in self:
            amount_total = amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_total += line.price_total
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            if order._is_tax_excluded():
                order.update({
                    'amount_untaxed': amount_untaxed,
                    'amount_tax': amount_tax,
                    'amount_total': amount_untaxed + amount_tax,
                })
            else:
                order.update({
                    'amount_total': amount_total,
                    'amount_tax': amount_tax,
                    'amount_untaxed': amount_total - amount_tax,
                })

    @api.multi
    def _get_tax_amount_by_group(self):
        self.ensure_one()
        res = {}
        curr = self.currency_id or self.company_id.currency_id
        for line in self.order_line:
            if line.product_uom_qty != 0.0:
                amount = line.product_uom_qty * line.price_unit
                discount = curr.round(curr.round(amount * (line.discount or 0.0)) / 100)
                price_reduce = (amount - discount) / line.product_uom_qty
            else:
                price_reduce = line.price_unit
            taxes = line.tax_id.compute_all(
                price_reduce,
                quantity=line.product_uom_qty,
                product=line.product_id,
                partner=self.partner_shipping_id,
            )
            for tax in line.tax_id:
                group = tax.tax_group_id
                res.setdefault(group, {'amount': 0.0, 'base': 0.0})
                for t in taxes.get('taxes', []):
                    if t['id'] == tax.id or t['id'] in tax.children_tax_ids.ids:
                        res[group]['amount'] += t['amount']
                        if self._is_tax_excluded():
                            res[group]['base'] += t['base']
                        else:
                            res[group]['base'] += t['base'] + t['amount']
        res = sorted(res.items(), key=lambda r: r[0].sequence)
        curr_fmt = partial(formatLang, self.with_context(lang=self.partner_id.lang).env, currency_obj=curr)
        res_len = len(res)
        res = [(
            r[0].name,                # 0
            r[1]['amount'],           # 1
            r[1]['base'],             # 2
            res_len,                  # 3
            curr_fmt(r[1]['amount']), # 4
            curr_fmt(r[1]['base']),   # 5
        ) for r in res]
        return res


class In4InvoiceSaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    @api.depends('product_uom_qty', 'price_unit', 'discount', 'tax_id')
    def _compute_amount(self):
        result = super(In4InvoiceSaleOrderLine, self)._compute_amount()
        for line in self:
            if line.order_id._is_tax_excluded():
                price_total_display = line.price_subtotal
            else:
                price_total_display = line.price_total
            line.update({
                'price_total_display': price_total_display,
            })
        return result

    price_total_display = fields.Monetary(string='Amount', compute=_compute_amount, store=True, readonly=True)
