# -*- coding: utf-8 -*-

from flectra import fields, models


class In4InvoiceResPartner(models.Model):

    _inherit = 'res.partner'

    invoice_generation = fields.Selection([('merge', 'Merge Invoice Lines'), ('split', 'One Invoice per Partner')], string='Invoice Generation', required=True, default='merge')
