# -*- coding: utf-8 -*-

from functools import partial

from flectra import api, fields, models
from flectra.tools.misc import formatLang


class In4InvoiceAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    def _is_tax_excluded(self):
        return self.env['ir.config_parameter'].sudo().get_param('sale.sale_show_tax', default='subtotal') == 'subtotal'

    @api.one
    def _compute_tax_amounts(self):
        curr = self.currency_id or self.company_id.currency_id
        self.amount_tax = sum(curr.round(line.amount_total) for line in self.tax_line_ids)
        if self._is_tax_excluded():
            self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
            self.amount_total = self.amount_untaxed + self.amount_tax
        else:
            self.amount_total = sum(line.price_total for line in self.invoice_line_ids)
            self.amount_untaxed = self.amount_total - self.amount_tax

    @api.multi
    def _get_tax_amount_by_group(self):
        self.ensure_one()
        res = {}
        curr = self.currency_id or self.company_id.currency_id
        for line in self.tax_line_ids:
            res.setdefault(line.tax_id.tax_group_id, {'base': 0.0, 'amount': 0.0})
            res[line.tax_id.tax_group_id]['amount'] += line.amount_total
            if self._is_tax_excluded():
                res[line.tax_id.tax_group_id]['base'] += line.base
            else:
                res[line.tax_id.tax_group_id]['base'] += line.base + line.amount_total
        res = sorted(res.items(), key=lambda r: r[0].sequence)
        curr_fmt = partial(formatLang, self.with_context(lang=self.partner_id.lang).env, currency_obj=curr)
        res = [(
            r[0].name,                # 0
            r[1]['amount'],           # 1
            r[1]['base'],             # 2
            curr_fmt(r[1]['amount']), # 3
            curr_fmt(r[1]['base']),   # 4
        ) for r in res]
        return res

    @api.multi
    def invoice_print(self):
        self.ensure_one()
        self.sent = True
        return self.env.ref('account.account_invoices_without_payment').report_action(self)


class In4InvoiceAccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.one
    @api.depends(
        'invoice_id.date', 'invoice_id.date_invoice',
        'invoice_id.company_id', 'invoice_id.partner_id', 'invoice_id.currency_id',
        'product_id', 'quantity', 'price_unit', 'discount', 'invoice_line_tax_ids',
    )
    def _compute_price(self):
        super(In4InvoiceAccountInvoiceLine, self)._compute_price()

    @api.one
    def _set_price_values(self):
        super(In4InvoiceAccountInvoiceLine, self)._set_price_values()
        if self.invoice_id._is_tax_excluded():
            self.price_total_display = self.price_subtotal
        else:
            self.price_total_display = self.price_total

    price_total_display = fields.Monetary(string='Amount', compute=_compute_price, store=True, readonly=True)
