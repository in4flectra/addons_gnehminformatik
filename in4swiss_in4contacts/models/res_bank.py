# -*- coding: utf-8 -*-

from flectra import api, models


class In4SwissIn4ContactsResBank(models.Model):

    _inherit = 'res.bank'

    @api.depends('zip', 'city', 'state')
    def _compute_city_line(self):
        for bank_id in self:
            city_line = ''
            if bank_id.zip:
                city_line += bank_id.zip
                if bank_id.city or bank_id.state:
                    city_line += ' '
            if bank_id.city:
                city_line += bank_id.city
                if bank_id.state:
                    city_line += ' '
            if bank_id.state and bank_id.display_state_on_views:
                city_line += bank_id.state.name
            bank_id.city_line = city_line
