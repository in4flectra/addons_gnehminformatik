# -*- coding: utf-8 -*-
{
    'name': 'Extensions of Addresses (Swiss Localization)',
    'summary': 'Swiss Localization of the Extensions of Addresses',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'contacts',
        'in4swiss',
    ],

    'data': [
        'views/res_bank.xml',
        'views/res_company.xml',
        'views/res_partner.xml',
    ],

    'auto_install': True,
}
