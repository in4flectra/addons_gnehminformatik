# -*- coding: utf-8 -*-

import pytz

from datetime import datetime

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4HRPayrollHRHolidays(models.Model):

    _inherit = 'hr.holidays'

    @api.depends('employee_id.slip_ids', 'employee_id.slip_ids.state', 'employee_id.slip_ids.date_from', 'employee_id.slip_ids.date_to')
    def _compute_payslip_is_done(self):
        for holidays_id in self:
            holidays_id.payslip_is_done = holidays_id.get_payslip_is_done(holidays_id.employee_id.id, holidays_id.date_from)

    @api.model
    def get_has_access(self, user_must_be_a_manager):
        result = False
        user = self.env.user
        group_hr_manager = self.env.ref('hr_holidays.group_hr_holidays_manager')
        if (group_hr_manager in user.groups_id) or (self.payslip_is_done == False):
            result = super(In4HRPayrollHRHolidays, self).get_has_access(user_must_be_a_manager)
        return result

    payslip_is_done = fields.Boolean(string='Payslip is done', compute=_compute_payslip_is_done, store=True, readonyl=True)

    @api.model
    def get_payslip_is_done(self, employee_id, date_from):
        debi_count = 0
        if employee_id and date_from:
            if isinstance(date_from, str):
                tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
                from_date_time = datetime.strptime(date_from, DEFAULT_SERVER_DATETIME_FORMAT)
                from_date_time = pytz.UTC.localize(from_date_time).astimezone(pytz.timezone(tz))
                from_date = datetime.strftime(from_date_time.date(), DEFAULT_SERVER_DATE_FORMAT)
            else:
                from_date = date_from
            domain = [
                ('state', '=', 'done'),
                ('employee_id', '=', employee_id),
                ('date_from', '<=', from_date),
                ('date_to', '>=', from_date),
            ]
            payslip_ids = self.env['hr.payslip'].sudo().search(domain)
            debi_count = 0
            for payslip_id in payslip_ids:
                if payslip_id.credit_note:
                    debi_count -= 1
                else:
                    debi_count += 1
        return debi_count > 0

    @api.model
    def create(self, vals):
        vals['payslip_is_done'] = self.get_payslip_is_done(vals.get('employee_id'), vals.get('date_from'))
        return super(In4HRPayrollHRHolidays, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'employee_id' in vals:
            employee_id = vals['employee_id']
        else:
            employee_id = self.employee_id.id
        if 'date_from' in vals:
            date_from = vals['date_from']
        else:
            date_from = self.date_from
        vals['payslip_is_done'] = self.get_payslip_is_done(employee_id, date_from)
        return super(In4HRPayrollHRHolidays, self).write(vals)
