# -*- coding: utf-8 -*-
{
    'name': 'Payroll (extended)',
    'summary': 'Extension of managing employee payroll records',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.1',

    'depends': [
        'hr',
        'hr_holidays',
        'hr_payroll',
    ],

    'data': [
        # security
        'security/in4hr_payroll.xml',

        # reports
        'views/report_payslip.xml',
    ],

    'pre_init_hook': 'pre_init_hook',
}
