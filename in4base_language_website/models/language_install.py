# -*- coding: utf-8 -*-

from flectra import api, models


class In4BaseLanguageWebsiteBaseLanguageInstall(models.TransientModel):

    _inherit = 'base.language.install'

    @api.model
    def update_language(self, lang_id):
        website_ids = self.env['website'].search([])
        for website_id in website_ids:
            self.write({'website_ids': [(4, website_id.id)]})
        return super(In4BaseLanguageWebsiteBaseLanguageInstall, self).update_language(lang_id)
