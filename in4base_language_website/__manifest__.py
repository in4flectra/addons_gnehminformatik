# -*- coding: utf-8 -*-
{
    'name': 'Language Functions: Website Builder (extended)',
    'summary': 'Extension of Language Functions for Websites',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Extra Tools',
    'version': '0.1',

    'depends': [
        'in4base_language',
        'website',
    ],

    'auto_install': True,
}
