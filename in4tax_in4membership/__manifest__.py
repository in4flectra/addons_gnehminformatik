# -*- coding: utf-8 -*-
{
    'name': 'Tax (extended): Membership Management (extended)',
    'summary': 'Extension of Tax: Membership Management (extended)',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'in4membership',
        'in4tax',
    ],

    'auto_install': True,
}
