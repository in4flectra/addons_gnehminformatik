# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, models


class In4TaxIn4MembershipResPartner(models.Model):

    _inherit = 'res.partner'

    @api.multi
    def get_membership_invoice_data(self, datas):
        result = super(In4TaxIn4MembershipResPartner, self).get_membership_invoice_data(datas)
        result.update({
            'sale_show_tax': self.sale_show_tax,
        })
        return result
