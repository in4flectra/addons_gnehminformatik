# -*- coding: utf-8 -*-

from . import models

from flectra import api, SUPERUSER_ID


def post_init_hook(cr, registry):
    module_list = []
    module_list.append('base_setup')
    module_list.append('base_branch_company')

    env = api.Environment(cr, SUPERUSER_ID, {})
    module_ids = env['ir.module.module'].search([('name', 'in', module_list), ('state', '=', 'uninstalled')])
    module_ids.sudo().button_install()
