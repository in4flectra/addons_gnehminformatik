# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_ui_view
         WHERE name = 'isr_residual_report_main'
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model
         WHERE name = 'report.l10n_ch_point_of_sale.isr_residual_report_main'
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE module IN ('l10n_ch_point_of_sale', 'l10n_ch_sale_point_of_sale')
               ;
    """)
