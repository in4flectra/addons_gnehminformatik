# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM res_groups_implied_rel
         WHERE gid IN (SELECT id FROM res_groups WHERE name = 'Settings')
           AND hid IN (SELECT id FROM res_groups WHERE name IN ('Billing Manager', 'Editor and Designer'))
               ;
    """)
