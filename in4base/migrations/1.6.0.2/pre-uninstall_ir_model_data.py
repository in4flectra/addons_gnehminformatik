# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        UPDATE ir_model_data
           SET module = 'l10n_ch'
         WHERE module = 'in4fix_base'
           AND name LIKE 'state_ch_%'
               ;
    """)
