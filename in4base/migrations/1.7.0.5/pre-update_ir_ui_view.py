# -*- coding: utf-8 -*-


def migrate(cr, version):
    cr.execute("""
        DELETE
          FROM ir_ui_view
         WHERE arch_fs LIKE 'point_of_sale_l10n_ch%'
            OR arch_fs LIKE 'l10n_ch_point_of_sale%'
            OR arch_fs LIKE 'l10n_ch_sale_point_of_sale%'
               ;
    """)
