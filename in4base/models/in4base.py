# -*- coding: utf-8 -*-


def format_data(data, format_string, lang_id):
    parts = []
    field_prefix = ''
    field_name = ''
    field_format = ''
    current_part = 'prefix'
    for c in format_string:
        if (current_part == 'prefix') and (c == '['):
            current_part = 'name'
        elif (current_part == 'name') and (c == ','):
            current_part = 'format'
        elif (current_part in ['name', 'format']) and (c == ']'):
            parts.append({'prefix': field_prefix, 'name': field_name, 'format': field_format})
            field_prefix = ''
            field_name = ''
            field_format = ''
            current_part = 'prefix'
        else:
            if current_part == 'prefix':
                field_prefix += c
            elif current_part == 'name':
                field_name += c
            elif current_part == 'format':
                field_format += c
    # if field_name or field_format:
    #     raise UserError(_("Unrecognized meta description format.") % format_string)

    result = ''
    for part in parts:
        field_name = part['name'].split('.')
        field_value = data
        for field_part in field_name:
            field_value = field_value[field_part]
        if field_value:
            part_format = part['format'] or 's'
            if part_format[:1] == '{':
                formatted_value = part_format.format(field_value)
                if lang_id and (part_format[-2:] == 'f}'):
                    if (part_format.find(',') >= 0) and (lang_id.thousands_sep != ','):
                        formatted_value = formatted_value.replace(',', lang_id.thousands_sep)
                    if (part_format.find('.') >= 0) and (lang_id.decimal_point != '.'):
                        formatted_value = formatted_value.replace('.', lang_id.decimal_point)
            else:
                formatted_value = ('%' + part_format) % (field_value,)
            result += part['prefix'] + formatted_value

    return result + field_prefix
