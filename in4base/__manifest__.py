# -*- coding: utf-8 -*-
{
    'name': 'Base (in4...)',
    'summary': "Extension of Base for 'in4...'-Moduls",

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Administration',
    'version': '0.5',

    'depends': [
        'base',
        'web',
    ],

    'data': [
        # views
        'views/assets.xml',
        'views/menu.xml',

        # data
        'data/in4base.xml',
    ],

    'auto_install': True,
    'post_init_hook': 'post_init_hook',
}
