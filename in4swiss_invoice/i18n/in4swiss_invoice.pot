# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4swiss_invoice
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4swiss_invoice
#: model:ir.ui.view,arch_db:in4swiss_invoice.report_invoice_layouted
#: model:ir.ui.view,arch_db:in4swiss_invoice.report_saleorder_document
msgid "<strong>Total incl. VAT</strong>"
msgstr ""

#. module: in4swiss_invoice
#: model:ir.ui.view,arch_db:in4swiss_invoice.report_invoice_layouted
#: model:ir.ui.view,arch_db:in4swiss_invoice.report_saleorder_document
msgid "<strong>Total</strong>"
msgstr ""

#. module: in4swiss_invoice
#: model:ir.model,name:in4swiss_invoice.model_res_partner_bank
msgid "Bank Accounts"
msgstr ""

