# -*- coding: utf-8 -*-

from flectra import api, models


class In4SwissInvoiceResPartnerBank(models.Model):

    _inherit = 'res.partner.bank'

    @api.multi
    @api.depends('bank_name')
    def name_get(self):
        result = []
        for bank_id in self:
            if bank_id.journal_id:
                name = '%s (%s)' % (', '.join([j.name for j in bank_id.journal_id]), bank_id.acc_number)
            else:
                name = bank_id.acc_number
            result.append((bank_id.id, name))
        return result
