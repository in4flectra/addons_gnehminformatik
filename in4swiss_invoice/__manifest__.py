# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (Swiss Localization)',
    'summary': 'Swiss Localization of Sales, Invoices and Payments',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'account',
        'in4invoice',
        'in4swiss',
        'sale',
    ],

    'data': [
        # reports
        'views/report_invoice.xml',
        'views/report_saleorder.xml',
    ],

    'auto_install': True,
}
