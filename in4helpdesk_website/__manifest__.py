# -*- coding: utf-8 -*-
{
    'name': 'Helpdesk Website (extended)',
    'summary': 'Extension of Helpdesk Website',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Helpdesk',
    'version': '0.1',

    'depends': [
        'website_helpdesk',
    ],

    'data': [
        'views/helpdesk_portal.xml',
    ],

    'auto_install': True,
}
