# -*- coding: utf-8 -*-
{
    'name': 'Email Marketing: First and Last Name',
    'summary': 'First and Last Name for Mailing List Contacts',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Marketing',
    'version': '0.1',

    'depends': [
        'in4contacts',
        'mass_mailing',
    ],

    'data': [
        'views/mass_mailing.xml',
    ],

    'auto_install': True,
    'post_init_hook': 'post_init_hook',
}
