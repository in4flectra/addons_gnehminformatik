# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsMassMailingContact(models.Model):

    _inherit = 'mail.mass_mailing.contact'

    @api.multi
    @api.depends('firstname', 'lastname')
    def _compute_name(self):
        for contact in self:
            contact.name = self._merge_name_parts_into_name(contact.lastname, contact.firstname)

    @api.multi
    def _inverse_name(self):
        for contact in self:
            name_parts = self._split_name_into_name_parts(contact.name)
            contact.lastname = name_parts['lastname']
            contact.firstname = name_parts['firstname']

    @api.model
    def _get_names_order(self):
        return self.env['ir.config_parameter'].sudo().get_param('partner_names_order', 'last_first')

    @api.one
    def _compute_names_order(self):
        self.names_order = self._get_names_order()

    @api.model
    def _default_names_order(self):
        return self._get_names_order()

    @api.multi
    def _compute_display_on_views(self):
        for contact in self:
            if contact.country_id:
                contact.display_state_on_views = contact.country_id.display_state_on_views
                contact.display_country_on_views = contact.country_id.display_country_on_views
            else:
                contact.display_state_on_views = True
                contact.display_country_on_views = True

    name = fields.Char(compute=_compute_name, inverse=_inverse_name, store=True)
    firstname = fields.Char(string='First Name', index=True)
    lastname = fields.Char(string='Last Name', index=True)
    names_order = fields.Char(string='Names Order', compute=_compute_names_order, default=_default_names_order)

    display_state_on_views = fields.Boolean(string='Display State on Views', compute=_compute_display_on_views)
    display_country_on_views = fields.Boolean(string='Display Country on Views', compute=_compute_display_on_views)

    @api.model
    def _merge_name_parts_into_name(self, lastname, firstname):
        last_name = ''
        first_name = ''
        if lastname:
            last_name = lastname.strip()
        if firstname:
            first_name = firstname.strip()

        result = last_name + first_name
        if (len(last_name) > 0) and (len(first_name) > 0):
            order = self._get_names_order()
            if order == 'last_first':
                result = last_name + ' ' + first_name
            elif order == 'last_first_comma':
                result = last_name + ', ' + first_name
            elif order == 'first_last':
                result = first_name + ' ' + last_name
            elif order == 'first_last_comma':
                result = first_name + ', ' + last_name
        return result

    @api.model
    def _split_name_into_name_parts(self, name):
        result = {'lastname': name or None, 'firstname': None}
        if name:
            order = self._get_names_order()
            clean_name = name.replace('  ', ' ')
            if order in ('last_first_comma', 'first_last_comma'):
                clean_name = clean_name.replace(', ', ',')
                clean_name = clean_name.replace(' ,', ',')
                name_parts = clean_name.split(',', 1)
            else:
                name_parts = clean_name.split(' ', 1)
            if len(name_parts) > 1:
                if order in ('last_first', 'last_first_comma'):
                    result = {'lastname': name_parts[0], 'firstname': name_parts[1]}
                else:
                    result = {'lastname': name_parts[1], 'firstname': name_parts[0]}
        return result

    @api.onchange('name')
    def _onchange_name(self):
        if self.env.context.get('skip_onchange'):
            self.env.context = self.with_context(skip_onchange=False).env.context
        else:
            self._inverse_name()

    @api.onchange('firstname', 'lastname')
    def _onchange_name_parts(self):
        self.env.context = self.with_context(skip_onchange=True).env.context
        self._compute_name()

    @api.model
    def create(self, vals):
        context = dict(self.env.context)
        name = vals.get('name', context.get('default_name'))

        if name:
            name_parts = self._split_name_into_name_parts(name)
            for key, value in name_parts.items():
                if (not vals.get(key)) or context.get('copy'):
                    vals[key] = value

            if 'name' in vals:
                vals['name'] = name
            if 'default_name' in context:
                del context['default_name']

        return super(In4ContactsMassMailingContact, self.with_context(context)).create(vals)

    @api.model
    def inverse_names(self):
        self._inverse_name()

    @api.model
    def install_names(self):
        self.search([('firstname', '=', False), ('lastname', '=', False)]).inverse_names()
