# -*- coding: utf-8 -*-
{
    'name': 'Flectra Sync',
    'summary': 'Synchronization with Flectra',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/sync_flectra.xml',
        'views/sync_flectra_wizard.xml',
    ],
}
