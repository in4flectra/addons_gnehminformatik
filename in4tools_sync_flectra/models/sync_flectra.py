# -*- coding: utf-8 -*-

import time

from flectra import api, fields, models

sync_direction = [
    ('download', 'Download'),
    ('upload', 'Upload'),
    ('both', 'Both'),
]

field_selection = [
    ('all', 'Synchronize all fields except the fields from the list'),
    ('none', 'Synchronize only fields from the list'),
]


class In4ToolsSyncFlectraDatabase(models.Model):

    _name = 'sync_flectra.database'
    _description = 'Database to synchronize'
    _order = 'name'

    name = fields.Char(string='Name', required=True)
    db = fields.Char(string='Database', required=True)
    url = fields.Char(string='URL', required=True, default='http://localhost:7073')
    login = fields.Char(string='User Name', required=True)
    password = fields.Char(string='Password', required=True)
    active = fields.Boolean(string='Active', default=True, copy=False)

    table_ids = fields.One2many('sync_flectra.table', 'database_id', string='Tables', copy=True)

    @api.multi
    def write(self, vals):
        result = super(In4ToolsSyncFlectraDatabase, self).write(vals)

        for database_id in self:
            for table_id in database_id.table_ids:
                table_id.display_name = '{0} ({1})'.format(table_id.name, table_id.database_id.name)

        return result


class In4ToolsSyncFlectraTable(models.Model):

    _name = 'sync_flectra.table'
    _description = 'Table to synchronize'
    _order = 'database_id, sequence, name'

    @api.depends('name', 'database_id')
    def compute_display_name(self):
        for table_id in self:
            table_id.display_name = '{0} ({1})'.format(table_id.name, table_id.database_id.name)

    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(compute=compute_display_name, store=True)
    active = fields.Boolean(string='Active', default=True, copy=False)
    database_id = fields.Many2one('sync_flectra.database', string='Database', required=True, ondelete='cascade')
    model_id = fields.Many2one('ir.model', string='Model', required=True, ondelete='cascade')
    direction = fields.Selection(sync_direction, string='Direction', required=True, default='download')
    sequence = fields.Integer(string='Sequence')
    sync_by_fields = fields.Char(string='Sync by Fields')
    main_table_id = fields.Many2one('sync_flectra.table', string='Main Table')
    delete_records = fields.Boolean(string='Delete Records', default=False)
    delete_multiple = fields.Boolean(string='Delete Multiple', default=False)
    domain = fields.Char(string='Domain', required=True, default='[]')

    field_selection = fields.Selection(field_selection, string='Selection', required=True, default='all')
    field_ids = fields.One2many('sync_flectra.field', 'table_id', string='Fields', copy=True)

    synchronize_date = fields.Datetime(string='Last Synchronization', copy=False)
    record_ids = fields.One2many('sync_flectra.record', 'table_id', string='IDs Affected')

    @api.model
    def get_ids(self, pool_this, pool_other, table_id):
        result = []

        domain = eval(table_id.domain)
        last_sync_date = table_id.synchronize_date
        if last_sync_date:
            if table_id.model_id.model == 'product.product':
                domain += [
                    '|', ('create_date', '>=', last_sync_date),
                         '|', ('write_date', '>=', last_sync_date),
                              ('product_tmpl_id.write_date', '>=', last_sync_date)
                ]
            else:
                domain += ['|', ('create_date', '>=', last_sync_date), ('write_date', '>=', last_sync_date)]
        if table_id.direction in ('download', 'both'):
            model_other = pool_other.get(table_id.model_id.model)
            if model_other:
                ids = model_other.search(domain)
                for id in ids:
                    values = model_other.read(id, ['create_date', 'write_date'])
                    result.append((values[0]['write_date'] or values[0]['create_date'], int(id), 'download'))
        if table_id.direction in ('upload', 'both'):
            this_ids = pool_this.env[table_id.model_id.model].with_context(active_test=False).search(domain)
            for values in this_ids.read(['create_date', 'write_date']):
                result.append((values['write_date'] or values['create_date'], values['id'], 'upload'))

        return result

    @api.model
    def del_remote_ids(self, pool_dest, table_id):
        all_dest_ids = pool_dest.get(table_id.model_id.model)
        if all_dest_ids:
            id_table = table_id.main_table_id.id or table_id.id
            record_ids = self.env['sync_flectra.record'].search([('table_id', '=', id_table)])
            for record_id in record_ids:
                obj_rec = self.env[table_id.model_id.model].with_context(active_test=False).search([
                    ('id', '=', record_id.local_id),
                ])
                if not obj_rec:
                    dest_id = all_dest_ids.search([('id', '=', record_id.remote_id)], limit=1)
                    if dest_id:
                        all_dest_ids.unlink([dest_id[0]])
                    record_id.unlink()


class In4ToolsSyncFlectraField(models.Model):

    _name = 'sync_flectra.field'
    _description = 'Field to synchronize'
    _order = 'table_id, name'

    name = fields.Char(string='Name', required=True)
    table_id = fields.Many2one('sync_flectra.table', string='Table', required=True, ondelete='cascade')


class In4ToolsSyncFlectraRecord(models.Model):

    _name = 'sync_flectra.record'
    _description = 'Record'
    _order = 'name desc'

    name = fields.Datetime(string='Date', required=True, default=lambda *args: time.strftime('%Y-%m-%d %H:%M:%S'))
    table_id = fields.Many2one('sync_flectra.table', string='Table', ondelete='cascade')
    local_id = fields.Integer(string='Local ID', readonly=True)
    remote_id = fields.Integer(string='Remote ID', readonly=True)


class In4ToolsSyncFlectraRequest(models.Model):

    _name = 'sync_flectra.request'
    _description = 'Request'
    _order = 'date desc'

    name = fields.Char(string='Name', required=True)
    date = fields.Datetime(string='Date')
    body = fields.Text(string='Request')
