# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissCRMCRMLead(models.Model):

    _inherit = 'crm.lead'

    @api.depends('zip', 'city', 'state_id')
    def _compute_city_line(self):
        for lead_id in self:
            city_line = ''
            if lead_id.zip:
                city_line += lead_id.zip
                if lead_id.city or lead_id.state_id:
                    city_line += ' '
            if lead_id.city:
                city_line += lead_id.city
                if lead_id.state_id:
                    city_line += ' '
            if lead_id.state_id:
                city_line += lead_id.state_id.name
            lead_id.city_line = city_line

    country_enforce_cities = fields.Boolean(string='Country Enforce Cities', related='country_id.enforce_cities', readonly=True)
    city_id = fields.Many2one('res.city', string='City')
    city_line = fields.Char(string='City Line', compute=_compute_city_line)

    @api.model
    def _set_city_id(self, vals):
        if (   ('city' in vals)
            or ('zip' in vals)
            or ('country_id' in vals)
        ):
            vals['city_id'] = None

            city = self.city
            if 'city' in vals:
                city = vals['city']
            zip = self.zip
            if 'zip' in vals:
                zip = vals['zip']
            id_country = self.country_id.id
            if 'country_id' in vals:
                id_country = int(vals['country_id'])

            domain = []
            if city:
                domain.append(('name', '=ilike', city))
            if zip:
                domain.append(('zipcode', '=', zip))
            if id_country:
                domain.append(('country_id', '=', id_country))
            city_ids = self.env['res.city'].search(domain)

            if (not city_ids) and city:
                domain = []
                domain.append(('name', '=ilike', city + '%'))
                if zip:
                    domain.append(('zipcode', '=', zip))
                if id_country:
                    domain.append(('country_id', '=', id_country))
                city_ids = self.env['res.city'].search(domain)

            if len(city_ids) == 1:
                vals['city_id'] = city_ids[0].id
                vals['city'] = city_ids[0].name
                vals['zip'] = city_ids[0].zipcode
                vals['state_id'] = city_ids[0].state_id.id
                vals['country_id'] = city_ids[0].country_id.id

    @api.model
    def create(self, vals):
        self._set_city_id(vals)
        return super(In4SwissCRMCRMLead, self).create(vals)

    @api.multi
    def write(self, vals):
        self._set_city_id(vals)
        return super(In4SwissCRMCRMLead, self).write(vals)

    @api.onchange('city_id')
    def _onchange_city_id(self):
        if self.city_id:
            self.city = self.city_id.name
            self.zip = self.city_id.zipcode
            self.state_id = self.city_id.state_id

    @api.onchange('city')
    def _onchange_city(self):
        if (not self.city) or (self.city_id.name != self.city):
            self.city_id = False

    @api.onchange('zip')
    def _onchange_zip(self):
        if (not self.zip) or (self.city_id.zipcode != self.zip):
            self.city_id = False

    @api.onchange('state_id')
    def _onchange_state_id(self):
        super(In4SwissCRMCRMLead, self)._onchange_state()
        if self.city_id and (self.city_id.state_id != self.state_id):
            self.city_id = False

    @api.onchange('country_id')
    def _onchange_country_id(self):
        super(In4SwissCRMCRMLead, self)._onchange_country()
        if self.city_id and (self.city_id.country_id != self.country_id):
            self.city_id = False
