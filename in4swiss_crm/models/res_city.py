# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4SwissCRMResCity(models.Model):

    _inherit = 'res.city'

    lead_ids = fields.One2many('crm.lead', 'city_id', string='Leads & Opportunities')

    @api.multi
    def write(self, vals):
        result = super(In4SwissCRMResCity, self).write(vals)

        if (   ('name' in vals)
            or ('zipcode' in vals)
            or ('state_id' in vals)
            or ('country_id' in vals)
        ):
            for lead_id in self.lead_ids:
                lead_id.write({
                    'city': self.name,
                    'zip': self.zipcode,
                    'state_id': self.state_id.id,
                    'country_id': self.country_id.id,
                })

        return result


class In4SwissCRMResCityImportSwiss(models.TransientModel):

    _inherit = 'res.city.swiss_zip_import'

    @api.multi
    def update_swiss_zip(self, ch_country_id):
        super(In4SwissCRMResCityImportSwiss, self).update_swiss_zip(ch_country_id)

        domain = [('country_id', '=', ch_country_id.id), ('city_id', '=', False)]
        lead_ids = self.env['crm.lead'].search(domain)
        for lead_id in lead_ids:
            lead_id.write({
                'city': lead_id.city,
                'zip': lead_id.zip,
                'country_id': lead_id.country_id.id,
            })
