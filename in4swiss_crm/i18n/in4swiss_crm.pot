# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4swiss_crm
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4swiss_crm
#: model:ir.model.fields,help:in4swiss_crm.field_crm_lead_country_enforce_cities
msgid "Check this box to ensure every address created in that country has a 'City' chosen in the list of the country's cities."
msgstr ""

#. module: in4swiss_crm
#: model:ir.model,name:in4swiss_crm.model_res_city
#: model:ir.model.fields,field_description:in4swiss_crm.field_crm_lead_city_id
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "City"
msgstr ""

#. module: in4swiss_crm
#: model:ir.model.fields,field_description:in4swiss_crm.field_crm_lead_city_line
msgid "City Line"
msgstr ""

#. module: in4swiss_crm
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "City, State, ZIP"
msgstr ""

#. module: in4swiss_crm
#: model:ir.model.fields,field_description:in4swiss_crm.field_crm_lead_country_enforce_cities
msgid "Country Enforce Cities"
msgstr ""

#. module: in4swiss_crm
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "e.g. +41 23 456 78 90"
msgstr ""

#. module: in4swiss_crm
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "e.g. +41 79 123 45 67"
msgstr ""

#. module: in4swiss_crm
#: model:ir.model,name:in4swiss_crm.model_res_city_swiss_zip_import
msgid "Import Swiss ZIP"
msgstr ""

#. module: in4swiss_crm
#: model:ir.model,name:in4swiss_crm.model_crm_lead
msgid "Lead/Opportunity"
msgstr ""

#. module: in4swiss_crm
#: model:ir.model.fields,field_description:in4swiss_crm.field_res_city_lead_ids
msgid "Leads & Opportunities"
msgstr ""

#. module: in4swiss_crm
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "State"
msgstr ""

#. module: in4swiss_crm
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_lead
#: model:ir.ui.view,arch_db:in4swiss_crm.crm_lead_form_opportunity
msgid "ZIP"
msgstr ""

