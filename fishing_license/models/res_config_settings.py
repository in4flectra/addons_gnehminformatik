# -*- coding: utf-8 -*-

from flectra import api, fields, models


class FishingLicenseResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    fish_count_count = fields.Integer(string='Fish Count', default=5)
    fish_length_count = fields.Integer(string='Length Count', default=5)

    def set_values(self):
        super(FishingLicenseResConfigSettings, self).set_values()

        set_param = self.env['ir.config_parameter'].set_param
        set_param('fishing_license.fish_count_count', self.fish_count_count)
        set_param('fishing_license.fish_length_count', self.fish_length_count)

        set_param('fishing_license.count_fish_5_invisible', self.fish_count_count < 5)

    @api.model
    def get_values(self):
        result = super(FishingLicenseResConfigSettings, self).get_values()

        get_param = self.env['ir.config_parameter'].sudo().get_param
        result.update(
            fish_count_count=int(get_param('fishing_license.fish_count_count', default=5)),
            fish_length_count=int(get_param('fishing_license.fish_length_count', default=5)),
        )

        return result


class FishingLicenseIrActionsActWindow(models.Model):

    _inherit = 'ir.actions.act_window'

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(FishingLicenseIrActionsActWindow, self).read(fields, load=load)

        if ('fish' in self.context):
            get_param = self.env['ir.config_parameter'].sudo().get_param
            fish_count_count = int(get_param('fishing_license.fish_count_count'))
            fish_length_count = int(get_param('fishing_license.fish_length_count'))
            for i in range(1, 6):
                n = '\'count_fish_' + str(i) +'_invisible\''
                if ((n + ': False' in self.context) and (fish_count_count < i)):
                    self.context = self.context.replace(n + ': False', n + ': True')
                if ((n + ': True' in self.context) and (fish_count_count >= i)):
                    self.context = self.context.replace(n + ': True', n + ': False')

                n = '\'fish_' + str(i) +'_length_invisible\''
                if ((n + ': False' in self.context) and (fish_length_count < i)):
                    self.context = self.context.replace(n + ': False', n + ': True')
                if ((n + ': True' in self.context) and (fish_length_count >= i)):
                    self.context = self.context.replace(n + ': True', n + ': False')

        return result
