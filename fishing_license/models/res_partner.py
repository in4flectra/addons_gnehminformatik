# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, fields, models


class FishingLicenseResPartner(models.Model):

    _inherit = 'res.partner'

    license_ids = fields.One2many('fishing_license.license', 'partner_id', string='Licenses')
    license_count_current_year = fields.Integer(string='Number of Licenses this Year', readonly=True)
    license_count_last_year = fields.Integer(string='Number of Licenses last Year', readonly=True)
    license_count_two_years_ago = fields.Integer(string='Number of Licenses 2 Years ago', readonly=True)
    license_count_three_years_ago = fields.Integer(string='Number of Licenses 3 Years ago', readonly=True)
    license_count_four_years_ago = fields.Integer(string='Number of Licenses 4 Years ago', readonly=True)
    license_count_five_years_ago = fields.Integer(string='Number of Licenses 5 Years ago', readonly=True)

    @api.model
    def recalc_license_counts(self):
        license_count_current_year = 0
        license_count_last_year = 0
        license_count_two_years_ago = 0
        license_count_three_years_ago = 0
        license_count_four_years_ago = 0
        license_count_five_years_ago = 0
        for license_id in self.license_ids:
            current_year = datetime.now().year
            if license_id.date_year == current_year:
                license_count_current_year += 1
            elif license_id.date_year == current_year - 1:
                license_count_last_year += 1
            elif license_id.date_year == current_year - 2:
                license_count_two_years_ago += 1
            elif license_id.date_year == current_year - 3:
                license_count_three_years_ago += 1
            elif license_id.date_year == current_year - 4:
                license_count_four_years_ago += 1
            elif license_id.date_year == current_year - 5:
                license_count_five_years_ago += 1
        if ((self.license_count_current_year != license_count_current_year)
            or (self.license_count_last_year != license_count_last_year)
            or (self.license_count_two_years_ago != license_count_two_years_ago)
            or (self.license_count_three_years_ago != license_count_three_years_ago)
            or (self.license_count_four_years_ago != license_count_four_years_ago)
            or (self.license_count_five_years_ago != license_count_five_years_ago)
        ):
            self.write({
                'license_count_current_year': license_count_current_year,
                'license_count_last_year': license_count_last_year,
                'license_count_two_years_ago': license_count_two_years_ago,
                'license_count_three_years_ago': license_count_three_years_ago,
                'license_count_four_years_ago': license_count_four_years_ago,
                'license_count_five_years_ago': license_count_five_years_ago,
            })

    @api.model
    def recalc_all_license_counts(self):
        partner_ids = self.search([])
        for partner_id in partner_ids:
            partner_id.recalc_license_counts()
