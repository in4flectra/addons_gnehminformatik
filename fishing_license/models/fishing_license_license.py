# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


def attrs(**kwargs):
    return {'data-oe-%s' % key: str(value) for key, value in kwargs.items()}


class FishingLicenseLicense(models.Model):

    _name = 'fishing_license.license'
    _order = 'date desc'

    name = fields.Char(compute='_compute_name')

    partner_id = fields.Many2one('res.partner', string='Partner', required=True, index=True)
    date = fields.Date(string='Date', required=True, default=fields.Date.today)
    date_year = fields.Integer(string='Year', compute='_compute_date', store=True)
    date_month = fields.Integer(string='Month', compute='_compute_date', store=True)
    date_week = fields.Integer(string='Week', compute='_compute_date', store=True)
    number = fields.Integer(string='Number')
    count_fish_1 = fields.Integer(string='Count Fish 1')
    count_fish_2 = fields.Integer(string='Count Fish 2')
    count_fish_3 = fields.Integer(string='Count Fish 3')
    count_fish_4 = fields.Integer(string='Count Fish 4')
    count_fish_5 = fields.Integer(string='Count Fish 5')
    total_count = fields.Integer(string='Total Count', compute='_compute_total_count', store=True)
    amount = fields.Monetary(string='Amount', currency_field='currency_id', default=32.0)
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
    remarks = fields.Text(string='Remarks')
    contest_category = fields.Selection([('children', 'Children'), ('women', 'Women'), ('men', 'Men')], string='Contest Category')
    fish_1_length = fields.Float(string='Fish 1 Length', digits=(10, 2))
    fish_2_length = fields.Float(string='Fish 2 Length', digits=(10, 2))
    fish_3_length = fields.Float(string='Fish 3 Length', digits=(10, 2))
    fish_4_length = fields.Float(string='Fish 4 Length', digits=(10, 2))
    fish_5_length = fields.Float(string='Fish 5 Length', digits=(10, 2))
    total_weight = fields.Float(string='Total Weight', digits=(10, 4))

    @api.depends('partner_id', 'date')
    def _compute_name(self):
        lang_code = self.env.context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        self.name = '{0}, {1}'.format(
            self.partner_id.name,
            datetime.strftime(datetime.strptime(self.date, DEFAULT_SERVER_DATE_FORMAT), lang_id.date_format)
        )

    @api.depends('date')
    def _compute_date(self):
        for record in self:
            current_date = datetime.strptime(record.date, DEFAULT_SERVER_DATE_FORMAT)
            record.date_year = current_date.year
            record.date_month = current_date.month
            record.date_week = current_date.isocalendar()[1]

    @api.depends('count_fish_1', 'count_fish_2', 'count_fish_3', 'count_fish_4', 'count_fish_5')
    def _compute_total_count(self):
        for record in self:
            record.total_count = record.count_fish_1 + record.count_fish_2 + record.count_fish_3 + record.count_fish_4 + record.count_fish_5

    @api.model
    def create(self, vals):
        result = super(FishingLicenseLicense, self).create(vals)
        if vals.get('partner_id'):
            self.env['res.partner'].browse(vals['partner_id']).recalc_license_counts()
        return result

    @api.multi
    def write(self, vals):
        id_partner_old = None
        id_partner_new = None
        if 'partner_id' in vals:
            id_partner_old = self.partner_id.id
            id_partner_new = vals['partner_id']
        elif 'date' in vals:
            id_partner_old = self.partner_id.id

        result = super(FishingLicenseLicense, self).write(vals)

        if id_partner_old:
            self.env['res.partner'].browse(id_partner_old).recalc_license_counts()
        if id_partner_new:
            self.env['res.partner'].browse(id_partner_new).recalc_license_counts()

        return result

    @api.multi
    def unlink(self):
        id_partner_old = None
        if self.partner_id:
            id_partner_old = self.partner_id.id
        result = super(FishingLicenseLicense, self).unlink()

        if id_partner_old:
            self.env['res.partner'].browse(id_partner_old).recalc_license_counts()

        return result


class FishingLicenseLicensePerWeekAndMonth(models.TransientModel):

    _name = 'fishing_license.license_per_week_and_month'
    _description = 'Licenses per Week and Month'

    year_sel = fields.Integer(string='Year', required=True, default=fields.Date.today()[:4])

    def print_report(self):
        self.ensure_one()
        data = {'form': self.read(['year_sel'])[0]}
        return self.env.ref('fishing_license.license_per_week_and_month_report').report_action(self, data=data, config=False)


class FishingLicenseLicensePerWeekAndMonthTemplate(models.AbstractModel):

    _name = 'report.fishing_license.license_per_week_and_month_template'

    @api.model
    def get_report_values(self, docids, data=None):
        query = """
            SELECT        date_week
                        , COUNT(id)
                        , COALESCE(SUM(count_fish_1), 0)
                        , COALESCE(SUM(count_fish_2), 0)
                        , COALESCE(SUM(count_fish_3), 0)
                        , COALESCE(SUM(count_fish_4), 0)
                        , COALESCE(SUM(count_fish_5), 0)
                        , COALESCE(SUM(amount), 0)
            FROM        fishing_license_license
            WHERE       date_year = %(year_sel)s
            GROUP BY    date_week
            ORDER BY    date_week
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        week_ids = self.env.cr.fetchall()

        query = """
            SELECT        date_month
                        , COUNT(id)
                        , COALESCE(SUM(count_fish_1), 0)
                        , COALESCE(SUM(count_fish_2), 0)
                        , COALESCE(SUM(count_fish_3), 0)
                        , COALESCE(SUM(count_fish_4), 0)
                        , COALESCE(SUM(count_fish_5), 0)
                        , COALESCE(SUM(amount), 0)
            FROM        fishing_license_license
            WHERE       date_year = %(year_sel)s
            GROUP BY    date_month
            ORDER BY    date_month
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        months = self.env.cr.fetchall()
        month_ids = []
        month_names = {
            1: _('January'),
            2: _('February'),
            3: _('March'),
            4: _('April'),
            5: _('May'),
            6: _('June'),
            7: _('July'),
            8: _('August'),
            9: _('September'),
            10: _('October'),
            11: _('November'),
            12: _('December'),
        }
        for month in months:
            month_ids.append((
                month_names[month[0]],
                month[1],
                month[2],
                month[3],
                month[4],
                month[5],
                month[6],
                month[7],
            ))

        get_param = self.env['ir.config_parameter'].sudo().get_param
        fish_count_count = int(get_param('fishing_license.fish_count_count'))
        fish_length_count = int(get_param('fishing_license.fish_length_count'))

        return {
            'data': data,
            'docs': self,
            'docids': docids,
            'year': data['form']['year_sel'],
            'week_ids': week_ids,
            'month_ids': month_ids,
            'fish_count_count': fish_count_count,
            'fish_length_count': fish_length_count,
        }


class FishingLicenseLicensePerYearTemplate(models.AbstractModel):

    _name = 'report.fishing_license.license_per_year_template'

    @api.model
    def get_report_values(self, docids, data=None):
        query = """
            SELECT        date_year
                        , COALESCE(SUM(CASE WHEN date_month <=  6 THEN 1 ELSE 0 END), 0)
                        , COALESCE(SUM(CASE WHEN date_month  =  7 THEN 1 ELSE 0 END), 0)
                        , COALESCE(SUM(CASE WHEN date_month  =  8 THEN 1 ELSE 0 END), 0)
                        , COALESCE(SUM(CASE WHEN date_month  =  9 THEN 1 ELSE 0 END), 0)
                        , COALESCE(SUM(CASE WHEN date_month >= 10 THEN 1 ELSE 0 END), 0)
            FROM        fishing_license_license
            WHERE       date_year >= 2018
            GROUP BY    date_year
            ORDER BY    date_year
        """
        self.env.cr.execute(query)
        years = self.env.cr.fetchall()

        year_ids = []
        year_ids.append((2009, 132, 430, 394, 278,  86))
        year_ids.append((2010, 121, 400, 380, 152, 145))
        year_ids.append((2011, 189, 289, 382, 262, 209))
        year_ids.append((2012,  96, 352, 390, 257, 119))
        year_ids.append((2013,  64, 436, 412, 274,  96))
        year_ids.append((2014, 147, 381, 361, 308, 130))
        year_ids.append((2015, 190, 463, 421, 255, 109))
        year_ids.append((2016, 145, 340, 448, 267, 132))
        year_ids.append((2017,   0,   0,   0,   0,   0))
        for year in years:
            year_ids.append((
                year[0],
                year[1],
                year[2],
                year[3],
                year[4],
                year[5],
            ))

        get_param = self.env['ir.config_parameter'].sudo().get_param
        fish_count_count = int(get_param('fishing_license.fish_count_count'))
        fish_length_count = int(get_param('fishing_license.fish_length_count'))

        return {
            'data': data,
            'docs': self,
            'docids': docids,
            'year_ids': year_ids,
            'fish_count_count': fish_count_count,
            'fish_length_count': fish_length_count,
        }


class FishingLicenseContestRanking(models.TransientModel):

    _name = 'fishing_license.contest_ranking'
    _description = 'Ranking of contest'

    year_sel = fields.Integer(string='Year', required=True, default=fields.Date.today()[:4])

    def print_report(self):
        self.ensure_one()
        data = {'form': self.read(['year_sel'])[0]}
        return self.env.ref('fishing_license.contest_ranking_report').report_action(self, data=data, config=False)


class FishingLicenseContestRankingTemplate(models.AbstractModel):

    _name = 'report.fishing_license.contest_ranking_template'

    @api.model
    def get_report_values(self, docids, data=None):
        query = """
            SELECT        partner.name || CASE WHEN partner.city IS NULL THEN '' ELSE ', ' || partner.city END AS full_name
                        , COALESCE(license.fish_1_length, 0)
                        , COALESCE(license.fish_2_length, 0)
                        , COALESCE(license.fish_3_length, 0)
                        , COALESCE(license.fish_4_length, 0)
                        , COALESCE(license.fish_5_length, 0)
            FROM        fishing_license_license AS license
            INNER JOIN  res_partner AS partner ON partner.id = license.partner_id
            WHERE       license.date_year = %(year_sel)s
            AND         contest_category = 'children'
            ORDER BY      COALESCE(license.fish_1_length, 0) DESC
                        , COALESCE(license.fish_2_length, 0) DESC
                        , COALESCE(license.fish_3_length, 0) DESC
                        , COALESCE(license.fish_4_length, 0) DESC
                        , COALESCE(license.fish_5_length, 0) DESC
                        , full_name ASC
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        children_ids = self.env.cr.fetchall()

        query = """
            SELECT        partner.name || CASE WHEN partner.city IS NULL THEN '' ELSE ', ' || partner.city END AS full_name
                        , COALESCE(license.fish_1_length, 0)
                        , COALESCE(license.fish_2_length, 0)
                        , COALESCE(license.fish_3_length, 0)
                        , COALESCE(license.fish_4_length, 0)
                        , COALESCE(license.fish_5_length, 0)
            FROM        fishing_license_license AS license
            INNER JOIN  res_partner AS partner ON partner.id = license.partner_id
            WHERE       license.date_year = %(year_sel)s
            AND         contest_category = 'women'
            ORDER BY      COALESCE(license.fish_1_length, 0) DESC
                        , COALESCE(license.fish_2_length, 0) DESC
                        , COALESCE(license.fish_3_length, 0) DESC
                        , COALESCE(license.fish_4_length, 0) DESC
                        , COALESCE(license.fish_5_length, 0) DESC
                        , full_name ASC
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        women_ids = self.env.cr.fetchall()

        query = """
            SELECT        partner.name || CASE WHEN partner.city IS NULL THEN '' ELSE ', ' || partner.city END AS full_name
                        , COALESCE(license.fish_1_length, 0)
                        , COALESCE(license.fish_2_length, 0)
                        , COALESCE(license.fish_3_length, 0)
                        , COALESCE(license.fish_4_length, 0)
                        , COALESCE(license.fish_5_length, 0)
            FROM        fishing_license_license AS license
            INNER JOIN  res_partner AS partner ON partner.id = license.partner_id
            WHERE       license.date_year = %(year_sel)s
            AND         contest_category = 'men'
            ORDER BY      COALESCE(license.fish_1_length, 0) DESC
                        , COALESCE(license.fish_2_length, 0) DESC
                        , COALESCE(license.fish_3_length, 0) DESC
                        , COALESCE(license.fish_4_length, 0) DESC
                        , COALESCE(license.fish_5_length, 0) DESC
                        , full_name ASC
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        men_ids = self.env.cr.fetchall()

        query = """
            SELECT        partner.name || CASE WHEN partner.city IS NULL THEN '' ELSE ', ' || partner.city END AS full_name
                        , COALESCE(license.total_weight, 0)
            FROM        fishing_license_license AS license
            INNER JOIN  res_partner AS partner ON partner.id = license.partner_id
            WHERE       license.date_year = %(year_sel)s
            AND         NOT license.contest_category IS NULL
            AND         COALESCE(license.total_weight, 0) <> 0
            ORDER BY      COALESCE(license.total_weight, 0) DESC
                        , full_name ASC
        """ % {'year_sel': data['form']['year_sel']}
        self.env.cr.execute(query)
        weight_ids = self.env.cr.fetchall()

        get_param = self.env['ir.config_parameter'].sudo().get_param
        fish_count_count = int(get_param('fishing_license.fish_count_count'))
        fish_length_count = int(get_param('fishing_license.fish_length_count'))

        return {
            'data': data,
            'docs': self,
            'docids': docids,
            'year': data['form']['year_sel'],
            'children_ids': children_ids,
            'women_ids': women_ids,
            'men_ids': men_ids,
            'weight_ids': weight_ids,
            'fish_count_count': fish_count_count,
            'fish_length_count': fish_length_count,
        }
