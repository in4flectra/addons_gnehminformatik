# -*- coding: utf-8 -*-
{
    'name': "Fishing License",
    'summary': 'Manage Fishing Licenses',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Specific Industry Applications',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/fishing_license_license.xml',
        'views/res_config_settings.xml',
        'views/res_partner.xml',
    ],

    'application': True,
}
