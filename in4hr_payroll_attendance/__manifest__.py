# -*- coding: utf-8 -*-
{
    'name': 'Payslip with Balance of Attendances',
    'summary': 'Calculates the balance of attendances on the payslip',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Human Resources',
    'version': '0.3',

    'depends': [
        'hr',
        'hr_attendance',
        'hr_payroll',
        'in4hr_attendance',
    ],

    'data': [
        # security
        'security/in4hr_payroll_attendance.xml',
        'security/ir.model.access.csv',

        # views
        'views/hr_employee.xml',
        'views/hr_payroll.xml',
        'views/hr_payslip.xml',

        # reports
        'views/hr_payroll_attendance_report.xml'
    ],

    'pre_init_hook': 'pre_init_hook',
}
