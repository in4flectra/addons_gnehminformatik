# -*- coding: utf-8 -*-

import pytz

from datetime import datetime

from flectra import api, fields, models
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT
from flectra.tools import DEFAULT_SERVER_DATETIME_FORMAT


class In4HRPayrollAttendanceHRAttendance(models.Model):

    _inherit = 'hr.attendance'

    @api.depends('employee_id.slip_ids', 'employee_id.slip_ids.state', 'employee_id.slip_ids.date_from', 'employee_id.slip_ids.date_to')
    def _compute_payslip_is_done(self):
        for attendance_id in self:
            attendance_id.payslip_is_done = attendance_id.get_payslip_is_done(attendance_id.employee_id.id, attendance_id.check_in)

    payslip_is_done = fields.Boolean(string='Payslip is done', compute=_compute_payslip_is_done, store=True, readonyl=True)

    @api.model
    def get_payslip_is_done(self, employee_id, check_in):
        debi_count = 0
        if employee_id and check_in:
            if isinstance(check_in, str):
                tz = self._context.get('tz') or self.env.user.partner_id.tz or 'UTC'
                check_in_date_time = datetime.strptime(check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                check_in_date_time = pytz.UTC.localize(check_in_date_time).astimezone(pytz.timezone(tz))
                check_in_date = datetime.strftime(check_in_date_time.date(), DEFAULT_SERVER_DATE_FORMAT)
            else:
                check_in_date = check_in
            domain = [
                ('state', '=', 'done'),
                ('employee_id', '=', employee_id),
                ('date_from', '<=', check_in_date),
                ('date_to', '>=', check_in_date),
            ]
            payslip_ids = self.env['hr.payslip'].sudo().search(domain)
            debi_count = 0
            for payslip_id in payslip_ids:
                if payslip_id.credit_note:
                    debi_count -= 1
                else:
                    debi_count += 1
        return debi_count > 0

    @api.model
    def create(self, vals):
        vals['payslip_is_done'] = self.get_payslip_is_done(vals.get('employee_id'), vals.get('check_in'))
        return super(In4HRPayrollAttendanceHRAttendance, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'employee_id' in vals:
            employee_id = vals['employee_id']
        else:
            employee_id = self.employee_id.id
        if 'check_in' in vals:
            check_in = vals['check_in']
        else:
            check_in = self.check_in
        vals['payslip_is_done'] = self.get_payslip_is_done(employee_id, check_in)
        return super(In4HRPayrollAttendanceHRAttendance, self).write(vals)
