# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from flectra import api, fields, models, tools, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


class In4HRPayrollAttendanceHRPayrollAttendanceDetail(models.Model):

    _name = 'hr.payroll_attendance.detail'
    _description = 'Detail Hours Analysis'

    detail_type = fields.Selection(
        [
            ('1', 'Actual'),
            ('2', 'Add to Worked Days'),
            ('3', 'Target'),
            ('4', 'Other Hours'),
            ('5', 'Total Others')
        ],
        string='Detail Type',
        readonly = True,
    )
    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    name = fields.Char(string='Name')
    date = fields.Date(string='Date', readonly=True)
    number_of_hours = fields.Float(string='Number of Hours', readonly=True)


class In4HRPayrollAttendanceHRPayrollAttendanceTarget(models.Model):

    _name = 'hr.payroll_attendance.target'
    _description = 'Target Hours Analysis'

    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    date = fields.Date(string='Date', readonly=True)
    target_hours = fields.Float(string='Target Hours', readonly=True)
    leaves_hours = fields.Float(string='Leaves Hours', readonly=True)
    correction_hours = fields.Float(string='Correction Hours', readonly=True)
    paid_hours = fields.Float(string='Paid Hours', readonly=True)


class In4HRPayrollAttendanceHRPayrollAttendanceReport(models.TransientModel):

    _name = 'hr.payroll_attendance.report'
    _description = 'Hours Analysis Calculation'

    @api.model
    def recalc_details_and_targets(self):
        self._cr.execute('DELETE FROM hr_payroll_attendance_detail;')
        self._cr.execute('DELETE FROM hr_payroll_attendance_target;')
        payslip = self.env['hr.payslip']
        employee_ids = self.env['hr.employee'].search([])
        for employee_id in employee_ids:
            for contract_id in employee_id.contract_ids:
                date_start = datetime.strptime(contract_id.date_start, DEFAULT_SERVER_DATE_FORMAT)
                if contract_id.date_end:
                    date_end = datetime.strptime(contract_id.date_end, DEFAULT_SERVER_DATE_FORMAT)
                else:
                    date_end = datetime.today().strftime(DEFAULT_SERVER_DATE_FORMAT)
                    date_end = datetime.strptime(date_end, DEFAULT_SERVER_DATE_FORMAT)
                for current_date in daterange(date_start, date_end):
                    date_str = current_date.strftime(DEFAULT_SERVER_DATE_FORMAT)
                    target_hours = 0.0
                    other_hours = 0.0
                    worked_days_line_ids = payslip.with_context(all_holidays=True).get_worked_day_lines(
                        contract_id,
                        date_str,
                        date_str,
                    )
                    for worked_days_line_id in worked_days_line_ids:
                        name = worked_days_line_id['name']
                        holiday_status_id = worked_days_line_id['holiday_status_id']
                        if holiday_status_id and holiday_status_id.name_for_reports:
                            name = holiday_status_id.name_for_reports
                        number_of_hours = worked_days_line_id['number_of_hours']
                        target_hours += number_of_hours
                        if worked_days_line_id['code'] != 'WORK100':
                            prefix = ''
                            if (not holiday_status_id) or holiday_status_id.add_to_worked_days:
                                detail_type = '2'
                            else:
                                detail_type = '4'
                                other_hours += number_of_hours
                            if name[:1] != detail_type:
                                prefix = detail_type + ' '
                            name = prefix + name
                            self._cr.execute(
                                '''
                                    INSERT
                                      INTO hr_payroll_attendance_detail
                                           ( detail_type
                                           , employee_id
                                           , department_id
                                           , name
                                           , date
                                           , number_of_hours
                                           )
                                    VALUES ( %s
                                           , %s
                                           , %s
                                           , %s
                                           , %s
                                           , %s
                                           )
                                ''',
                                (
                                    detail_type,
                                    employee_id.id,
                                    employee_id.department_id.id or None,
                                    name,
                                    date_str,
                                    number_of_hours,
                                )
                            )
                    self._cr.execute(
                        '''
                            INSERT
                              INTO hr_payroll_attendance_detail
                                   ( detail_type
                                   , employee_id
                                   , department_id
                                   , name
                                   , date
                                   , number_of_hours
                                   )
                            VALUES ( %s
                                   , %s
                                   , %s
                                   , %s
                                   , %s
                                   , %s
                                   )
                        ''',
                        (
                            '3',
                            employee_id.id,
                            employee_id.department_id.id or None,
                            _('3 Target'),
                            date_str,
                            0 - target_hours,
                        )
                    )
                    if other_hours != 0.0:
                        self._cr.execute(
                            '''
                                INSERT
                                  INTO hr_payroll_attendance_detail
                                       ( detail_type
                                       , employee_id
                                       , department_id
                                       , name
                                       , date
                                       , number_of_hours
                                       )
                                VALUES ( %s
                                       , %s
                                       , %s
                                       , %s
                                       , %s
                                       , %s
                                       )
                            ''',
                            (
                                '5',
                                employee_id.id,
                                employee_id.department_id.id or None,
                                _('5 Other'),
                                date_str,
                                0 - other_hours,
                            )
                        )
                    target_hours = 0.0
                    leaves_hours = 0.0
                    worked_days_line_ids = payslip.with_context(all_holidays=False).get_worked_day_lines(
                        contract_id,
                        date_str,
                        date_str,
                    )
                    for worked_days_line_id in worked_days_line_ids:
                        number_of_hours = worked_days_line_id['number_of_hours']
                        target_hours += number_of_hours
                        if worked_days_line_id['code'] != 'WORK100':
                            leaves_hours += number_of_hours
                    if target_hours != 0.0 or leaves_hours != 0.0:
                        self._cr.execute(
                            '''
                                INSERT
                                  INTO hr_payroll_attendance_target
                                       ( employee_id
                                       , department_id
                                       , date
                                       , target_hours
                                       , leaves_hours
                                       , correction_hours
                                       , paid_hours
                                       )
                                VALUES ( %s
                                       , %s
                                       , %s
                                       , %s
                                       , %s
                                       , %s
                                       , %s
                                       )
                            ''',
                            (
                                employee_id.id,
                                employee_id.department_id.id or None,
                                date_str,
                                target_hours,
                                leaves_hours,
                                0.0,
                                0.0,
                            )
                        )
        payslip_ids = payslip.search([])
        for payslip_id in payslip_ids:
            if (abs(payslip_id.attendance_correction) > 0.00001) or (abs(payslip_id.attendance_paid) > 0.00001):
                self._cr.execute(
                    '''
                        INSERT
                          INTO hr_payroll_attendance_target
                               ( employee_id
                               , department_id
                               , date
                               , target_hours
                               , leaves_hours
                               , correction_hours
                               , paid_hours
                               )
                        VALUES ( %s
                               , %s
                               , %s
                               , %s
                               , %s
                               , %s
                               , %s
                               )
                    ''',
                    (
                        payslip_id.employee_id.id,
                        payslip_id.employee_id.department_id.id or None,
                        payslip_id.date_to,
                        0.0,
                        0.0,
                        payslip_id.attendance_correction,
                        payslip_id.attendance_paid,
                    )
                )


class In4HRPayrollAttendanceHRPayrollAttendanceWorkedDetailReport(models.Model):

    _name = 'hr.payroll_attendance.worked_detail_report'
    _description = 'Worked/Detail Hours Analysis'
    _auto = False

    detail_type = fields.Selection(
        [
            ('1', 'Actual'),
            ('2', 'Add to Worked Days'),
            ('3', 'Target'),
            ('4', 'Other Hours'),
            ('5', 'Total Others')
        ],
        string='Detail Type',
        readonly = True,
    )
    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    name = fields.Char(string='Name')
    date = fields.Date(string='Date', readonly=True)
    number_of_hours = fields.Float(string='Number of Hours', readonly=True)

    def init(self):
        tools.drop_view_if_exists(self._cr, 'hr_payroll_attendance_worked_detail_report')
        self._cr.execute('''
            CREATE OR REPLACE VIEW hr_payroll_attendance_worked_detail_report AS
                SELECT   1 AS id
                       , detail_type
                       , employee_id
                       , department_id
                       , name
                       , date
                       , SUM(number_of_hours) AS number_of_hours
                  FROM hr_payroll_attendance_detail
              GROUP BY   detail_type
                       , employee_id
                       , department_id
                       , name
                       , date
                 UNION
                SELECT   2 AS id
                       , '1' AS detail_type
                       , employee_id
                       , department_id
                       , ''' + '\'' + _('1 Actual') + '\'' + ''' as name
                       , DATE(check_in AT TIME ZONE 'UTC') AS date
                       , SUM(worked_hours) AS number_of_hours
                  FROM hr_attendance
              GROUP BY   employee_id
                       , department_id
                       , name
                       , date
                       ;
        ''')


class In4HRPayrollAttendanceHRPayrollAttendanceWorkedTargetReport(models.Model):

    _name = 'hr.payroll_attendance.worked_target_report'
    _description = 'Worked/Target Hours Analysis'
    _auto = False

    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    date = fields.Date(string='Date', readonly=True)
    worked_hours = fields.Float(string='Worked Hours', readonly=True)
    leaves_hours = fields.Float(string='Leaves Hours', readonly=True)
    correction_hours = fields.Float(string='Correction Hours', readonly=True)
    paid_hours = fields.Float(string='Paid Hours', readonly=True)
    target_hours = fields.Float(string='Target Hours', readonly=True)
    delta_hours = fields.Float(string='Delta Hours', readonly=True)
    total_hours = fields.Float(string='Total Hours', readonly=True, group_operator='max')

    def init(self):
        tools.drop_view_if_exists(self._cr, 'hr_payroll_attendance_worked_target_report')
        tools.drop_view_if_exists(self._cr, 'hr_payroll_attendance_worked_target_union')
        self._cr.execute('''
            CREATE OR REPLACE VIEW hr_payroll_attendance_worked_target_union AS
                SELECT   employee_id
                       , department_id
                       , date
                       , 0.0 AS worked_hours
                       , SUM(leaves_hours) AS leaves_hours
                       , SUM(correction_hours) AS correction_hours
                       , SUM(paid_hours) AS paid_hours
                       , SUM(target_hours) AS target_hours
                  FROM hr_payroll_attendance_target
              GROUP BY   employee_id
                       , department_id
                       , date
                 UNION
                SELECT   employee_id
                       , department_id
                       , DATE(check_in AT TIME ZONE 'UTC') AS date
                       , SUM(worked_hours) AS worked_hours
                       , 0.0 AS leaves_hours
                       , 0.0 AS correction_hours
                       , 0.0 AS paid_hours
                       , 0.0 AS target_hours
                  FROM hr_attendance
              GROUP BY   employee_id
                       , department_id
                       , date
                       ;
        ''')
        self._cr.execute('''
            CREATE OR REPLACE VIEW hr_payroll_attendance_worked_target_report AS
                SELECT   1000000 * employee_id + (date - DATE '1900-01-01') AS id
                       , employee_id
                       , department_id
                       , date
                       , SUM(worked_hours) AS worked_hours
                       , SUM(leaves_hours) AS leaves_hours
                       , SUM(correction_hours) AS correction_hours
                       , SUM(paid_hours) AS paid_hours
                       , SUM(target_hours) AS target_hours
                       , SUM(worked_hours + leaves_hours + correction_hours - paid_hours - target_hours) AS delta_hours
                       , (SELECT 'MAX_WITH_DATE ' || CAST(MAX(date) AS TEXT) || ' ' || CAST(SUM(worked_hours + leaves_hours + correction_hours - paid_hours - target_hours) AS TEXT)
                            FROM hr_payroll_attendance_worked_target_union AS saldo
                           WHERE saldo.employee_id = hr_payroll_attendance_worked_target_union.employee_id
                             AND saldo.date <= hr_payroll_attendance_worked_target_union.date
                         ) AS total_hours
                  FROM hr_payroll_attendance_worked_target_union
              GROUP BY   employee_id
                       , department_id
                       , date
                       ;
        ''')
