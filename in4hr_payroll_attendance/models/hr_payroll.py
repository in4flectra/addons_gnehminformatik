# -*- coding: utf-8 -*-

from flectra import fields, models


class In4HRPayrollAttendanceHRPayrollStructure(models.Model):

    _inherit = 'hr.payroll.structure'

    with_worked_days = fields.Boolean(string='With Worked Days', default=True)
    with_attendances = fields.Boolean(string='With Attendances', default=True)
