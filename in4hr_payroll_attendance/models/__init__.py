# -*- coding: utf-8 -*-

from . import hr_attendance
from . import hr_employee
from . import hr_payroll
from . import hr_payroll_attendance_report
from . import hr_payslip
