# -*- coding: utf-8 -*-

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class In4HRPayrollAttendanceHREmployee(models.Model):

    _inherit = 'hr.employee'

    attendances = fields.Char(string='Attendances', compute='_compute_attendances')

    @api.model # Copy from hr_payroll/Models/hr_payslip
    def get_contract(self, id_employee, date_from, date_to):
        # a contract is valid if it ends between the given dates
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        # OR if it starts between the given dates
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        # OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', id_employee), ('state', '=', 'open'), '|', '|'] + clause_1 + clause_2 + clause_3
        return self.env['hr.contract'].search(clause_final).ids

    @api.one
    def _compute_attendances(self):
        attendance_balance = 0.0
        attendance_today = 0.0

        if (   (    self.user_id
                and (   (self.user_id.id == self.env.user.id)
                     or (self.parent_id.user_id.id == self.env.user.id)
                     or (self.parent_id.parent_id.user_id.id == self.env.user.id)
                     or (self.parent_id.parent_id.parent_id.user_id.id == self.env.user.id)
                     or (self.parent_id.parent_id.parent_id.parent_id.user_id.id == self.env.user.id)
                     or (self.parent_id.parent_id.parent_id.parent_id.parent_id.user_id.id == self.env.user.id)
                    )
               )
            or (self.env.user.has_group('hr_attendance.group_hr_attendance_user'))
        ):
            date_from = date.today()
            date_to = date.today()
            ids_contract = self.sudo().get_contract(self.id, date_from, date_to)
            if len(ids_contract) > 0:
                contract_id = self.env['hr.contract'].sudo().browse(ids_contract[0])
                if contract_id.struct_id:
                    struct_id = contract_id.struct_id

                    attendance_balance_old = 0.0
                    attendance_hours_actual = 0.0
                    attendance_hours_target = 0.0

                    if struct_id.with_attendances:
                        check_out_max = date_to
                        check_out_max += relativedelta(days=1)
                        attendance_domain = [
                            ('employee_id', '=', self.id),
                            ('check_in', '<', date.strftime(check_out_max, DEFAULT_SERVER_DATE_FORMAT)),
                        ]

                        domain = [
                            ('employee_id', '=', self.id),
                            ('struct_id.with_attendances', '=', True),
                            ('date_to', '<', date.strftime(date_from, DEFAULT_SERVER_DATE_FORMAT)),
                        ]
                        date_start = datetime.strptime(contract_id.date_start, DEFAULT_SERVER_DATE_FORMAT)
                        prev_payslip_id = self.env['hr.payslip'].sudo().search(domain, order='date_to desc, id desc', limit=1)
                        if prev_payslip_id:
                            attendance_balance_old = prev_payslip_id.attendance_balance_new or 0
                            date_start = datetime.strptime(prev_payslip_id.date_to, DEFAULT_SERVER_DATE_FORMAT)
                            date_start += relativedelta(days=1)
                            attendance_domain.append(('check_in', '>=', date.strftime(date_start, DEFAULT_SERVER_DATE_FORMAT)))

                        attendance_ids = self.env['hr.attendance'].sudo().search(attendance_domain)
                        for attendance_id in attendance_ids:
                            if attendance_id.check_in:
                                check_in = datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                            else:
                                check_in = datetime.now()
                            if attendance_id.check_out:
                                check_out = datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT)
                            elif check_in > datetime.now():
                                check_out = check_in
                            else:
                                check_out = datetime.now()
                            if attendance_id.check_in and attendance_id.check_out and (check_in.date() < date_to):
                                attendance_hours_actual += (check_out - check_in).seconds / 3600
                            elif attendance_id.check_in and (check_in.date() == date_to):
                                attendance_today += (check_out - check_in).seconds / 3600

                        date_end = date_to - relativedelta(days=1)
                        payslip_id = self.env['hr.payslip']
                        worked_days_line_ids = payslip_id.get_worked_day_lines(
                            contract_id,
                            date.strftime(date_start, DEFAULT_SERVER_DATE_FORMAT),
                            date.strftime(date_end, DEFAULT_SERVER_DATE_FORMAT),
                        )
                        for worked_days_line_id in worked_days_line_ids:
                            if worked_days_line_id['code'] == 'WORK100':
                                attendance_hours_target += worked_days_line_id['number_of_hours']

                        attendance_balance += attendance_balance_old
                        attendance_balance += attendance_hours_actual
                        attendance_balance -= attendance_hours_target

        balance_prefix = ''
        balance_hours = int(attendance_balance)
        balance_minutes = round((attendance_balance - balance_hours) * 60)
        if balance_minutes < 0:
            balance_minutes = abs(balance_minutes)
            if balance_hours == 0:
                balance_prefix = '-'

        today_prefix = ''
        today_hours = int(attendance_today)
        today_minutes = round((attendance_today - today_hours) * 60)
        if today_minutes < 0:
            today_minutes = abs(today_minutes)
            if today_hours == 0:
                today_prefix = '-'
        if (attendance_balance != 0.0) and (attendance_today != 0.0):
            self.attendances = _('Balance: %sh %sm, Today: %sh %sm') % (balance_prefix + str(balance_hours),
                                                                        str(balance_minutes),
                                                                        today_prefix + str(today_hours),
                                                                        str(today_minutes),
                                                                       )
        elif attendance_balance != 0.0:
            self.attendances = _('Balance: %sh %sm') % (balance_prefix + str(balance_hours),
                                                        str(balance_minutes),
                                                       )
        elif attendance_today != 0.0:
            self.attendances = _('Today: %sh %sm') % (today_prefix + str(today_hours),
                                                      str(today_minutes),
                                                     )
        else:
            self.attendances = None
