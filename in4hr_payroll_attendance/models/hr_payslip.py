# -*- coding: utf-8 -*-

import babel
import time

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, tools, _
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class In4HRPayrollAttendanceHRPayslip(models.Model):

    _inherit = 'hr.payslip'

    with_worked_days = fields.Boolean(string='With Worked Days', related='struct_id.with_worked_days')
    with_attendances = fields.Boolean(string='With Attendances', related='struct_id.with_attendances')

    attendance_balance_old = fields.Float(string='Old Balance', digits=(16, 5), readonly=True)
    attendance_hours_actual = fields.Float(string='Actual Hours', digits=(16, 5), readonly=True)
    attendance_hours_target = fields.Float(string='Target Hours', digits=(16, 5), readonly=True)
    attendance_correction = fields.Float(string='Correction', digits=(16, 5))
    attendance_justification = fields.Char(string='Justification')
    attendance_paid = fields.Float(string='Paid', digits=(16, 5), readonly=True)
    attendance_balance_new = fields.Float(string='New Balance', digits=(16, 5), compute='_compute_attendance_balance_new', store=True, readonly=True)

    has_worked_days = fields.Boolean(string='Has Worked Days', compute='_compute_has_worked_days')
    has_attendances = fields.Boolean(string='Has Attendances', compute='_compute_has_attendances')

    @api.depends('attendance_balance_old', 'attendance_hours_actual', 'attendance_hours_target', 'attendance_correction', 'attendance_paid')
    def _compute_attendance_balance_new(self):
        for record in self:
            attendance_balance_new = 0.0
            attendance_balance_new += record.attendance_balance_old
            attendance_balance_new += record.attendance_hours_actual
            attendance_balance_new -= record.attendance_hours_target
            attendance_balance_new += record.attendance_correction
            attendance_balance_new -= record.attendance_paid
            record.attendance_balance_new = attendance_balance_new

    @api.one
    def _compute_has_worked_days(self):
        has_worked_days = False
        if len(self.worked_days_line_ids) > 0:
            has_worked_days = True
        self.has_worked_days = has_worked_days

    @api.one
    def _compute_has_attendances(self):
        has_attendances = False
        if (   (abs(self.attendance_balance_old) > 0.00001)
            or (abs(self.attendance_hours_actual) > 0.00001)
            or (abs(self.attendance_hours_target) > 0.00001)
            or (abs(self.attendance_correction) > 0.00001)
            or (abs(self.attendance_paid) > 0.00001)
            or self.attendance_justification
        ):
            has_attendances = True
        self.has_attendances = has_attendances

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):

        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return

        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to
        contract_ids = []

        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        locale = self.env.context.get('lang') or 'en_US'
        self.name = _('Salary Slip of %s for %s') % (employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format=_('MMMM-y'), locale=locale)))
        self.company_id = employee.company_id

        if not self.env.context.get('contract') or not self.contract_id:
            contract_ids = self.get_contract(employee, date_from, date_to)
            if not contract_ids:
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])

        if not self.contract_id.struct_id:
            return
        self.struct_id = self.contract_id.struct_id
        return

    @api.multi
    def compute_worked_days(self):
        for payslip_id in self:
            if payslip_id.date_from and payslip_id.date_to and payslip_id.contract_id:
                contract_id = payslip_id.contract_id
                date_from = payslip_id.date_from
                date_to = payslip_id.date_to

                if payslip_id.with_worked_days:
                    worked_days_line_ids = payslip_id.get_worked_day_lines(contract_id, date_from, date_to)
                    worked_days_lines = payslip_id.worked_days_line_ids.browse([])
                    for worked_days_line_id in worked_days_line_ids:
                        worked_days_lines += worked_days_lines.new(worked_days_line_id)
                    payslip_id.worked_days_line_ids = worked_days_lines
                else:
                    for worked_days_line_id in payslip_id.worked_days_line_ids:
                        worked_days_line_id.unlink()

                input_line_ids = payslip_id.get_inputs(contract_id, date_from, date_to)
                input_lines = payslip_id.input_line_ids.browse([])
                for r in input_line_ids:
                    input_lines += input_lines.new(r)
                payslip_id.input_line_ids = input_lines

        return True

    @api.multi
    def compute_attendance(self):
        for payslip_id in self:
            attendance_balance_old = 0.0
            attendance_hours_actual = 0.0
            attendance_hours_target = 0.0
            attendance_correction = 0.0
            attendance_justification = None
            attendance_paid = 0.0

            if payslip_id.with_attendances:
                check_out_max = datetime.strptime(payslip_id.date_to, DEFAULT_SERVER_DATE_FORMAT)
                check_out_max += relativedelta(days=1)
                attendance_domain = [
                    ('employee_id', '=', payslip_id.employee_id.id),
                    ('check_in', '<', date.strftime(check_out_max, DEFAULT_SERVER_DATE_FORMAT)),
                ]

                domain = [
                    ('employee_id', '=', payslip_id.employee_id.id),
                    ('struct_id.with_attendances', '=', True),
                    ('date_to', '<', payslip_id.date_from),
                ]
                prev_payslip_id = self.env['hr.payslip'].search(domain, order='date_to desc, id desc', limit=1)
                if prev_payslip_id:
                    attendance_balance_old = prev_payslip_id.attendance_balance_new or 0
                    date_start = datetime.strptime(prev_payslip_id.date_to, DEFAULT_SERVER_DATE_FORMAT)
                    date_start += relativedelta(days=1)
                    attendance_domain.append(('check_in', '>=', date.strftime(date_start, DEFAULT_SERVER_DATE_FORMAT)))

                attendance_ids = self.env['hr.attendance'].search(attendance_domain)
                for attendance_id in attendance_ids:
                    if attendance_id.check_in and attendance_id.check_out:
                        check_in = datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                        check_out = datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT)
                        attendance_hours_actual += (check_out - check_in).seconds / 3600

                for worked_days_line_id in payslip_id.worked_days_line_ids:
                    if worked_days_line_id.code == 'WORK100':
                        attendance_hours_target += worked_days_line_id.number_of_hours

                attendance_correction = payslip_id.attendance_correction
                attendance_justification = payslip_id.attendance_justification

            payslip_id.write({
                'attendance_balance_old': attendance_balance_old,
                'attendance_hours_actual': attendance_hours_actual,
                'attendance_hours_target': attendance_hours_target,
                'attendance_correction': attendance_correction,
                'attendance_justification': attendance_justification,
                'attendance_paid': attendance_paid,
            })

        return True
