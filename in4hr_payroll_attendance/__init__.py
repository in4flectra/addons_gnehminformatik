# -*- coding: utf-8 -*-

from . import models

from flectra import api, SUPERUSER_ID


def pre_init_hook(cr):
    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'hr_attendance'
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'hr_attendance'
               ;
    """)

    env = api.Environment(cr, SUPERUSER_ID, {})
    module_id = env['ir.module.module'].search([('name', '=', 'in4hr_payroll_attendance')], limit=1)
    if module_id:
        domain = [
            ('model', '=', 'ir.rule'),
            ('module', '=', 'hr_attendance'),
            ('name', 'in', ['hr_attendance_rule_attendance_employee',
                            'hr_attendance_change_unlink_rule',
                            'hr_attendance_rule_attendance_manager',
                           ]),
        ]
        data_ids = env['ir.model.data'].search(domain)
        ids_rule = []
        for data_id in data_ids:
            ids_rule.append(data_id.res_id)
        rule_ids = env['ir.rule'].search([('id', 'in', ids_rule)])
        rule_ids.unlink()
        data_ids.unlink()
