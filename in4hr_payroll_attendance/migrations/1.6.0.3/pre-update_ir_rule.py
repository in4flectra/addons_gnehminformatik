# -*- coding: utf-8 -*-


def migrate(cr, version):

    cr.execute("""
        DELETE
          FROM ir_rule
         WHERE id IN (SELECT res_id
                        FROM ir_model_data
                       WHERE model = 'ir.rule'
                         AND module = 'in4hr_payroll_attendance'
                         AND name IN ('hr_payroll_attendance_report_rule_change', 'hr_payroll_attendance_report_rule_user')
                     )
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_model_data
         WHERE model = 'ir.rule'
           AND module = 'in4hr_payroll_attendance'
           AND name IN ('hr_payroll_attendance_report_rule_change', 'hr_payroll_attendance_report_rule_user')
               ;
    """)

    cr.execute("""
        DELETE
          FROM ir_ui_menu_group_rel
         WHERE menu_id IN (SELECT id
                             FROM ir_ui_menu
                            WHERE name = 'Reporting'
                              AND parent_id IN (SELECT id
                                                  FROM ir_ui_menu
                                                 WHERE name = 'Employees'
                                                   AND parent_id IS NULL
                                               )
                          )
               ;
    """)
