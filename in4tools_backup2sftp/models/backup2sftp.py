# -*- coding: utf-8 -*-

import logging
import os
import socket
import time

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra import service, tools
from flectra.exceptions import Warning

_logger = logging.getLogger(__name__)

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP.\n'
        'Please install paramiko on your system. (sudo pip3 install paramiko)'
    )


def execute(connector, method, *args):
    try:
        res = getattr(connector, method)(*args)
    except socket.error as error:
        _logger.critical('Error while executing the method "execute". Error: ' + str(error))
        raise error
    return res


class In4ToolsBackup2SFTPConfig(models.Model):

    _name = 'backup2sftp.config'

    host = fields.Char(string='Database Host', required=True, default='localhost')
    port = fields.Char(string='Database Port', required=True, default=7073)
    name = fields.Char(string='Database Name', required=True, default=lambda self: self._get_db_name())
    folder = fields.Char(string='Backup Directory', required='True', default=lambda self: self._default_folder())
    backup_type = fields.Selection(
        [('zip', 'Zip'), ('dump', 'Dump')],
        string='Backup Type',
        required=True,
        default='zip'
    )
    auto_remove = fields.Boolean(string='Remove local Backups')
    days_to_keep = fields.Integer(string='Days to keep local Backups', default=1)

    sftp_write = fields.Boolean(string='Write to SFTP')
    sftp_host = fields.Char(string='SFTP Host')
    sftp_port = fields.Integer(string='SFTP Port', default=22)
    sftp_user = fields.Char(string='SFTP Username')
    sftp_password = fields.Char(string='SFTP Password')
    sftp_path = fields.Char(string='SFTP Directory', default=lambda self: self._default_sftp_path())
    files_to_copy = fields.Char(
        string='Files to copy',
        default=lambda self: self._default_files_to_copy(),
        help="Format: 'Filename,Days,Weeks,Months,Years;Filename,Days,Weeks,Months,Years;...'\n"
             "Days, Weeks, Months, Years: Save files to subfolders and delete after n-days, -weeks, -months or -years\n"
             "('-': Don't save files to subfolders, '0': Don't delete files from subfolders)"
    )
    days_to_keep_sftp = fields.Integer(string='Days to keep SFTP Backups', default=30)
    send_mail_sftp_fail = fields.Boolean(string='Send Email on fail')
    email_to_notify = fields.Char(string='Send Email to')

    @api.multi
    def _get_db_name(self):
        return self._cr.dbname

    @api.model
    def _default_folder(self):
        return os.path.join(tools.config['data_dir'], 'backup', self.env.cr.dbname)

    @api.model
    def _default_sftp_path(self):
        return '/' + self._cr.dbname

    @api.model
    def _default_files_to_copy(self):
        return self._cr.dbname + ',30,-,12,0;nextcloud,7,4,6,-'

    @api.multi
    def _check_db_exist(self):
        self.ensure_one()
        return True

    _constraints = [(_check_db_exist, _('Error: No such database exists!'), [])]

    @api.multi
    def test_sftp_connection(self, context=None):
        self.ensure_one()
        client = None
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(self.sftp_host, self.sftp_port, self.sftp_user, self.sftp_password, timeout=10)
            client.open_sftp()
            msg_title = _('Connection Test Succeeded!')
            msg_content = _('Everything seems properly set up for SFTP Backups.')
        except Exception as error:
            msg_title = _('Connection Test Failed!')
            msg_content = _('Here is what we got instead:\n%s') % (str(error))
            _logger.critical('There was a problem connecting to the remote SFTP! Error: %s' % (str(error)))
        finally:
            if client:
                client.close()
        raise Warning(msg_title + '\n\n' + msg_content)

    def save_file_to_sftp(self, sftp, filename, full_path, curr_path, file):
        try:
            found = False
            sftp.chdir(curr_path)
            for found_file in sftp.listdir(curr_path):
                if filename in found_file:
                    found = True
            if not found:
                sftp.put(full_path, os.path.join(curr_path, file))
        except:
            sftp.mkdir(curr_path)
            sftp.chdir(curr_path)
            sftp.put(full_path, os.path.join(curr_path, file))

    def delete_file_from_sftp(self, sftp, filename, path):
        try:
            sftp.chdir(path)
            for old_file in sftp.listdir(path):
                if filename in old_file:
                    sftp.unlink(old_file)
            if len(sftp.listdir(path)) == 0:
                sftp.rmdir(path)
        except:
            pass

    @api.model
    def schedule_backup(self):
        conf_ids = self.search([])
        for rec in conf_ids:
            try:
                if not os.path.isdir(rec.folder):
                    os.makedirs(rec.folder)
            except:
                raise

            file_name = '%s_%s.%s' % (time.strftime('%Y_%m_%d_%H_%M_%S'), rec.name, rec.backup_type)
            file_path = os.path.join(rec.folder, file_name)
            try:
                fp = open(file_path, 'wb')
                can_list_dbs = tools.config['list_db']
                if not can_list_dbs:
                    tools.config['list_db'] = True
                try:
                    service.db.dump_db(rec.name, fp, rec.backup_type)
                finally:
                    if not can_list_dbs:
                        tools.config['list_db'] = False
                fp.close()
            except Exception as error:
                _logger.critical(
                    'Backup of Database %s running at http://%s:%s failed! Error: %s' % (
                        rec.name,
                        rec.host,
                        rec.port,
                        str(error),
                    )
                )
                continue

            files_to_copy = rec.files_to_copy
            if not files_to_copy:
                files_to_copy = rec.name + ',30,-,12,0'

            sftp = None
            if rec.sftp_write is True:
                try:
                    client = paramiko.SSHClient()
                    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    client.connect(rec.sftp_host, rec.sftp_port, rec.sftp_user, rec.sftp_password, timeout=20)
                    sftp = client.open_sftp()
                except Exception as error:
                    _logger.critical('Connecting to SFTP Server failed! Error: ' + str(error))

            if sftp:
                try:
                    sftp.chdir(rec.sftp_path)
                except IOError:
                    path = ''
                    for dirElement in rec.sftp_path.split('/'):
                        path += dirElement + '/'
                        try:
                            sftp.chdir(path)
                        except:
                            _logger.info('Full path not exists. Create it now: ' + path)
                            sftp.mkdir(path)
                            sftp.chdir(path)
                            pass
                try:
                    for file in sorted(os.listdir(rec.folder)):
                        sftp.chdir(rec.sftp_path)
                        for file_to_copy_str in files_to_copy.split(';'):
                            filename = ''
                            days= '-'
                            weeks = '-'
                            months = '-'
                            years = '-'
                            file_to_copy = file_to_copy_str.split(',')
                            if len(file_to_copy) > 0:
                                filename = file_to_copy[0]
                            if len(file_to_copy) > 1:
                                days = file_to_copy[1]
                            if len(file_to_copy) > 2:
                                weeks = file_to_copy[2]
                            if len(file_to_copy) > 3:
                                months = file_to_copy[3]
                            if len(file_to_copy) > 4:
                                years = file_to_copy[4]
                            if filename and (filename in file):
                                full_path = os.path.join(rec.folder, file)
                                if os.path.isfile(full_path):
                                    try:
                                        sftp.stat(os.path.join(rec.sftp_path, file))
                                        _logger.critical('Copy file %s to SFTP Server failed! Error: File already exists.' % (full_path))
                                    except IOError:
                                        try:
                                            sftp.put(full_path, os.path.join(rec.sftp_path, file))
                                            _logger.info('File %s successfully copied.' % full_path)
                                        except Exception as err:
                                            _logger.critical('Write file to the SFTP Server failed! Error: ' + str(err))

                                        try:
                                            file_datetime = datetime.strptime(file[:10], '%Y_%m_%d')
                                        except:
                                            file_datetime = datetime.fromtimestamp(os.stat(full_path).st_ctime)

                                        curr_path = rec.sftp_path
                                        curr_year = file_datetime.year
                                        curr_path += '/' + str(curr_year)
                                        if years != '-':
                                            self.save_file_to_sftp(sftp, filename, full_path, curr_path, file)

                                        curr_month = file_datetime.month
                                        month_str = str(curr_month)
                                        if curr_month < 10:
                                            month_str = '0' + month_str
                                        curr_path += '_' + month_str
                                        if months != '-':
                                            self.save_file_to_sftp(sftp, filename, full_path, curr_path, file)

                                        curr_day = file_datetime.day
                                        day_str = str(curr_day)
                                        if curr_day < 10:
                                            day_str = '0' + day_str
                                        curr_path += '_' + day_str
                                        if days != '-':
                                            self.save_file_to_sftp(sftp, filename, full_path, curr_path, file)

                                        if weeks != '-':
                                            curr_path = rec.sftp_path
                                            curr_year = file_datetime.year
                                            curr_path += '/' + str(curr_year)
                                            curr_week = file_datetime.isocalendar()[1]
                                            week_str = str(curr_week)
                                            if curr_week < 10:
                                                week_str = '0' + week_str
                                            curr_path += 'W' + week_str
                                            self.save_file_to_sftp(sftp, filename, full_path, curr_path, file)

                    for file_to_copy_str in files_to_copy.split(';'):
                        filename = ''
                        days = '-'
                        weeks = '-'
                        months = '-'
                        years = '-'
                        file_to_copy = file_to_copy_str.split(',')
                        if len(file_to_copy) > 0:
                            filename = file_to_copy[0]
                        if len(file_to_copy) > 1:
                            days = file_to_copy[1]
                        if len(file_to_copy) > 2:
                            weeks = file_to_copy[2]
                        if len(file_to_copy) > 3:
                            months = file_to_copy[3]
                        if len(file_to_copy) > 4:
                            years = file_to_copy[4]
                        if filename:
                            if (days != '-') and (days.isdigit()) and (int(days) > 0):
                                for day in range(int(days) + 1, int(days) + 61):
                                    prev_file_datetime = date.today() - relativedelta(days=day)
                                    prev_path = rec.sftp_path
                                    prev_path += '/' + str(prev_file_datetime.year)
                                    if prev_file_datetime.month < 10:
                                        prev_path += '_0' + str(prev_file_datetime.month)
                                    else:
                                        prev_path += '_' + str(prev_file_datetime.month)
                                    if prev_file_datetime.day < 10:
                                        prev_path += '_0' + str(prev_file_datetime.day)
                                    else:
                                        prev_path += '_' + str(prev_file_datetime.day)
                                    self.delete_file_from_sftp(sftp, filename, prev_path)

                            if (weeks != '-') and (weeks.isdigit()) and (int(weeks) > 0):
                                for week in range(int(weeks) + 1, int(weeks) + 53):
                                    prev_file_datetime = date.today() - relativedelta(weeks=week)
                                    prev_path = rec.sftp_path
                                    prev_path += '/' + str(prev_file_datetime.year)
                                    if prev_file_datetime.isocalendar()[1] < 10:
                                        prev_path += 'W0' + str(prev_file_datetime.isocalendar()[1])
                                    else:
                                        prev_path += 'W' + str(prev_file_datetime.isocalendar()[1])
                                    self.delete_file_from_sftp(sftp, filename, prev_path)

                            if (months != '-') and (months.isdigit()) and (int(months) > 0):
                                for month in range(int(months) + 1, int(months) + 25):
                                    prev_file_datetime = date.today() - relativedelta(months=month)
                                    prev_path = rec.sftp_path
                                    prev_path += '/' + str(prev_file_datetime.year)
                                    if prev_file_datetime.month < 10:
                                        prev_path += '_0' + str(prev_file_datetime.month)
                                    else:
                                        prev_path += '_' + str(prev_file_datetime.month)
                                    self.delete_file_from_sftp(sftp, filename, prev_path)

                            if (years != '-') and (years.isdigit()) and (int(years) > 0):
                                for year in range(int(years) + 1, int(years) + 11):
                                    prev_file_datetime = date.today() - relativedelta(years=year)
                                    prev_path = rec.sftp_path
                                    prev_path += '/' + str(prev_file_datetime.year)
                                    self.delete_file_from_sftp(sftp, filename, prev_path)

                    sftp.chdir(rec.sftp_path)
                    for file in sftp.listdir(rec.sftp_path):
                        for file_to_copy_str in files_to_copy.split(';'):
                            filename = ''
                            file_to_copy = file_to_copy_str.split(',')
                            if len(file_to_copy) > 0:
                                filename = file_to_copy[0]
                            if filename and (filename in file):
                                full_path = os.path.join(rec.sftp_path, file)
                                try:
                                    file_datetime = datetime.strptime(file[:10], '%Y_%m_%d')
                                except:
                                    file_datetime = datetime.fromtimestamp(sftp.stat(full_path).st_atime)
                                delta_datetime = datetime.now() - file_datetime
                                if (    (('.dump' in file) or ('.zip' in file))
                                    and (delta_datetime.days >= rec.days_to_keep_sftp)
                                ):
                                    try:
                                        sftp.unlink(file)
                                        _logger.info('Old file %s from SFTP server deleted.' % (file))
                                    except:
                                        pass

                    sftp.close()
                except Exception as error:
                    _logger.debug('Backup to SFTP Server failed! Error: %s' % str(error))
                    if rec.send_mail_sftp_fail:
                        try:
                            mail_server = self.env['ir.mail_server']
                            message = _(
                                'Dear,\n'
                                '\n'
                                'The backup for the SFTP Server %s failed.\n'
                                'Please check the following details:\n'
                                '\n'
                                'IP: %s\n'
                                'Username: %s\n'
                                'Password: %s\n'
                                '\n'
                                'Error details: %s\n'
                                '\n'
                                'With kind regards'
                            ) % (rec.host, rec.sftp_host, rec.sftp_user, rec.sftp_password, tools.ustr(error))
                            msg = mail_server.build_email(
                                'administrator@in4fix.ch',
                                [rec.email_to_notify],
                                'Backup from %s (%s) failed' % (rec.host, rec.sftp_host),
                                message
                            )
                            mail_server.send_email(self._cr, self._uid, msg)
                        except Exception:
                            pass

            if rec.auto_remove:
                for file in os.listdir(rec.folder):
                    full_path = os.path.join(rec.folder, file)
                    for file_to_copy_str in files_to_copy.split(';'):
                        filename = ''
                        file_to_copy = file_to_copy_str.split(',')
                        if len(file_to_copy) > 0:
                            filename = file_to_copy[0]
                        if filename and (filename in full_path):
                            try:
                                file_datetime = datetime.strptime(file[:10], '%Y_%m_%d')
                            except:
                                file_datetime = datetime.fromtimestamp(os.stat(full_path).st_ctime)
                            delta_datetime = datetime.now() - file_datetime
                            if (    (os.path.isfile(full_path))
                                and (('.dump' in file) or ('.zip' in file))
                                and (delta_datetime.days >= rec.days_to_keep)
                            ):
                                _logger.info('Delete old file %s from local Server.' % (file))
                                os.remove(full_path)
