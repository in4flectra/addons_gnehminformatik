# -*- coding: utf-8 -*-
{
    'name': 'Backup with SFTP-Upload',
    'summary': 'Automated Backup with SFTP-Upload',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/backup2sftp.xml',
    ],
}
