flectra.define('website_docu.website_docu_part', function (require) {
    'use strict';

    //console.info('Test');

    require('web.dom_ready');

    var open_category_ids = function() {
        var ids_category_open = '';
        var category_chevron = document.getElementsByClassName('category_chevron');
        for (var i = 0; i < category_chevron.length; ++i) {
            var category_item = category_chevron[i];
            if (category_item.classList.contains('fa-chevron-down')) {
                if (ids_category_open.length > 0) {
                    ids_category_open = ids_category_open + ',';
                }
                ids_category_open = ids_category_open + category_item.id.split('_')[2];
            }
        }
        return ids_category_open;
    }

    var selected_category_ids = function() {
        var ids_category_selected = '';
        var category_checkbox = document.getElementsByClassName('category_checkbox');
        for (var i = 0; i < category_checkbox.length; ++i) {
            var category_item = category_checkbox[i];
            if (category_item.checked) {
                if (ids_category_selected.length > 0) {
                    ids_category_selected = ids_category_selected + ',';
                }
                ids_category_selected = ids_category_selected + category_item.value;
            }
        }
        return ids_category_selected;
    }

    var open_part_ids = function() {
        var ids_part_open = '';
        var part_chevron = document.getElementsByClassName('part_chevron');
        for (var i = 0; i < part_chevron.length; ++i) {
            var part_item = part_chevron[i];
            if (part_item.classList.contains('fa-chevron-down')) {
                if (ids_part_open.length > 0) {
                    ids_part_open = ids_part_open + ',';
                }
                ids_part_open = ids_part_open + part_item.id.split('_')[2];
            }
        }
        return ids_part_open;
    }

    var link_params = function(with_search_params) {
        if (with_search_params === undefined) {
            with_search_params = true;
        }
        var link_sep = '?';
        var link_params = '';

        var search_advanced_chevron = document.getElementById('search_advanced_chevron');
        if (search_advanced_chevron.classList.contains('fa-chevron-down')) {
            link_params = link_params + link_sep + 'search_advanced=1';
            link_sep = '&';
        }
        var ids_category_open = open_category_ids();
        if (ids_category_open.length > 0) {
            link_params = link_params + link_sep + 'ids_category_open=' + ids_category_open;
            link_sep = '&';
        }
        if (with_search_params == true) {
            var ids_category_selected = selected_category_ids();
            if (ids_category_selected.length > 0) {
                link_params = link_params + link_sep + 'ids_category_selected=' + ids_category_selected;
                link_sep = '&';
            }
            var search_date_create = document.getElementById('search_date_create');
            if (search_date_create.value.length > 0) {
                link_params = link_params + link_sep + 'search_date_create=' + search_date_create.value;
                link_sep = '&';
            }
            var search_date_write = document.getElementById('search_date_write');
            if (search_date_write.value.length > 0) {
                link_params = link_params + link_sep + 'search_date_write=' + search_date_write.value;
                link_sep = '&';
            }
            var search_text_part = document.getElementById('search_text_part');
            if (search_text_part.value.length > 0) {
                link_params = link_params + link_sep + 'search_text_part=' + encodeURIComponent(search_text_part.value);
                link_sep = '&';
            }
        }
        var ids_part_open = open_part_ids();
        if (ids_part_open.length > 0) {
            link_params = link_params + link_sep + 'ids_part_open=' + ids_part_open;
            link_sep = '&';
        }
        return link_params;
    }

    if(!$('#o_docu_collapse_search').length) {
        return $.Deferred().reject("DOM doesn't contain '#o_docu_collapse_search'");
    }
    $('#o_docu_collapse_search').on('click', '.fa-chevron-right', function(){
        var tr_search_advanced = document.getElementsByClassName('tr_search_advanced');
        for (var i = 0; i < tr_search_advanced.length; ++i) {
            tr_search_advanced[i].classList.remove('hidden');
        }
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });
    $('#o_docu_collapse_search').on('click', '.fa-chevron-down', function(){
        var tr_search_advanced = document.getElementsByClassName('tr_search_advanced');
        for (var i = 0; i < tr_search_advanced.length; ++i) {
            tr_search_advanced[i].classList.add('hidden');
        }
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });

    if(!$('#o_docu_collapse_category').length) {
        return $.Deferred().reject("DOM doesn't contain '#o_docu_collapse_category'");
    }
    $('#o_docu_collapse_category').on('click', '.fa-chevron-right', function(){
        $(this).parents('li').find('ul:first').show('normal');
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });
    $('#o_docu_collapse_category').on('click', '.fa-chevron-down', function(){
        $(this).parent().find('ul:first').hide('normal');
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });
/*
    $(document).on('click', '.category_checkbox', function(event) {
        var sep = '?';
        var params = link_params();
        if (params.length > 0) {
            sep = '&';
        }
        if (event.target.value.length > 0) {
            params = params + sep + 'id_category_changed=' + event.target.value;
            //sep = '&';
        }
        window.location = '/docu' + params;
    });
*/
    $(document).on('keyup', '.search_text', function(event) {
        if (event.keyCode === 13) {
            window.location = '/docu' + link_params();
        }
    });

    $(document).on('click', '#search_find_button', function(event) {
        window.location = '/docu' + link_params(true);
    });
    $(document).on('click', '#search_reset_button', function(event) {
        window.location = '/docu' + link_params(false);
    });

    if(!$('#o_docu_collapse_part').length) {
        return $.Deferred().reject("DOM doesn't contain '#o_docu_collapse_part'");
    }
    $('#o_docu_collapse_part').on('click', '.fa-chevron-right', function(){
        $(this).parent().parents('li').find('ul:first').show('normal');
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });
    $('#o_docu_collapse_part').on('click', '.fa-chevron-down', function(){
        $(this).parent().parent().find('ul:first').hide('normal');
        $(this).toggleClass('fa-chevron-down fa-chevron-right');
    });

    $(document).on('click', '.part_link', function(event) {
        var id = event.target.id.split('_')[2];
        window.location = '/docu/part/' + id + link_params();
    });
    $(document).on('click', '.part_scroll', function(event) {
        var id_split = event.target.id.split('_');
        var part_text = document.getElementById('part_text_' + id_split[2]);
        var part_with_link = document.getElementsByClassName('part_with_link');
        if ((part_text !== null) && (part_with_link.length == 1)) {
            var docu_part_text = document.getElementById('o_docu_part_text');
            if (docu_part_text !== null) {
                docu_part_text.scrollTop = part_text.offsetTop;
            }
            var part_chevron = document.getElementById('part_chevron_' + id_split[2]);
            if ((part_chevron !== null) && (part_chevron.classList.contains('fa-chevron-right'))) {
                part_chevron.click();
            }
        } else {
            window.location = id_split[3] + '/scroll/' + id_split[2] + link_params();
        }
    });


    var resize_docu_part_text = function() {
        var docu_part_text = document.getElementById('o_docu_part_text');
        if (docu_part_text !== null) {
            var height = 0;

            height = height + window.innerHeight;

            var navbar = document.getElementById('oe_main_menu_navbar');
            if (navbar !== null) {
                height = height - navbar.offsetHeight;
            }

            var footers = document.getElementsByTagName('footer');
            for (var i = 0; i < footers.length; ++i) {
                height = height - footers[i].offsetHeight;
            }

            height = height - docu_part_text.offsetTop - 10;

            docu_part_text.style.height = height.toString() + 'px';
        }
    }

    $(window).on('load', function(event) {
        resize_docu_part_text();

        var docu_part_text = document.getElementById('o_docu_part_text');
        if (docu_part_text !== null) {
            var load_scroll = document.getElementsByClassName('load_scroll');
            for (var i = 0; i < load_scroll.length; ++i) {
                var id = load_scroll[i].id.split('_')[2];
                var part_text = document.getElementById('part_text_' + id);
                if (part_text !== null) {
                    docu_part_text.scrollTop = part_text.offsetTop;
                }
            }
        }
    });

    $(window).on('resize', function(event) {
        resize_docu_part_text();
    });
});