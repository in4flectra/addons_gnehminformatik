# -*- coding: utf-8 -*-

from werkzeug.exceptions import NotFound

from flectra import http
from flectra.http import request


class WebsiteDocumentation(http.Controller):

    def _get_all_child_categories(self, category_id, ids_category):
        result = ids_category
        if category_id.id not in result:
            result.append(category_id.id)
            for child_category_id in category_id.child_ids:
                result = self._get_all_child_categories(child_category_id, result)
        return result

    def _get_ids_category_partner(self):
        result = []
        current_partner_id = request.env['res.users'].browse(request.uid).partner_id
        while current_partner_id:
            for category_id in current_partner_id.web_docu_category_ids:
                if category_id.id not in result:
                    result = self._get_all_child_categories(category_id, result)
            current_partner_id = current_partner_id.parent_id
        return result

    def _get_ids_category_visible(self, ids_part_with_access):
        result = []
        part_ids = request.env['web_docu.part'].sudo().search([('id', 'in', ids_part_with_access)])
        for part_id in part_ids:
            for category_id in part_id.category_ids:
                current_category_id = category_id
                while current_category_id:
                    if current_category_id.id in result:
                        current_category_id = None
                    else:
                        result.append(current_category_id.id)
                        current_category_id = current_category_id.parent_id
        return result

    def _get_all_child_parts_with_access(self, part_id, parent_has_category, ids_category_partner, ids_part):
        result = ids_part
        has_access = True
        if part_id.groups_ids:
            ids_group = ()
            for groups_id in part_id.groups_ids:
                ids_group += (groups_id.id,)
            request.env.cr.execute(
                '''
                    SELECT 1
                      FROM res_groups_users_rel
                     WHERE uid = %s
                       AND gid IN %s
                           ;
                ''',
                (str(request.uid), ids_group)
            )
            has_access = bool(request.env.cr.fetchone())
        if has_access:
            has_category = True
            if parent_has_category:
                result.append(part_id.id)
            else:
                if ids_category_partner:
                    has_category = False
                    if part_id.category_ids:
                        i = 0
                        while (not has_category) and (i < len(part_id.category_ids)):
                            has_category = part_id.category_ids[i].id in ids_category_partner
                            i += 1
                if has_category:
                    current_part_id = part_id
                    while current_part_id:
                        if current_part_id.id not in result:
                            result.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
            for child_part_id in part_id.child_ids:
                result = self._get_all_child_parts_with_access(child_part_id, has_category, ids_category_partner, result)
        return result

    def _get_ids_part_with_access(self, ids_category_partner):
        result = []
        part_ids = request.env['web_docu.part'].sudo().search([
            ('parent_id', '=', False),
            ('website_ids', 'in', request.website.id),
        ])
        for part_id in part_ids:
            result = self._get_all_child_parts_with_access(part_id, False, ids_category_partner, result)
        return result

    def _get_all_child_parts_visible(self, part_id, parent_has_category, ids_part_with_access, ids_category_selected, ids_part):
        result = ids_part
        has_access = part_id.id in ids_part_with_access
        if has_access:
            has_category = True
            if parent_has_category:
                result.append(part_id.id)
            else:
                if ids_category_selected:
                    has_category = False
                    if part_id.category_ids:
                        i = 0
                        while (not has_category) and (i < len(part_id.category_ids)):
                            has_category = part_id.category_ids[i].id in ids_category_selected
                            i += 1
                if has_category:
                    current_part_id = part_id
                    while current_part_id:
                        if current_part_id.id not in result:
                            result.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
            for child_part_id in part_id.child_ids:
                result = self._get_all_child_parts_visible(child_part_id, has_category, ids_part_with_access, ids_category_selected, result)
        return result

    def _get_ids_part_visible(self, ids_part_with_access, ids_category_selected):
        result = []
        part_ids = request.env['web_docu.part'].sudo().search([
            ('parent_id', '=', False),
            ('id', 'in', ids_part_with_access),
        ])
        for part_id in part_ids:
            result = self._get_all_child_parts_visible(part_id, False, ids_part_with_access, ids_category_selected, result)
        return result

    def _get_all_child_parts_selected(self, part_id, ids_part_visible, ids_part):
        result = ids_part
        if part_id.id in ids_part_visible:
            result.append(part_id.id)
            for child_part_id in part_id.child_ids:
                result = self._get_all_child_parts_selected(child_part_id, ids_part_visible, result)
        return result

    def _get_ids_part_selected(self, part_id, ids_part_visible):
        result = []
        result = self._get_all_child_parts_selected(part_id, ids_part_visible, result)
        return result

    def _get_ids_as_list(self, ids):
        result = []
        if ids:
            result = ids.split(',')
        return result

    @http.route([
        '/docu',
        '/docu/part/<model("web_docu.part"):part>',
        '/docu/part/<model("web_docu.part"):part>/scroll/<model("web_docu.part"):scroll>',
    ], type='http', auth='public', website=True)
    def documentation(self, part=None, scroll=None, **post):

        ids_category_partner = self._get_ids_category_partner()
        ids_part_with_access = self._get_ids_part_with_access(ids_category_partner)
        ids_category_visible = self._get_ids_category_visible(ids_part_with_access)

        ids_category_open = []
        ids_category_open_all = []
        if 'ids_category_open' in post.keys():
            ids_category_open_all = self._get_ids_as_list(post['ids_category_open'])
        if ids_category_open_all:
            for id_category_open in ids_category_open_all:
                if id_category_open in ids_category_visible:
                    ids_category_open.append(id_category_open)

        id_category_changed = None
        ids_category_selected = []
        ids_category_selected_all = []
        if 'ids_category_selected' in post.keys():
            ids_category_selected_all = self._get_ids_as_list(post['ids_category_selected'])
        if 'id_category_changed' in post.keys():
            id_category_changed = int(post['id_category_changed'])
        if id_category_changed:
            category_id = request.env['web_docu.category'].sudo().search([('id', '=', id_category_changed)])
            ids_category_child = self._get_all_child_categories(category_id, [])
            if id_category_changed in ids_category_selected_all:
                for id_category_child in ids_category_child:
                    if id_category_child not in ids_category_selected_all:
                        ids_category_selected_all.append(id_category_child)
            if id_category_changed not in ids_category_selected_all:
                for id_category_child in ids_category_child:
                    if id_category_child in ids_category_selected_all:
                        ids_category_selected_all.remove(id_category_child)
                parent_category_id = category_id.parent_id
                while parent_category_id:
                    if parent_category_id.id in ids_category_selected_all:
                        ids_category_selected_all.remove(parent_category_id.id)
                    parent_category_id = parent_category_id.parent_id
        if ids_category_selected_all:
            for id_category_selected in ids_category_selected_all:
                if id_category_selected in ids_category_visible:
                    ids_category_selected.append(id_category_selected)

        root_category_ids = request.env['web_docu.category'].sudo().search([
            ('parent_id', '=', False),
            ('id', 'in', ids_category_visible),
        ])

        ids_part_visible = self._get_ids_part_visible(ids_part_with_access, ids_category_selected)

        ids_part_open = []
        ids_part_open_all = []
        if 'ids_part_open' in post.keys():
            ids_part_open_all = self._get_ids_as_list(post['ids_part_open'])
        if ids_part_open_all:
            for id_part_open in ids_part_open_all:
                if id_part_open in ids_part_visible:
                    ids_part_open.append(id_part_open)

        if part:
            is_visible = part.id in ids_part_with_access
            if is_visible:
                if not part.self_with_link:
                    if not scroll:
                        scroll = part
                    while part and not part.self_with_link:
                        part = part.parent_id
                current_part_id = part
                while current_part_id:
                    if current_part_id.id in ids_part_visible:
                        current_part_id = None
                    else:
                        ids_part_visible.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
                current_part_id = part
                while current_part_id:
                    if current_part_id.id in ids_part_open:
                        current_part_id = None
                    else:
                        ids_part_open.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
            else:
                raise NotFound()

        if scroll:
            is_visible = scroll.id in ids_part_with_access
            if is_visible:
                current_part_id = scroll
                while current_part_id:
                    if current_part_id.id in ids_part_visible:
                        current_part_id = None
                    else:
                        ids_part_visible.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
                current_part_id = scroll
                while current_part_id:
                    if current_part_id.id in ids_part_open:
                        current_part_id = None
                    else:
                        ids_part_open.append(current_part_id.id)
                        current_part_id = current_part_id.parent_id
            else:
                raise NotFound()

        root_part_ids = request.env['web_docu.part'].sudo().search([
            ('parent_id', '=', False),
            ('id', 'in', ids_part_visible),
        ])

        search_advanced = False
        if 'search_advanced' in post.keys():
            if post['search_advanced'] == '1':
                search_advanced = True

        domain = []
        search_text_part = ''
        search_date_create = ''
        search_date_write = ''
        if part:
            ids_part_selected = self._get_ids_part_selected(part, ids_part_visible)
        else:
            ids_part_selected = ids_part_visible
            if 'search_text_part' in post.keys():
                search_text_part = post['search_text_part']
                domain.append(('plain_text', 'ilike', search_text_part))
            if 'search_date_create' in post.keys():
                search_date_create = post['search_date_create']
                domain.append(('create_date', '>=', search_date_create))
            if 'search_date_write' in post.keys():
                search_date_write = post['search_date_write']
                domain.append(('last_update', '>=', search_date_write))
        domain.append(('id', 'in', ids_part_selected))
        domain.append(('website_ids', 'in', request.website.id))
        part_ids = request.env['web_docu.part'].sudo().search(domain)

        return request.render('in4web_docu.main_docu_page', {
            'part': part,
            'scroll': scroll,
            'part_ids': part_ids,

            'ids_category_visible': ids_category_visible,
            'ids_category_open': ids_category_open,
            'ids_category_selected': ids_category_selected,
            'root_category_ids': root_category_ids,

            'ids_part_visible': ids_part_visible,
            'ids_part_open': ids_part_open,
            'ids_part_selected': ids_part_selected,
            'root_part_ids': root_part_ids,

            'search_advanced': search_advanced,
            'search_text_part': search_text_part,
            'search_date_create': search_date_create,
            'search_date_write': search_date_write,
        })
