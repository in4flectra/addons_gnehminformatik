# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4WebDocuResPartner(models.Model):

    _inherit = 'res.partner'

    web_docu_category_ids = fields.Many2many('web_docu.category', 'web_docu_category_res_partner_rel', 'res_partner_id', 'web_docu_category_id', string='Categories')
