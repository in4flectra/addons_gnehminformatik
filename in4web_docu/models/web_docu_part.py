# -*- coding: utf-8 -*-

import html2text

from flectra import api, fields, models, _


class In4WebDocuWebDocuPart(models.Model):

    _name = 'web_docu.part'
    _description = 'Part'
    _order = 'sequence, name'

    def _default_sequence(self):
        self.env.cr.execute(
            '''
            SELECT MAX(sequence) as max_sequence
              FROM web_docu_part
                   ;
            '''
        )
        return (self.env.cr.fetchone()[0] or 0) + 1

    @api.multi
    def _compute_link(self):
        for part_id in self:
            part_id.link = '/docu/part/%s' % (str(part_id.id))

    def _default_website(self):
        default_website_id = self.env.ref('website.default_website')
        return [default_website_id.id] if default_website_id else None

    name = fields.Char(string='Name', required=True, translate=True)
    active = fields.Boolean(string='Active', default=True, copy=False)
    level = fields.Integer(string='Level', compute='_compute_level', store=True, readonly=True)
    parent_id = fields.Many2one('web_docu.part', string='Parent Part', index=True)
    child_ids = fields.One2many('web_docu.part', 'parent_id', string='Child Parts')
    child_parts_count = fields.Integer(string='Child Parts Count', compute='_compute_child_parts_count')
    parents_are_active = fields.Boolean(string='All Parent Parts are active', default=True, readonly=True)
    sequence = fields.Integer(string='Sequence', required=True, default=_default_sequence, copy=False)
    children_with_link = fields.Boolean(string='Child Parts are Main Parts', default=True)
    self_with_link = fields.Boolean(string='Is Main Part', compute='_compute_self_with_link')
    link = fields.Char(string='Link', compute=_compute_link)
    website_ids = fields.Many2many('website', 'website_web_docu_part_rel', 'web_docu_part_id', 'website_id', string='Websites', default=_default_website, copy=False)
    groups_ids = fields.Many2many('res.groups', 'res_groups_web_docu_part_rel', 'web_docu_part_id', 'res_groups_id', string='Visible for Groups')
    category_ids = fields.Many2many('web_docu.category', 'web_docu_category_web_docu_part_rel', 'web_docu_part_id', 'web_docu_category_id', string='Categories')
    linked_parent_id = fields.Many2one('web_docu.part', string='Linked Parent Part', index=True)
    linked_child_ids = fields.One2many('web_docu.part', 'linked_parent_id', string='Linked Parts')
    linked_parts_count = fields.Integer(string='Linked Parts Count', compute='_compute_linked_parts_count')
    text = fields.Html(string='Text', translate=True)
    plain_text = fields.Text(string='Plain Text', translate=True, compute='_compute_plain_text', store=True, readonly=True)
    last_update = fields.Datetime(string='Last Update', default=lambda self: fields.Datetime.now())
    last_update_uid = fields.Many2one('res.users', string='Last Update by', default=lambda self: self.env.user)

    @api.multi
    @api.depends('parent_id')
    def _compute_level(self):
        for part_id in self:
            level = 0
            if part_id.parent_id:
                level = part_id.parent_id.level + 1
            if part_id.level != level:
                part_id.level = level

    @api.multi
    def _compute_child_parts_count(self):
        for part_id in self:
            part_id.child_parts_count = self.search_count([('id', 'child_of', part_id.id), ('id', '!=', part_id.id)])

    @api.multi
    def _compute_linked_parts_count(self):
        for part_id in self:
            part_id.linked_parts_count = self.search_count([('linked_parent_id', '=', part_id.id)])

    @api.multi
    def _compute_self_with_link(self):
        for part_id in self:
            self_with_link = True
            parent_part_id = part_id.parent_id
            while parent_part_id and self_with_link:
                self_with_link = parent_part_id.children_with_link
                parent_part_id = parent_part_id.parent_id
            part_id.self_with_link = self_with_link

    @api.multi
    @api.depends('name', 'text')
    def _compute_plain_text(self):
        parser = html2text.HTML2Text()
        parser.ignore_links = True
        parser.ul_item_mark = ''
        parser.emphasis_mark = ''
        parser.strong_mark = ''
        for part_id in self:
            plain_text = None
            if part_id.text:
                plain_text = parser.handle(part_id.text)
            if plain_text:
                part_id.plain_text = part_id.name + ' ' + plain_text
            else:
                part_id.plain_text = part_id.name

    @api.model
    def _reorder_all(self, move_down, part_id=None, sequence=0):
        if part_id:
            domain = [('parent_id', '=', part_id.id)]
        else:
            domain = [('parent_id', '=', False)]
        if move_down:
            order = 'sequence ASC, write_date ASC'
        else:
            order = 'sequence ASC, write_date DESC'
        child_part_ids = self.env['web_docu.part'].with_context(active_test=False).search(domain, order=order)
        for child_part_id in child_part_ids:
            sequence += 1
            if child_part_id.sequence != sequence:
                child_part_id.with_context(reorder_all=False).write({'sequence': sequence})
            sequence = self._reorder_all(move_down, child_part_id, sequence)
        return sequence

    @api.model
    def _recalc_all_levels(self):
        level = self.level + 1
        domain = [('parent_id', '=', self.id), ('level', '!=', level)]
        child_part_ids = self.env['web_docu.part'].with_context(active_test=False).search(domain)
        for child_part_id in child_part_ids:
            child_part_id.level = level
            child_part_id._recalc_all_levels()
        return True


    @api.model
    def create(self, vals):
        result = super(In4WebDocuWebDocuPart, self).create(vals)
        if result.linked_parent_id:
            result.write({
                'text': result.linked_parent_id.text,
            })
        if result.parent_id:
            result.parent_id.write({
                'website_ids': [(4, website_id.id) for website_id in result.website_ids]
            })
            self._reorder_all(True)
        return result

    @api.multi
    def write(self, vals):
        old_sequence = None
        if 'sequence' in vals:
            old_sequence = vals['sequence']

        if 'parent_id' in vals:
            parents_are_active = True
            if vals['parent_id']:
                current_parent_id = self.env['web_docu.part'].search([('id', '=', vals['parent_id'])])
                while parents_are_active and current_parent_id:
                    parents_are_active = current_parent_id.active
                    current_parent_id = current_parent_id.parent_id
            vals['parents_are_active'] = parents_are_active

        if (not self.linked_parent_id) and (('name' in vals) or ('text' in vals)):
            vals['last_update'] = fields.Datetime.now()
            vals['last_update_uid'] = self.env.uid

        result = super(In4WebDocuWebDocuPart, self).write(vals)

        if self.parent_id and self.website_ids.ids:
            self.parent_id.write({
                'website_ids': [(4, website_id.id) for website_id in self.website_ids]
            })

        if self.child_ids:
            for child_id in self.with_context(active_test=False).child_ids:
                values = {}
                if ('active' in vals) or ('parents_are_active' in vals):
                    values['parents_are_active'] = self.active and self.parents_are_active
                for website_id in child_id.website_ids:
                    if website_id not in self.website_ids:
                        values['website_ids'] = [(3, website_id.id)]
                if values:
                    child_id.write(values)

        if (    (self._context.get('reorder_all', True))
            and (self._context.get('last_reorder_record', True))
            and (('parent_id' in vals) or ('sequence' in vals))
        ):
            move_down = False
            if old_sequence:
                move_down = self.sequence > old_sequence
            self._reorder_all(move_down)

        if 'parent_id' in vals:
            self._recalc_all_levels()

        if self.linked_parent_id and ('linked_parent_id' in vals):
            self.write({
                'text': self.linked_parent_id.text,
                'last_update': self.linked_parent_id.last_update,
                'last_update_uid': self.linked_parent_id.last_update_uid.id,
            })

        if self.linked_child_ids and (('text' in vals) or ('last_update' in vals) or ('last_update_uid' in vals)):
            for linked_child_id in self.linked_child_ids:
                linked_child_id.write({
                    'text': self.text,
                    'last_update': self.last_update,
                    'last_update_uid': self.last_update_uid.id,
                })

        return result

    @api.constrains('parent_id')
    def check_parent_id(self):
        if not self._check_recursion():
            raise ValueError(_('Error! You cannot create recursive parts.'))

    @api.multi
    def name_get(self):
        result = []
        for part_id in self:
            names = [part_id.name]
            parent_part_id = part_id.parent_id
            while parent_part_id:
                names.append(parent_part_id.name)
                parent_part_id = parent_part_id.parent_id
            result.append((part_id.id, ' / '.join(reversed(names))))
        return result

    @api.multi
    def web_docu_part_show_on_website_action(self):
        self.ensure_one()
        return {
            'name': 'Show on Website',
            'type': 'ir.actions.act_url',
            'target': 'self',
            'url': self.link,
        }

    @api.multi
    def web_docu_part_children_action(self):
        self.ensure_one()
        action = self.env.ref('in4web_docu.web_docu_part_children_action').read()[0]
        action['domain'] = [('id', 'child_of', self.id), ('id', '!=', self.id)]
        return action

    @api.multi
    def web_docu_part_linked_action(self):
        self.ensure_one()
        action = self.env.ref('in4web_docu.web_docu_part_linked_action').read()[0]
        action['domain'] = [('linked_parent_id', '=', self.id)]
        return action
