# -*- coding: utf-8 -*-

from flectra import api, fields, models, _


class In4WebDocuWebDocuCategory(models.Model):

    _name = 'web_docu.category'
    _description = 'Category'
    _order = 'sequence, name'

    def _default_sequence(self):
        self.env.cr.execute(
            '''
            SELECT MAX(sequence) as max_sequence
              FROM web_docu_category
                   ;
            '''
        )
        return (self.env.cr.fetchone()[0] or 0) + 1

    name = fields.Char(string='Category Name', required=True, translate=True)
    active = fields.Boolean(string='Active', default=True, copy=False)
    parent_id = fields.Many2one('web_docu.category', string='Parent Category', index=True)
    child_ids = fields.One2many('web_docu.category', 'parent_id', string='Child Categories')
    child_categories_count = fields.Integer(string='Child Categories Count', compute='_compute_child_categories_count')
    parents_are_active = fields.Boolean(string='All Parent Categories are active', default=True, readonly=True)
    sequence = fields.Integer(string='Sequence', required=True, default=_default_sequence, copy=False)

    @api.multi
    def _compute_child_categories_count(self):
        for category_id in self:
            category_id.child_categories_count = self.search_count([('id', 'child_of', category_id.id), ('id', '!=', category_id.id)])

    @api.model
    def _reorder_all(self, move_down, category_id=None, sequence=0):
        if category_id:
            domain = [('parent_id', '=', category_id.id)]
        else:
            domain = [('parent_id', '=', False)]
        if move_down:
            order = 'sequence ASC, write_date ASC'
        else:
            order = 'sequence ASC, write_date DESC'
        child_category_ids = self.env['web_docu.category'].with_context(active_test=False).search(domain, order=order)
        for child_category_id in child_category_ids:
            sequence += 1
            if child_category_id.sequence != sequence:
                child_category_id.with_context(reorder_all=False).write({'sequence': sequence})
            sequence = self._reorder_all(move_down, child_category_id, sequence)
        return sequence

    @api.model
    def create(self, vals):
        result = super(In4WebDocuWebDocuCategory, self).create(vals)
        if result.parent_id:
            self._reorder_all(True)
        return result

    @api.multi
    def write(self, vals):
        old_sequence = None
        if 'sequence' in vals:
            old_sequence = vals['sequence']

        if 'parent_id' in vals:
            parents_are_active = True
            if vals['parent_id']:
                current_parent_id = self.env['web_docu.category'].search([('id', '=', vals['parent_id'])])
                while parents_are_active and current_parent_id:
                    parents_are_active = current_parent_id.active
                    current_parent_id = current_parent_id.parent_id
            vals['parents_are_active'] = parents_are_active

        result = super(In4WebDocuWebDocuCategory, self).write(vals)

        if self.child_ids:
            for child_id in self.with_context(active_test=False).child_ids:
                values = {}
                if ('active' in vals) or ('parents_are_active' in vals):
                    values['parents_are_active'] = self.active and self.parents_are_active
                if values:
                    child_id.write(values)

        if (    (self._context.get('reorder_all', True))
            and (self._context.get('last_reorder_record', True))
            and (('parent_id' in vals) or ('sequence' in vals))
        ):
            move_down = False
            if old_sequence:
                move_down = self.sequence > old_sequence
            self._reorder_all(move_down)

        return result

    @api.constrains('parent_id')
    def check_parent_id(self):
        if not self._check_recursion():
            raise ValueError(_('Error! You cannot create recursive categories.'))

    @api.multi
    def name_get(self):
        result = []
        for category_id in self:
            names = [category_id.name]
            parent_category_id = category_id.parent_id
            while parent_category_id:
                names.append(parent_category_id.name)
                parent_category_id = parent_category_id.parent_id
            result.append((category_id.id, ' / '.join(reversed(names))))
        return result

    @api.multi
    def web_docu_category_children_action(self):
        self.ensure_one()
        action = self.env.ref('in4web_docu.web_docu_category_children_action').read()[0]
        action['domain'] = [('id', 'child_of', self.id), ('id', '!=', self.id)]
        return action
