# -*- coding: utf-8 -*-
{
    'name': 'Online Documentation',
    'summary': 'Create Documentations and Display on the Website',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Website',
    'version': '0.1',

    'depends': [
        'base',
        'website',
    ],

    'data': [
        # security
        'security/ir.model.access.csv',

        # views
        'views/res_partner.xml',
        'views/web_docu_base.xml',
        'views/web_docu_category.xml',
        'views/web_docu_part.xml',
        'views/web_docu_web.xml',
    ],

    'application': True,
}
