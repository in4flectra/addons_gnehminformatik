# -*- coding: utf-8 -*-

import base64
import csv
import fnmatch
import io
import os
import sys
import urllib

from flectra import api, fields, models, _


class In4ToolsImagesImportWizard(models.TransientModel):

    _name = 'images_import.wizard'

    @api.one
    @api.depends('model_id')
    def _compute_model_name(self):
        if self.model_id:
            self.model_name = self.model_id.model
        else:
            self.model_name = None

    model_id = fields.Many2one('ir.model', string='Target')
    model_name = fields.Char(string='Target Name', compute=_compute_model_name)
    domain = fields.Char(string='Domain', required=True, default='[]')
    remove_images = fields.Boolean(string='Remove Images', default=False)
    ignore_first_image = fields.Boolean(string='Ignore first Image', default=False)
    file = fields.Binary(string='File to import', required=True)
    log = fields.Text(string='Log')

    @api.multi
    def images_import(self):
        model = self.model_id.model
        file = io.StringIO(base64.decodebytes(self.file).decode('utf-8'))

        log = ''
        row_nr = 0
        header = []
        image_path_col_idx = -1
        image_link_col_idx = -1
        images_removed = []
        reader = csv.reader(file, delimiter=',')
        csv.field_size_limit(sys.maxsize)
        for row in reader:
            row_nr += 1

            if row_nr == 1:
                for cell in row:
                    header.append(cell)
                try:
                    image_path_col_idx = header.index('image_path')
                except ValueError:
                    image_path_col_idx = -1
                try:
                    image_link_col_idx = header.index('image_link')
                except ValueError:
                    image_link_col_idx = -1
            else:
                if model == 'product.image':
                    search_in_model = 'product.template'
                else:
                    search_in_model = model

                image_path = ''
                if image_path_col_idx >= 0:
                    image_path = row[image_path_col_idx]
                image_link = ''
                if image_link_col_idx >= 0:
                    image_link = row[image_link_col_idx]

                col_nr = 0
                domain_str = self.domain
                for cell in row:
                    col_nr += 1
                    if len(cell) > 0:
                        val = cell
                        if (val[:1] in ('"', '\'')) and val[-1:] in ('"', '\''):
                            val = val[1:-1]
                    else:
                        val = ''
                    domain_str = domain_str.replace('%' + str(col_nr) + 's', val)
                    domain_str = domain_str.replace('{' + header[col_nr - 1] + '}', val)

                domain = []
                domain_eval = eval(domain_str)
                for part in domain_eval:
                    if part[0] == 'id':
                        res_ids = []
                        if isinstance(part[2], list):
                            part_ids = part[2]
                        else:
                            part_ids = [part[2]]
                        for part_id in part_ids:
                            if isinstance(part_id, int):
                                res_ids.append(part_id)
                            else:
                                part_id_split = part_id.split('.')
                                if len(part_id_split) > 1:
                                    part_id_name = part_id_split[1]
                                    part_id_module = part_id_split[0]
                                else:
                                    part_id_name = part_id_split[0]
                                    part_id_module = ''
                                model_data_domain = [
                                    ('model', '=', search_in_model),
                                    ('name', '=', part_id_name),
                                    ('module', '=', part_id_module),
                                ]
                                model_data_id = self.env['ir.model.data'].search(model_data_domain, limit=1)
                                if model_data_id:
                                    res_ids.append(model_data_id.res_id)
                        if res_ids:
                            if isinstance(part[2], list):
                                domain.append(('id', part[1], res_ids))
                            else:
                                domain.append(('id', part[1], res_ids[0]))
                        else:
                            domain.append(('id', '=', 0))
                    else:
                        domain.append(part)

                object_ids = self.env[search_in_model].with_context(active_test=False).search(domain)
                if not object_ids:
                    log += _('%s: Could not find object for domain "%s"\n') % (str(row_nr), domain_str)
                if len(object_ids) > 1:
                    object_ids = self.env[search_in_model].search(domain)
                    if not object_ids or (len(object_ids) > 1):
                        log += _('%s: Found more then one object for domain "%s"\n') % (str(row_nr), domain_str)

                if object_ids and len(object_ids) == 1:
                    object_id = object_ids[0]

                    ok = False
                    image_log = ''
                    image_msg = _('%s: Could not find image for object "%s" on "%s" or check the image size (Exception: %s)\n')
                    image_base64 = None
                    image_list_base64 = []
                    if (not ok) and (image_path.endswith(('.jpg', '.jpeg', '.png'))):
                        path = None
                        filter = None
                        image_list = None
                        if image_path.endswith('*.jpg'):
                            path = image_path[:-5]
                            filter = '*.jpg'
                        elif image_path.endswith('*.jpeg'):
                            path = image_path[:-6]
                            filter = '*.jpeg'
                        elif image_path.endswith('*.png'):
                            path = image_path[:-5]
                            filter = '*.png'
                        if path:
                            image_path = None
                            if os.path.isdir(path):
                                image_list = fnmatch.filter(os.listdir(path), filter)
                                image_list.sort(key=lambda s: s.lower())
                                if (len(image_list) > 0) and self.ignore_first_image:
                                    image_list.pop(0)
                                if len(image_list) > 0:
                                    image_path = path + image_list[0]
                        if image_list and (model == 'product.image'):
                            for image_name in image_list:
                                try:
                                    with open(path + image_name, 'rb') as image:
                                        image_list_base64.append(base64.b64encode(image.read()))
                                    ok = True
                                except Exception as e:
                                    image_log += image_msg % (str(row_nr), object_id.name, image_path, e)
                        elif image_path:
                            try:
                                with open(image_path, 'rb') as image:
                                    image_base64 = base64.b64encode(image.read())
                                ok = True
                            except Exception as e:
                                image_log += image_msg % (str(row_nr), object_id.name, image_path, e)
                        else:
                            image_log += _('%s: Could not find a image for object "%s" on "%s"\n') % (str(row_nr), object_id.name, path)

                    if (not ok) and (('http://' in image_link) or ('https://' in image_link)):
                        try:
                            link = urllib.request.urlopen(image_link).read()
                            image_base64 = base64.encodebytes(link)
                            ok = True
                        except Exception as e:
                            image_log += image_msg % (str(row_nr), object_id.name, image_link, e)

                    if ok:
                        vals_prep = {'image': None}
                        vals_load = {'image': image_base64}
                        try:
                            if model == 'product.image':
                                if self.remove_images and (object_id.id not in images_removed):
                                    self.env['product.image'].search([('product_tmpl_id', '=', object_id.id)]).unlink()
                                    images_removed.append(object_id.id)
                                if image_list_base64:
                                    for image in image_list_base64:
                                        self.env['product.image'].create({
                                            'image': image,
                                            'product_tmpl_id': object_id.id,
                                        })
                                else:
                                    self.env['product.image'].create({
                                        'image': image_base64,
                                        'product_tmpl_id': object_id.id,
                                    })
                            else:
                                if model == 'product.product':
                                    object_id.product_tmpl_id.write(vals_prep)
                                else:
                                    object_id.write(vals_prep)
                                object_id.write(vals_load)
                        except Exception as e:
                            log += _('%s: Could not write image on object "%s" (Exception: %s)\n') % (str(row_nr), object_id.name, e)
                    else:
                        log += image_log

        finished_id = self.env.ref('in4tools_images_import.images_import_wizard_finished_form').id
        self.log = log
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'images_import.wizard',
            'views': [(finished_id, 'form')],
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }
