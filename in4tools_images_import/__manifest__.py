# -*- coding: utf-8 -*-
{
    'name': 'Images Import',
    'summary': 'Import Images from CSV File',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Tools',
    'version': '0.1',

    'depends': [
        'base',
        'in4base',
    ],

    'data': [
        'views/images_import_wizard.xml',
    ],
}
