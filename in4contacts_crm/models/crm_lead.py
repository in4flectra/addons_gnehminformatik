# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ContactsCRMCRMLead(models.Model):

    _inherit = 'crm.lead'

    @api.multi
    @api.depends('firstname', 'lastname')
    def _compute_contact_name(self):
        for lead in self:
            lead.contact_name = self._merge_contact_name_parts_into_contact_name(lead.lastname, lead.firstname)

    @api.multi
    def _inverse_contact_name(self):
        for lead in self:
            contact_name_parts = self._split_contact_name_into_contact_name_parts(lead.contact_name)
            lead.lastname = contact_name_parts['lastname']
            lead.firstname = contact_name_parts['firstname']

    @api.model
    def _get_contact_names_order(self):
        return self.env['ir.config_parameter'].sudo().get_param('partner_names_order', 'last_first')

    @api.one
    def _compute_contact_names_order(self):
        self.contact_names_order = self._get_contact_names_order()

    @api.model
    def _default_contact_names_order(self):
        return self._get_contact_names_order()

    @api.multi
    def _compute_display_on_views(self):
        for lead in self:
            if lead.country_id:
                lead.display_state_on_views = lead.country_id.display_state_on_views
                lead.display_country_on_views = lead.country_id.display_country_on_views
            else:
                lead.display_state_on_views = True
                lead.display_country_on_views = True

    contact_name = fields.Char(compute=_compute_contact_name, inverse=_inverse_contact_name, store=True)
    firstname = fields.Char(string='First Name', index=True)
    lastname = fields.Char(string='Last Name', index=True)
    contact_names_order = fields.Char(string='Names Order', compute=_compute_contact_names_order, default=_default_contact_names_order)

    display_state_on_views = fields.Boolean(string='Display State on Views', compute=_compute_display_on_views)
    display_country_on_views = fields.Boolean(string='Display Country on Views', compute=_compute_display_on_views)

    @api.model
    def _merge_contact_name_parts_into_contact_name(self, lastname, firstname):
        last_name = ''
        first_name = ''
        if lastname:
            last_name = lastname.strip()
        if firstname:
            first_name = firstname.strip()

        result = last_name + first_name
        if (len(last_name) > 0) and (len(first_name) > 0):
            order = self._get_contact_names_order()
            if order == 'last_first':
                result = last_name + ' ' + first_name
            elif order == 'last_first_comma':
                result = last_name + ', ' + first_name
            elif order == 'first_last':
                result = first_name + ' ' + last_name
            elif order == 'first_last_comma':
                result = first_name + ', ' + last_name
        return result

    @api.model
    def _split_contact_name_into_contact_name_parts(self, contact_name):
        result = {'lastname': contact_name or None, 'firstname': None}
        if contact_name:
            order = self._get_contact_names_order()
            clean_contact_name = contact_name.replace('  ', ' ')
            if order in ('last_first_comma', 'first_last_comma'):
                clean_contact_name = clean_contact_name.replace(', ', ',')
                clean_contact_name = clean_contact_name.replace(' ,', ',')
                contact_name_parts = clean_contact_name.split(',', 1)
            else:
                contact_name_parts = clean_contact_name.split(' ', 1)
            if len(contact_name_parts) > 1:
                if order in ('last_first', 'last_first_comma'):
                    result = {'lastname': contact_name_parts[0], 'firstname': contact_name_parts[1]}
                else:
                    result = {'lastname': contact_name_parts[1], 'firstname': contact_name_parts[0]}
        return result

    @api.onchange('contact_name')
    def _onchange_contact_name(self):
        if self.env.context.get('skip_onchange'):
            self.env.context = self.with_context(skip_onchange=False).env.context
        else:
            self._inverse_contact_name()

    @api.onchange('firstname', 'lastname')
    def _onchange_contact_name_parts(self):
        self.env.context = self.with_context(skip_onchange=True).env.context
        self._compute_contact_name()

    @api.model
    def create(self, vals):
        context = dict(self.env.context)
        contact_name = vals.get('contact_name', context.get('default_contact_name'))

        if contact_name:
            contact_name_parts = self._split_contact_name_into_contact_name_parts(contact_name)
            for key, value in contact_name_parts.items():
                if (not vals.get(key)) or context.get('copy'):
                    vals[key] = value

            if 'contact_name' in vals:
                vals['contact_name'] = contact_name
            if 'default_contact_name' in context:
                del context['default_contact_name']

        return super(In4ContactsCRMCRMLead, self.with_context(context)).create(vals)

    def _onchange_partner_id_values(self, partner_id):
        result = super(In4ContactsCRMCRMLead, self)._onchange_partner_id_values(partner_id)
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)
            result.update({
                'firstname': partner.firstname,
                'lastname': partner.lastname,
            })
        return result

    @api.multi
    def _create_lead_partner_data(self, name, is_company, company_and_contact, parent_id=False):
        result = super(In4ContactsCRMCRMLead, self)._create_lead_partner_data(name, is_company, company_and_contact, parent_id)
        if not is_company:
            result.update({
                'firstname': self.firstname,
                'lastname': self.lastname,
            })
        return result

    @api.model
    def inverse_contact_names(self):
        self._inverse_contact_name()

    @api.model
    def install_contact_names(self):
        self.search([('firstname', '=', False), ('lastname', '=', False)]).inverse_contact_names()
