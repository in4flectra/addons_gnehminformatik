# -*- coding: utf-8 -*-
{
    'name': 'CRM: First and Last Name',
    'summary': 'First and Last Name for Leads and Opportunities',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'crm',
        'in4contacts',
        'in4crm',
    ],

    'data': [
        'views/crm_lead.xml',
    ],

    'auto_install': True,
    'post_init_hook': 'post_init_hook',
}
