# -*- coding: utf-8 -*-
{
    'name': 'Customer Portal (Swiss Localization)',
    'summary': 'Swiss Localization of Customer Portal',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4swiss',
        'portal',
    ],

    'data': [
        'views/portal.xml',
    ],

    'auto_install': True,
}
