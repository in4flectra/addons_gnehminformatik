# -*- coding: utf-8 -*-

from flectra.addons.portal.controllers.portal import CustomerPortal


class In4SwissPortalCustomerPortal(CustomerPortal):
    
    OPTIONAL_BILLING_FIELDS = ['company_name', 'mobile', 'street2', 'zipcode', 'state_id', 'vat']
