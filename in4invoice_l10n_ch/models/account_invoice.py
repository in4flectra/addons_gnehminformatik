# -*- coding: utf-8 -*-

from flectra import api, fields, models
from flectra.tools.float_utils import float_split_str
from flectra.tools.misc import mod10r


class In4InvoiceL10NCHAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.depends('currency_id.name', 'residual', 'partner_bank_id.bank_id', 'number', 'partner_bank_id.l10n_ch_postal', 'partner_bank_id.bank_id.l10n_ch_postal_eur', 'partner_bank_id.bank_id.l10n_ch_postal_chf', 'partner_bank_id.journal_id.l10n_ch_postal_chf')
    def _compute_l10n_ch_isr_optical_line_residual(self):
        for record in self:
            if record.l10n_ch_isr_number and record.l10n_ch_isr_postal and record.currency_id.name:
                #Left part
                currency_code = None
                if record.currency_id.name == 'CHF':
                    currency_code = '01'
                elif record.currency_id.name == 'EUR':
                    currency_code = '03'
                units, cents = float_split_str(record.residual, 2)
                amount_to_display = units + cents
                amount_ref = amount_to_display.zfill(10)
                left = currency_code + amount_ref
                left = mod10r(left)
                #Final assembly (the space after the '+' is no typo, it stands in the specs.)
                record.l10n_ch_isr_optical_line_residual = left + '>' + record.l10n_ch_isr_number + '+ ' + record.l10n_ch_isr_postal + '>'

    l10n_ch_isr_optical_line_residual = fields.Char(
        string='Optical Line with Residual for ISR',
        compute=_compute_l10n_ch_isr_optical_line_residual,
        help="Optical reading line with residual, as it will be printed on ISR",
    )

    def split_residual(self):
        return float_split_str(self.residual, 2)
