# -*- coding: utf-8 -*-
{
    'name': 'Invoicing (extended): Switzerland - Accounting',
    'summary': 'Extension of Sales, Invoices and Payments: Swiss Accounting',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Localization',
    'version': '0.1',

    'depends': [
        'in4invoice',
        'l10n_ch',
    ],

    'data': [
        'views/account_invoice.xml',
    ],

    'auto_install': True,
}
