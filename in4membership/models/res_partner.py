# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, models, _
from flectra.exceptions import UserError
from flectra.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class In4MembershipResPartner(models.Model):

    _inherit = 'res.partner'

    @api.multi
    def get_membership_invoice_data(self, datas):
        result = datas.copy()
        result.update({
            'partner_id': self.id,
            'account_id': self.property_account_receivable_id.id,
            'pricelist_id': self.property_product_pricelist.id,
            'payment_term_id': self.property_payment_term_id.id,
            'fiscal_position_id': self.property_account_position_id.id,
        })
        return result

    @api.multi
    def create_membership_invoice(self, product_id=None, datas=None):
        product_id = product_id or datas.get('product_id')
        quantity = datas.get('quantity')
        amount = datas.get('amount', 0.0)
        invoice_list = []
        for partner in self:
            addr = partner.address_get(['invoice', 'delivery'])
            if partner.free_member:
                raise UserError(_('Partner is a free Member.'))
            if not addr.get('invoice', False):
                raise UserError(_('Partner doesn\'t have an address to make the invoice.'))
            if addr.get('invoice', partner.id) == partner.id:
                invoice_partner = partner
            else:
                invoice_partner = self.env['res.partner'].browse(addr.get('invoice', partner.id))
            if addr.get('delivery', partner.id) == partner.id:
                partner_shipping_id = None
            else:
                partner_shipping_id = addr.get('delivery', partner.id)

            invoice_datas = invoice_partner.get_membership_invoice_data({'partner_shipping_id': partner_shipping_id})
            domain = [
                ('state', '=', 'draft'),
                ('type', '=', 'out_invoice'),
                ('partner_id', '=', invoice_datas['partner_id']),
                ('account_id', '=', invoice_datas['account_id']),
                ('pricelist_id', '=', invoice_datas['pricelist_id']),
                ('payment_term_id', '=', invoice_datas['payment_term_id']),
                ('fiscal_position_id', '=', invoice_datas['fiscal_position_id']),
            ]
            invoice = None
            if invoice_partner.invoice_generation == 'merge':
                invoice = self.env['account.invoice'].search(domain, limit=1)
            if not invoice:
                invoice = self.env['account.invoice'].create(invoice_datas)
            line_values = {
                'product_id': product_id,
                'quantity': quantity,
                'price_unit': amount,
                'invoice_id': invoice.id,
            }

            lang_code = self.env.context.get('lang') or 'en_US'
            lang = self.env['res.lang']
            lang_id = lang._lang_get(lang_code)
            date_format = lang_id.date_format

            date_from = datetime.strptime(datas.get('date_from'), DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)
            date_to = datetime.strptime(datas.get('date_to'), DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)

            # create a record in cache, apply onchange then revert back to a dictionnary
            invoice_line = self.env['account.invoice.line'].new(line_values)
            invoice_line._onchange_product_id()
            invoice_line.price_unit = amount
            line_values = invoice_line._convert_to_write({name: invoice_line[name] for name in invoice_line._cache})
            line_values['membership_line'] = datas.get('date_join')
            if (    invoice
                and (invoice.partner_id.id != partner.id)
                and (invoice.partner_id.parent_id.id != partner.id)
            ):
                line_values['name'] = _('%s: %s from %s to %s') % (partner.name, line_values['name'], date_from, date_to)
            else:
                line_values['name'] = _('%s from %s to %s') % (line_values['name'], date_from, date_to)
            line_values['date_join'] = datas.get('date_join')
            line_values['date_from'] = datas.get('date_from')
            line_values['date_to'] = datas.get('date_to')
            line_values['quantity'] = quantity
            line_values['price_unit'] = amount
            if invoice_partner.id != partner.id:
                line_values['member_partner_id'] = partner.id
            if datas.get('membership_line_id'):
                line_values['membership_line_id'] = datas.get('membership_line_id')
            invoice.write({'invoice_line_ids': [(0, 0, line_values)]})
            invoice._onchange_invoice_line_ids()
            invoice_list.append(invoice.id)
        return invoice_list
