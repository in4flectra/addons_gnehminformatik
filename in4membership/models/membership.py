# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.exceptions import UserError
from flectra.tools.misc import DEFAULT_SERVER_DATE_FORMAT

from addons import decimal_precision as dp


class In4MembershipMembershipMembershipLine(models.Model):

    _inherit = 'membership.membership_line'
    _rec_name = 'name'
    _order = 'partner asc, membership_id asc, date desc, date_from desc'

    @api.depends('partner', 'partner.name', 'membership_id', 'membership_id.name')
    def _compute_name(self):
        for line in self:
            name = ''
            if line.partner and line.partner.name and line.membership_id and line.membership_id.name:
                name = line.partner.name + ': ' + line.membership_id.name
            elif line.partner and line.partner.name:
                name = line.partner.name
            elif line.membership_id and line.membership_id.name:
                name = line.membership_id.name
            line.name = name

    @api.multi
    @api.depends('date_to', 'partner', 'partner.member_lines', 'partner.member_lines.date_to')
    def _compute_active(self):
        for line in self:
            active = True
            if line.date_to and line.partner:
                for other in line.partner.member_lines:
                    if (    active
                        and (line.membership_id == other.membership_id)
                        and ((not other.date_to) or (other.date_to > line.date_to))
                    ):
                        active = False
            line.active = active

    name = fields.Char(string='Name', compute=_compute_name, store=True, readonly=True)
    active = fields.Boolean(string='Active', compute=_compute_active, store=True, readonly=True)
    partner = fields.Many2one(required=True)
    membership_id = fields.Many2one(domain=[('membership', '=', True)])
    date = fields.Date(default=fields.Date.context_today)
    date_from = fields.Date(readonly=False)
    date_to = fields.Date(readonly=False)
    quantity = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1)
    membership_unlimited = fields.Boolean(string='Unlimited', related='membership_id.membership_unlimited', readonly=True)

    @api.multi
    @api.depends(
        'date_cancel',
        'account_invoice_line.invoice_id.state',
        'account_invoice_line.invoice_id.payment_ids',
        'account_invoice_line.invoice_id.payment_ids.invoice_ids.type'
    )
    def _compute_state(self):
        today = fields.Date.today()
        for line in self:
            state = ''
            if line.date_cancel and line.date_cancel <= today:
                state = 'old'
            elif line.account_invoice_id:
                invoice_state = line.account_invoice_id.state
                if invoice_state == 'draft':
                    state = 'waiting'
                elif invoice_state == 'open':
                    state = 'invoiced'
                elif invoice_state == 'paid':
                    state = 'paid'
                    refund_invoices = line.account_invoice_id.refund_invoice_ids
                    if refund_invoices:
                        refund_invoices = refund_invoices.filtered(lambda invoice: invoice.type == 'out_refund')
                        if refund_invoices:
                            state = 'canceled'
                elif invoice_state == 'cancel':
                    state = 'canceled'
            line.state = state

    @api.model
    def _get_membership_values(self, partner, product):
        result = {
            'date': fields.Date.today(),
            'quantity': 1,
            'member_price': None,
        }
        date_from = None
        date_to = None
        previous_date_to = None
        for member_line in partner.member_lines:
            if (    member_line.id
                and member_line.date
                and member_line.date_to
                and (member_line.membership_id.id == product.id)
                and (  (not previous_date_to)
                    or (datetime.strptime(member_line.date_to, DEFAULT_SERVER_DATE_FORMAT) > previous_date_to)
                    )
            ):
                previous_date_to = datetime.strptime(member_line.date_to, DEFAULT_SERVER_DATE_FORMAT)
                result['date'] = datetime.strptime(member_line.date, DEFAULT_SERVER_DATE_FORMAT)
                result['quantity'] = member_line.quantity
                result['member_price'] = member_line.member_price
                if product.membership_unlimited:
                    date_from = previous_date_to + relativedelta(days=1)
        if date_from:
            interval_qty = product.membership_interval_qty
            interval_unit = product.membership_interval_unit
            if interval_unit == 'days':
                date_to = date_from + relativedelta(days=interval_qty)
            elif interval_unit == 'weeks':
                date_to = date_from + relativedelta(weeks=interval_qty)
            elif interval_unit == 'months':
                date_to = date_from + relativedelta(months=interval_qty)
            else:
                date_to = date_from + relativedelta(years=interval_qty)
            date_to = date_to - relativedelta(days=1)
        elif not product.membership_unlimited:
            date_from = product.membership_date_from
            date_to = product.membership_date_to
        result['date_from'] = date_from
        result['date_to'] = date_to

        if product.list_price:
            member_price_dict = product.price_compute('list_price')
            result['member_price'] = member_price_dict.get(product.id) or None

        return result

    @api.multi
    @api.onchange('partner', 'membership_id')
    def _onchange_membership(self):
        for line in self:
            if line.partner and line.membership_id:
                values = self._get_membership_values(line.partner, line.membership_id)
                line.date = values['date']
                line.date_from = values['date_from']
                line.date_to = values['date_to']
                line.quantity = values['quantity']
                line.member_price = values['member_price']

    @api.model
    def create(self, values):
        if values.get('membership_id'):
            membership = self.env['product.product'].browse(values['membership_id'])
            if membership and (not membership.membership_unlimited):
                if not values.get('date_from'):
                    values['date_from'] = membership.membership_date_from
                if not values.get('date_to'):
                    values['date_to'] = membership.membership_date_to
        result = super(In4MembershipMembershipMembershipLine, self).create(values)
        if result.partner and result.membership_id and (not result.account_invoice_line):
            datas = {
                'membership_line_id': result.id,
                'product_id': result.membership_id.id,
                'date_join': result.date or fields.Date.today(),
                'date_from': result.date_from or result.membership_id.membership_date_from,
                'date_to': result.date_to or result.membership_id.membership_date_to,
                'quantity': result.quantity,
                'amount': result.member_price,
            }
            result.partner.create_membership_invoice(datas=datas)
        return result

    @api.multi
    def write(self, values):
        result = super(In4MembershipMembershipMembershipLine, self).write(values)
        if ('quantity' in values) or ('member_price' in values):
            for membership_line_id in self:
                if (     membership_line_id.account_invoice_line
                    and (  (membership_line_id.account_invoice_line.quantity != membership_line_id.quantity)
                        or (membership_line_id.account_invoice_line.price_unit != membership_line_id.member_price)
                        )
                ):
                    membership_line_id.account_invoice_line.write({
                        'quantity': membership_line_id.quantity,
                        'price_unit': membership_line_id.member_price,
                    })
        return result

    @api.multi
    def unlink(self):
        partner_ids = []
        for line in self:
            if line.partner.id not in partner_ids:
                partner_ids.append(line.partner.id)
            if line.account_invoice_line:
                invoice = line.account_invoice_line.invoice_id
                if invoice.state not in ['draft', 'cancel']:
                    raise UserError(_(
                        'You cannot delete an membership which invoice is not draft or cancelled.'
                    ))
                line.account_invoice_line.unlink()
                if invoice.invoice_line_ids:
                    invoice._onchange_invoice_line_ids()
                else:
                    invoice.unlink()

        result = super(In4MembershipMembershipMembershipLine, self).unlink()

        domain = [('partner', 'in', partner_ids)]
        lines = self.env['membership.membership_line'].with_context(active_test=False).search(domain)
        for line in lines:
            line._compute_active()

        return result

    @api.multi
    def renew_membership_lines(self):
        for line in self:
            if line.partner and line.membership_id:
                values = self._get_membership_values(line.partner, line.membership_id)
                values['partner'] = line.partner.id
                values['membership_id'] = line.membership_id.id
                self.env['membership.membership_line'].create(values)

    @api.multi
    def open_account_invoice(self):
        form_id = self.env.ref('account.invoice_form')
        return {
            'name': _('Membership Invoice'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'views': [(form_id.id, 'form')],
            'res_id': self.account_invoice_id.id,
        }


class In4MembershipMembershipInvoice(models.TransientModel):

    _inherit = 'membership.invoice'

    date_join = fields.Date(string='Join Date', default=fields.Date.context_today)
    date_from = fields.Date(string='From')
    date_to = fields.Date(string='To')
    quantity = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1)
    membership_unlimited = fields.Boolean(string='Unlimited', readonly=True)

    @api.onchange('product_id')
    def onchange_product(self):
        date_join = fields.Date.today()
        date_from = None
        quantity = 1
        member_price = None
        membership_unlimited = self.product_id.product_tmpl_id.membership_unlimited
        partner_id = self.env.context.get('active_id')
        if (    self.env.context.get('params')
            and self.env.context['params'].get('id')
            and self.env.context['params'].get('model')
            and (self.env.context['params']['model'] == 'res.partner')
        ):
            partner_id = self.env.context['params']['id']
        if partner_id:
            domain = [
                ('partner', '=', partner_id),
                ('membership_id', '=', self.product_id.id),
                ('date_to', '!=', False),
            ]
            membership_line_id = self.env['membership.membership_line'].search(domain, order='date_to desc', limit=1)
            if membership_line_id and (not membership_line_id.date_cancel):
                date_join = datetime.strptime(membership_line_id.date, DEFAULT_SERVER_DATE_FORMAT)
                quantity = membership_line_id.quantity
                member_price = membership_line_id.member_price
                if membership_unlimited:
                    date_to = datetime.strptime(membership_line_id.date_to, DEFAULT_SERVER_DATE_FORMAT)
                    date_from = date_to + relativedelta(days=1)
        self.date_join = date_join
        self.date_from = date_from
        self.quantity = quantity
        if self.product_id.list_price:
            super(In4MembershipMembershipInvoice, self).onchange_product()
        else:
            self.member_price = member_price
        self.membership_unlimited = membership_unlimited

    @api.onchange('date_from')
    def onchange_date_from(self):
        date_to = None
        self.membership_unlimited = self.product_id.product_tmpl_id.membership_unlimited
        if self.membership_unlimited and self.date_from:
            interval_qty = self.product_id.product_tmpl_id.membership_interval_qty
            interval_unit = self.product_id.product_tmpl_id.membership_interval_unit
            date_from = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT)
            if interval_unit == 'days':
                date_to = date_from + relativedelta(days=interval_qty)
            elif interval_unit == 'weeks':
                date_to = date_from + relativedelta(weeks=interval_qty)
            elif interval_unit == 'months':
                date_to = date_from + relativedelta(months=interval_qty)
            else:
                date_to = date_from + relativedelta(years=interval_qty)
            date_to = date_to - relativedelta(days=1)
        self.date_to = date_to

    @api.multi
    def membership_invoice(self):
        datas = {
            'product_id': self.product_id.id,
            'date_join': self.date_join,
            'date_from': self.date_from or self.product_id.membership_date_from,
            'date_to': self.date_to or self.product_id.membership_date_to,
            'quantity': self.quantity,
            'amount': self.member_price,
        }
        partner_ids = self._context.get('active_ids')
        if (    self.env.context.get('params')
            and self.env.context['params'].get('id')
            and self.env.context['params'].get('model')
            and (self.env.context['params']['model'] == 'res.partner')
        ):
            partner_ids = [self.env.context['params']['id']]
        invoice_list = self.env['res.partner'].browse(partner_ids).create_membership_invoice(datas=datas)

        search_view_ref = self.env.ref('account.view_account_invoice_filter', False)
        form_view_ref = self.env.ref('account.invoice_form', False)
        tree_view_ref = self.env.ref('account.invoice_tree', False)

        return {
            'domain': [('id', 'in', invoice_list)],
            'name': _('Membership Invoices'),
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'views': [(tree_view_ref.id, 'tree'), (form_view_ref.id, 'form')],
            'search_view_id': search_view_ref and search_view_ref.id,
        }
