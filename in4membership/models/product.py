# -*- coding: utf-8 -*-

from datetime import datetime

from flectra import api, fields, models, _
from flectra.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class In4MembershipProductTemplate(models.Model):

    _inherit = 'product.template'

    membership_unlimited = fields.Boolean(string='Unlimited')
    membership_interval_qty = fields.Integer(string='Interval Qty', default=1)
    membership_interval_unit = fields.Selection([('days', 'Days'), ('weeks', 'Weeks'), ('months', 'Months'), ('years', 'Years')], string='Interval Unit', default='years')
    membership_duration = fields.Char(string='Duration', compute='_compute_membership_duration', store=True)

    _sql_constraints = [
        ('membership_date_greater', 'check(membership_unlimited = True or membership_date_to >= membership_date_from)', 'Error ! Ending Date cannot be set before Beginning Date.')
    ]

    @api.multi
    @api.depends('membership_unlimited', 'membership_date_from', 'membership_date_to', 'membership_interval_qty', 'membership_interval_unit')
    def _compute_membership_duration(self):
        for product_id in self:
            if product_id.membership_unlimited:
                interval_unit_dict = dict(product_id.fields_get(allfields=['membership_interval_unit'])['membership_interval_unit']['selection'])
                interval_unit_name = interval_unit_dict.get(product_id.membership_interval_unit)
                product_id.membership_duration = '%s %s' % (product_id.membership_interval_qty, interval_unit_name)
            elif product_id.membership_date_from and product_id.membership_date_to:
                lang_code = self.env.context.get('lang') or 'en_US'
                lang = self.env['res.lang']
                lang_id = lang._lang_get(lang_code)
                date_format = lang_id.date_format
                date_from = datetime.strptime(product_id.membership_date_from, DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)
                date_to = datetime.strptime(product_id.membership_date_to, DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)
                product_id.membership_duration = '%s - %s' % (date_from, date_to)
            else:
                product_id.membership_duration = None

    @api.model
    def create(self, values):
        if 'membership_unlimited' in values:
            if values['membership_unlimited']:
                values['membership_date_from'] = None
                values['membership_date_to'] = None
            else:
                values['membership_interval_qty'] = None
                values['membership_interval_unit'] = None

        return super(In4MembershipProductTemplate, self).create(values)

    @api.multi
    def write(self, values):
        if 'membership_unlimited' in values:
            if values['membership_unlimited']:
                values['membership_date_from'] = None
                values['membership_date_to'] = None
            else:
                values['membership_interval_qty'] = None
                values['membership_interval_unit'] = None

        return super(In4MembershipProductTemplate, self).write(values)
