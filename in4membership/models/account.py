# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4MembershipAccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.multi
    def unlink(self):
        partner_ids = []
        membership_line_obj = self.env['membership.membership_line']
        for invoice in self:
            for invoice_line in invoice.invoice_line_ids:
                domain = [('account_invoice_line', '=', invoice_line.id)]
                membership_line = membership_line_obj.with_context(active_test=False).search(domain, limit=1)
                if membership_line and membership_line.partner and membership_line.partner.id not in partner_ids:
                    partner_ids.append(membership_line.partner.id)

        result = super(In4MembershipAccountInvoice, self).unlink()

        domain = [('partner', 'in', partner_ids)]
        membership_lines = membership_line_obj.with_context(active_test=False).search(domain)
        for membership_line in membership_lines:
            membership_line._compute_active()

        return result

class In4MembershipAccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.model
    def create(self, values):
        invoice_line = super(In4MembershipAccountInvoiceLine, self).create(values)

        membership_line_datas = {}
        membership_unlimited = invoice_line.product_id.product_tmpl_id.membership_unlimited
        if 'member_partner_id' in values:
            membership_line_datas.update({
                'partner': values['member_partner_id'],
            })
        if 'date_join' in values:
            membership_line_datas.update({
                'date': values['date_join'],
            })
        if membership_unlimited and ('date_from' in values) and ('date_to' in values):
            membership_line_datas.update({
                'date_from': values['date_from'],
                'date_to': values['date_to'],
            })
        if 'quantity' in values:
            membership_line_datas.update({
                'quantity': values['quantity'],
            })
        if 'price_unit' in values:
            membership_line_datas.update({
                'member_price': values['price_unit'],
            })
        if membership_line_datas:
            domain = [('account_invoice_line', '=', invoice_line.id)]
            membership_line_obj = self.env['membership.membership_line']
            membership_line = membership_line_obj.with_context(active_test=False).search(domain, limit=1)
            if membership_line:
                membership_line.write(membership_line_datas)

        return invoice_line

    @api.multi
    def write(self, values):
        result = super(In4MembershipAccountInvoiceLine, self).write(values)
        if ('quantity' in values) or ('price_unit' in values):
            membership_line_obj = self.env['membership.membership_line']
            for invoice_line_id in self.filtered(lambda line: line.invoice_id.type == 'out_invoice'):
                if invoice_line_id.product_id.membership:
                    membership_lines = membership_line_obj.search([('account_invoice_line', '=', invoice_line_id.id)])
                    for membership_line in membership_lines:
                        if (   (membership_line.quantity != membership_line.account_invoice_line.quantity)
                            or (membership_line.member_price != membership_line.account_invoice_line.price_unit)
                        ):
                            membership_line.write({
                                'quantity': invoice_line_id.quantity,
                                'member_price': invoice_line_id.price_unit,
                            })
        return result


class In4MembershipAccountInvoiceRefund(models.TransientModel):

    _inherit = 'account.invoice.refund'

    @api.one
    @api.depends('date_invoice')
    def _compute_cancel_only(self):
        self.cancel_only = self._get_membership_line_count() > 0

    @api.model
    def _default_filter_refund(self):
        if self._get_membership_line_count() > 0:
            return 'cancel'
        else:
            return 'refund'

    cancel_only = fields.Boolean(string='Cancel only', compute=_compute_cancel_only)
    filter_refund = fields.Selection(default=_default_filter_refund)

    @api.model
    def _get_membership_line_count(self):
        invoice = self.env['account.invoice'].browse(self._context.get('active_id', False))
        domain = [('account_invoice_line', 'in', invoice.invoice_line_ids.ids)]
        return self.env['membership.membership_line'].search_count(domain)
