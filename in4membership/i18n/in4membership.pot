# Translation of Flectra Server.
# This file contains the translation of the following modules:
# * in4membership
#
# Translators:
#
msgid ""
msgstr ""
"Project-Id-Version: Flectra Server\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: in4membership
#: code:addons/in4membership/models/res_partner.py:89
#, python-format
msgid "%s from %s to %s"
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/res_partner.py:87
#, python-format
msgid "%s: %s from %s to %s"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.product_template_form
msgid "<span>&amp;nbsp;</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>Cancel date:</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>From:</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>Join Date:</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>Renew</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>Status:</span>"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "<span>To:</span>"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_membership_line_active
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
msgid "Active"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
msgid "Archived"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_account_invoice_refund_cancel_only
msgid "Cancel only"
msgstr ""

#. module: in4membership
#: model:ir.ui.menu,name:in4membership.membership_configuration_menu
msgid "Configuration"
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_res_partner
msgid "Contact"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "Create"
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_account_invoice_refund
msgid "Credit Note"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.product_template_form
msgid "Customer Taxes"
msgstr ""

#. module: in4membership
#: selection:product.template,membership_interval_unit:0
msgid "Days"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_product_product_membership_duration
#: model:ir.model.fields,field_description:in4membership.field_product_template_membership_duration
msgid "Duration"
msgstr ""

#. module: in4membership
#: sql_constraint:product.template:0
msgid "Error ! Ending Date cannot be set before Beginning Date."
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
msgid "Expired"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_invoice_date_from
msgid "From"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
msgid "Group by"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.product_template_form
msgid "Interval"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_product_product_membership_interval_qty
#: model:ir.model.fields,field_description:in4membership.field_product_template_membership_interval_qty
msgid "Interval Qty"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_product_product_membership_interval_unit
#: model:ir.model.fields,field_description:in4membership.field_product_template_membership_interval_unit
msgid "Interval Unit"
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_account_invoice
msgid "Invoice"
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_account_invoice_line
msgid "Invoice Line"
msgstr ""

#. module: in4membership
#: model:ir.ui.menu,name:in4membership.membership_invoicing_menu
msgid "Invoicing"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_invoice_date_join
msgid "Join Date"
msgstr ""

#. module: in4membership
#: model:res.groups,name:in4membership.membership_manager_group
msgid "Manager"
msgstr ""

#. module: in4membership
#: model:ir.actions.act_window,name:in4membership.membership_membership_line_action
msgid "Members"
msgstr ""

#. module: in4membership
#: model:ir.module.category,name:in4membership.membership_module_category
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "Membership"
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/membership.py:225
#: model:ir.model,name:in4membership.model_membership_invoice
#, python-format
msgid "Membership Invoice"
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/membership.py:325
#, python-format
msgid "Membership Invoices"
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_membership_membership_line
msgid "membership.membership_line"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.res_partner_form
msgid "Memberships"
msgstr ""

#. module: in4membership
#: model:ir.actions.act_window,name:in4membership.membership_lines_to_invoice_action
#: model:ir.ui.menu,name:in4membership.membership_lines_to_invoice_menu
msgid "Memberships to Invoice"
msgstr ""

#. module: in4membership
#: selection:product.template,membership_interval_unit:0
msgid "Months"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_membership_line_name
msgid "Name"
msgstr ""

#. module: in4membership
#: model:res.groups,name:in4membership.membership_officer_group
msgid "Officer"
msgstr ""

#. module: in4membership
#: model:ir.ui.view,arch_db:in4membership.membership_membership_line_search
msgid "Partner"
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/res_partner.py:37
#, python-format
msgid "Partner doesn't have an address to make the invoice."
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/res_partner.py:35
#, python-format
msgid "Partner is a free Member."
msgstr ""

#. module: in4membership
#: model:ir.model,name:in4membership.model_product_template
msgid "Product Template"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_invoice_quantity
#: model:ir.model.fields,field_description:in4membership.field_membership_membership_line_quantity
msgid "Quantity"
msgstr ""

#. module: in4membership
#: model:ir.actions.server,name:in4membership.membership_lines_renew_action
msgid "Renew"
msgstr ""

#. module: in4membership
#: model:ir.ui.menu,name:in4membership.membership_reporting_menu
msgid "Reporting"
msgstr ""

#. module: in4membership
#: model:res.groups,comment:in4membership.membership_manager_group
msgid "The user will gain access to the \"Membership\" menu incl. the \"Configuration\" menu, enabling him to create and manage members and memberships."
msgstr ""

#. module: in4membership
#: model:res.groups,comment:in4membership.membership_officer_group
msgid "The user will gain access to the \"Membership\" menu, enabling him to create and manage members and memberships."
msgstr ""

#. module: in4membership
#: model:res.groups,comment:in4membership.membership_user_group
msgid "The user will gain access to the \"Membership\" menu, enabling him to create and manage members."
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_invoice_date_to
msgid "To"
msgstr ""

#. module: in4membership
#: model:ir.model.fields,field_description:in4membership.field_membership_invoice_membership_unlimited
#: model:ir.model.fields,field_description:in4membership.field_membership_membership_line_membership_unlimited
#: model:ir.model.fields,field_description:in4membership.field_product_product_membership_unlimited
#: model:ir.model.fields,field_description:in4membership.field_product_template_membership_unlimited
msgid "Unlimited"
msgstr ""

#. module: in4membership
#: model:res.groups,name:in4membership.membership_user_group
msgid "User"
msgstr ""

#. module: in4membership
#: selection:product.template,membership_interval_unit:0
msgid "Weeks"
msgstr ""

#. module: in4membership
#: selection:product.template,membership_interval_unit:0
msgid "Years"
msgstr ""

#. module: in4membership
#: code:addons/in4membership/models/membership.py:194
#, python-format
msgid "You cannot delete an membership which invoice is not draft or cancelled."
msgstr ""

