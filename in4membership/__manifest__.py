# -*- coding: utf-8 -*-
{
    'name': 'Membership Management (extended)',
    'summary': 'Extension of Membership Management',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'account',
        'base',
        'in4invoice',
        'membership',
    ],

    'data': [
        # security
        'security/in4membership.xml',
        'security/ir.model.access.csv',

        # views
        'views/account.xml',
        'views/membership.xml',
        'views/product.xml',
        'views/res_partner.xml',
    ],
}
