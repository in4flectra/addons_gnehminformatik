# -*- coding: utf-8 -*-

from flectra import api, fields, models, _


class In4ConstructionResPartner(models.Model):

    _inherit = 'res.partner'

    @api.multi
    def _compute_customer_project_count(self):
        for partner in self:
            partner.customer_project_count = self.env['project.project'].search_count(['|', ('partner_id', '=', partner.id), ('partner_id.parent_id', '=', partner.id)])

    @api.multi
    def _compute_customer_project_count_text(self):
        for partner in self:
            if partner.customer_project_count == 1:
                partner.customer_project_count_text = str(partner.customer_project_count) + ' ' + _('Project')
            else:
                partner.customer_project_count_text = str(partner.customer_project_count) + ' ' + _('Projects')

    @api.multi
    def _compute_architect_project_count(self):
        for partner in self:
            count = self.env['project.project'].search_count(['|', ('architect_partner_id', '=', partner.id), ('architect_partner_id.parent_id', '=', partner.id)])
            partner.architect_project_count = count

    @api.multi
    def _compute_architect_project_count_text(self):
        for partner in self:
            if partner.architect_project_count == 1:
                partner.architect_project_count_text = str(partner.architect_project_count) + ' ' + _('Project')
            else:
                partner.architect_project_count_text = str(partner.architect_project_count) + ' ' + _('Projects')

    is_object = fields.Boolean(string='Is a Object')
    is_architect = fields.Boolean(string='Is a Architect')

    customer_project_ids = fields.One2many('project.project', 'partner_id', string='Projects as Customer')
    customer_project_count = fields.Integer(string='Number of Projects as Customer', compute=_compute_customer_project_count, store=True)
    customer_project_count_text = fields.Char(string='Text for Number of Projects as Customer', compute=_compute_customer_project_count_text)

    architect_project_ids = fields.One2many('project.project', 'architect_partner_id', string='Projects as Architect')
    architect_project_count = fields.Integer(string='Number of Projects as Architect', compute=_compute_architect_project_count, store=True)
    architect_project_count_text = fields.Char(string='Text for Number of Projects as Architect', compute=_compute_architect_project_count_text)
