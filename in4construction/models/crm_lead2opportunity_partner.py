# -*- coding: utf-8 -*-

import base64

from flectra import api, fields, models, tools
from flectra.modules import get_module_resource


class In4ConstructionCRMLead2OpportunityPartner(models.TransientModel):

    _inherit = 'crm.lead2opportunity.partner'

    @api.model
    def default_get(self, fields):
        result = super(In4ConstructionCRMLead2OpportunityPartner, self).default_get(fields)

        object_partner_id = self._find_matching_object()
        if 'object_action' in fields and not result.get('object_action'):
            result['object_action'] = 'exist' if object_partner_id else 'create'
        if 'object_partner_id' in fields:
            result['object_partner_id'] = object_partner_id

        project_id = self._find_matching_project()
        if 'project_action' in fields and not result.get('project_action'):
            result['project_action'] = 'exist' if project_id else 'create'
        if 'project_id' in fields:
            result['project_id'] = project_id

        return result

    object_action = fields.Selection(
        [
            ('nothing', 'Do not link to a object'),
            ('create', 'Create a new object'),
            ('exist', 'Link to an existing object'),
        ],
        string='Related Object',
        required=True,
    )
    object_partner_id = fields.Many2one('res.partner', string='Object')

    project_action = fields.Selection(
        [
            ('nothing', 'Do not link to a project'),
            ('create', 'Create a new project'),
            ('exist', 'Link to an existing project'),
        ],
        string='Related Project',
        required=True,
    )
    project_id = fields.Many2one('project.project', string='Project')

    @api.onchange('object_action')
    def onchange_object_action(self):
        if self.object_action == 'exist':
            self.object_partner_id = self._find_matching_object()
        else:
            self.object_partner_id = False

    @api.onchange('project_action')
    def onchange_project_action(self):
        if self.project_action == 'exist':
            self.project_id = self._find_matching_project()
        else:
            self.project_id = False

    @api.model
    def _find_matching_object(self):
        if self._context.get('active_model') != 'crm.lead' or not self._context.get('active_id'):
            return False

        lead = self.env['crm.lead'].browse(self._context.get('active_id'))

        if lead.object_partner_id:
            return lead.object_partner_id.id

        if lead.name:
            object_partner_id = self.env['res.partner'].search([('name', 'ilike', '%' + lead.name + '%')], limit=1)
            return object_partner_id.id

        return False

    @api.model
    def _find_matching_project(self):
        if self._context.get('active_model') != 'crm.lead' or not self._context.get('active_id'):
            return False

        lead = self.env['crm.lead'].browse(self._context.get('active_id'))

        if lead.project_id:
            return lead.project_id.id

        if lead.name:
            project_id = self.env['project.project'].search([('name', 'ilike', '%' + lead.name + '%')], limit=1)
            return project_id.id

        return False

    @api.multi
    def _convert_opportunity(self, values):
        result = super(In4ConstructionCRMLead2OpportunityPartner, self)._convert_opportunity(values)

        leads = self.env['crm.lead'].browse(values.get('lead_ids'))
        for lead in leads:
            if self.object_action == 'nothing':
                lead.write({'object_partner_id': None})
            if self.object_action == 'exist':
                lead.write({'object_partner_id': self.object_partner_id.id})
            elif self.object_action == 'create':
                img_path = get_module_resource('in4construction', 'static/src/img', 'construction.png')
                with open(img_path, 'rb') as f:
                    image = f.read()
                object = self.env['res.partner'].create({
                    'is_company': True,
                    'is_object': True,
                    'image': tools.image_resize_image_big(base64.b64encode(image)),
                    'name': lead.name,
                    'street': lead.object_street,
                    'street2': lead.object_street2,
                    'zip': lead.object_zip,
                    'city': lead.object_city,
                    'state_id': lead.object_state_id,
                    'country_id': lead.object_country_id.id,
                    'customer': False,
                })
                lead.write({'object_partner_id': object.id})

            if self.project_action == 'nothing':
                lead.write({'project_id': None})
            if self.project_action == 'exist':
                lead.write({'project_id': self.project_id.id})
                values = {}
                if not self.project_id.partner_id:
                    values.update({'partner_id': lead.partner_id.id})
                if not self.project_id.object_partner_id:
                    values.update({'object_partner_id': lead.object_partner_id.id})
                if not self.project_id.architect_partner_id:
                    values.update({'architect_partner_id': lead.architect_partner_id.id})
                if values:
                    self.project_id.write(values)
            elif self.project_action == 'create':
                project = self.env['project.project'].create({
                    'name': lead.name,
                    'partner_id': lead.partner_id.id,
                    'object_partner_id': lead.object_partner_id.id,
                    'architect_partner_id': lead.architect_partner_id.id,
                })
                lead.write({'project_id': project.id})

        return result
