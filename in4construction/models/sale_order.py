# -*- coding: utf-8 -*-

from flectra import fields, models


class In4ConstructionSaleOrder(models.Model):

    _inherit = 'sale.order'

    architect_partner_id = fields.Many2one('res.partner', string='Architect', domain=[('is_architect', '=', True)])
