# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ConstructionCRMLead(models.Model):

    _inherit = 'crm.lead'

    object_street = fields.Char(string='Street')
    object_street2 = fields.Char(string='Street 2')
    object_zip = fields.Char(string='Zip', change_default=True)
    object_city = fields.Char(string='City')
    object_state_id = fields.Many2one('res.country.state', string='State', ondelete='restrict')
    object_country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default=lambda self: self.env.user.company_id.country_id)
    object_partner_id = fields.Many2one('res.partner', string='Object', domain=[('is_object', '=', True)])
    architect_partner_id = fields.Many2one('res.partner', string='Architect', domain=[('is_architect', '=', True)])
    project_id = fields.Many2one('project.project', string='Project')

    @api.onchange('object_state_id')
    def _onchange_object_state(self):
        if self.object_state_id and (self.object_country_id != self.object_state_id.country_id):
            self.object_country_id = self.object_state_id.country_id

    @api.onchange('object_country_id')
    def _onchange_object_country(self):
        if self.object_state_id and (self.object_country_id != self.object_state_id.country_id):
            self.object_state_id = None

    @api.multi
    def write(self, values):
        result = super(In4ConstructionCRMLead, self).write(values)

        if self.project_id:
            project_values = {}
            if self.project_id.partner_id != self.partner_id:
                project_values.update({'partner_id': self.partner_id.id})
            if self.project_id.object_partner_id != self.object_partner_id:
                project_values.update({'object_partner_id': self.object_partner_id.id})
            if self.project_id.architect_partner_id != self.architect_partner_id:
                project_values.update({'architect_partner_id': self.architect_partner_id.id})
            if project_values:
                self.project_id.write(project_values)

        return result
