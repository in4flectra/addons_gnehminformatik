# -*- coding: utf-8 -*-

from flectra import api, fields, models


class In4ConstructionProjectProject(models.Model):

    _inherit = 'project.project'

    object_partner_id = fields.Many2one('res.partner', string='Object', domain=[('is_object', '=', True)])
    architect_partner_id = fields.Many2one('res.partner', string='Architect', domain=[('is_architect', '=', True)])

    @api.model
    def create(self, values):
        result = super(In4ConstructionProjectProject, self).create(values)

        if self.partner_id:
            self.partner_id._compute_customer_project_count()
        if self.architect_partner_id:
            self.architect_partner_id._compute_architect_project_count()

        return result

    @api.multi
    def write(self, values):
        partner_id_old = None
        architect_partner_id_old = None
        if 'partner_id' in values:
            partner_id_old = self.partner_id
        if 'architect_partner_id' in values:
            architect_partner_id_old = self.architect_partner_id

        result = super(In4ConstructionProjectProject, self).write(values)

        if partner_id_old:
            partner_id_old._compute_customer_project_count()
        if self.partner_id:
            self.partner_id._compute_customer_project_count()
        if architect_partner_id_old:
            architect_partner_id_old._compute_architect_project_count()
        if self.architect_partner_id:
            self.architect_partner_id._compute_architect_project_count()

        return result

    @api.multi
    def unlink(self):
        partner_id_old = None
        architect_partner_id_old = None
        if self.partner_id:
            partner_id_old = self.partner_id
        if self.architect_partner_id:
            architect_partner_id_old = self.architect_partner_id

        result = super(In4ConstructionProjectProject, self).unlink()

        if partner_id_old:
            partner_id_old._compute_customer_project_count()
        if architect_partner_id_old:
            architect_partner_id_old._compute_architect_project_count()

        return result
