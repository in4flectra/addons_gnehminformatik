# -*- coding: utf-8 -*-
{
    'name': 'Construction',
    'summary': 'Extension for Construction',

    'author': 'Gnehm Informatik',
    'website': 'https://www.gnehm-informatik.ch',

    'category': 'Construction',
    'version': '0.1',

    'depends': [
        'base',
        'crm',
        'in4contacts',
        'in4invoice_sale_timesheet',
        'in4project',
        'project',
        'sale',
        'sale_crm',
    ],

    'data': [
        'views/crm_lead.xml',
        'views/crm_lead2opportunity_partner.xml',
        'views/project_project.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',
    ],

    'application': True,
}
